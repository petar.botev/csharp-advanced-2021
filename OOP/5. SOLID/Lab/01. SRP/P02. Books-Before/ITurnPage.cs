﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P02._Books_Before
{
    public interface ITurnPage
    {
        string TurnPage(int page);
    }
}
