﻿namespace P01._FileStream_After
{
    using P01._FileStream_After.Contracts;
    using System;

    class Program
    {
        static void Main(string[] args)
        {
            IResult file = new Music();

            Progress progress = new Progress(file);

            progress.CurrentPercent("Ivan", "baraban", 20, 13);
        }
    }
}
