﻿namespace P01._FileStream_Before
{
    public class Progress
    {
        private readonly ITransfer stream;

        public Progress(ITransfer file)
        {
            this.stream = file;
        }

        public int CurrentPercent()
        {
            return this.stream.Sent * 100 / this.stream.Length;
        }
    }
}
