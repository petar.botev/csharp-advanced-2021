﻿using P02._DrawingShape_Before.Contracts;
using System;

namespace P02._DrawingShape_Before
{
    public class DrawRectangle : IDrawingManager
    {
        public void Draw(IShape shape) => Console.WriteLine("Draw Rectangle");
    }
}
