﻿namespace P02._DrawingShape_Before
{
    using Contracts;
    using System;

    public abstract class DrawingManager : IDrawingManager
    {
        public abstract void Draw(IShape shape);
    }
}
