﻿using System;

namespace P01.Stream_Progress
{
    public class Program
    {
        static void Main()
        {
            IMyStream fileStream = new Music();
            fileStream.BytesSent = 20;
            fileStream.Length = 10;

            StreamProgressInfo progress = new StreamProgressInfo(fileStream);
            Console.WriteLine(progress.CalculateCurrentPercent());
        }
    }
}
