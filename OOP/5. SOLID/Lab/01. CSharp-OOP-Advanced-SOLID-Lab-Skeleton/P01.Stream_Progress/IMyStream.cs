﻿namespace P01.Stream_Progress
{
    public interface IMyStream
    {
        int Length { get; set; }

        int BytesSent { get; set; }
    }
}
