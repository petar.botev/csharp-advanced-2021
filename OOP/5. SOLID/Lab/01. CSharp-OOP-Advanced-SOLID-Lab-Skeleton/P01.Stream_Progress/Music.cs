﻿namespace P01.Stream_Progress
{
    public class Music : IMyStream
    {
        
        public int Length { get; set; }
        public int BytesSent { get; set; }
        public string Artist { get; set; }
        public string Album { get; set; }
    }
}
