﻿namespace P01.Stream_Progress
{
    public abstract class MyStream
    {
        public MyStream(int length, int bytesSent)
        {
            this.Length = length;
            this.BytesSent = bytesSent;
        }

        public int Length { get; set; }

        public int BytesSent { get; set; }
    }
}
