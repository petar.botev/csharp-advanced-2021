﻿using System.Collections.Generic;

namespace P03.DetailPrinter
{
    public interface IManager
    {
        IReadOnlyCollection<string> Documents { get; set; }
    }
}