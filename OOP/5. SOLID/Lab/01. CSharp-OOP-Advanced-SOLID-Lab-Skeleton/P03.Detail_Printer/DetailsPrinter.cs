﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P03.DetailPrinter
{
    public class DetailsPrinter
    {
        private IList<IEmployee> employees;

        public DetailsPrinter(IList<IEmployee> employees)
        {
            this.employees = employees;
        }

        public void PrintDetails()
        {
            foreach (IWorker employee in this.employees)
            {
                if (employee is Employee)
                {
                    this.PrintEmployee(employee);
                    
                }
                else
                {
                    this.PrintManager(employee);
                }
            }
        }

        private void PrintEmployee(IEmployee employee)
        {
            Console.WriteLine(employee.Name);
        }

        private void PrintManager(IWorker worker)
        {
            Console.WriteLine(worker.Name);
            Console.WriteLine(string.Join(Environment.NewLine, worker.Documents));
        }
    }
}
