﻿using System.Collections.Generic;

namespace P03.DetailPrinter
{
    public class Biologist : Employee, IWorker
    {
        public Biologist(string name) 
            : base(name)
        {
        }

        public IReadOnlyCollection<string> Documents { get; set; }
    }
}
