﻿using System.Collections.Generic;

namespace P03.DetailPrinter
{
    public interface IWorker : IEmployee
    {
        IReadOnlyCollection<string> Documents { get; set; }
    }
}
