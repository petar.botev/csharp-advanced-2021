﻿namespace P04.Recharge
{
    using System;

    public class Employee : ISleeper
    {
        public void Sleep() => Console.WriteLine("Sleep");
    }
}
