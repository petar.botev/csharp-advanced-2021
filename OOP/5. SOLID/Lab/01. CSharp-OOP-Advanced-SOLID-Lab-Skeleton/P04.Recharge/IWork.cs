﻿namespace P04.Recharge
{
    public interface IWork
    {
        void Work(int hours);
    }
}