﻿using P04WildFarm.Models.Animal;

namespace P04WildFarm.Core
{
    public static class Factory
    {
        public static Animal CreateAnimal(string[] args)
        {
            string animalType = args[0];
            string name = args[1];
            double weight = double.Parse(args[2]);
            string livingRegion = string.Empty;
            string breed = string.Empty;
            double wingSize = 0.0;

            Animal animal = null;

            switch (animalType)
            {
                case "Cat":

                    livingRegion = args[3];
                    breed = args[4];

                    animal = new Cat(name, weight, 0, livingRegion, breed);

                    break;
                case "Tiger":

                    livingRegion = args[3];
                    breed = args[4];

                    animal = new Tiger(name, weight, 0, livingRegion, breed);

                    break;
                case "Mouse":

                    livingRegion = args[3];
                    animal = new Mouse(name, weight, 0, livingRegion);

                    break;
                case "Dog":

                    livingRegion = args[3];
                    animal = new Dog(name, weight, 0, livingRegion);

                    break;
                case "Hen":

                    wingSize = double.Parse(args[3]);

                    animal = new Hen(name, weight, 0, wingSize);

                    break;
                case "Owl":

                    wingSize = double.Parse(args[3]);

                    animal = new Owl(name, weight, 0, wingSize);

                    break;

                default:
                    break;
            }

            return animal;
        }
    }
}
