﻿using P04WildFarm.Models.AnimalFood;

namespace P04WildFarm.Core
{
    public class CommandInterpreter : ICommandInterpreter
    {
        public Food Read(string[] args)
        {
            string foodType = args[0];
            int foodQuantity = int.Parse(args[1]);

            Food food = null;

            switch (foodType)
            {
                case "Vegetable":

                    food = new Vegetable(foodQuantity);

                    break;
                case "Fruit":

                    food = new Fruit(foodQuantity);

                    break;
                case "Meat":

                    food = new Meat(foodQuantity);

                    break;
                case "Seeds":

                    food = new Seeds(foodQuantity);

                    break;
                
                default:
                    break;
            }

            return food;
        }
    }
}
