﻿using P04WildFarm.Models.Animal;
using P04WildFarm.Models.AnimalFood;
using System;
using System.Collections.Generic;

namespace P04WildFarm.Core
{
    public class Engine : IEngine
    {
        public void Run()
        {
            ICommandInterpreter interpreter = new CommandInterpreter();
            Animal animal = null;
            Food food = null;
            List<Animal> animals = new List<Animal>();
            int rowCounter = 0;
            string info;
            while ((info = Console.ReadLine()) != "End")
            {
                if (rowCounter % 2 == 0)
                {
                    string[] animalArgs = info
                        .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                   animal = Factory.CreateAnimal(animalArgs);
                    animals.Add(animal);
                }
                else
                {
                    string[] foodArgs = info
                        .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                    food = interpreter.Read(foodArgs);
                }

                if (rowCounter % 2 != 0)
                {
                    Console.WriteLine(animal.AskForFood());
                    try
                    {
                        animal.Eat(food.GetType().Name, food.Quantity);
                    }
                    catch (ArgumentException ae)
                    {

                        Console.WriteLine(ae.Message);
                    }
                }

                
                rowCounter++;
            }

            foreach (var item in animals)
            {
                Console.WriteLine(item);
            }
        }
    }
}
