﻿namespace P04WildFarm.Core
{
    public interface IEngine
    {
        void Run();
    }
}
