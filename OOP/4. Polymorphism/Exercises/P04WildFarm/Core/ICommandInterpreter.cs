﻿using P04WildFarm.Models.AnimalFood;

namespace P04WildFarm.Core
{
    public interface ICommandInterpreter
    {
        Food Read(string[] args);
    }
}
