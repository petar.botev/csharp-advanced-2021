﻿namespace P04WildFarm.Models.AnimalFood
{
    public class Vegetable : Food
    {
        public Vegetable(int quantity) 
            : base(quantity)
        {
        }
    }
}
