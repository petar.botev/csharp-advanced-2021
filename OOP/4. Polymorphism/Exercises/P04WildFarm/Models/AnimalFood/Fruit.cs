﻿namespace P04WildFarm.Models.AnimalFood
{
    public class Fruit : Food
    {
        public Fruit(int quantity) 
            : base(quantity)
        {
        }
    }
}
