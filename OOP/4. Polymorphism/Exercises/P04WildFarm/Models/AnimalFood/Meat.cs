﻿namespace P04WildFarm.Models.AnimalFood
{
    public class Meat : Food
    {
        public Meat(int quantity) 
            : base(quantity)
        {
        }
    }
}
