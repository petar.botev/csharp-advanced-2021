﻿namespace P04WildFarm.Models.Animal
{
    public abstract class Animal
    {
        private string name;
        private double weight;
        private int foodEaten;

        protected Animal(string name, double weight, int foodEaten)
        {
            this.Name = name;
            this.Weight = weight;
            this.FoodEaten = foodEaten;
        }

       
        public string Name { get => name; set => name = value; }
        public double Weight { get => weight; set => weight = value; }
        public int FoodEaten { get => foodEaten; set => foodEaten = value; }

        public virtual string AskForFood() => $"{this.GetType().Name} - sound";

        public abstract void Eat(string foodType, int foodPieces);
       

        public override string ToString()
        {
            return $"{this.GetType().Name} [{this.Name}, ";
        }
    }
}
