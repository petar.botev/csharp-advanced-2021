﻿using System;

namespace P04WildFarm.Models.Animal
{
    public class Tiger : Feline
    {
        private const double INCREACE_WEIGHT = 1.00;

        public Tiger(string name, double weight, int foodEaten, string livingRegion, string breed) 
            : base(name, weight, foodEaten, livingRegion, breed)
        {
        }

        public override string AskForFood() => $"ROAR!!!";

        public override void Eat(string foodType, int foodPieces)
        {
            if (foodType == "Meat")
            {
                this.Weight += INCREACE_WEIGHT * foodPieces;
                this.FoodEaten += foodPieces;
            }
            else
            {
                throw new ArgumentException($"{ this.GetType().Name } does not eat {foodType}!");
            }
        }
    }
}
