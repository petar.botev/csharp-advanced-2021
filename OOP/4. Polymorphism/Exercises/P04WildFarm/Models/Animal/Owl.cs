﻿using P04WildFarm.Models.AnimalFood;
using System;
using System.Collections.Generic;

namespace P04WildFarm.Models.Animal
{
    public class Owl : Bird
    {
        private const double INCREACE_WEIGHT = 0.25;

        public Owl(string name, double weight, int foodEaten, double wingSize) 
            : base(name, weight, foodEaten, wingSize)
        {
           
        }

       
        public override string AskForFood() => $"Hoot Hoot";

        public override void Eat(string foodType, int foodPieces)
        {
            if (foodType == "Meat")
            {
                this.Weight += INCREACE_WEIGHT * foodPieces;
                this.FoodEaten += foodPieces;
            }
            else
            {
                throw new ArgumentException($"{ this.GetType().Name } does not eat {foodType}!");
            }
        }
    }
}
