﻿using P04WildFarm.Models.AnimalFood;
using System.Collections.Generic;


namespace P04WildFarm.Models.Animal
{
    public class Hen : Bird
    {
        private const double INCREACE_WEIGHT = 0.35;
        public Hen(string name, double weight, int foodEaten, double wingSize) 
            : base(name, weight, foodEaten, wingSize)
        {
           
        }
        
        public override string AskForFood() => $"Cluck";

        public override void Eat(string foodType, int foodPieces)
        {
            this.Weight += INCREACE_WEIGHT * foodPieces;
            this.FoodEaten += foodPieces;
        }
    }
}
