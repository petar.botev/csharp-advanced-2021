﻿using System;

namespace P04WildFarm.Models.Animal
{
    public class Cat : Feline
    {
        private const double INCREACE_WEIGHT = 0.30;

        public Cat(string name, double weight, int foodEaten, string livingRegion, string breed) 
            : base(name, weight, foodEaten, livingRegion, breed)
        {
        }

        public override string AskForFood() => $"Meow";

        public override void Eat(string foodType, int foodPieces)
        {
            if (foodType == "Vegetable" || foodType == "Meat")
            {
                this.Weight += INCREACE_WEIGHT * foodPieces;
                this.FoodEaten += foodPieces;
            }
            else
            {
                throw new ArgumentException($"{ this.GetType().Name } does not eat {foodType}!");
            }
        }
    }
}
