﻿namespace P04WildFarm.Models.Animal
{
    public class Mammal : Animal
    {
        private string livingRegion;

        public Mammal(string name, double weight, int foodEaten, string livingRegion)
            : base(name, weight, foodEaten)
        {
            this.LivingRegion = livingRegion;
        }

        public string LivingRegion { get => livingRegion; set => livingRegion = value; }

        public override void Eat(string foodType, int foodPieces) => throw new System.NotImplementedException();
        public override string ToString() => $"{base.ToString()}{this.Weight}, {this.LivingRegion}, {FoodEaten}]";
    }
}
