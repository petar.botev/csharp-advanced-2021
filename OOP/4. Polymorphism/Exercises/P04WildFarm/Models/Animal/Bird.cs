﻿namespace P04WildFarm.Models.Animal
{
    public abstract class Bird : Animal
    {
        private double wingSize;

        protected Bird(string name, double weight, int foodEaten, double wingSize)
            : base(name, weight, foodEaten)
        {
            this.WingSize = wingSize;
        }

        public double WingSize { get => wingSize; set => wingSize = value; }

        public override string ToString() => $"{base.ToString()}{this.WingSize}, {this.Weight}, {this.FoodEaten}]";
    }
}
