﻿using System;

namespace P04WildFarm.Models.Animal
{
    public class Dog : Mammal
    {
        private const double INCREACE_WEIGHT = 0.40;
        public Dog(string name, double weight, int foodEaten, string livingRegion) 
            : base(name, weight, foodEaten, livingRegion)
        {
        }

        public override string AskForFood() => $"Woof!";

        public override void Eat(string foodType, int foodPieces)
        {
            if (foodType == "Meat")
            {
                this.Weight += INCREACE_WEIGHT * foodPieces;
                this.FoodEaten += foodPieces;
            }
            else
            {
                throw new ArgumentException($"{ this.GetType().Name } does not eat {foodType}!");
            }
        }
    }
}
