﻿using P04WildFarm.Core;
using System;

namespace P04WildFarm
{
    public class Program
    {
        static void Main()
        {
            IEngine engine = new Engine();

            engine.Run();
        }
    }
}
