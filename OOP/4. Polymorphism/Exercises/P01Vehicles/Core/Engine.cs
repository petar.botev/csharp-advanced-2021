﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P01Vehicles
{
    public class Engine : IEngine
    {
        public void Run()
        {
            string[] carArgs = Console.ReadLine()
                 .Split(' ', StringSplitOptions.RemoveEmptyEntries);

            Vehicle car = new Car(double.Parse(carArgs[1]), double.Parse(carArgs[2]));

            string[] truckArgs = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries);

            Vehicle truck = new Truck(double.Parse(truckArgs[1]), double.Parse(truckArgs[2]));

            int commandsNumber = int.Parse(Console.ReadLine());

            for (int i = 0; i < commandsNumber; i++)
            {
                string[] commandArgs = Console.ReadLine()
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);


                if (commandArgs[0] == "Drive")
                {
                    double distance = double.Parse(commandArgs[2]);
                    if (commandArgs[1] == "Car")
                    {
                        Console.WriteLine(car.Drive(distance));
                    }
                    if (commandArgs[1] == "Truck")
                    {
                        Console.WriteLine(truck.Drive(distance));
                    }
                }
                if (commandArgs[0] == "Refuel")
                {
                    double litters = double.Parse(commandArgs[2]);
                    if (commandArgs[1] == "Car")
                    {
                        car.Refuel(litters);
                    }
                    if (commandArgs[1] == "Truck")
                    {
                        truck.Refuel(litters);
                    }
                }
            }

            Console.WriteLine($"Car: {car.FuelQuantity:f2}");
            Console.WriteLine($"Truck: {truck.FuelQuantity:f2}");
        }
    }
}
