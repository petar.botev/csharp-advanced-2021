﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P01Vehicles
{
    public interface IEngine
    {
        void Run();
    }
}
