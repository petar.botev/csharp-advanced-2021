﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P01Vehicles
{
    public static class Constants
    {
        public const double CAR_EXTRA_FUEL_CONSUMPTION = 0.9;
        public const double TRUCK_EXTRA_FUEL_CONSUMPTION = 1.6;
    }
}
