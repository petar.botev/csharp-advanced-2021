﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P01Vehicles
{
    public class Truck : Vehicle
    {
        public Truck(double fuelQuantity, double fuelConsumption) 
            : base(fuelQuantity, fuelConsumption)
        {
        }

        public override string Drive(double distance)
        {
            double neededFuel = distance * (this.FuelConsumption + Constants.TRUCK_EXTRA_FUEL_CONSUMPTION);

            if (this.FuelQuantity >= neededFuel)
            {
                this.FuelQuantity -= neededFuel;

                return $"Truck travelled {distance} km";
            }

            return "Truck needs refueling";
        }

        public override double Refuel(double litters)
        {
            
            return this.FuelQuantity += litters * 0.95;
        }
    }
}
