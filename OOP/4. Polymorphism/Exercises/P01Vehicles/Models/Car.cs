﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P01Vehicles
{
    public class Car : Vehicle
    {
        
        public Car(double fuelQuantity, double fuelConsumption) 
            : base(fuelQuantity, fuelConsumption)
        {
        }

        public override string Drive(double distance)
        {
            double neededFuel = distance * (this.FuelConsumption + Constants.CAR_EXTRA_FUEL_CONSUMPTION);

            if (this.FuelQuantity >= neededFuel)
            {
                this.FuelQuantity -= neededFuel;

                return $"Car travelled {distance} km";
            }

            return "Car needs refueling";
        }

        public override double Refuel(double litters)
        {
            return this.FuelQuantity += litters;
        }
    }
}
