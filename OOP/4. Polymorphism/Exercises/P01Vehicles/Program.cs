﻿using System;

namespace P01Vehicles
{
    class Program
    {
        static void Main()
        {
            IEngine engine = new Engine();
            engine.Run();
        }
    }
}
