﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P03Raiding
{
    public class Paladin : BaseHero
    {
        public Paladin(string name) 
            : base(name)
        {
            this.Power = 100;
        }

        public override string CastAbility() => $"{this.GetType().Name} - {this.Name} healed for {this.Power}";
    }
}
