﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P03Raiding
{
    public abstract class BaseHero : IBaseHero
    {
        private string name;
        private int power;

        protected BaseHero(string name)
        {
            this.Name = name;
            
        }

        public string Name { get => name; set => name = value; }
        public int Power { get => power; set => power = value; }

        public abstract string CastAbility();
    }
}
