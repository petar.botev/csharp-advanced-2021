﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P03Raiding
{
    public class Raid
    {
        public Raid()
        {
            this.RaidGroup = new List<IBaseHero>();
        }
        public List<IBaseHero> RaidGroup { get; set; }

        public double CalculateGroupPower()
        {
            return RaidGroup.Sum(x => x.Power);
        }
    }
}
