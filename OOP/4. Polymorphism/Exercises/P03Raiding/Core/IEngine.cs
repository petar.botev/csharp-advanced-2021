﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P03Raiding.Core
{
    public interface IEngine
    {
        void Run();
    }
}
