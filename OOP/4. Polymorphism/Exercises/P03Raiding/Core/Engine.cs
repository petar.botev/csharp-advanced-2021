﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P03Raiding.Core
{
    public class Engine : IEngine
    {
        public void Run()
        {
            int herosNumber = int.Parse(Console.ReadLine());

            Raid raid = new Raid();

            while (herosNumber > 0)
            {
                string name = Console.ReadLine();
                string heroType = Console.ReadLine();

                var hero = Factory.CreateHero(heroType, name);

                if (hero != null)
                {
                    raid.RaidGroup.Add(hero);
                    herosNumber--;
                }
                else
                {
                    Console.WriteLine("Invalid hero!");
                }
            }

            int bossPower = int.Parse(Console.ReadLine());

            if (raid.RaidGroup.Count != 0)
            {
                foreach (var item in raid.RaidGroup)
                {
                    Console.WriteLine(item.CastAbility());
                }
            }

            if (bossPower <= raid.CalculateGroupPower())
            {
                Console.WriteLine("Victory!");
            }
            else
            {
                Console.WriteLine("Defeat...");
            }
        }
    }
}
