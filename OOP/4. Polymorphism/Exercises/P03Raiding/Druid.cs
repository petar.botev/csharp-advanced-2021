﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P03Raiding
{
    public class Druid : BaseHero
    {
        public Druid(string name) 
            : base(name)
        {
            this.Power = 80;
        }

        public override string CastAbility() => $"{this.GetType().Name} - {this.Name} healed for {this.Power}";
    }
}
