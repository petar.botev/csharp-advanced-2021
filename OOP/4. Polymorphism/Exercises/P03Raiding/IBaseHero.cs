﻿namespace P03Raiding
{
    public interface IBaseHero
    {
        string CastAbility();
        string Name { get; set; }
        int Power { get; set; }
    }
}