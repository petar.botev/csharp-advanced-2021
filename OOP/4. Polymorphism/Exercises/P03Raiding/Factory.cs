﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P03Raiding
{
    public static class Factory
    {
        public static IBaseHero CreateHero(string heroType, string name)
        {
            IBaseHero hero = null;

            switch (heroType)
            {
                case "Druid":

                    hero = new Druid(name);

                    break;
                case "Paladin":

                    hero = new Paladin(name);

                    break;
                case "Rogue":

                    hero = new Rogue(name);

                    break;
                case "Warrior":

                    hero = new Warrior(name);

                    break;
            }

            return hero;
        }
    }
}
