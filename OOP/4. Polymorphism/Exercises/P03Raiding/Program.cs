﻿using P03Raiding.Core;
using System;

namespace P03Raiding
{
    class Program
    {
        static void Main()
        {
            IEngine engine = new Engine();

            engine.Run();
        }
    }
}
