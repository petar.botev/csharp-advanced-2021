﻿namespace P02VehiclesExtension
{
    class Program
    {
        static void Main()
        {
            IEngine engine = new Engine();

            engine.Run();
        }
    }
}
