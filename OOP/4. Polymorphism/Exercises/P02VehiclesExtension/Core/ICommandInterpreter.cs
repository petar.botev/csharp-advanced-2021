﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P02VehiclesExtension.Core
{
    public interface ICommandInterpreter
    {
        void Read(Vehicle vehicle, string[] commandArgs);
    }
}
