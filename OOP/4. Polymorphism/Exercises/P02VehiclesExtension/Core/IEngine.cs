﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P02VehiclesExtension
{
    public interface IEngine
    {
        void Run();
    }
}
