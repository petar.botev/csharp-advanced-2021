﻿using P02VehiclesExtension.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace P02VehiclesExtension.Core
{
    public static class Factory
    {
        public static Vehicle CreateVehicle(string[] vehicleArgs)
        {
            double fuelQuantity = double.Parse(vehicleArgs[1]);
            double litters = double.Parse(vehicleArgs[2]);
            double tankCapacity = double.Parse(vehicleArgs[3]);

            Vehicle vehicle = null;

            switch (vehicleArgs[0])
            {
                case "Car":

                    vehicle = new Car(fuelQuantity, litters, tankCapacity);

                    break;
                case "Truck":

                    vehicle = new Truck(fuelQuantity, litters, tankCapacity);

                    break;
                case "Bus":

                    vehicle = new Bus(fuelQuantity, litters, tankCapacity);

                    break;
                default:
                    break;
            }

            return vehicle;
        }
    }
}
