﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P02VehiclesExtension.Core
{
    public class CommandInterpreter : ICommandInterpreter
    {
        public void Read(Vehicle vehicle, string[] commandArgs)
        {
            double commandParameter = double.Parse(commandArgs[2]);
            switch (commandArgs[0])
            {
                case "Drive":

                    Console.WriteLine(vehicle.Drive(commandParameter));

                    break;
                case "DriveEmpty":

                    Console.WriteLine(vehicle.DriveEmpty(commandParameter));

                    break;
                case "Refuel":
                    try
                    {
                        vehicle.Refuel(commandParameter);
                    }
                    catch (ArgumentOutOfRangeException aoe)
                    {

                        Console.WriteLine(aoe.ParamName);
                    }

                    break;
                default:
                    break;
            }
        }
    }
}
