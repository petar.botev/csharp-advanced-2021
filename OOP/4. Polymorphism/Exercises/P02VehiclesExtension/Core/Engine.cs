﻿using P02VehiclesExtension.Core;
using System;
using System.Linq;

namespace P02VehiclesExtension
{
    public class Engine : IEngine
    {
        public void Run()
        {
            Vehicle[] vehicles = new Vehicle[3];

            for (int i = 0; i < 3; i++)
            {
                string[] vehicleArgs = Console.ReadLine()
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                var vehicle = Factory.CreateVehicle(vehicleArgs);

                vehicles[i] = vehicle;
            }

            int numberOfCommands = int.Parse(Console.ReadLine());

            ICommandInterpreter interpreter = new CommandInterpreter();

            for (int i = 0; i < numberOfCommands; i++)
            {
                string[] commandArgs = Console.ReadLine()
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                Vehicle currentVehicle = vehicles.FirstOrDefault(x => x.GetType().Name == commandArgs[1]);

                interpreter.Read(currentVehicle, commandArgs);
            }

            foreach (var vehicle in vehicles)
            {
                Console.WriteLine($"{vehicle.GetType().Name}: {vehicle.FuelQuantity:f2}");
            }
        }
    }
}
