﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P02VehiclesExtension.Models
{
    public class Car : Vehicle
    {
        
        public Car(double fuelQuantity, double fuelConsumption, double tankCapacity) 
            : base(fuelQuantity, fuelConsumption, tankCapacity)
        {
        }

        public override string Drive(double distance)
        {
            double neededFuel = distance * (this.FuelConsumption + Constants.CAR_EXTRA_FUEL_CONSUMPTION);

            if (this.FuelQuantity >= neededFuel)
            {
                this.FuelQuantity -= neededFuel;

                return $"Car travelled {distance} km";
            }

            return "Car needs refueling";
        }

        public override string DriveEmpty(double distance) => throw new NotImplementedException();

        public override double Refuel(double litters)
        {
            base.Refuel(litters);
            return this.FuelQuantity += litters;
        }
    }
}
