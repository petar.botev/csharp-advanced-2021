﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P02VehiclesExtension
{
    public abstract class Vehicle
    {
        private double fuelQuantity;
        private double fuelConsumption;
        private double tankCapacity;

        protected Vehicle(double fuelQuantity, double fuelConsumption, double tankCapacity)
        {
            this.TankCapacity = tankCapacity;
            this.FuelQuantity = fuelQuantity;
            this.FuelConsumption = fuelConsumption;
        }

        public double FuelQuantity
        {
            get => fuelQuantity;
            set
            {
                if (value > tankCapacity)
                {
                    fuelQuantity = 0;
                }
                else
                {
                    fuelQuantity = value;
                }
            }
        }

        public double FuelConsumption { get => fuelConsumption; set => fuelConsumption = value; }
        public double TankCapacity { get => tankCapacity; set => tankCapacity = value; }

        public abstract string Drive(double distance);

        public abstract string DriveEmpty(double distance);

        public virtual double Refuel(double litters)
        {
            if (litters <= 0)
            {
                throw new ArgumentOutOfRangeException("Fuel must be a positive number");
            }
            if (tankCapacity < fuelQuantity + litters)
            {
                throw new ArgumentOutOfRangeException($"Cannot fit {litters} fuel in the tank");
            }
            return 0.0;
        }

    }
}
