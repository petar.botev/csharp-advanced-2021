﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P02VehiclesExtension.Models
{
    public class Bus : Vehicle
    {
        public Bus(double fuelQuantity, double fuelConsumption, double tankCapacity) 
            : base(fuelQuantity, fuelConsumption, tankCapacity)
        {
        }

        public override string Drive(double distance)
        {
            double neededFuel = distance * (FuelConsumption + Constants.BUS_EXTRA_FUEL_CONSUMPTION);

            if (this.FuelQuantity >= neededFuel)
            {
                this.FuelQuantity -= neededFuel;

                return $"Bus travelled {distance} km";
            }

            return "Bus needs refueling";
        }

        public override string DriveEmpty(double distance)
        {
            double neededFuel = distance * FuelConsumption;

            if (this.FuelQuantity >= neededFuel)
            {
                this.FuelQuantity -= neededFuel;

                return $"Bus travelled {distance} km";
            }

            return "Bus needs refueling";
        }

        public override double Refuel(double litters)
        {
            base.Refuel(litters);
            return this.FuelQuantity += litters;
        }
    }
}
