﻿using System;

namespace Shapes
{
    public class Rectangle : Shape
    {
        private double height;
        private double width;

        public Rectangle(double width, double height)
        {
            this.Height = height;
            this.Width = width;
        }

        public double Height { get => this.height; private set => height = value; }
        public double Width { get => this.width; private set => width = value; }

        public override double CalculateArea() => this.Height * this.Width;
        public override double CalculatePerimeter() => 2 * Height + 2 * Width;

        public override string Draw() => $"{base.Draw()}{this.GetType().Name}";
    }
}
