﻿using CommandPattern.Core.Contracts;
using System;
using System.Linq;
using System.Reflection;

namespace CommandPattern.Core
{
    public class CommandInterpreter : ICommandInterpreter
    {
        public string Read(string args)
        {
            string[] commandArgs = args
                .Split(' ', StringSplitOptions.RemoveEmptyEntries);

            string commandName = commandArgs[0];
            string[] arguments = commandArgs.Skip(1).ToArray();

            var assembly = Assembly.GetCallingAssembly()
                    .GetTypes()
                    .FirstOrDefault(x => x.Name == $"{commandName}Command");

            if (assembly == null)
            {
                throw new ArgumentException("Invalid command!");
            }

            var commandInstance = (ICommand)Activator.CreateInstance(assembly);

            if (commandInstance == null)
            {
                throw new ArgumentException("Invalid command!");
            }

            string result = commandInstance.Execute(arguments);
            return result;
        }
    }
}
