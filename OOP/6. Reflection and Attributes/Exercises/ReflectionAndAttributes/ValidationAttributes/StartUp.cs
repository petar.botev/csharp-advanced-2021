﻿using System;
using ValidationAttributes.Entities;

namespace ValidationAttributes
{
    public class StartUp
    {
        public static void Main(string[] args)
        {
            var person = new Person
             (
                 null,
                 -1
             );
            try
            {
                bool isValidEntity = Validator.IsValid(person);

                Console.WriteLine(isValidEntity);
            }
            catch (ArgumentException ae)
            {

                Console.WriteLine(ae.Message);
            }

            
        }
    }
}
