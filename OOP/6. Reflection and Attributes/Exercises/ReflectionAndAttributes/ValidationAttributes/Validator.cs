﻿using System;
using System.Linq;
using ValidationAttributes.Attributes;

namespace ValidationAttributes
{
    public static class Validator
    {
        public static bool IsValid(object obj)
        {
            Type currentObject = obj.GetType();

            var propertiesInfo = currentObject.GetProperties();

            foreach (var propertyInfo in propertiesInfo)
            {
                var propertyAttributes = propertyInfo.GetCustomAttributes(false).Select(x => x as MyValidationAttribute);

                foreach (var attribute in propertyAttributes)
                {
                    bool result = attribute.IsValid(propertyInfo.GetValue(obj));

                    if (!result)
                    {
                        return result;
                    }
                }
            }

            return true;
        }
    }
}
