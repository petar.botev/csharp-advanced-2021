﻿using System;

namespace P01Stealer
{
    class Program
    {
        static void Main(string[] args)
        {
            Spy spyInfo = new Spy();

            string result = spyInfo.StealFieldInfo("Hacker", "username", "password");

            Console.WriteLine(result);
            Console.WriteLine("--------------------------------------------------");

            string resultAccessMod = spyInfo.AnalyzeAcessModifiers("Hacker");

            Console.WriteLine(resultAccessMod);
            Console.WriteLine("--------------------------------------------------");


            string privateMethods = spyInfo.RevealPrivateMethods("Hacker");

            Console.WriteLine(privateMethods);
            Console.WriteLine("--------------------------------------------------");

            string collection = spyInfo.Collector("Hacker");

            Console.WriteLine(collection);
            Console.WriteLine("--------------------------------------------------");
        }
    }
}
