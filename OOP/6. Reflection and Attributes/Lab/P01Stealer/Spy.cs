﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace P01Stealer
{
    public class Spy
    {
        Assembly assembly = Assembly.GetExecutingAssembly();

        public string Collector(string className)
        {
            string fullName = assembly.GetTypes().FirstOrDefault(x => x.Name == className).FullName;
            Type classOfInterest = Type.GetType(fullName);

            StringBuilder strb = new StringBuilder();

            PropertyInfo[] properties = classOfInterest.GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static);

            MethodInfo[] getters = properties.Select(x => x.GetGetMethod(true)).ToArray();

            MethodInfo[] setters = properties.Select(x => x.GetSetMethod(true)).ToArray();

            foreach (var getter in getters)
            {
                strb.AppendLine($"{getter.Name} will return {getter.ReturnType}");
            }

            foreach (var setter in setters)
            {
                strb.AppendLine($"{setter.Name} will set field of {setter.GetParameters().FirstOrDefault().ParameterType}");
            }

            return strb.ToString().Trim();
        }

        public string RevealPrivateMethods(string className)
        {
            string classFullName = assembly.GetTypes().FirstOrDefault(x => x.Name == className).FullName;
            Type classOfInterest = Type.GetType(classFullName);

            StringBuilder strb = new StringBuilder();

            MethodInfo[] methods = classOfInterest.GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).Where(x => x.IsPrivate).ToArray();

            strb.AppendLine($"All Private Methods of Class: {classOfInterest.Name}");
            strb.AppendLine($"Base Class: {classOfInterest.BaseType.Name}");

            foreach (var method in methods)
            {
                strb.AppendLine(method.Name);
            }

            return strb.ToString().Trim();
        }

        public string AnalyzeAcessModifiers(string className)
        {

            string classFullName = assembly.GetTypes().FirstOrDefault(x => x.Name == className).FullName;
            Type classOfInterest = Type.GetType(classFullName);

            StringBuilder strb = new StringBuilder();

            var fields = classOfInterest.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance).Where(x => !x.IsPrivate).ToArray();

            foreach (var field in fields)
            {
                strb.AppendLine($"{field.Name} must be private!");
            }

            PropertyInfo[] properties = classOfInterest.GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static);

            foreach (var prop in properties)
            {
                var getMethod = prop.GetGetMethod(true);
                if (getMethod.IsPrivate)
                {
                    strb.AppendLine($"{getMethod.Name} have to be public!");
                }
            }

            foreach (var prop in properties)
            {
                var setMethod = prop.GetSetMethod(true);
                if (setMethod.IsPublic)
                {
                    strb.AppendLine($"{setMethod.Name} have to be private!");
                }
            }

            return strb.ToString().Trim();
        }

        public string StealFieldInfo(string observedClass, params string[] observedFields)
        {

            string classFullName = assembly.GetTypes().FirstOrDefault(x => x.Name == "Hacker").FullName;

            Type classOfInterest = Type.GetType(classFullName);

            var fields = classOfInterest.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance).Where(x => observedFields.Contains(x.Name)).ToArray();

            StringBuilder strb = new StringBuilder();

            strb.AppendLine($"Class under investigation: {classOfInterest.Name}");

            Object classInstance = Activator.CreateInstance(classOfInterest);

            foreach (var field in fields)
            {
                strb.AppendLine($"{field.Name} = {field.GetValue(classInstance)}");
            }

            return strb.ToString().Trim();
        }
    }
}
