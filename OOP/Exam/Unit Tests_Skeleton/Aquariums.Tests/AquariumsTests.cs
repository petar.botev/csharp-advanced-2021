﻿namespace Aquariums.Tests
{
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;

    public class AquariumsTests
    {
        
       

        [Test]
        public void Constructor_Sets_Values_Correctly()
        {
            Aquarium aq = new Aquarium("a", 5);
            Assert.AreEqual(0, aq.Count);
            Assert.AreEqual("a", aq.Name);
            Assert.AreEqual(5, aq.Capacity);
        }

        [Test]
        public void Prop_Name()
        {
            Assert.Throws<ArgumentNullException>(() => new Aquarium(null, 5));
        }

        [Test]
        public void Prop_NameEmpty()
        {
            Assert.Throws<ArgumentNullException>(() => new Aquarium("", 5));
        }

        [Test]
        public void Prop_CapacityEmpty()
        {
            Assert.Throws<ArgumentException>(() => new Aquarium("a", -1));
        }

        [Test]
        public void Add_CounEqualtoCapacity()
        {
            Fish fish = new Fish("caca");
            Aquarium aq = new Aquarium("a", 0);

            Assert.Throws<InvalidOperationException>(() => aq.Add(fish));
        }

        [Test]
        public void Add_AddCorrectly()
        {
            Fish fish = new Fish("caca");
            Aquarium aq = new Aquarium("a", 5);

            aq.Add(fish);

            Assert.AreEqual(1, aq.Count);
        }

        [Test]
        public void remove_exceptionIfFishIsNull()
        {
            //Fish fish = new Fish("caca");
            Aquarium aq = new Aquarium("a", 0);

            Assert.Throws<InvalidOperationException>(() => aq.RemoveFish(null));
        }

        [Test]
        public void Remove_RemoveCorrectly()
        {
            Fish fish = new Fish("caca");
            Aquarium aq = new Aquarium("a", 5);

            aq.Add(fish);
            aq.RemoveFish("caca");

            Assert.AreEqual(0, aq.Count);

        }

        [Test]
        public void Sell_exceptionIfFishIsNull()
        {
            //Fish fish = new Fish("caca");
            Aquarium aq = new Aquarium("a", 1);

            Assert.Throws<InvalidOperationException>(() => aq.SellFish(null));
        }

        [Test]
        public void Sell_available()
        {
            Fish fish = new Fish("caca");
            Aquarium aq = new Aquarium("a", 1);

            aq.Add(fish);
            aq.SellFish("caca");

            Assert.IsFalse(fish.Available);
        }

        [Test]
        public void Sell_returnFish()
        {
            Fish fish = new Fish("caca");
            Aquarium aq = new Aquarium("a", 1);

            aq.Add(fish);
            var fi = aq.SellFish("caca");

            Assert.AreSame(fish, fi);
        }

        [Test]
        public void Report()
        {
            Fish fish = new Fish("caca");
            Fish fish2 = new Fish("lin");
            Aquarium aq = new Aquarium("a", 1);
            
            aq.Add(fish);
            aq.Add(fish2);

            string str = $"Fish available at {aq.Name}: \"caca\", \"lin\"";

            Assert.AreEqual($"Fish available at a: \"caca\", \"lin\"", str);
        }
    }
}

