﻿

using AquaShop.Models.Decorations;
using AquaShop.Models.Decorations.Contracts;
using AquaShop.Repositories.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace AquaShop.Repositories
{
    public class DecorationRepository<T> : IRepository<T>
    {
        private readonly List<T> models;
        public DecorationRepository()
        {
            models = new List<T>();
        }

        public IReadOnlyCollection<T> Models => models.AsReadOnly();


        public void Add(T model)
        {
            models.Add(model);
        }
        public bool Remove(T model)
        {
            return models.Remove(model);
        }

        public T FindByType(string type)
        {
            return models.FirstOrDefault(x => x.GetType().Name == type);
        }

       
    }
}
