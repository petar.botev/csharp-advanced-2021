﻿using AquaShop.Models.Aquariums.Contracts;
using AquaShop.Models.Decorations.Contracts;
using AquaShop.Models.Fish.Contracts;
using AquaShop.Utilities.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AquaShop.Models.Aquariums
{
    public abstract class Aquarium : IAquarium
    {
        private string name;
        private int capacity;

        private readonly List<IDecoration> decorations;
        private readonly List<IFish> fishes;

        protected Aquarium(string name, int capacity)
        {
            this.Name = name;
            this.capacity = capacity;

            decorations = new List<IDecoration>();
            fishes = new List<IFish>();
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            private set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(ExceptionMessages.InvalidAquariumName);
                }
                this.name = value;
            }
        }

        public int Capacity
        {
            get
            {
                return capacity;
            }
        }


        public ICollection<IDecoration> Decorations => decorations.AsReadOnly();

        public ICollection<IFish> Fish => fishes.AsReadOnly();

        public int Comfort => decorations.Sum(x => x.Comfort);

        public void AddFish(IFish fish)
        {
            if (fishes.Count >= this.capacity)
            {
                throw new InvalidOperationException(ExceptionMessages.NotEnoughCapacity);
            }

            fishes.Add(fish);
        }

        public bool RemoveFish(IFish fish)
        {
            return fishes.Remove(fish);
        }

        public void AddDecoration(IDecoration decoration)
        {
            decorations.Add(decoration);
        }

        public void Feed()
        {
            foreach (var fish in fishes)
            {
                fish.Eat();
            }
        }

        public string GetInfo()
        {
            StringBuilder strb = new StringBuilder();
            StringBuilder strb2 = new StringBuilder();
            if (fishes.Count > 0)
            {
                strb.AppendLine($"{this.name} ({this.GetType().Name}):");
                for (int i = 0; i < this.fishes.Count; i++)
                {
                    if (i == 0)
                    {
                        strb2.Append($"Fish: {fishes[i].Name}, ");
                    }
                    else
                    {
                        strb2.Append($"{fishes[i].Name}, ");
                    }
                    
                }
                //foreach (var fish in this.fishes)
                //{
                //    strb2.Append($"Fish: {fish.Name}, ");
                //}
                strb.AppendLine(strb2.ToString().TrimEnd(new char[] { ',', ' ' }));
                strb.AppendLine($"Decorations: {decorations.Count}");
                strb.AppendLine($"Comfort: {Comfort}");
            }
            else
            {
                strb.AppendLine($"{this.name} ({this.GetType().Name}):");
                strb.AppendLine("Fish: none");
                strb.AppendLine($"Decorations: {decorations.Count}");
                strb.AppendLine($"Comfort: {Comfort}");
            }

            return strb.ToString().Trim();
        }
    }
}
