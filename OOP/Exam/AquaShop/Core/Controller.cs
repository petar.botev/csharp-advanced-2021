﻿using AquaShop.Core.Contracts;
using AquaShop.Models.Aquariums;
using AquaShop.Models.Aquariums.Contracts;
using AquaShop.Models.Decorations;
using AquaShop.Models.Decorations.Contracts;
using AquaShop.Models.Fish;
using AquaShop.Models.Fish.Contracts;
using AquaShop.Repositories;
using AquaShop.Repositories.Contracts;
using AquaShop.Utilities.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AquaShop.Core
{
    public class Controller : IController
    {
        private IRepository<Ornament> decorationsOrnaments;
        private IRepository<Plant> decorationsPlant;
        private List<IAquarium> aquariums;

        public Controller()
        {
            decorationsOrnaments = new DecorationRepository<Ornament>();
            decorationsPlant = new DecorationRepository<Plant>();
            this.aquariums = new List<IAquarium>();
        }

        public string AddAquarium(string aquariumType, string aquariumName)
        {
            if (aquariumType != "FreshwaterAquarium" && aquariumType != "SaltwaterAquarium")
            {
                throw new InvalidOperationException(ExceptionMessages.InvalidAquariumType);
            }

            if (aquariumType == "FreshwaterAquarium")
            {
                this.aquariums.Add(new FreshwaterAquarium(aquariumName));
            }
            else
            {
                this.aquariums.Add(new SaltwaterAquarium(aquariumName));
            }

            return string.Format(OutputMessages.SuccessfullyAdded, aquariumType);
        }

        public string AddDecoration(string decorationType)
        {

            if (decorationType != "Ornament" && decorationType != "Plant")
            {
                throw new InvalidOperationException(ExceptionMessages.InvalidDecorationType);
            }

            if (decorationType == "Ornament")
            {
                this.decorationsOrnaments.Add(new Ornament());
            }
            else
            {
                this.decorationsPlant.Add(new Plant());
            }

            return string.Format(OutputMessages.SuccessfullyAdded, decorationType);
        }

        public string InsertDecoration(string aquariumName, string decorationType)
        {
            if (decorationType != "Ornament" && decorationType != "Plant")
            {
                throw new InvalidOperationException(string.Format(ExceptionMessages.InexistentDecoration, decorationType));
            }

            
            var aquarium = aquariums.FirstOrDefault(x => x.Name == aquariumName);

            if (aquarium.GetType().Name != "FreshwaterAquarium" && aquarium.GetType().Name != "SaltwaterAquarium")
            {
                throw new InvalidOperationException(ExceptionMessages.InvalidAquariumType);
            }

            IDecoration decoration = null;
            if (decorationType == "Ornament")
            {
                decoration = decorationsOrnaments.Models.FirstOrDefault(x => x.GetType().Name == decorationType);
            }
            else
            {
                decoration = decorationsPlant.Models.FirstOrDefault(x => x.GetType().Name == decorationType);
            }

            if (decoration == null)
            {
                throw new InvalidOperationException(string.Format(ExceptionMessages.InexistentDecoration, decorationType));
            }

            aquarium.AddDecoration(decoration);
            if (decorationType == nameof(Ornament))
            {
                decorationsOrnaments.Remove((Ornament)decoration);
            }
            else
            {
                decorationsPlant.Remove((Plant)decoration);
            }
            

            return string.Format(OutputMessages.EntityAddedToAquarium, decorationType, aquariumName);
        }

        public string AddFish(string aquariumName, string fishType, string fishName, string fishSpecies, decimal price)
        {

            if (fishType != "FreshwaterFish" && fishType != "SaltwaterFish")
            {
                throw new InvalidOperationException(ExceptionMessages.InvalidFishType);
            }

            var aquarium = aquariums.FirstOrDefault(x => x.Name == aquariumName);

            if (aquarium.GetType().Name != "FreshwaterAquarium" && aquarium.GetType().Name != "SaltwaterAquarium")
            {
                throw new InvalidOperationException(ExceptionMessages.InvalidAquariumType);
            }
            if (aquarium.Name != aquariumName)
            {
                throw new InvalidOperationException(ExceptionMessages.InvalidAquariumName);
            }

            if (fishType == "FreshwaterFish" && aquarium.GetType().Name == "FreshwaterAquarium")
            {
                IFish fish = null;
                if (fishType == "FreshwaterFish")
                {
                    fish = new FreshwaterFish(fishName, fishSpecies, price);
                }
                else
                {
                    fish = new FreshwaterFish(fishName, fishSpecies, price);
                }
                aquarium.AddFish(fish);

                return string.Format(OutputMessages.EntityAddedToAquarium, fishType, aquariumName);
            }
            else if (fishType == "SaltwaterFish" && aquarium.GetType().Name == "SaltwaterAquarium")
            {
                IFish fish = null;
                if (fishType == "FreshwaterFish")
                {
                    fish = new FreshwaterFish(fishName, fishSpecies, price);
                }
                else
                {
                    fish = new FreshwaterFish(fishName, fishSpecies, price);
                }
                aquarium.AddFish(fish);

                return string.Format(OutputMessages.EntityAddedToAquarium, fishType, aquariumName);
            }
            else
            {
                return string.Format(OutputMessages.UnsuitableWater);
            }
            
        }

        public string CalculateValue(string aquariumName)
        {
            var aquarium = aquariums.FirstOrDefault(x => x.Name == aquariumName);

            var aquariumSum = aquarium.Fish.Sum(x => x.Price) + aquarium.Decorations.Sum(x => x.Price);

            return string.Format(OutputMessages.AquariumValue, aquariumName, aquariumSum);
        }

        public string FeedFish(string aquariumName)
        {
            var aquarium = aquariums.FirstOrDefault(x => x.Name == aquariumName);

            if (aquarium.GetType().Name != "FreshwaterAquarium" && aquarium.GetType().Name != "SaltwaterAquarium")
            {
                throw new InvalidOperationException(ExceptionMessages.InvalidAquariumType);
            }

            aquarium.Feed();

            return string.Format(OutputMessages.FishFed, aquarium.Fish.Count);
        }

        public string Report()
        {
            StringBuilder strb = new StringBuilder();
            foreach (var aquarium in aquariums)
            {
                strb.AppendLine(aquarium.GetInfo());
            }

            return strb.ToString().Trim();
        }
    }
}
