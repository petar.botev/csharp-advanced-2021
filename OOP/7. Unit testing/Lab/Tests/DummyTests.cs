﻿using NUnit.Framework;
using System;

[TestFixture]
public class DummyTests
{
    private const int DummyHealth = 20;
    private const int DummyXp = 10;

    private Dummy dummy;


    [Test]
    public void DummyLosesHealthAfterAttack()
    {
        // Arrange
        dummy = new Dummy(DummyHealth, DummyXp);

        // Act
        dummy.TakeAttack(5);

        // Assert
        Assert.IsTrue(dummy.Health == 15);
    }

    [Test]
    public void DeadDummyThrowsExceptionIfAttacked()
    {
        // Arrange
        dummy = new Dummy(0, DummyXp);

        // Assert
        var ex = Assert.Throws<InvalidOperationException>(() => dummy.TakeAttack(1));

        Assert.That(ex.Message, Is.EqualTo("Dummy is dead."));
    }

    [Test]
    public void DeadDummyGivesXP()
    {
        // Arrange
        dummy = new Dummy(0, DummyXp);

        int result = dummy.GiveExperience();

        Assert.That(result, Is.Positive);
    }


    [Test]
    public void AliveDummyDoesNotGiveXP()
    {
        // Arrange
        dummy = new Dummy(1, DummyXp);

        Assert.Throws<InvalidOperationException>(() => dummy.GiveExperience());

    }
}

