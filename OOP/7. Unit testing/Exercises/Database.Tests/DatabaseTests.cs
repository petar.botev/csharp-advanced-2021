using NUnit.Framework;
using System;

namespace Tests
{
    public class DatabaseTests
    {
        private Database.Database dataBase;

        [SetUp]
        public void Setup()
        {
            dataBase = new Database.Database();
        }

        [Test]
        
        public void DataArrayMustBeMaximum16ElementsLong()
        {
            this.dataBase = new Database.Database(new int[16]);

            Assert.AreEqual(dataBase.Count, 16);
        }

        [Test]
        public void AllParametersOfConstructorMustBeAddedToArray()
        {
            this.dataBase = new Database.Database(1, 2, 3);
            
            Assert.That(dataBase.Count == 3);
        }
        
        [Test]
        public void ThrowInvalidOperationExceptionIfAddMoreThan16ElementsToDataArray()
        {
            this.dataBase = new Database.Database(new int[16]);

            var ex = Assert.Throws<InvalidOperationException>(() => this.dataBase.Add(1));
            Assert.AreEqual(ex.Message, "Array's capacity must be exactly 16 integers!");
        }

        [Test]
        public void RemoveMustThrowExceptionIfDataCountIsUnderZero()
        {
            var ex = Assert.Throws<InvalidOperationException>(() => this.dataBase.Remove());
            Assert.AreEqual(ex.Message, "The collection is empty!");
        }

        [Test]
        public void FetchMethodReturnsAllElementsInData()
        {
            this.dataBase = new Database.Database(1, 2, 3);

            var elements = dataBase.Fetch();

            Assert.AreEqual(dataBase.Count, elements.Length);
        }

        [Test]
        public void RemoveOnlyLastElement()
        {
            this.dataBase = new Database.Database(1, 2, 3);

            dataBase.Remove();

            Assert.That(dataBase.Fetch, Is.EquivalentTo(new[] { 1, 2 }));
        }
    }
}