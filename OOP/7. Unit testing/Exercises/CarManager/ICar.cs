﻿namespace CarManager
{
    public interface ICar
    {
        double FuelAmount { get; }
        double FuelCapacity { get; }
        double FuelConsumption { get; }
        string Make { get; }
        string Model { get; }

        void Drive(double distance);
        void Refuel(double fuelToRefuel);
    }
}