using CarManager;
using Moq;
using NUnit.Framework;
using System;

namespace Tests
{
    public class CarTests
    {
        Car car;
        [SetUp]
        public void Setup()
        {
            car = new Car("Opel", "Astra", 7.0, 50.0);
        }

        [Test]
        public void CtorsSetValuesCorrectly()
        {
            Assert.AreEqual(0, car.FuelAmount);
            Assert.AreEqual("Opel", car.Make);
            Assert.AreEqual("Astra", car.Model);
            Assert.AreEqual(7.0, car.FuelConsumption);
            Assert.AreEqual(50.0, car.FuelCapacity);
        }

        [Test]
        [TestCase("")]
        [TestCase(null)]
        public void MakeThrowsExceptionIfIncorrectValue(string make)
        {
            Assert.Throws<ArgumentException>(() => new Car(make, "string", 1.0, 5.0));
        }

        [Test]
        [TestCase("")]
        [TestCase(null)]
        public void ModelThrowsExceptionIfIncorrectValue(string model)
        {
            Assert.Throws<ArgumentException>(() => new Car("string", model, 1.0, 5.0));
        }

        [Test]
        [TestCase(0)]
        [TestCase(-1)]
        public void FuelConsumptionThrowsExceptionIfIncorrectValue(double consumption)
        {
            Assert.Throws<ArgumentException>(() => new Car("string", "neshto", consumption, 5.0));
        }

        [Test]
        [TestCase(0)]
        [TestCase(-1.0)]
        public void FuelCapacityThrowsExceptionIfIncorrectValue(double capacity)
        {
            Assert.Throws<ArgumentException>(() => new Car("string", "neshto", 1.0, capacity));
        }

        [Test]
        [TestCase(0.0)]
        [TestCase(-0.1)]
        public void RefuelThrowExceptionIfValueIsZeroOrNegative(double fiel)
        {
            Assert.Throws<ArgumentException>(() => car.Refuel(fiel));
        }

        [Test]
        public void RefuelAddFuelToFuelAmount()
        {
            car.Refuel(10.0);

            Assert.AreEqual(10, car.FuelAmount);
        }

        [Test]
        public void RefuelAdjustFuelAmountToFuelCapacityIfFuelAmoutIsMoreThanFuelCapacity()
        {
            car.Refuel(51.0);
            Assert.AreEqual(car.FuelCapacity, car.FuelAmount);
        }

        [Test]
        public void DriveThrowsExceptionIfFuelNeededIsMoreThanRuelAmount()
        {
            Assert.Throws<InvalidOperationException>(() => car.Drive(10));
        }

        [Test]
        public void DriveReduceFuelAmount()
        {
            car.Refuel(10);
            car.Drive(5);

            Assert.AreEqual(9.65, car.FuelAmount);
        }

        [Test]
        public void FuelAmountThrowsExceptionIfValueIsNegative()
        {
            Assert.Throws<InvalidOperationException>(() => car.Drive(1));
        }
    }
}