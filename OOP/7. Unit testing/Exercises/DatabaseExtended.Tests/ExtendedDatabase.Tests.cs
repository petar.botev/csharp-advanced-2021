using ExtendedDatabase;
using NUnit.Framework;
using System;

namespace Tests
{
    public class ExtendedDatabaseTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void UserNamePropertyMustBeSet()
        {
            Person person = new Person(1, "Gosho");

            Assert.AreEqual("Gosho", person.UserName);
        }

        [Test]
        public void IdPropertyMustBeSet()
        {
            Person person = new Person(1, "Gosho");

            Assert.AreEqual(1, person.Id);
        }

        [Test]
        public void ExtendedCtorPersonCollectionMustBeOf16Elements()
        {
            Person person1 = new Person(1, "Gosho");
            Person person2 = new Person(2, "Pesho");

            ExtendedDatabase.ExtendedDatabase database = new ExtendedDatabase.ExtendedDatabase(person1, person2);

            Assert.AreEqual(database.Count, 2);
        }

        [Test]
        public void AddRangeThrowExceptionWhenAddMoreThan16Persons()
        {
            Person[] personArr = new Person[17];
            for (int i = 0; i < 16; i++)
            {
                Person person = new Person(1 + i, $"Gosho{i}");
                personArr[i] = person;
            }

            Assert.Throws<ArgumentException>(() => new ExtendedDatabase.ExtendedDatabase(personArr), "Provided data length should be in range[0..16]!");
        }

        [Test]
        public void AddRangeShouldAddCorrectCount()
        {
            ExtendedDatabase.ExtendedDatabase database = new ExtendedDatabase.ExtendedDatabase();

            Person[] personArr = new Person[15];
            Person person = null;
            for (int i = 0; i < 15; i++)
            {
                person = new Person(1 + i, $"Gosho{i}");
                database.Add(person);
                personArr[i] = person;
            }

            Assert.AreEqual(15, database.Count);
        }

        [Test]
        public void AddThrowExceptionWhenCountIs16()
        {
            Person[] personArr = new Person[16];
            for (int i = 0; i < 16; i++)
            {
                Person person = new Person(1 + i, $"Gosho{i}");
                personArr[i] = person;
            }

            ExtendedDatabase.ExtendedDatabase database = new ExtendedDatabase.ExtendedDatabase(personArr);

            Assert.Throws<InvalidOperationException>(() => database.Add(new Person(100, "Pesho")), "Array's capacity must be exactly 16 integers!");
        }

        [Test]
        [TestCase(100, "Gosho0")]
        [TestCase(100, null)]
        public void AddThrowsExceptionWhenTtryToAddExistingUserName(int id, string name)
        {
            Person[] personArr = new Person[15];
            Person person = null;
            for (int i = 0; i < 15; i++)
            {
                person = new Person(1 + i, $"Gosho{i}");
                personArr[i] = person;
            }

            ExtendedDatabase.ExtendedDatabase database = new ExtendedDatabase.ExtendedDatabase(personArr);

            Assert.Throws<InvalidOperationException>(() => database.Add(new Person(id, name)), "There is already user with this username!"); 
        }

        [Test]
        public void AddShouldAddPersonToCollection()
        {

            ExtendedDatabase.ExtendedDatabase database = new ExtendedDatabase.ExtendedDatabase();

            database.Add(new Person(1, "Gosho"));

            Assert.AreEqual(1, database.Count);
        }

        [Test]
        public void AddThrowsExceptionWhenTtryToAddExistingUserId()
        {
            Person[] personArr = new Person[15];
            Person person = null;
            for (int i = 0; i < 15; i++)
            {
                person = new Person(1 + i, $"Gosho{i}");
                personArr[i] = person;
            }

            ExtendedDatabase.ExtendedDatabase database = new ExtendedDatabase.ExtendedDatabase(personArr);

            Assert.Throws<InvalidOperationException>(() => database.Add(new Person(1, "Pesho")), "There is already user with this username!");
        }

        [Test]
        public void RemoveThrowsExceptionIfCountIsZero()
        {
            Person[] personArr = new Person[15];
            Person person = null;
            for (int i = 0; i < 15; i++)
            {
                person = new Person(1 + i, $"Gosho{i}");
                personArr[i] = person;
            }

            ExtendedDatabase.ExtendedDatabase database = new ExtendedDatabase.ExtendedDatabase();

            Assert.Throws<InvalidOperationException>(() => database.Remove());
        }

        [Test]
        public void RemoveShouldDecreaseCount()
        {
            Person[] personArr = new Person[15];
            Person person = null;
            for (int i = 0; i < 15; i++)
            {
                person = new Person(1 + i, $"Gosho{i}");
                personArr[i] = person;
            }

            ExtendedDatabase.ExtendedDatabase database = new ExtendedDatabase.ExtendedDatabase(personArr);

            database.Remove();

            Assert.AreEqual(14, database.Count);
        }

        [Test]
        public void FindByUsenameThrowsExceptionIfParameterIsNullOrEmpty()
        {
            ExtendedDatabase.ExtendedDatabase database = new ExtendedDatabase.ExtendedDatabase();

            Assert.Throws<ArgumentNullException>(() => database.FindByUsername(null));
        }

        [Test]
        public void FindByUsenameThrowsExceptionIfTheNameDoesNotExists()
        {
            ExtendedDatabase.ExtendedDatabase database = new ExtendedDatabase.ExtendedDatabase();

            Assert.Throws<InvalidOperationException>(() => database.FindByUsername(" "));
        }

        [Test]
        public void FindByUsernameReturnsUserWithGivenName()
        {
            ExtendedDatabase.ExtendedDatabase database = new ExtendedDatabase.ExtendedDatabase();

            Person[] personArr = new Person[15];
            Person person = null;
            for (int i = 0; i < 15; i++)
            {
                person = new Person(1 + i, $"Gosho{i}");
                database.Add(person);
                personArr[i] = person;
            }

            Assert.AreEqual(personArr[0], database.FindByUsername("Gosho0"));
        }


        [Test]
        public void FindByIdThrowsExceptionIfParameterIsNegativeNumber()
        {
            ExtendedDatabase.ExtendedDatabase database = new ExtendedDatabase.ExtendedDatabase();

            Assert.Throws<ArgumentOutOfRangeException>(() => database.FindById(-1));
        }

        [Test]
        public void FindByIdThrowsExceptionIfTheIdDoesNotExists()
        {
            ExtendedDatabase.ExtendedDatabase database = new ExtendedDatabase.ExtendedDatabase();

            Assert.Throws<InvalidOperationException>(() => database.FindById(100));
        }

        [Test]
        public void FindByIdReturnsUserWithGivenId()
        {
            ExtendedDatabase.ExtendedDatabase database = new ExtendedDatabase.ExtendedDatabase();

            Person[] personArr = new Person[15];
            Person person = null;
            for (int i = 0; i < 15; i++)
            {
                person = new Person(1 + i, $"Gosho{i}");
                database.Add(person);
                personArr[i] = person;
            }

            Assert.AreEqual(personArr[0], database.FindById(1));
        }
    }
}