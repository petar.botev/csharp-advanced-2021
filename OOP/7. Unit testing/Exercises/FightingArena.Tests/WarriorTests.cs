using FightingArena;
using NUnit.Framework;
using System;

namespace Tests
{
    public class WarriorTests
    {
        private Warrior warrior;

        [SetUp]
        public void Setup()
        {
            warrior = new Warrior("Pesho", 10, 100);
        }

        [Test]
        public void CtorValuesSetCorrectly()
        {
            Warrior warrior = new Warrior("Pesho", 10, 100);

            Assert.AreEqual("Pesho", warrior.Name);
            Assert.AreEqual(10, warrior.Damage);
            Assert.AreEqual(100, warrior.HP);
        }

        [Test]
        [TestCase(" ")]
        [TestCase(null)]
        public void NameThrowsExceptionIfValueIsNullOrWhiteSpace(string name)
        {
            Assert.Throws<ArgumentException>(() => new Warrior(name, 10, 100));
        }

        [Test]
        public void DamageReturnCorrectValue()
        {
            Assert.AreEqual(10, warrior.Damage);
        }

        [Test]
        [TestCase(0)]
        [TestCase(-1)]
        public void DamageThrowsExceptionIfValueIsZeroOrNegative(int damage)
        {
            Assert.Throws<ArgumentException>(() => new Warrior("nAME", damage, 100));
        }

        [Test]
        public void HPReturnCorrectValue()
        {
            Assert.AreEqual(100, warrior.HP);
        }

        [Test]
        [TestCase(-1)]
        public void HPThrowsExceptionIfValueIsZeroOrNegative(int HP)
        {
            Assert.Throws<ArgumentException>(() => new Warrior("nAME", 10, HP));
        }

        [Test]
        [TestCase(30)]
        [TestCase(29)]
        public void AttackThrowsExceptionIfAttackerHpIsEqualOrLessThan30(int hP)
        {
            Assert.Throws<InvalidOperationException>(() => new Warrior("Name", 10, hP).Attack(new Warrior("Gosho", 10, 100)));
        }

        [Test]
        [TestCase(30)]
        [TestCase(29)]
        public void AttackThrowsExceptionIfDefenderHpIsEqualOrLessThan30(int hP)
        {
            Assert.Throws<InvalidOperationException>(() => new Warrior("Name", 10, 100).Attack(new Warrior("Gosho", 10, hP)));
        }

        [Test]
        public void AttackThrowsExceptionIfAttackerrHpIsLessThanDefenderDamage()
        {
            Assert.Throws<InvalidOperationException>(() => new Warrior("Name", 100, 100).Attack(new Warrior("Gosho", 101, 100)));
        }

        [Test]
        public void AttackDamageDecreaseDefenderHPCorrectly()
        {
            Warrior defender = new Warrior("Kynio", 10, 100);
            warrior.Attack(defender);

            Assert.AreEqual(90, defender.HP);
        }

        [Test]
        public void AttackDefenderDamageDecreaseAttackHPCorrectly()
        {
            Warrior defender = new Warrior("Kynio", 10, 100);
            warrior.Attack(defender);

            Assert.AreEqual(90, warrior.HP);
        }

        [Test]
        public void AttackIfAttackerDamageIsGreaterThanDefenderHpSetDefenderHpToZero()
        {
            Warrior attacker = new Warrior("Gorcho", 50, 100);
            Warrior defender = new Warrior("Kynio", 10, 49);
            attacker.Attack(defender);

            Assert.AreEqual(0, defender.HP);
        }
    }
}