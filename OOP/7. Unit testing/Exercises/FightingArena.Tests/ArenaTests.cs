﻿using FightingArena;
using NUnit.Framework;
using System;
using System.Linq;

namespace Tests
{
    public class ArenaTests
    {
        private Arena arena;

        [SetUp]
        public void Setup()
        {
            arena = new Arena();
        }

        [Test]
        public void CtorSetsArrayCorrectly()
        {
            Assert.AreNotEqual(null, arena.Count);
        }

        [Test]
        public void EnrollThrowsExceptionIfTryToAddWariorWithExistingName()
        {
            Warrior warrior = new Warrior("Ivan", 10, 100);
            arena.Enroll(warrior);
            Warrior warrior2 = new Warrior("Ivan", 11, 101);

            Assert.Throws<InvalidOperationException>(() => arena.Enroll(warrior2));

        }

        [Test]
        public void EnrollAddsWariorsCorrectly()
        {
            Warrior warrior = new Warrior("Ivan", 10, 100);
            arena.Enroll(warrior);

            Assert.AreEqual(1, arena.Warriors.Count);
        }

        [Test]
        public void FightWarriorsShouldExists()
        {
            Warrior warrior = new Warrior("Pesho", 10, 100);
            Warrior warrior2 = new Warrior("Gosho", 5, 50);
            arena.Enroll(warrior);
            arena.Enroll(warrior2);

            Assert.AreEqual(warrior, arena.Warriors.First());
            Assert.AreEqual(warrior2, arena.Warriors.Skip(1).First());
        }

        [Test]
        public void FightThrowExceptionIfAttackerIsNull()
        {
            Warrior warrior = new Warrior("Pesho", 10, 100);
            Warrior warrior2 = new Warrior("Gosho", 5, 50);
            arena.Enroll(warrior);
            arena.Enroll(warrior2);

            Assert.Throws<InvalidOperationException>(() => arena.Fight("Tosho", "Gosho"), $"There is no fighter with name {warrior} enrolled for the fights!");
        }

        [Test]
        public void FightThrowExceptionIfDefenderIsNull()
        {
            Warrior warrior = new Warrior("Pesho", 10, 100);
            Warrior warrior2 = new Warrior("Gosho", 5, 50);
            arena.Enroll(warrior);
            arena.Enroll(warrior2);

            Assert.Throws<InvalidOperationException>(() => arena.Fight("Pesho", "Vasko"), $"There is no fighter with name {warrior2} enrolled for the fights!");
        }

        [Test]
        public void FightAttackerAttackDefender()
        {
            //Arena arena = new Arena();

            Warrior attacker = new Warrior("Pesho", 10, 100);
            Warrior defender = new Warrior("Gosho", 5, 50);

            arena.Enroll(attacker);
            arena.Enroll(defender);

            arena.Fight("Pesho", "Gosho");

            Assert.AreEqual(40, defender.HP);
        }
    }
}
