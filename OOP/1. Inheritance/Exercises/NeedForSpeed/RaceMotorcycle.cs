﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeedForSpeed
{
    public class RaceMotorcycle : Motorcycle
    {
        private const double defaultFuelConsumption = 8;

        public RaceMotorcycle(int horsePower, double fuel) : base(horsePower, fuel)
        {
        }

        public override double FuelConsumption => defaultFuelConsumption;

        public override void Drive(double kilometers)
        {
            var reducedFuel = this.Fuel - kilometers * FuelConsumption;
            if (reducedFuel >= 0)
            {
                Fuel = reducedFuel;
            }
        }
    }
}
