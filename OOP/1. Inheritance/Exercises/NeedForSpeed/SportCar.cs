﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeedForSpeed
{
    public class SportCar : Car
    {
        private const double defaultFuelConsumption = 10;

        public SportCar(int horsePower, double fuel) : base(horsePower, fuel)
        {
        }

        public override double FuelConsumption => defaultFuelConsumption;

        public override void Drive(double kilometers)
        {
            var reducedFuel = this.Fuel - kilometers * FuelConsumption;
            if (reducedFuel >= 0)
            {
                Fuel = reducedFuel;
            }
        }
    }
}
