﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeedForSpeed
{
    public class Vehicle
    {
        private int horsePower;
        private double fuel;

        private const double defaultFuelConsumption = 1.25;


        public Vehicle(int horsePower, double fuel)
        {
            this.HorsePower = horsePower;
            this.Fuel = fuel;
        }

        public int HorsePower { get => horsePower; set => horsePower = value; }
        public double Fuel { get => fuel; set => fuel = value; }
        public virtual double FuelConsumption => defaultFuelConsumption;

        public virtual void Drive(double kilometers)
        {
            var reducedFuel = Fuel - kilometers * defaultFuelConsumption;
            if (reducedFuel >= 0)
            {
                Fuel = reducedFuel;
            }
        }


    }
}