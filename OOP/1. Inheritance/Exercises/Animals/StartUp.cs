﻿using System;

namespace Animals
{
    public class StartUp
    {
        public static void Main(string[] args)
        {
            string kindOfAnimal = string.Empty;

            while ((kindOfAnimal = Console.ReadLine()) != "Beast!")
            {
                
                string[] animalParams = Console.ReadLine()
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                string name = animalParams[0];
                int age = int.Parse(animalParams[1]);
                string gender = animalParams[2];

                Animal animal = null;
                try
                {
                    switch (kindOfAnimal)
                    {
                        case "Cat":
                            animal = new Cat(name, age, gender);
                            
                            break;

                        case "Dog":
                            animal = new Dog(name, age, gender);
                            break;

                        case "Frog":
                            animal = new Frog(name, age, gender);
                            break;

                        case "Kitten":
                            animal = new Kitten(name, age);
                            break;

                        case "Tomcat":
                            animal = new Tomcat(name, age);
                            break;
                        default:
                            break;
                    }

                    Console.WriteLine(animal.GetType().Name);
                    Console.WriteLine($"{animal.Name} {animal.Age} {animal.Gender}");

                    Console.WriteLine(animal.ProduceSound());
                }
                catch (ArgumentException ae)
                {

                    Console.WriteLine(ae.Message);
                }
            }
        }
    }
}
