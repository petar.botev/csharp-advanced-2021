﻿using System;
using System.Collections.Generic;

namespace CustomRandomList
{
    public class RandomList : List<string>
    {
        private Random randy;
        public string RandomString()
        {
            int randIndex = randy.Next(0, this.Count);
            string element = this[randIndex];
            this.RemoveAt(randIndex);

            return element;
        }
    }
}
