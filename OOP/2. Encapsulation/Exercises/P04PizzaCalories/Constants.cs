﻿namespace P04PizzaCalories
{
    public static class Constants
    {
        internal static int DOUGH_CALORIES = 2;
        internal static double WHITE_DOUGH_MODIFIER = 1.5;
        internal static double WHOLEGRAIN_DOUGH_MODIFIER = 1;
        internal static double CRISPY_MODIFIER = 0.9;
        internal static double CHEWY_MODIFIER = 1.1;
        internal static double HOMEMADE_MODIFIER = 1.0;

        internal static int TOPPING_BASE_CALORIES = 2;
        internal static double MEAT_MODIFIER = 1.2;
        internal static double VEGGIES_MODIFIER = 0.8;
        internal static double CHEESE_MODIFIER = 1.1;
        internal static double SAUCE_MODIFIER = 0.9;

    }
}
