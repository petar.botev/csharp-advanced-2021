﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P04PizzaCalories
{
    public class Dough
    {
        private string flourType;
        private string bakingTechnique;
        private double weight;
        
        public Dough(string flourType, string bakingTechnique, double weight)
        {
            this.FlourType = flourType.ToLower();
            this.BakingTechnique = bakingTechnique.ToLower();
            this.Weight = weight;
        }

        public string FlourType
        {
            get => flourType; private set
            {
                if (value != "white" && value != "wholegrain")
                {
                    throw new ArgumentException("Invalid type of dough.");
                }
                flourType = value;
            }
        }
        public string BakingTechnique
        {
            get => bakingTechnique; private set
            {
                if (value != "crispy" && value != "chewy" && value != "homemade")
                {
                    throw new ArgumentException("Invalid type of dough.");
                }
                bakingTechnique = value;
            }
        }
        public double Weight
        {
            get => weight; private set
            {
                if (value < 1 || value > 200)
                {
                    throw new ArgumentException("Dough weight should be in the range [1..200].");
                }
                weight = value;
            }
        }

        public double DoughCalories()
        {
            double baking = 0.0;
            double flourTypeMod = 0.0;
            double doughDefaultCalories = Constants.DOUGH_CALORIES;

            if (FlourType == "white")
            {
                flourTypeMod = Constants.WHITE_DOUGH_MODIFIER;
                baking = BakingModifier(bakingTechnique);
            }
            else
            {
                flourTypeMod = Constants.WHOLEGRAIN_DOUGH_MODIFIER;
                baking = BakingModifier(bakingTechnique);
            }

            return doughDefaultCalories * baking * flourTypeMod * this.Weight;
        }

        private double BakingModifier(string BakingTechnique)
        {
            double baking = 0.0;
            switch (BakingTechnique)
            {
                case "crispy":
                    baking = Constants.CRISPY_MODIFIER;
                    break;
                case "chewy":
                    baking = Constants.CHEWY_MODIFIER;
                    break;
                case "homemade":
                    baking = Constants.HOMEMADE_MODIFIER;
                    break;
                default:
                    break;
            }

            return baking;
        }
    }
}
