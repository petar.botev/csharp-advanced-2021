﻿using System;

namespace P04PizzaCalories
{
    class Program
    {
        static void Main()
        {
            string pizzaName = Console.ReadLine()
                .Split(' ')[1];

            string[] doughArgs = Console.ReadLine()
                .Split(' ');
            string doughType = doughArgs[1];
            string bakingType = doughArgs[2];
            double doughWeight = double.Parse(doughArgs[3]);

            try
            {
                Dough dough = new Dough(doughType, bakingType, doughWeight);

                Pizza pizza = new Pizza(pizzaName, dough);

                string command;
                string toppingType = string.Empty;
                double toppingWeight = 0.0;

                while ((command = Console.ReadLine()) != "END")
                {
                    string[] toppingArgs = command
                       .Split(' ');
                    toppingType = toppingArgs[1];
                    toppingWeight = double.Parse(toppingArgs[2]);

                    Topping topping = new Topping(toppingWeight, toppingType);

                    pizza.AddToppings(topping);
                }

                Console.WriteLine($"{pizza.Name} - {pizza.TotalCalories:f2} Calories.");
            }
            catch (ArgumentException ae)
            {

                Console.WriteLine(ae.Message);
            }
           
        }
    }
}
