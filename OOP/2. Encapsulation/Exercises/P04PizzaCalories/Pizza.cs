﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P04PizzaCalories
{
    public class Pizza
    {
        private string name;
        private List<Topping> toppings;
        private Dough dough;
        private double totalCalories;
        private int numberOfToppings;

        public Pizza(string name, Dough dough)
        {
            this.Name = name;
            this.toppings = new List<Topping>();
            this.Dough = dough;
            
        }

        public string Name
        {
            get => name; 
            private set
            {
                if ((value.Length < 1 || value.Length > 15) || value == string.Empty || value == null || value == " ")
                {
                    throw new ArgumentException("Pizza name should be between 1 and 15 symbols.");
                }
                name = value;
            }
        }
        //public IReadOnlyCollection<Topping> Toppings { get => toppings.AsReadOnly();}
        public Dough Dough { get => dough; private set => dough = value; }

        public double TotalCalories { get => PizzaCalories(); }
        public int NumberOfToppings
        {
            get
            {
                return numberOfToppings;
            }
            private set
            {
                if (value < 0 || value > 10)
                {
                    throw new ArgumentException("Number of toppings should be in range [0..10].");
                }

                numberOfToppings = value;
            }
        }

        private double PizzaCalories()
        {
            totalCalories = toppings.Sum(x => x.ToppingCalories()) + Dough.DoughCalories();

            return totalCalories;
        }

        public void AddToppings(Topping topping)
        {
            toppings.Add(topping);
            NumberOfToppings++;
        }
    }
}
