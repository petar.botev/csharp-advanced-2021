﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P04PizzaCalories
{
    public class Topping
    {
        private double weight;
        private string toppingType;

        public Topping(double weight, string toppingType)
        {
            this.ToppingType = toppingType;
            this.Weight = weight;
        }

        public double Weight
        {
            get => weight; private set
            {
                if (value < 1 || value > 50)
                {
                    throw new ArgumentException($"{toppingType} weight should be in the range [1..50].");
                }
                weight = value;
            }
        }

        public string ToppingType
        {
            get => toppingType; 
            set
            {
                if (value.ToLower() != "meat" && value.ToLower() != "veggies" && value.ToLower() != "cheese" && value.ToLower() != "sauce")
                {
                    throw new ArgumentException($"Cannot place {value} on top of your pizza.");
                }
                toppingType = value;
            }
        }

        public double ToppingCalories()
        {
            int toppingBaseCalories = Constants.TOPPING_BASE_CALORIES;
            double toppingModifier = 0.0;

            switch (this.ToppingType.ToLower())
            {
                case "meat":
                    toppingModifier = Constants.MEAT_MODIFIER;
                    break;

                case "veggies":
                    toppingModifier = Constants.VEGGIES_MODIFIER;
                    break;

                case "cheese":
                    toppingModifier = Constants.CHEESE_MODIFIER;
                    break;

                case "sauce":
                    toppingModifier = Constants.SAUCE_MODIFIER;
                    break;
                default:
                    break;
            }

            return toppingBaseCalories * toppingModifier * Weight;
        }
    }
}
