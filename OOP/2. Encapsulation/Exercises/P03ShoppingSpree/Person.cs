﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P03ShoppingSpree
{
    public class Person
    {
        private string name;
        private decimal money;
        private List<Product> products;

        public Person(string name, decimal money)
        {
            this.Name = name;
            this.Money = money;
            this.products = new List<Product>();
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                if (value == string.Empty || value == null || value == " ")
                {
                    throw new ArgumentException("Name cannot be empty");
                }
                name = value;
            }
        }
        public decimal Money
        {
            get
            {
                return money;
            }

            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("Money cannot be negative");
                }
                money = value;
            }
        }
        //public IReadOnlyCollection<Product> Products => this.products.AsReadOnly();

        public void Buy(Product product)
        {
            if (this.Money >= product.Cost)
            {
                products.Add(product);
                this.Money -= product.Cost;
                Console.WriteLine($"{this.Name} bought {product.Name}");
            }
            else
            {
                Console.WriteLine($"{this.Name} can't afford {product.Name}");
            }
        }

        public override string ToString()
        {
            StringBuilder strb = new StringBuilder();

            foreach (var item in products)
            {
                strb.Append($"{item.Name}, ");
            }

            if (this.products.Count == 0)
            {
                return $"{this.Name} - Nothing bought";
            }
            return $"{this.Name} - {strb.ToString().Trim(new char[] { ',', ' '})}"; 
        }
    }
}
