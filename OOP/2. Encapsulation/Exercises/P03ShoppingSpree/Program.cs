﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P03ShoppingSpree
{
    class Program
    {
        static void Main()
        {
            string[] people = Console.ReadLine()
                .Split(';', StringSplitOptions.RemoveEmptyEntries);
            string[] products = Console.ReadLine()
                .Split(';', StringSplitOptions.RemoveEmptyEntries);

            Person person = null;
            Product product = null;

            List<Person> peopleList = new List<Person>();
            List<Product> productList = new List<Product>();

            try
            {
                foreach (var man in people)
                {
                    string[] manArgs = man
                        .Split('=', StringSplitOptions.RemoveEmptyEntries);
                    person = new Person(manArgs[0], decimal.Parse(manArgs[1]));
                    
                    peopleList.Add(person);
                }

                foreach (var item in products)
                {
                    string[] itemArgs = item
                        .Split('=',StringSplitOptions.RemoveEmptyEntries);

                    product = new Product(itemArgs[0], decimal.Parse(itemArgs[1]));

                    productList.Add(product);
                }

                string command;
                while ((command = Console.ReadLine()) != "END")
                {
                    string[] commandArgs = command
                        .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                    var buyer = peopleList.FirstOrDefault(x => x.Name == commandArgs[0]);
                    var productToBuy = productList.FirstOrDefault(x => x.Name == commandArgs[1]);

                    buyer.Buy(productToBuy);
                }

                StringBuilder strb = new StringBuilder();

                foreach (var man in peopleList)
                {
                    strb.AppendLine(man.ToString());
                }

                Console.Write(strb.ToString());
            }
            catch (ArgumentException ae)
            {

                Console.WriteLine(ae.Message);
            }
        }
    }
}
