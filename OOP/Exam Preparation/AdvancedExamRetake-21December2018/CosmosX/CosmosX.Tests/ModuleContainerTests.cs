namespace CosmosX.Tests
{
    using CosmosX.Entities.Containers;
    using CosmosX.Entities.Modules.Absorbing;
    using CosmosX.Entities.Modules.Absorbing.Contracts;
    using CosmosX.Entities.Modules.Contracts;
    using CosmosX.Entities.Modules.Energy;
    using NUnit.Framework;
    using System;
    using System.Linq;

    [TestFixture]
    public class ModuleContainerTests
    {
        //private ModuleContainer container;

        //[SetUp]
        //public void SetUp()
        //{
        //    container = new ModuleContainer(10);
        //}
               
        [Test]
        public void AddEnergyModule_Throws_Exception_If_EnergyModule_IsNull()
        {
            ModuleContainer container = new ModuleContainer(10);
            Assert.Throws<ArgumentException>(() => container.AddEnergyModule(null));
        }

        [Test]
        public void AddEnergyModule_Executes_RemoveOldestModule()
        {
            ModuleContainer container = new ModuleContainer(2);
            CryogenRod energyModule = new CryogenRod(1, 10);
            CryogenRod energyModule2 = new CryogenRod(2, 5);
            CryogenRod energyModule3 = new CryogenRod(3, 5);

            container.AddEnergyModule(energyModule);
            container.AddEnergyModule(energyModule2);
            container.AddEnergyModule(energyModule3);

            Assert.AreEqual(2, container.ModulesByInput.Count);
            Assert.That(!container.ModulesByInput.Any(x => x.Id == 1));
        }

        [Test]
        public void AddEnergyModule_Add_Module_To_Array()
        {
            ModuleContainer container = new ModuleContainer(2);
            CryogenRod energyModule = new CryogenRod(1, 10);

            container.AddEnergyModule(energyModule);

            Assert.AreEqual(1, container.ModulesByInput.Count);
            Assert.AreEqual(10, container.TotalEnergyOutput);
        }

        [Test]
        public void AddAbsorbingModule_Throws_Exception_If_Module_IsNull()
        {
            ModuleContainer container = new ModuleContainer(10);
            Assert.Throws<ArgumentException>(() => container.AddAbsorbingModule(null));
        }

        [Test]
        public void AddAbsorbingModule_Executes_RemoveOldestModule()
        {
            ModuleContainer container = new ModuleContainer(2);
            IAbsorbingModule absorbingModule = new HeatProcessor(1, 10);
            IAbsorbingModule absorbingModule2 = new HeatProcessor(2, 5);
            IAbsorbingModule absorbingModule3 = new HeatProcessor(3, 5);

            container.AddAbsorbingModule(absorbingModule);
            container.AddAbsorbingModule(absorbingModule2);
            container.AddAbsorbingModule(absorbingModule3);

            Assert.AreEqual(2, container.ModulesByInput.Count);
            Assert.That(!container.ModulesByInput.Any(x => x.Id == 1));
        }

        [Test]
        public void AddAbsorbingModule_Add_Module_To_Array()
        {
            ModuleContainer container = new ModuleContainer(2);
            IAbsorbingModule absorbing = new HeatProcessor(1, 10);

            container.AddAbsorbingModule(absorbing);

            Assert.AreEqual(1, container.ModulesByInput.Count);
            Assert.AreEqual(10, container.TotalHeatAbsorbing);
        }

        [Test]
        public void RemoveOldestModule_Remove_First_Elemet()
        {
            ModuleContainer container = new ModuleContainer(2);
            IAbsorbingModule absorbingModule = new HeatProcessor(1, 10);
            IAbsorbingModule absorbingModule2 = new HeatProcessor(2, 5);
            IAbsorbingModule absorbingModule3 = new HeatProcessor(3, 5);

            container.AddAbsorbingModule(absorbingModule);
            container.AddAbsorbingModule(absorbingModule2);
            container.AddAbsorbingModule(absorbingModule3);

            Assert.AreEqual(2, container.ModulesByInput.Count);
            Assert.AreEqual(10, container.TotalHeatAbsorbing);
            Assert.That(container.ModulesByInput.First() == absorbingModule2);
        }

        [Test]
        public void RemoveOldestModule_Remove_Element_From_EnergyModule()
        {

            ModuleContainer container = new ModuleContainer(2);

            IAbsorbingModule absorbingModule = new HeatProcessor(1, 10);
            CryogenRod absorbingModule2 = new CryogenRod(2, 5);
            IAbsorbingModule absorbingModule3 = new HeatProcessor(3, 5);

            container.AddEnergyModule(absorbingModule2);
            container.AddAbsorbingModule(absorbingModule);
            container.AddAbsorbingModule(absorbingModule3);

            Assert.AreEqual(0, container.TotalEnergyOutput);
        }

        [Test]
        public void RemoveOldestModule_Remove_Element_From_AbsorbingModule()
        {
            ModuleContainer container = new ModuleContainer(2);

            IAbsorbingModule absorbingModule = new HeatProcessor(2, 10);
            CryogenRod absorbingModule2 = new CryogenRod(1, 5);
            IAbsorbingModule absorbingModule3 = new HeatProcessor(3, 5);

            container.AddAbsorbingModule(absorbingModule);
            container.AddEnergyModule(absorbingModule2);
            container.AddAbsorbingModule(absorbingModule3);

            Assert.AreEqual(5, container.TotalHeatAbsorbing);
        }
    }
}