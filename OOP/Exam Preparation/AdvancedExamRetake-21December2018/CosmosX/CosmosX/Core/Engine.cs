﻿using CosmosX.Core.Contracts;
using CosmosX.IO.Contracts;
using System;

namespace CosmosX.Core
{
    public class Engine : IEngine
    {
        private IReader reader;
        private IWriter writer;
        private ICommandParser commandParser;
        private bool isRunning;

        public Engine(IReader reader, IWriter writer, ICommandParser commandParser)
        {
            this.reader = reader;
            this.writer = writer;
            this.commandParser = commandParser;
            this.isRunning = false;
        }

        public void Run()
        {
            //It's not really necessary to implement this method

            string command;
            do
            {
                command = reader.ReadLine();
                string[] commandArgs = command.Split();
                string result = string.Empty;
                try
                {
                    result = commandParser.Parse(commandArgs);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                writer.WriteLine(result);
            } 
            while (command != "Exit");
        }
    }
}