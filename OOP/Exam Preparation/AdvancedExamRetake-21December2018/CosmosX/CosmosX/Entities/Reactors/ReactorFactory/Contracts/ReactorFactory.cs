﻿using CosmosX.Entities.Containers.Contracts;
using CosmosX.Entities.Reactors.Contracts;
using CosmosX.Entities.Reactors.ReactorFactory.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CosmosX.Entities.Reactors.ReactorFactory
{
    public class ReactorFactory : IReactorFactory
    {
        public IReactor CreateReactor(string reactorTypeName, int id, IContainer moduleContainer, int additionalParameter)
        {
            Assembly assembly = Assembly.GetCallingAssembly();
            Type[] allType = assembly.GetTypes();
            Type type = allType.First(x => x.Name == reactorTypeName + "Reactor");

            object[] parameters = new object[] { id, moduleContainer, additionalParameter };
            var act = Activator.CreateInstance(type, parameters);

            return (IReactor)act;
        }
    }
}
