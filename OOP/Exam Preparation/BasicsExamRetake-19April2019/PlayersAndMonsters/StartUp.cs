﻿namespace PlayersAndMonsters
{
    using Core;
    using Core.Contracts;
    using Core.Factories;
    using Core.Factories.Contracts;
    using Repositories;
    using Repositories.Contracts;
    using IO;
    using IO.Contracts;
    using Models.BattleFields;
    using Models.BattleFields.Contracts;

    public class StartUp
    {
        private static IPlayerRepository playerRepository = new PlayerRepository();
        private static ICardRepository cardRepository = new CardRepository();
        private static IPlayerFactory playerFactory = new PlayerFactory();
        private static ICardFactory cardFactory = new CardFactory();
        private static IReader reader = new Reader();
        private static IWriter writer = new Writer();
        private static CommandInterpreter commandInterpreter = new CommandInterpreter();
        private static IBattleField battleField = new BattleField();
        
        private static IManagerController managerController = new ManagerController(playerRepository, playerFactory, cardFactory, cardRepository, battleField);

        public static void Main(string[] args)
        {
            IEngine engine = new Engine(reader, writer, commandInterpreter, managerController);
            engine.Run();
        }
    }
}