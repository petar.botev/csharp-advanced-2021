﻿using PlayersAndMonsters.Models.BattleFields.Contracts;
using PlayersAndMonsters.Models.Players.Contracts;
using System;
using System.Linq;

namespace PlayersAndMonsters.Models.BattleFields
{
    public class BattleField : IBattleField
    {
        public void Fight(IPlayer attackPlayer, IPlayer enemyPlayer)
        {
            if (attackPlayer.IsDead == true || enemyPlayer.IsDead == true)
            {
                throw new ArgumentException("Player is dead!");
            }

            if (attackPlayer.GetType().Name == "Beginner")
            {
                attackPlayer.Health += 40;
                foreach (var card in attackPlayer.CardRepository.Cards)
                {
                    card.DamagePoints += 30;
                }
            }

            if (enemyPlayer.GetType().Name == "Beginner")
            {
                enemyPlayer.Health += 40;
                foreach (var card in enemyPlayer.CardRepository.Cards)
                {
                    card.DamagePoints += 30;
                }
            }

            AddPlayersHealthBonuses(attackPlayer, enemyPlayer);

            while (true)
            {
                var attackerHitPoints = attackPlayer.CardRepository.Cards.Sum(x => x.DamagePoints);
                enemyPlayer.TakeDamage(attackerHitPoints);

                if (enemyPlayer.IsDead)
                {
                    break;
                }

                var receiverHitPoints = enemyPlayer.CardRepository.Cards.Sum(x => x.DamagePoints);

                attackPlayer.TakeDamage(receiverHitPoints);

                if (attackPlayer.IsDead)
                {
                    break;
                }
            }
        }

        private void AddPlayersHealthBonuses(IPlayer attackPlayer, IPlayer enemyPlayer)
        {
            attackPlayer.Health += attackPlayer.CardRepository.Cards.Sum(x => x.HealthPoints);
            enemyPlayer.Health += enemyPlayer.CardRepository.Cards.Sum(x => x.HealthPoints);
        } 
    }
}
