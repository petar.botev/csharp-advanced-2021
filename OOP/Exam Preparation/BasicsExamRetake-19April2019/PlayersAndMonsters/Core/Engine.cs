﻿using PlayersAndMonsters.Core.Contracts;
using PlayersAndMonsters.IO.Contracts;
using System;

namespace PlayersAndMonsters.Core
{
    public class Engine : IEngine
    {
        private readonly IReader reader;
        private readonly IWriter writer;
        private readonly CommandInterpreter commandInterpreter;
        private readonly IManagerController controller;

        public Engine(IReader reader, IWriter writer, CommandInterpreter commandInterpreter, IManagerController controller)
        {
            this.reader = reader;
            this.writer = writer;
            this.controller = controller;
            this.commandInterpreter = commandInterpreter;
        }

        public void Run()
        {
            while (true)
            {
                string[] data = this.reader.ReadLine().Split();
                string msg;

                try
                {
                    msg = this.commandInterpreter.ExecuteCommand(data, this.controller);
                }
                catch (ArgumentException e)
                {
                    msg = e.Message;
                }
                catch (InvalidOperationException e)
                {
                    msg = e.Message;
                }

                this.writer.WriteLine(msg);
            }
        }
    }
}
