﻿namespace PlayersAndMonsters.Core
{
    using System;
    using System.Linq;
    using System.Text;
    using Contracts;
    using PlayersAndMonsters.Common;
    using PlayersAndMonsters.Core.Factories.Contracts;
    using PlayersAndMonsters.Models.BattleFields;
    using PlayersAndMonsters.Models.BattleFields.Contracts;
    using PlayersAndMonsters.Repositories.Contracts;

    public class ManagerController : IManagerController
    {
        private IPlayerRepository playerRepository;
        private ICardRepository cardRepository;
        private IPlayerFactory playerFactory;
        private ICardFactory cardFactory;
        private IBattleField battleField;

        public ManagerController(IPlayerRepository playerRepository, IPlayerFactory playerFactory, ICardFactory cardFactory, ICardRepository cardRepository, IBattleField battleField)
        {
            this.playerRepository = playerRepository;
            this.playerFactory = playerFactory;
            this.cardFactory = cardFactory;
            this.cardRepository = cardRepository;
            this.battleField = battleField;
        }

        public string AddPlayer(string type, string username)
        {
            var player = playerFactory.CreatePlayer(type, username);

            playerRepository.Add(player);

            return string.Format(ConstantMessages.SuccessfullyAddedPlayer, type, username);
        }

        public string AddCard(string type, string name)
        {
            var card = cardFactory.CreateCard(type, name);

            cardRepository.Add(card);

            return string.Format(ConstantMessages.SuccessfullyAddedCard, type, name);
        }

        public string AddPlayerCard(string username, string cardName)
        {
            var card = cardRepository.Find(cardName);
            var player = playerRepository.Find(username);

            player.CardRepository.Add(card);
            
            return string.Format(ConstantMessages.SuccessfullyAddedPlayerWithCards, cardName, username);
        }

        public string Fight(string attackUser, string enemyUser)
        {
            var attacker = playerRepository.Find(attackUser);
            var enemy = playerRepository.Find(enemyUser);

            //IBattleField battleField = new BattleField();
            battleField.Fight(attacker, enemy);

            return string.Format(ConstantMessages.FightInfo, attacker.Health, enemy.Health);
        }

        public string Report()
        {
            StringBuilder strb = new StringBuilder();

            foreach (var user in playerRepository.Players)
            {
                strb.AppendLine($"Username: {user.Username} - Health: {user.Health} – Cards {user.CardRepository.Count}");
                foreach (var card in user.CardRepository.Cards)
                {
                    strb.AppendLine($"Card: {card.Name} - Damage: {card.DamagePoints}");
                }
                strb.AppendLine("###");
            }

            return strb.ToString().Trim();
        }
    }
}
