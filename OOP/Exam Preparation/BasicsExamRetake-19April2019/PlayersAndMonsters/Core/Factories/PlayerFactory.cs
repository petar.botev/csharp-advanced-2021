﻿using PlayersAndMonsters.Core.Factories.Contracts;
using PlayersAndMonsters.Models.Players;
using PlayersAndMonsters.Models.Players.Contracts;
using PlayersAndMonsters.Repositories;
using PlayersAndMonsters.Repositories.Contracts;

namespace PlayersAndMonsters.Core.Factories
{
    public class PlayerFactory : IPlayerFactory
    {

        public IPlayer CreatePlayer(string type, string username)
        {
            IPlayer player;

            if (type == "Advanced")
            {
                player = new Advanced(new CardRepository(), username);
            }
            else
            {
                player = new Beginner(new CardRepository(), username);
            }

            return player;
        }
    }
}
