﻿using PlayersAndMonsters.Core.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace PlayersAndMonsters.Core
{
    public class CommandInterpreter
    {
        
        public string ExecuteCommand(string[] data, IManagerController controller)
        {
            string result = string.Empty;
            switch (data[0])
            {
                case "AddPlayer":
                    string playerType = data[1];
                    string playerName = data[2];

                    result = controller.AddPlayer(playerType, playerName);
                    break;
                case "AddCard":
                    string cardType = data[1];
                    string cardrName = data[2];

                    result = controller.AddCard(cardType, cardrName);
                    break;
                case "AddPlayerCard":
                    string username = data[1];
                    string cardName = data[2];

                    result = controller.AddPlayerCard(username, cardName);
                    break;
                case "Fight":
                    string attackUser = data[1];
                    string enemyUser = data[2];

                    result = controller.Fight(attackUser, enemyUser);
                    break;
                case "Report":
                    result = controller.Report();
                    break;
                default:
                    break;
            }

            return result;
        }
    }
}
