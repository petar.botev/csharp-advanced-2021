﻿using HAD.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace HAD.Entities.Items
{
    public class RecipeItem : BaseItem, IRecipe
    {
        private readonly List<string> requiredItems; 

        public RecipeItem(string name, long strengthBonus, long agilityBonus, long intelligenceBonus, long hitPointsBonus, long damageBonus) 
            : base(name, strengthBonus, agilityBonus, intelligenceBonus, hitPointsBonus, damageBonus)
        {
            this.requiredItems = new List<string>();
        }

        public IReadOnlyList<string> RequiredItems => requiredItems;

        
    }
}
