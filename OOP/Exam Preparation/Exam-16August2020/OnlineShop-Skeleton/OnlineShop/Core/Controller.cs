﻿using OnlineShop.Common.Constants;
using OnlineShop.Models.Products.Components;
using OnlineShop.Models.Products.Computers;
using OnlineShop.Models.Products.Peripherals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineShop.Core
{
    public class Controller : IController
    {
        private List<IComputer> computers;
        //private List<IComponent> components;
        //private List<IPeripheral> peripherals;

        public Controller()
        {
            this.computers = new List<IComputer>();
            //this.components = new List<IComponent>();
            //this.peripherals = new List<IPeripheral>();
        }

        public string AddComponent(int computerId, int id, string componentType, string manufacturer, string model, decimal price, double overallPerformance, int generation)
        {
            var computer = computers.FirstOrDefault(x => x.Id == computerId);

            if (computer == null)
            {
                throw new ArgumentException(ExceptionMessages.NotExistingComputerId);
            }

            if (computer.Components.Any(x => x.Id == id))
            {
                throw new ArgumentException(ExceptionMessages.ExistingComponentId);
            }

            string @namespace = "OnlineShop.Models.Products.Components";
            var componentTypes = AppDomain.CurrentDomain.GetAssemblies()
                       .SelectMany(t => t.GetTypes())
                       .Where(t => t.IsClass && t.Namespace == @namespace);

            if (!componentTypes.Any(x => x.Name == componentType))
            {
                throw new ArgumentException(ExceptionMessages.InvalidComponentType);
            }

            var singleType = componentTypes.FirstOrDefault(x => x.Name == componentType);

            if (id <= 0)
            {
                throw new ArgumentException(ExceptionMessages.InvalidProductId);
            }

            if (string.IsNullOrWhiteSpace(manufacturer))
            {
                throw new ArgumentException(ExceptionMessages.InvalidManufacturer);
            }

            if (string.IsNullOrWhiteSpace(model))
            {
                throw new ArgumentException(ExceptionMessages.InvalidModel);
            }

            if (price <= 0)
            {
                throw new ArgumentException(ExceptionMessages.InvalidPrice);
            }

            if (overallPerformance <= 0)
            {
                throw new ArgumentException(ExceptionMessages.InvalidOverallPerformance);
            }

            var componentInstance = (IComponent)Activator.CreateInstance(singleType, id, manufacturer, model, price, overallPerformance, generation);

            computer.AddComponent(componentInstance);

            return string.Format(SuccessMessages.AddedComponent, componentType, id, computerId);
        }

        public string AddComputer(string computerType, int id, string manufacturer, string model, decimal price)
        {
            if (computers.Any(x => x.Id == id))
            {
                throw new ArgumentException(ExceptionMessages.ExistingComputerId);
            }

            if (computerType != "DesktopComputer" && computerType != "Laptop")
            {
                throw new ArgumentException(ExceptionMessages.InvalidComputerType);
            }

            IComputer computer;
            if (computerType == "DesktopComputer")
            {
                computer = new DesktopComputer(id, manufacturer, model, price);
            }
            else
            {
                computer = new Laptop(id, manufacturer, model, price);
            }
             
            computers.Add(computer);

            return string.Format(SuccessMessages.AddedComputer, id);
        }

        public string AddPeripheral(int computerId, int id, string peripheralType, string manufacturer, string model, decimal price, double overallPerformance, string connectionType)
        {
            var computer = computers.FirstOrDefault(x => x.Id == computerId);

            if (computer == null)
            {
                throw new ArgumentException(ExceptionMessages.NotExistingComputerId);
            }

            if (computer.Peripherals.Any(x => x.GetType().Name == peripheralType))
            {
                throw new ArgumentException(ExceptionMessages.ExistingPeripheralId);
            }

            string @namespace = "OnlineShop.Models.Products.Peripherals";
            var peripheralTypes = AppDomain.CurrentDomain.GetAssemblies()
                       .SelectMany(t => t.GetTypes())
                       .Where(t => t.IsClass && t.Namespace == @namespace);

            if (!peripheralTypes.Any(x => x.Name == peripheralType))
            {
                throw new ArgumentException(ExceptionMessages.InvalidPeripheralType);
            }

            var singleType = peripheralTypes.FirstOrDefault(x => x.Name == peripheralType);

            if (id <= 0)
            {
                throw new ArgumentException(ExceptionMessages.InvalidProductId);
            }

            if (string.IsNullOrWhiteSpace(manufacturer))
            {
                throw new ArgumentException(ExceptionMessages.InvalidManufacturer);
            }

            if (string.IsNullOrWhiteSpace(model))
            {
                throw new ArgumentException(ExceptionMessages.InvalidModel);
            }

            if (price <= 0)
            {
                throw new ArgumentException(ExceptionMessages.InvalidPrice);
            }

            if (overallPerformance <= 0)
            {
                throw new ArgumentException(ExceptionMessages.InvalidOverallPerformance);
            }

            var peripheralInstance = (IPeripheral)Activator.CreateInstance(singleType, id, manufacturer, model, price, overallPerformance, connectionType);

            computer.AddPeripheral(peripheralInstance);

            return string.Format(SuccessMessages.AddedPeripheral, peripheralType, id, computerId);
        }

        public string BuyBest(decimal budget)
        {
            if (computers.Count == 0 || computers.Any(x => x.Price > budget))
            {
                throw new ArgumentException(string.Format(ExceptionMessages.CanNotBuyComputer, budget));
            }

            var computerToRemove = computers.OrderByDescending(x => x.OverallPerformance).FirstOrDefault(x => x.Price <= budget);
            computers.Remove(computerToRemove);

            return computerToRemove.ToString();
        }

        public string BuyComputer(int id)
        {
            var computer = computers.FirstOrDefault(x => x.Id == id);

            if (computer == null)
            {
                throw new ArgumentException(ExceptionMessages.NotExistingComputerId);
            }
            computers.Remove(computer);

            return computer.ToString();
        }

        public string GetComputerData(int id)
        {
            var computer = computers.FirstOrDefault(x => x.Id == id);

            if (computer == null)
            {
                throw new ArgumentException(ExceptionMessages.NotExistingComputerId);
            }

            return computer.ToString();
        }

        public string RemoveComponent(string componentType, int computerId)
        {
            var computer = computers.FirstOrDefault(x => x.Id == computerId);

            if (computer == null)
            {
                throw new ArgumentException(ExceptionMessages.NotExistingComputerId);
            }

            var component = computer.Components.FirstOrDefault(x => x.GetType().Name == componentType);

            computer.RemoveComponent(componentType);

            // component.id or computer.id!!!
            return string.Format(SuccessMessages.RemovedComponent, componentType, component.Id);
        }

        public string RemovePeripheral(string peripheralType, int computerId)
        {
            var computer = computers.FirstOrDefault(x => x.Id == computerId);

            if (computer == null)
            {
                throw new ArgumentException(ExceptionMessages.NotExistingComputerId);
            }

            var peripheral = computer.Peripherals.FirstOrDefault(x => x.GetType().Name == peripheralType);
           

            computer.RemovePeripheral(peripheralType);

            return string.Format(SuccessMessages.RemovedPeripheral, peripheralType, peripheral.Id);
        }
    }
}
