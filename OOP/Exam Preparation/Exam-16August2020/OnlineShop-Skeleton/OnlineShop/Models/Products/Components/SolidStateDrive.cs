﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineShop.Models.Products.Components
{
    public class SolidStateDrive : Component
    {
        private double overallPerformance;

        public SolidStateDrive(int id, string manufacturer, string model, decimal price, double overallPerformance, int generation) 
            : base(id, manufacturer, model, price, overallPerformance, generation)
        {
            this.OverallPerformance = overallPerformance;
        }

        public override double OverallPerformance
        {
            get
            {
                return this.overallPerformance;
            }
            protected set
            {
                // is it checked in base property
                this.overallPerformance = value * 1.20;
            }
        }
    }
}
