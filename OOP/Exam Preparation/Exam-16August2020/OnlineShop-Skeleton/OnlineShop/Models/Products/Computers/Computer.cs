﻿using OnlineShop.Common.Constants;
using OnlineShop.Models.Products.Components;
using OnlineShop.Models.Products.Peripherals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OnlineShop.Models.Products.Computers
{
    public abstract class Computer : Product, IComputer
    {
        private List<IComponent> components;
        private readonly List<IPeripheral> peripherals;

        private double overallPerformance;
        private decimal price;

        private double peripheralPerformanceSum = 0;
        private decimal componentPriceSum = 0;

        public Computer(int id, string manufacturer, string model, decimal price, double overallPerformance)
            : base(id, manufacturer, model, price, overallPerformance)
        {
            this.components = new List<IComponent>();
            this.peripherals = new List<IPeripheral>();

            this.OverallPerformance = overallPerformance;
            this.Price = price;
        }

        public override double OverallPerformance
        {
            get
            {
                return this.overallPerformance;
            }

            protected set
            {
                if (value <= 0)
                {
                    throw new ArgumentException(ExceptionMessages.InvalidOverallPerformance);
                }
                this.overallPerformance = value;

            }
        }

        public override decimal Price
        {
            get
            {
                return this.price;
            }
            protected set
            {
                if (value <= 0)
                {
                    throw new ArgumentException(ExceptionMessages.InvalidPrice);
                }

                this.price = value;
            }
        }

        public IReadOnlyCollection<IComponent> Components => components;

        public IReadOnlyCollection<IPeripheral> Peripherals => peripherals;


        public void AddComponent(IComponent component)
        {
            if (components.Any(x => x.GetType().Name == nameof(component)))
            {
                throw new ArgumentException(string.Format(ExceptionMessages.ExistingComponent, component.GetType().Name, this.GetType().Name, this.Id));
            }

            components.Add(component);
            this.OverallPerformance += component.OverallPerformance;
            this.Price += component.Price;
        }

        public void AddPeripheral(IPeripheral peripheral)
        {
            if (peripherals.Any(x => x.GetType().Name == nameof(peripheral)))
            {
                throw new ArgumentException(string.Format(ExceptionMessages.ExistingPeripheral, peripheral.GetType().Name, this.GetType().Name, this.Id));
            }

            peripherals.Add(peripheral);
            this.Price += peripheral.Price;
            peripheralPerformanceSum += peripheral.OverallPerformance;
        }

        public IComponent RemoveComponent(string componentType)
        {
            var component = components.FirstOrDefault(x => x.GetType().Name == componentType);
            if (component == null)
            {
                throw new ArgumentException(string.Format(ExceptionMessages.NotExistingComponent, componentType, this.GetType().Name, this.Id));
            }

            this.Price -= component.Price;
            this.OverallPerformance -= component.OverallPerformance;
            components.Remove(component);

            return component;
        }

        public IPeripheral RemovePeripheral(string peripheralType)
        {
            var peripheral = peripherals.FirstOrDefault(x => x.GetType().Name == peripheralType);
            if (peripheral == null)
            {
                throw new ArgumentException(string.Format(ExceptionMessages.NotExistingPeripheral, peripheralType, this.GetType().Name, this.Id));
            }

            this.Price -= peripheral.Price;
            this.OverallPerformance -= peripheral.OverallPerformance;
            peripherals.Remove(peripheral);

            return peripheral;
        }

        public override string ToString()
        {
            StringBuilder strb = new StringBuilder();

            strb.AppendLine($"Overall Performance: {this.OverallPerformance / components.Count:f2}. Price: {this.Price:f2} - {this.GetType().Name}: {this.Manufacturer} {this.Model} (Id: {this.Id})");
            strb.AppendLine($" Components ({this.components.Count}):");

            foreach (var component in this.components)
            {
                strb.AppendLine(component.ToString());
            }

            if (peripherals.Count == 0)
            {
                strb.AppendLine($" Peripherals ({this.peripherals.Count}); Average Overall Performance ({0:f2}):");
            }
            else
            {
                strb.AppendLine($" Peripherals ({this.peripherals.Count}); Average Overall Performance ({peripheralPerformanceSum / (this.peripherals.Count):f2}):");
            }

            foreach (var peripheral in this.peripherals)
            {
                strb.AppendLine(peripheral.ToString());
            }

            return strb.ToString().Trim();
        }
    }
}
