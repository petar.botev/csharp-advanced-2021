﻿using OnlineShop.Common.Constants;
using OnlineShop.Models.Products.Components;
using OnlineShop.Models.Products.Peripherals;
using System;
using System.Collections.Generic;

namespace OnlineShop.Models.Products.Computers
{
    public class Laptop : Computer
    {
        private double overallPerformance;

        public Laptop(int id, string manufacturer, string model, decimal price) : base(id, manufacturer, model, price, 10)
        {
            this.OverallPerformance = 10;
        }

        public override double OverallPerformance
        {
            get
            {
                return this.overallPerformance;
            }

            protected set
            {
                if (value <= 0)
                {
                    throw new ArgumentException(ExceptionMessages.InvalidOverallPerformance);
                }
                this.overallPerformance = value;

            }
        }
    }
}
