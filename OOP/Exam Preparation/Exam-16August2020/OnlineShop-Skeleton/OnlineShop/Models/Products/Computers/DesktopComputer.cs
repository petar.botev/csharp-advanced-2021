﻿using OnlineShop.Common.Constants;
using System;

namespace OnlineShop.Models.Products.Computers
{
    public class DesktopComputer : Computer
    {
        private double overallPerformance;

        public DesktopComputer(int id, string manufacturer, string model, decimal price) : base(id, manufacturer, model, price, 15)
        {
            this.OverallPerformance = 15;
        }

        public override double OverallPerformance
        {
            get
            {
                return this.overallPerformance;
            }

            protected set
            {
                if (value <= 0)
                {
                    throw new ArgumentException(ExceptionMessages.InvalidOverallPerformance);
                }
                this.overallPerformance = value;

            }
        }
    }
}
