using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Computers.Tests
{
    public class Tests
    {
        private ComputerManager manager;
        [SetUp]
        public void Setup()
        {
            manager = new ComputerManager();
        }

        ////Computer Tests
        //[Test]
        //public void CheckIfComputerConstructorWorksCorrectly()
        //{
        //    Assert.AreEqual(this.defaultPrice, this.defaultComputer.Price);
        //    Assert.AreEqual(this.defaultManufacturer, this.defaultComputer.Manufacturer);
        //    Assert.AreEqual(this.defaultModel, this.defaultComputer.Model);
        //}

        ////ComputerManager Tests

        //[Test]
        //public void CheckIfConstructorWorksCorrectly()
        //{
        //    Assert.That(this.computerManager.Count, Is.EqualTo(0));
        //    Assert.That(this.computerManager.Computers, Is.Empty);
        //}
        //[Test]
        //public void CheckIfCountWorksCorrectly()
        //{
        //    this.computerManager.AddComputer(this.defaultComputer);
        //    Assert.That(this.computerManager.Count, Is.EqualTo(1));
        //    Assert.That(this.computerManager.Computers, Has.Member(this.defaultComputer));
        //}

        //[Test]
        //public void AddShouldThrowExceptionWithExistingComputer()
        //{
        //    this.computerManager.AddComputer(this.defaultComputer);
        //    Assert.Throws<ArgumentException>(() =>
        //    {
        //        this.computerManager.AddComputer(this.defaultComputer);
        //    }, "This computer already exists.");
        //}

        //[Test]
        //public void AddShouldIncreaseCountWhenSuccessful()
        //{
        //    this.computerManager.AddComputer(this.defaultComputer);

        //    Assert.That(this.computerManager.Count, Is.EqualTo(1));
        //    Assert.That(this.computerManager.Computers, Has.Member(this.defaultComputer));
        //}

        //[Test]
        //public void AddShouldThrowExceptionWithNullValue()
        //{
        //    Assert.Throws<ArgumentNullException>(() =>
        //    {
        //        this.computerManager.AddComputer(null);
        //    }, "Can not be null.");
        //}

        //[Test]
        //public void RemoveComputerShouldRemoveCorrectComputer()
        //{
        //    this.computerManager.AddComputer(this.defaultComputer);
        //    var result = this.computerManager.RemoveComputer(this.defaultManufacturer, this.defaultModel);

        //    Assert.AreSame(this.defaultComputer, result);
        //    Assert.That(this.computerManager.Count, Is.EqualTo(0));
        //}

        //[Test]
        //public void RemoveShouldThrowExceptionWithInvalidComputer()
        //{
        //    Assert.Throws<ArgumentException>(() =>
        //    {
        //        this.computerManager.RemoveComputer(this.defaultManufacturer, "Acer");
        //    }, "There is no computer with this manufacturer and model.");
        //}

        //[Test]
        //public void GetComputerShouldWorkCorrectly()
        //{
        //    this.computerManager.AddComputer(this.defaultComputer);
        //    var result = this.computerManager.GetComputer(this.defaultManufacturer, this.defaultModel);

        //    Assert.AreSame(this.defaultComputer, result);
        //}

        //[Test]
        //public void GetComputerShouldThrowExceptionWithInvalidData()
        //{
        //    this.computerManager.AddComputer(this.defaultComputer);
        //    Assert.Throws<ArgumentException>(() =>
        //    {
        //        this.computerManager.GetComputer(
        //            "HP", "XC300");
        //    }, "There is no computer with this manufacturer and model.");
        //}

        //[Test]
        //public void GetComputerShouldThrowExceptionWithNullManufacturer()
        //{
        //    Assert.Throws<ArgumentNullException>(() => { this.computerManager.GetComputer(null, this.defaultModel); });
        //}

        //[Test]
        //public void GetComputerShouldThrowExceptionWithNullModel()
        //{
        //    Assert.Throws<ArgumentNullException>(() =>
        //    {
        //        this.computerManager.GetComputer(this.defaultManufacturer, null);
        //    });
        //}

        //[Test]
        //public void GetByManufacturerShouldThrowExceptionWithNullManufacturer()
        //{
        //    Assert.Throws<ArgumentNullException>(() =>
        //    {
        //        this.computerManager.GetComputersByManufacturer(null);
        //    });
        //}

        //[Test]
        //public void GetAllByManufacturerShouldReturnCorrectCollection()
        //{
        //    this.computerManager.AddComputer(this.defaultComputer);
        //    this.computerManager.AddComputer(new Computer(this.defaultManufacturer, "K210", 899.99m));
        //    this.computerManager.AddComputer(new Computer(this.defaultManufacturer, "P34", 420));
        //    var collection = this.computerManager.GetComputersByManufacturer(this.defaultManufacturer);

        //    Assert.That(collection.Count, Is.EqualTo(3));

        //}

        //[Test]
        //public void GetAllByManufacturerShouldReturnZeroWithNoMatches()
        //{
        //    var collection = this.computerManager.GetComputersByManufacturer("Mac");

        //    Assert.That(collection.Count, Is.EqualTo(0));
        //}


        [Test]
        public void ConstructorSetsManagerCorrectly()
        {

            Assert.That(manager.Count, Is.EqualTo(0));
            Assert.That(manager.Computers, Is.Empty);
        }

        
        [Test]
        public void CheckIfCountWorksCorrectly()
        {
            Computer computer = new Computer("MS", "IBM", 2000m);
            this.manager.AddComputer(computer);
            Assert.That(manager.Count, Is.EqualTo(1));
            Assert.That(manager.Computers, Has.Member(computer));
        }

        [Test]
        public void AddComputerThrowsExceptionIfManufactorerAndModelExist()
        {
            Computer computer = new Computer("MS", "IBM", 2000m);
            manager.AddComputer(computer);

            Assert.Throws<ArgumentException>(() => manager.AddComputer(computer), "This computer already exists.");
        }

        [Test]
        public void AddComputerIfComputerPropertiesSetCorrectly()
        {
            Computer computer = new Computer("MS", "IBM", 2000m);
            
            Assert.AreEqual("MS", computer.Manufacturer);
            Assert.AreEqual("IBM", computer.Model);
            Assert.AreEqual(2000, computer.Price);
        }

        [Test]
        public void AddComputerAddCorrectly()
        {
            Computer computer = new Computer("MS", "IBM", 2000m);

            
            manager.AddComputer(computer);

            Assert.AreEqual(1, manager.Computers.Count);
            Assert.That(manager.Computers, Has.Member(computer));
        }

        [Test]
        public void AddComputerThrowsExceptionIfManufacturerIsNull()
        {
            Assert.Throws<ArgumentNullException>(() => manager.AddComputer(null), "Can not be null!");
        }

        [Test]
        public void RemoveComputerRemoveComputerFromCollection()
        {
            Computer computer = new Computer("MS", "IBM", 2000m);
            manager.AddComputer(computer);

            var result = manager.RemoveComputer("MS", "IBM");

            Assert.AreEqual(0, manager.Computers.Count);
        }

        
        [Test]
        public void RemoveComputerReturnComputer()
        {
            Computer computer = new Computer("MS", "IBM", 2000m);
            manager.AddComputer(computer);

            var result = manager.RemoveComputer("MS", "IBM");

            Assert.AreEqual(computer, result);
        }

        ////NEW
        //[Test]
        //public void RemoveShouldThrowExceptionWithInvalidComputer()
        //{
        //    Assert.Throws<ArgumentException>(() =>
        //    {
        //        this.manager.RemoveComputer("MS", "Acer");
        //    }, "There is no computer with this manufacturer and model.");
        //}

        [Test]
        public void GetComputerReturnsCorrectValue()
        {
            Computer computer = new Computer("MS", "IBM", 2000m);
            manager.AddComputer(computer);

            Assert.IsNotNull(manager.GetComputer("MS", "IBM"));
        }

        [Test]
        public void GetComputerThrowsExceptionIfComputerIsNull()
        {
            Computer computer = new Computer("MS", "IBM", 2000m);
            manager.AddComputer(computer);

            Assert.Throws<ArgumentException>(() => manager.GetComputer("Mr", "IB"), "There is no computer with this manufacturer and model.");
        }

        [Test]
        public void GetComputerThrowsExceptionIfManufacturerIsNull()
        {
            Computer computer = new Computer(null, "IBM", 2000m);
            manager.AddComputer(computer);

            Assert.Throws<ArgumentNullException>(() => manager.GetComputer(null, "IBM"));
        }

        [Test]
        public void GetComputerThrowsExceptionIfModelIsNull()
        {
            Computer computer = new Computer("MS", "as", 2000m);
            manager.AddComputer(computer);

            Assert.Throws<ArgumentNullException>(() => manager.GetComputer("MS", null));
        }

        [Test]
        public void GetComputerReturnsComputer()
        {
            Computer computer = new Computer("MS", "IBM", 2000m);
            manager.AddComputer(computer);

            var result = manager.GetComputer("MS", "IBM");
            
            Assert.AreSame(result, computer);
        }

        [Test]
        public void GetComputersByManufacturerReturnsCorrectValue()
        {
            Computer computer = new Computer("MS", "IBM", 2000m);
            Computer computer2 = new Computer("MS", "rand", 2000m);
            manager.AddComputer(computer);
            manager.AddComputer(computer2);

            var result = manager.GetComputersByManufacturer("MS");

            Assert.That(result.All(x => x.Manufacturer == "MS"));
            
        }

        [Test]
        public void GetComputersByManufacturerReturnsSpecificCount()
        {
            Computer computer = new Computer("Mr", "IBM", 2000m);
            Computer computer2 = new Computer("MS", "rand", 2000m);
            manager.AddComputer(computer);
            manager.AddComputer(computer2);

            var result = manager.GetComputersByManufacturer("MS");

            Assert.AreEqual(1, result.Count());

        }

        
        [Test]
        public void GetComputersByManufacturerReturnsExactCount()
        {
            Computer computer = new Computer("MS", "IBM", 2000m);
            Computer computer2 = new Computer("MS", "rand", 2000m);
            manager.AddComputer(computer);
            manager.AddComputer(computer2);

            var result = manager.GetComputersByManufacturer("MS");

            Assert.AreEqual(2, result.Count());

        }

        [Test]
        public void GetComputersByManufacturerThrowsExceptionIfManufacturerIsNull()
        {
            Computer computer = new Computer(null, "IBM", 2000m);
            manager.AddComputer(computer);


            Assert.Throws<ArgumentNullException>(() => manager.GetComputersByManufacturer(null));
        }

        [Test]
        public void GetComputersByManufacturerReturnZeroIfNoManufacturer()
        {
            Computer computer = new Computer(null, "IBM", 2000m);
            manager.AddComputer(computer);
            var result = manager.GetComputersByManufacturer("nqkoj");

            Assert.AreEqual(0, result.Count());
        }

        [Test]
        [TestCase(null, "IBM")]
        [TestCase("MS", null)]
        public void ValidateNullValueThrowsExceptionIfValueIsNull(string manufacturer, string model)
        {
            Assert.Throws<ArgumentNullException>(() => manager.GetComputer(manufacturer, model), "Can not be null!");
        }

        [Test]
        public void CountSetCorrectly()
        {
            Computer computer = new Computer("MS", "IBM", 2000m);
            Computer computer2 = new Computer("MS", "rand", 2000m);
            manager.AddComputer(computer);
            manager.AddComputer(computer2);

            Assert.AreEqual(2, manager.Count);
        }
    }
}