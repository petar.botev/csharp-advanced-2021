﻿using EasterRaces.Core.Contracts;
using EasterRaces.Models.Cars.Contracts;
using EasterRaces.Models.Cars.Entities;
using EasterRaces.Models.Drivers.Contracts;
using EasterRaces.Models.Drivers.Entities;
using EasterRaces.Models.Races.Contracts;
using EasterRaces.Models.Races.Entities;
using EasterRaces.Repositories.Contracts;
using EasterRaces.Repositories.Entities;
using EasterRaces.Utilities.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EasterRaces.Core.Entities
{
    public class ChampionshipController : IChampionshipController
    {
        private readonly IRepository<IRace> raceRepo;
        private readonly IRepository<IDriver> driverRepo;
        private readonly IRepository<ICar> carRepo;

        public ChampionshipController(IRepository<ICar> carRepo, IRepository<IDriver> driverRepo, IRepository<IRace> raceRepo)
        {
            this.raceRepo = raceRepo;
            this.driverRepo = driverRepo;
            this.carRepo = carRepo;
        }

        private const int MIN_NUMBER_PARTICIPANTS = 3;

        public string AddCarToDriver(string driverName, string carModel)
        {
            ICar car = carRepo.GetByName(carModel);
            IDriver driver = driverRepo.GetByName(driverName);

            if (driver == null)
            {
                throw new InvalidOperationException(string.Format(ExceptionMessages.DriverNotFound, driverName));
            }

            if (car == null)
            {
                throw new InvalidOperationException(string.Format(ExceptionMessages.CarNotFound, carModel));
            }

            driver.AddCar(car);
            return string.Format(OutputMessages.CarAdded, driverName, carModel);
        }

        public string AddDriverToRace(string raceName, string driverName)
        {
            IRace race = raceRepo.GetByName(raceName);
            IDriver driver = driverRepo.GetByName(driverName);

            if (race == null)
            {
                throw new InvalidOperationException(string.Format(ExceptionMessages.RaceNotFound, raceName));
            }
            if (driver == null)
            {
                throw new InvalidOperationException(string.Format(ExceptionMessages.DriverNotFound, driverName));
            }

            race.AddDriver(driver);
            return string.Format(OutputMessages.DriverAdded, driverName, raceName);
        }

        public string CreateCar(string type, string model, int horsePower)
        {
            ICar car;
            if (type == "Muscle")
            {
                car = new MuscleCar(model, horsePower);
            }
            else
            {
                car = new SportsCar(model, horsePower);
            }

            if (carRepo.GetAll().Any(x => x.Model == model))
            {
                throw new ArgumentException(string.Format(ExceptionMessages.CarExists, model));
            }

            carRepo.Add(car);

            return string.Format(OutputMessages.CarCreated, car.GetType().Name, model);
        }

        public string CreateDriver(string driverName)
        {
            IDriver driver = new Driver(driverName);

            if (driverRepo.GetAll().Any(x => x.Name == driverName))
            {
                throw new ArgumentException(string.Format(ExceptionMessages.DriversExists, driverName));
            }

            driverRepo.Add(driver);

            return string.Format(OutputMessages.DriverCreated, driverName);
        }

        public string CreateRace(string name, int laps)
        {
            IRace race = new Race(name, laps);

            if (raceRepo.GetAll().Any(x => x.Name == name))
            {
                throw new InvalidOperationException(string.Format(ExceptionMessages.RaceExists, name));
            }

            raceRepo.Add(race);

            return string.Format(OutputMessages.RaceCreated, name);
        }

        public string StartRace(string raceName)
        {
            IRace race = raceRepo.GetByName(raceName);

            if (race == null)
            {
                throw new InvalidOperationException(string.Format(ExceptionMessages.RaceNotFound, raceName));
            }
            if (race.Drivers.Count < MIN_NUMBER_PARTICIPANTS)
            {

                throw new InvalidOperationException(string.Format(ExceptionMessages.RaceInvalid, raceName, MIN_NUMBER_PARTICIPANTS));
            }

            List<IDriver> topThreeDrivers = race.Drivers.OrderByDescending(x => x.Car.CalculateRacePoints(race.Laps)).Take(3).ToList();

            StringBuilder strb = new StringBuilder();

            strb.AppendLine(string.Format(OutputMessages.DriverFirstPosition, topThreeDrivers[0].Name, raceName));
            strb.AppendLine(string.Format(OutputMessages.DriverSecondPosition, topThreeDrivers[1].Name, raceName));
            strb.AppendLine(string.Format(OutputMessages.DriverThirdPosition, topThreeDrivers[2].Name, raceName));

            raceRepo.Remove(race);

            return strb.ToString().Trim();
        }
    }
}
