﻿using EasterRaces.Models.Cars.Contracts;
using EasterRaces.Models.Drivers.Contracts;
using EasterRaces.Utilities.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace EasterRaces.Models.Drivers.Entities
{
    public class Driver : IDriver
    {
        private string name;
        private ICar car;
        private int numberOfWins;
        private bool canPrticipate;

        private const int NAME_MIN_SYMBOLS = 5;

        public Driver(string name)
        {
            this.Name = name;
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            private set
            {
                if (string.IsNullOrEmpty(value) || value.Length < NAME_MIN_SYMBOLS)
                {
                    throw new ArgumentException(string.Format(ExceptionMessages.InvalidName, value, NAME_MIN_SYMBOLS));
                }

                this.name = value;
            }
        }

        public ICar Car => car;

        public int NumberOfWins => this.numberOfWins;

        public bool CanParticipate
        {
            get
            {
                return this.canPrticipate;
            }
            private set
            {

                this.canPrticipate = false;
            }
        }

        public void AddCar(ICar car)
        {
            if (car == null)
            {
                throw new ArgumentNullException(ExceptionMessages.CarInvalid);
            }

            this.car = car;
            canPrticipate = true;
        }

        public void WinRace()
        {
            this.numberOfWins++;
        }
    }
}
