﻿using EasterRaces.Core.Contracts;
using EasterRaces.IO;
using EasterRaces.IO.Contracts;
using EasterRaces.Core.Entities;
using EasterRaces.Repositories.Contracts;
using EasterRaces.Models.Cars.Contracts;
using EasterRaces.Repositories.Entities;
using EasterRaces.Models.Races.Contracts;
using EasterRaces.Models.Drivers.Contracts;

namespace EasterRaces
{
    public class StartUp
    {
        public static void Main()
        {
            IRepository<ICar> carRepo = new CarRepository();
            IRepository<IDriver> driverRepo = new DriverRepository();
            IRepository<IRace> raceRepo = new RaceRepository();

            IChampionshipController controller = new ChampionshipController(carRepo, driverRepo, raceRepo);

            IReader reader = new ConsoleReader();
            IWriter writer = new ConsoleWriter();

            IEngine enigne = new Engine(controller, reader, writer);
            enigne.Run();
        }
    }
}
