﻿using EasterRaces.Models.Races.Contracts;
using EasterRaces.Models.Races.Entities;
using EasterRaces.Repositories.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace EasterRaces.Repositories.Entities
{
    public class RaceRepository : IRepository<IRace>
    {

        private List<IRace> allRaces;

        public RaceRepository()
        {
             allRaces = new List<IRace>();
        }

        public void Add(IRace race)
        {

            allRaces.Add(race);
        }

        public IReadOnlyCollection<IRace> GetAll()
        {
            return allRaces;
        }

        public IRace GetByName(string name)
        {
            var race = allRaces.FirstOrDefault(x => x.Name == name);
            return race;
        }

        public bool Remove(IRace race)
        {
           return allRaces.Remove(race);
        }
    }
}
