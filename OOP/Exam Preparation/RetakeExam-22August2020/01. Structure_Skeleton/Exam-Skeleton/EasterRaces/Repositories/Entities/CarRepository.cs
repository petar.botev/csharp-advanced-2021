﻿using EasterRaces.Models.Cars.Contracts;
using EasterRaces.Models.Cars.Entities;
using EasterRaces.Repositories.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace EasterRaces.Repositories.Entities
{
    public class CarRepository : IRepository<ICar>
    {
        private List<ICar> allCars;

        public CarRepository()
        {
            allCars = new List<ICar>();
        }

        public void Add(ICar car)
        {
            allCars.Add(car);
        }

        public IReadOnlyCollection<ICar> GetAll()
        {
            return allCars;
        }

        public ICar GetByName(string model)
        {
            var car = allCars.FirstOrDefault(x => x.Model == model);
            return car;
        }

        public bool Remove(ICar car)
        {
            return allCars.Remove(car);
        }
    }
}
