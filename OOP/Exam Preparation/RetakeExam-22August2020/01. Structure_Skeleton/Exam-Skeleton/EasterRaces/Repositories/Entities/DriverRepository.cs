﻿    using EasterRaces.Models.Drivers.Contracts;
using EasterRaces.Models.Drivers.Entities;
using EasterRaces.Repositories.Contracts;
using System.Collections.Generic;
using System.Linq;

namespace EasterRaces.Repositories.Entities
{
    public class DriverRepository : IRepository<IDriver>
    {
        private List<IDriver> allDrivers;

        public DriverRepository()
        {
            allDrivers = new List<IDriver>();
        }

        public void Add(IDriver driver)
        {
            allDrivers.Add(driver);
        }

        public IReadOnlyCollection<IDriver> GetAll()
        {
            return allDrivers;
        }

        public IDriver GetByName(string name)
        {
            var driver = allDrivers.FirstOrDefault(x => x.Name == name);
            return driver;
        }

        public bool Remove(IDriver driver)
        {
            return allDrivers.Remove(driver);
        }
    }
}
