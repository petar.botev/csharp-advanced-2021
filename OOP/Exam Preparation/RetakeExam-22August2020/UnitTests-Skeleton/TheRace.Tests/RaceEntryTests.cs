using NUnit.Framework;
using System;
using System.Collections.Generic;
using TheRace;

namespace TheRace.Tests
{
    public class RaceEntryTests
    {
        private RaceEntry race;

        [SetUp]
        public void Setup()
        {
            race = new RaceEntry();
        }

        [Test]
        public void AddDriverThrowsExceptionIfDriverIsNull()
        {
            Assert.Throws<InvalidOperationException>(() => race.AddDriver(null));
        }

        [Test]
        public void AddDriverThrowsExceptionIfDriverExists()
        {
            UnitCar car = new UnitCar("VW", 2000, 111);
            UnitDriver driver = new UnitDriver("gosho", car);
            race.AddDriver(driver);

            Assert.Throws<InvalidOperationException>(() => race.AddDriver(driver));
        }

        [Test]
        public void CalculateAverageHorsePowerThrowsExceptionIfDriverCountIsLessThanMinParticipants()
        {
            Assert.Throws<InvalidOperationException>(() => race.CalculateAverageHorsePower());
        }

        [Test]
        public void CalculateAverageHorsePowerCalculateCorrectly()
        {
            UnitCar car = new UnitCar("VW", 2000, 111);
            UnitDriver driver = new UnitDriver("gosho", car);
            UnitDriver driver2 = new UnitDriver("pesho", car);
            race.AddDriver(driver);
            race.AddDriver(driver2);

            var result = race.CalculateAverageHorsePower();

            Assert.AreEqual(2000, result);
        }


        [Test]
        public void CounterSetCorrectly()
        {
            UnitCar car = new UnitCar("VW", 2000, 111);
            UnitDriver driver = new UnitDriver("gosho", car);
            race.AddDriver(driver);

            Assert.AreEqual(1, race.Counter);
        }
    }
}