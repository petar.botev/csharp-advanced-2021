﻿using Grand_Prix.Core;
using Grand_Prix.Core.Contracts;
using System;

namespace Grand_Prix
{
    class Program
    {
        static void Main(string[] args)
        {
            IEngine engine = new Engine();
            engine.Run();
        }
    }
}
