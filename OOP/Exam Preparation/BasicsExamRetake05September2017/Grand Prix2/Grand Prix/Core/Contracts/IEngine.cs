﻿namespace Grand_Prix.Core.Contracts
{
    public interface IEngine
    {
        void Run();
    }
}
