﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Grand_Prix.Models.Tyres
{
    public class HardTyre : Tyre
    {
        public HardTyre(string name, double hardness) 
            : base(name, hardness)
        {
        }
    }
}
