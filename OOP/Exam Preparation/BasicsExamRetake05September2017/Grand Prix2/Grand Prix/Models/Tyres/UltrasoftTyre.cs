﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Grand_Prix.Models.Tyres
{
    public class UltrasoftTyre : Tyre
    {
        //private double grip;
        public UltrasoftTyre(string name, double hardness, double grip) 
            : base(name, hardness)
        {
            this.Grip = grip;
        }

        public double Grip { get; private set; }
    }
}
