﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Grand_Prix.Models.Tyres
{
    public abstract class Tyre
    {
        private string name;
        private double hardness;
        private double degradation;

        protected Tyre(string name, double hardness)
        {
            this.Name = name;
            this.Hardness = hardness;
            this.Degradation = 100;
        }

        public string Name { get => name; set => name = value; }
        public double Hardness { get => hardness; set => hardness = value; }
        public double Degradation
        {
            get => degradation; 
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("Tyre blows");
                }
                degradation = value;
            }
        }

        public void DegradeTyre()
        {

        }
    }
}
