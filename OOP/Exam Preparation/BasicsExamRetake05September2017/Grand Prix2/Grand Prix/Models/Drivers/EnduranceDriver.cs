﻿using Grand_Prix.Models.Cars;

namespace Grand_Prix.Models.Drivers
{
    public class EnduranceDriver : Driver
    {
        public EnduranceDriver(string name, double totalTime, Car car, double speed) 
            : base(name, totalTime, car, 1.5, speed)
        {
        }
    }
}
