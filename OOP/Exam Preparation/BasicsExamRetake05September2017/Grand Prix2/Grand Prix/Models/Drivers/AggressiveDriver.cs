﻿using Grand_Prix.Models.Cars;

namespace Grand_Prix.Models.Drivers
{
    public class AggressiveDriver : Driver
    {
        public AggressiveDriver(string name, double totalTime, Car car, double speed) 
            : base(name, totalTime, car, 2.7, speed * 1.3)
        {
        }
    }
}
