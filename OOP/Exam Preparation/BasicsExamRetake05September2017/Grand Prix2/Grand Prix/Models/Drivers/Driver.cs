﻿using Grand_Prix.Models.Cars;

namespace Grand_Prix.Models.Drivers
{
    public abstract class Driver 
    {
        private string name;
        private double totalTime;
        private Car car;
        private double fuelConsumptionPerKm;
        private double speed;

        protected Driver(string name, double totalTime, Car car, double fuelConsumptionPerKm, double speed)
        {
            this.Name = name;
            this.TotalTime = totalTime;
            this.Car = car;
            this.FuelConsumptionPerKm = fuelConsumptionPerKm;
            this.Speed = speed;
        }

        public string Name { get => name; private set => name = value; }
        public double TotalTime { get => totalTime; private set => totalTime = value; }
        public Car Car { get => car; private set => car = value; }
        public double FuelConsumptionPerKm { get => fuelConsumptionPerKm; set => fuelConsumptionPerKm = value; }
        public double Speed { get => speed; set => speed = SpeedChange(); }


        public double SpeedChange()
        {
            return this.speed = (this.Car.Hp + this.Car.Tyre.Degradation) / this.Car.FuelAmount;
        }
    }
}
