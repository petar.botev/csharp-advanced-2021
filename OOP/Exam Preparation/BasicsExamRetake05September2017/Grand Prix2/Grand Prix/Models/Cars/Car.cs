﻿using Grand_Prix.Models.Tyres;
using System;

namespace Grand_Prix.Models.Cars
{
    public class Car
    {
        private int hp;
        private double fuelAmount;
        private Tyre tyre;

        public Car(int hp, double fuelAmount, Tyre tyre)
        {
            this.Hp = hp;
            this.FuelAmount = fuelAmount;
            this.Tyre = tyre;
        }

        public int Hp { get => hp; set => hp = value; }
        public double FuelAmount
        {
            get => fuelAmount; 
            set
            {
                if (value > 160)
                {
                    this.fuelAmount = 160;
                }
                else if (value < 0)
                {
                    throw new ArgumentException();
                }
                else
                {
                    this.fuelAmount = value;
                }
            }
        }
        public Tyre Tyre { get => tyre; set => tyre = value; }
    }
}
