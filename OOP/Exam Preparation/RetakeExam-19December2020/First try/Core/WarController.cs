﻿using System;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using WarCroft.Constants;
using WarCroft.Entities.Characters.Contracts;
using WarCroft.Entities.Items;

namespace WarCroft.Core
{
	public class WarController
	{
		private ICharactersParty party;
		private ItemPool itemPool;

		Assembly assembly = Assembly.GetExecutingAssembly();

		public WarController()
		{
			this.party = new CharactersParty();
			this.itemPool = new ItemPool();
		}

		public string JoinParty(string[] args)
		{
			string characterType = args[0];
			string name = args[1];

			var typesOfInterest = assembly.GetTypes().Where(x => x.Namespace == "WarCroft.Entities.Characters");

			var typeToActivate = typesOfInterest.FirstOrDefault(x => x.Name == characterType);

			if (typeToActivate == null)
            {
				throw new ArgumentException(string.Format(ExceptionMessages.InvalidCharacterType, characterType));
            }

			Object[] ctorArgs = { name };

			var myCharacter = (ICharacter)Activator.CreateInstance(typeToActivate, ctorArgs);

			party.Party.Add(myCharacter);

			return string.Format(SuccessMessages.JoinParty, name);
		}

        public string AddItemToPool(string[] args)
        {
			
			string itemName = args[0];

			var typesOfInterest = assembly.GetTypes().Where(x => x.Namespace == "WarCroft.Entities.Items");

			var typeToActivate = typesOfInterest.FirstOrDefault(x => x.Name == itemName);

			if (typeToActivate == null)
			{
				throw new ArgumentException(string.Format(ExceptionMessages.InvalidItem, itemName));
			}

			Object[] ctorArgs = { itemName };
			var myItem = (IItem)Activator.CreateInstance(typeToActivate, ctorArgs);

			itemPool.ItemsPool.Add(myItem);

			return string.Format(SuccessMessages.AddItemToPool, itemName); 
        }

        public string PickUpItem(string[] args)
		{
			string characterName = args[0];
			var character = party.Party.FirstOrDefault(x => x.Name == characterName);
            if (character == null)
            {
				throw new ArgumentException(string.Format(ExceptionMessages.CharacterNotInParty, characterName));
            }

			if (itemPool.ItemsPool.Count == 0)
            {
				throw new InvalidOperationException(ExceptionMessages.ItemPoolEmpty);
            }

			var lastItem = itemPool.ItemsPool.Last();
			character.Bag.AddItem(lastItem);

			return string.Format(SuccessMessages.PickUpItem, character.Name, lastItem.Name);
		}

		public string UseItem(string[] args)
		{
			string characterName = args[0];
			string itemName = args[1];

			var character = party.Party.FirstOrDefault(x => x.Name == characterName);
			if (character == null)
			{
				throw new ArgumentException(string.Format(ExceptionMessages.CharacterNotInParty, itemName));
			}

            if (character.Bag.Items.Count == 0)
            {
				throw new InvalidOperationException(ExceptionMessages.EmptyBag);
            }

			var lastItem = character.Bag.Items.FirstOrDefault(x => x.Name == itemName);

			if (lastItem == null)
			{
				throw new InvalidOperationException(string.Format(ExceptionMessages.ItemNotFoundInBag, lastItem));
			}

			character.UseItem(lastItem);

			return string.Format(SuccessMessages.UsedItem, characterName, itemName);
		}

		public string GetStats()
		{
			var heroes = party.Party
				.OrderByDescending(x => x.IsAlive)
				.ThenByDescending(x => x.Health);

			StringBuilder strb = new StringBuilder();
            foreach (var hero in heroes)
            {
                if (hero.IsAlive)
                {
					strb.AppendLine($"{hero.Name} - HP: {hero.Health}/{hero.BaseHealth}, AP: {hero.Armor}/{hero.BaseArmor}, Status: Alive");
				}
                else
                {
					strb.AppendLine($"{hero.Name} - HP: {hero.Health}/{hero.BaseHealth}, AP: {hero.Armor}/{hero.BaseArmor}, Status: Dead");
				}
            }

			return strb.ToString().Trim();
		}

		public string Attack(string[] args)
		{
			string attackerName = args[0];
			string receiverName = args[1];

			var attacker = party.Party.FirstOrDefault(x => x.Name == attackerName);
			var receiver = party.Party.FirstOrDefault(x => x.Name == receiverName);

			if (attacker == null)
            {
				throw new ArgumentException(string.Format(ExceptionMessages.CharacterNotInParty, attackerName));
            }

            if (receiver == null)
            {
				throw new ArgumentException(ExceptionMessages.AttackFail, attackerName);
			}

			//   if (attacker.IsAlive == false)
			//{
			//	throw new ArgumentException(ExceptionMessages.AttackFail);
			//}

			attacker.Attack((Character)receiver);

			StringBuilder strb = new StringBuilder();

			strb.AppendLine($"{attackerName} attacks {receiverName} for {attacker.AbilityPoints} hit points! {receiverName} has {receiver.Health}/{receiver.BaseHealth} HP and {receiver.Armor}/{receiver.BaseArmor} AP left!");

            if (receiver.IsAlive == false)
            {
				strb.AppendLine($"{receiver.Name} is dead!");
			}

			return strb.ToString().Trim();
		}

		public string Heal(string[] args)
		{
			string healerName = args[0];
			string healerReceiverName = args[1];

			var healer = party.Party.FirstOrDefault(x => x.Name == healerName);
			var healerReceiver = party.Party.FirstOrDefault(x => x.Name == healerReceiverName);

			if (healer == null)
			{
				throw new ArgumentException(string.Format(ExceptionMessages.CharacterNotInParty, healerName));
			}

			if (healerReceiver == null)
			{
				throw new ArgumentException(string.Format(ExceptionMessages.CharacterNotInParty, healerReceiverName));
			}

			if (healer.IsAlive == false)
			{
				throw new ArgumentException(string.Format(ExceptionMessages.HealerCannotHeal, healerName));
			}

			healer.Heal((Character)healerReceiver);

			StringBuilder strb = new StringBuilder();

			strb.AppendLine($"{healer.Name} heals {healerReceiver.Name} for {healer.AbilityPoints}! {healerReceiver.Name} has {healerReceiver.Health} health now!");

			return strb.ToString().Trim();
		}
	}
}
