﻿using WarCroft.Entities.Characters.Contracts;

namespace WarCroft.Entities.Items
{
    public interface IItem
    {
        int Weight { get; }

        string Name { get; set; }

        void AffectCharacter(ICharacter character);
    }
}