﻿using WarCroft.Entities.Characters.Contracts;

namespace WarCroft.Entities.Items
{
    public class HealthPotion : IItem
    {
        private string name;
        public HealthPotion(string name)
        {
            this.Name = name;
        }

        public int Weight { get; } = 5;
        public string Name { get => name; set => name = value; }

        public void AffectCharacter(ICharacter character)
        {
            if (character.IsAlive)
            {
                character.Health += 20;
            }
        }
    }
}
