﻿using WarCroft.Entities.Characters.Contracts;

namespace WarCroft.Entities.Items
{
    public class FirePotion : IItem
    {
        private string name;
        public FirePotion(string name)
        {
            this.Name = name;
        }
        public string Name { get => name; set => name = value; }

        public int Weight { get; } = 5;

        public void AffectCharacter(ICharacter character)
        {
            if (character.IsAlive)
            {
                character.Health -= 20;
                if (character.Health <= 0)
                {
                    character.IsAlive = false;
                }
            }
        }
    }
}
