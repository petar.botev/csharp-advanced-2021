﻿using System;
using WarCroft.Entities.Characters.Contracts;
using WarCroft.Constants;

namespace WarCroft.Entities.Items
{
    // Christmas came early this year - this class is already implemented for you!
    public abstract class Item : IItem
    {
        public Item(int weight)
        {
            this.Weight = weight;
        }

        public string Name { get; set; }

        public int Weight { get; }

        public virtual void AffectCharacter(ICharacter character)
        {
            if (!character.IsAlive)
            {
                throw new InvalidOperationException(ExceptionMessages.AffectedCharacterDead);
            }
        }
    }
}
