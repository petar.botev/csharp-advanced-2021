﻿using System.Collections.Generic;

namespace WarCroft.Entities.Items
{
    public class ItemPool
    {
        private List<IItem> itemsPool;

        public ItemPool()
        {
            this.itemsPool = new List<IItem>();
        }

        public List<IItem> ItemsPool 
        { 
            get
            {
                return itemsPool;
            }
            set
            {
                itemsPool = value;
            }
        }
    }
}
