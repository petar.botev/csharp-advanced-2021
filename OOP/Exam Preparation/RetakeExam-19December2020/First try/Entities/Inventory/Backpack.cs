﻿namespace WarCroft.Entities.Inventory
{
    public class Backpack : Bag, IBag
    {
        private int capacity;

        public Backpack(int capacity) 
            : base(capacity)
        {
            this.Capacity = capacity;
        }

        public override int Capacity { get => this.capacity; set => this.capacity = value; }
    }
}
