﻿namespace WarCroft.Entities.Inventory
{
    public class Satchel : Bag, IBag
    {
        private int capacity;

        public Satchel() 
        {
            this.capacity = 20;
        }

        public override int Capacity { get => this.capacity;}
    }
}
