﻿using System.Collections.Generic;
using WarCroft.Entities.Items;

namespace WarCroft.Entities.Inventory
{
    public interface IBag
    {
        int Capacity { get; set; }

        int Load { get; }

        IReadOnlyCollection<IItem> Items { get; }

        void AddItem(IItem item);

        IItem GetItem(string name);
    }
}
