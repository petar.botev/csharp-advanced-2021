﻿using System;
using System.Collections.Generic;
using System.Linq;
using WarCroft.Entities.Items;

namespace WarCroft.Entities.Inventory
{
    public abstract class Bag : IBag
    {
        private int capacity;
        private readonly List<IItem> items;

        public Bag(int capacity = 100)
        {
            this.items = new List<IItem>();

            this.capacity = capacity;
        }


        public virtual int Capacity { get => capacity; set => capacity = value; }

        public int Load => Items.Sum(x => x.Weight);

        public IReadOnlyCollection<IItem> Items => items;

        public void AddItem(IItem item)
        {
            if ((this.Load + item.Weight) > this.Capacity)
            {
                throw new InvalidOperationException("Bag is full!");
            }

            this.items.Add(item);
        }

        public IItem GetItem(string name)
        {
            if (this.Items.Count == 0)
            {
                throw new InvalidOperationException("Bag is empty!");
            }

            if (!this.Items.Any(x => nameof(x) == name))
            {
                throw new ArgumentException($"No item with name {name} in bag!");
            }

            IItem item = this.items.FirstOrDefault(x => nameof(x) == name);
            this.items.Remove(item);

            return item;
        }
    }
}
