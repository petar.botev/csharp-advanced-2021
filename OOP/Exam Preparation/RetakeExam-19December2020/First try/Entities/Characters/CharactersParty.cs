﻿using System.Collections.Generic;

namespace WarCroft.Entities.Characters.Contracts
{
    public class CharactersParty : ICharactersParty
    {
        private List<ICharacter> party;

        public CharactersParty()
        {
            this.Party = new List<ICharacter>();
        }

        public List<ICharacter> Party { get; set; }
    }
}
