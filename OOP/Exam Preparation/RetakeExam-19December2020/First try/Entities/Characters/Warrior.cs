﻿using System;
using WarCroft.Constants;
using WarCroft.Entities.Characters.Contracts;
using WarCroft.Entities.Inventory;
using WarCroft.Entities.Items;

namespace WarCroft.Entities.Characters
{
    public class Warrior : ICharacter, IAttacker
    {
        private string name;
        private double armor;
        private double health;

        public Warrior(string name)
        {
            this.Name = name;
            this.Bag = new Satchel();
            this.armor = this.BaseArmor;
            this.Health = this.BaseHealth;
        }

        public double AbilityPoints { get; set; } = 40;
        public double Armor { get => armor; }
        public Bag Bag { get; set; }
        public double BaseArmor { get; set; } = 50;
        public double BaseHealth { get; set; } = 100;
        public double Health { get => health; set => health = value; }
        public bool IsAlive { get; set; } = true;
        public string Name { get => name; set => name = value; }

        public void TakeDamage(double hitPoints)
        {
            if (this.IsAlive)
            {
                this.armor -= hitPoints;
                if (this.Armor < 0)
                {
                    this.health -= Math.Abs(this.Armor);
                    this.armor = 0;
                    if (this.Health <= 0)
                    {
                        this.IsAlive = false;
                        this.Health = 0;
                    }
                }
            }
            else
            {
                throw new InvalidOperationException(ExceptionMessages.AffectedCharacterDead);
            }
        }

        public void UseItem(IItem item)
        {
            if (this.IsAlive)
            {
                item.AffectCharacter(this);
            }
        }

        public void Attack(Character character)
        {
            if (this.IsAlive == true && character.IsAlive == true)
            {
                if (Object.Equals(this, character))
                {
                    throw new InvalidOperationException(ExceptionMessages.CharacterAttacksSelf);
                }

                character.TakeDamage(this.AbilityPoints);
            }
        }

        public void Heal(Character character)
        {
            throw new NotImplementedException();
        }
    }
}
