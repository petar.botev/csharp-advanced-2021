﻿using System;
using WarCroft.Entities.Characters.Contracts;
using WarCroft.Entities.Inventory;
using WarCroft.Entities.Items;

namespace WarCroft.Entities.Characters
{
    public class Priest : ICharacter, IHealer
    {
        private string name;
        private double armor;
        private double health;

        public Priest(string name) 
        {
            this.Name = name;
            this.Bag = new Backpack(100);
            this.Armor = this.BaseArmor;
            this.Health = this.BaseHealth;
        }

        public string Name { get => name; set => name = value; }

        public double AbilityPoints { get; set; } = 40;

        public Bag Bag { get; set; }

        public double BaseArmor { get; set; } = 25;

        public double BaseHealth { get; set; } = 50;

        public bool IsAlive { get; set; } = true;


        public double Armor { get => armor; set => armor = value; }
        public double Health { get => health; set => health = value; }

        public void Attack(Character character)
        {
            throw new NotImplementedException();
        }

        public void Heal(Character character)
        {
            if (this.IsAlive == true && character.IsAlive == true)
            {
                character.Health += this.AbilityPoints;
            }
        }

        public void TakeDamage(double hitPoints)
        {
            if (this.IsAlive)
            {
                this.armor -= hitPoints;
                if (this.Armor < 0)
                {
                    this.health -= Math.Abs(this.Armor);
                    this.armor = 0;
                    if (this.Health <= 0)
                    {
                        this.IsAlive = false;
                        this.Health = 0;
                    }
                }
            }
        }

        public void UseItem(IItem item)
        {
            if (this.IsAlive)
            {
                item.AffectCharacter(this);
            }
        }
    }
}
