﻿using System;

using WarCroft.Constants;
using WarCroft.Entities.Inventory;
using WarCroft.Entities.Items;

namespace WarCroft.Entities.Characters.Contracts
{
    public abstract class Character : ICharacter
    {
        // TODO: Implement the rest of the class.
        private string name;
        private double health;
        private double baseHealth;
        private double baseArmor;
        private double armor;

        public Character(string name, double health, double armor, double abilityPoints, Bag bag)
        {
            this.Name = name;
            this.Health = health;
            this.Armor = armor;
            this.AbilityPoints = abilityPoints;
            this.Bag = bag;
        }

        public virtual string Name
        {
            get => name;
            set
            {
                if (value == null || value == " ")
                {
                    throw new ArgumentException(ExceptionMessages.CharacterNameInvalid);
                }
            }
        }

        public double BaseHealth { get; set; }

        public double Health
        {
            get => health;
            set
            {
                if (health <= baseHealth && health > 0)
                {
                    health = value;
                }
            }
        }

        public virtual double BaseArmor { get; set; }

        public double Armor
        {
            get => armor;
            set
            {
                if (value > -1)
                {
                    armor = value;
                }
            }
        }

        public virtual double AbilityPoints { get; set; }

        public Bag Bag { get; set; }

        public bool IsAlive { get; set; } = true;

        protected void EnsureAlive()
        {
            if (!this.IsAlive)
            {
                throw new InvalidOperationException(ExceptionMessages.AffectedCharacterDead);
            }
        }

        public void TakeDamage(double hitPoints)
        {
            if (this.IsAlive)
            {
                this.Armor -= hitPoints;
                if (this.Armor < 0)
                {
                    this.Health -= this.Armor;
                    this.Armor = 0;
                    if (this.Health <= 0)
                    {
                        this.IsAlive = false;
                    }
                }
            }
        }

        public void UseItem(IItem item)
        {
            if (this.IsAlive)
            {
                item.AffectCharacter(this);
            }
        }

        public abstract void Attack(Character character);

        public abstract void Heal(Character character);
    }   
}