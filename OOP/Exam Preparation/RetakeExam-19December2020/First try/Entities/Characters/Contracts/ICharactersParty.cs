﻿using System.Collections.Generic;
namespace WarCroft.Entities.Characters.Contracts
{
    public interface ICharactersParty
    {
        List<ICharacter> Party { get; }
    }
}