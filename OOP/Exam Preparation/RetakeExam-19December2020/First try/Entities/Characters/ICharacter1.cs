﻿using WarCroft.Entities.Inventory;
using WarCroft.Entities.Items;

namespace WarCroft.Entities.Characters.Contracts
{
    public interface ICharacter1
    {
        double AbilityPoints { get; set; }
        double Armor { get; set; }
        Bag Bag { get; set; }
        double BaseArmor { get; set; }
        double BaseHealth { get; set; }
        double Health { get; set; }
        bool IsAlive { get; set; }
        string Name { get; set; }

        void TakeDamage(double hitPoints);
        void UseItem(Item item);
    }
}