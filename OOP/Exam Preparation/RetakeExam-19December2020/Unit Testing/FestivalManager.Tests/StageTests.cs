// Use this file for your unit tests.
// When you are ready to submit, REMOVE all using statements to Festival Manager (entities/controllers/etc)
// Test ONLY the Stage class. 


namespace FestivalManager.Tests
{
	using FestivalManager.Entities;
	using NUnit.Framework;
	using System;

	[TestFixture]
	public class StageTests
    {
		Stage stage;

		[SetUp]
		public void SetUp()
        {
			stage = new Stage();
        }

		[Test]
		[TestCase(17)]
	    public void AddPerformerThrowExceptionIfAgeUnder18(int age)
	    {
			Performer performer = new Performer("Ivan", "Ivanov", 17);
			Assert.Throws<ArgumentException>(() => stage.AddPerformer(performer));
		}

		[Test]
		public void AddPerformerValueIsAddedToPerformer()
		{
			Performer performer = new Performer("Ivan", "Ivanov", 18);

			stage.AddPerformer(performer);
			Assert.AreEqual(1, stage.Performers.Count);
		}

		[Test]
		public void AddSongThrowExceptionIfSongIsUnder1Min()
		{
			Song song = new Song("Song", new TimeSpan(0, 0, 59));
			
			Assert.Throws<ArgumentException>(() => stage.AddSong(song));
		}

		//[Test]
		//public void AddSongValueIsAddedCorrectly()
  //      {
		//	Song song = new Song("Song", new TimeSpan(0, 1, 59));

		//	stage.AddSong(song);

		//	Assert.AreEqual(1, stage.)
		//}

		[Test]
		public void AddSongToPerformerSongAddedToSonglist()
        {
			Performer performer = new Performer("Ivan", "Ivanov", 18);
			Song song = new Song("Song", new TimeSpan(0, 1, 0));

			stage.AddPerformer(performer);
			stage.AddSong(song);

			stage.AddSongToPerformer(song.Name, performer.FullName);

			Assert.AreEqual(1, performer.SongList.Count);
        }

		[Test]
		public void AddSongToPerformerReturnsCorrectResult()
        {
			Performer performer = new Performer("Ivan", "Ivanov", 18);
			Song song = new Song("Song", new TimeSpan(0, 1, 0));

			stage.AddPerformer(performer);
			stage.AddSong(song);

			var result = stage.AddSongToPerformer(song.Name, performer.FullName);

			Assert.AreEqual($"{song} will be performed by {performer}", result);
		}

		[Test]
		public void PlayReturnsCorrectResult()
        {
			Performer performer = new Performer("Ivan", "Ivanov", 18);
			Song song = new Song("Song", new TimeSpan(0, 1, 0));

			stage.AddPerformer(performer);
			stage.AddSong(song);
			stage.AddSongToPerformer(song.Name, performer.FullName);

			var result = stage.Play();

			Assert.AreEqual($"{stage.Performers.Count} performers played 1 songs", result);
		}

		[Test]
		public void GetPerformerThrowsExceptionIfPerformerIsNull()
        {
			Performer performer = new Performer("Ivan", "Ivanov", 18);
			Song song = new Song("Song", new TimeSpan(0, 1, 0));
			stage.AddPerformer(performer);

			Assert.Throws<ArgumentException>(() => stage.AddSongToPerformer(song.Name, "Gosho"));
		}

		[Test]
		public void GetSongThrowsExceptionIfPerformerIsNull()
		{
			Performer performer = new Performer("Ivan", "Ivanov", 18);
			Song song = new Song("Song", new TimeSpan(0, 1, 0));
			stage.AddPerformer(performer);

			Assert.Throws<ArgumentException>(() => stage.AddSongToPerformer("Tralala", performer.FullName));
		}

		[Test]
		public void ValidateNullValueThrowsExceptionIfSongIsNull()
		{
			Performer performer = new Performer("Ivan", "Ivanov", 18);
			Song song = new Song("Song", new TimeSpan(0, 1, 0));
			stage.AddPerformer(performer);

			Assert.Throws<ArgumentNullException>(() => stage.AddSongToPerformer(null, performer.FullName));
		}

		[Test]
		public void ValidateNullValueThrowsExceptionIfPerformerIsNull()
		{
			Performer performer = new Performer("Ivan", "Ivanov", 18);
			Song song = new Song("Song", new TimeSpan(0, 1, 0));
			stage.AddPerformer(performer);

			Assert.Throws<ArgumentNullException>(() => stage.AddSongToPerformer(song.Name, null));
		}
	}
}