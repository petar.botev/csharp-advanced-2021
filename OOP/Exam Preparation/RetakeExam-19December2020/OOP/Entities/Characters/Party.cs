﻿using System.Collections.Generic;
using WarCroft.Entities.Characters.Contracts;

namespace WarCroft.Entities.Characters
{
    public class Party
    {

        public Party()
        {
            this.PartyCharacters = new List < Character >();
        }

        public List<Character> PartyCharacters { get; set; }

    }
}
