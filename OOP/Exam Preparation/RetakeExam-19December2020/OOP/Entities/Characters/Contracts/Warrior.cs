﻿using System;
using WarCroft.Constants;
using WarCroft.Entities.Inventory;

namespace WarCroft.Entities.Characters.Contracts
{
    public class Warrior : Character, IAttacker
    {
        private const double BASE_HEALTH = 100;
        private const double BASE_ARMOR = 50;
        private const double ABILITY_POINTS = 40;

        public Warrior(string name) 
            : base(name, BASE_HEALTH, BASE_ARMOR, ABILITY_POINTS, new Satchel())
        {
        }

        public void Attack(Character character)
        {
            if (this.IsAlive || character.IsAlive)
            {
                if (this.Equals(character))
                {
                    throw new InvalidOperationException(ExceptionMessages.CharacterAttacksSelf);
                }

                character.Armor -= this.AbilityPoints;
                if (character.Armor < 0)
                {
                    character.Health -= Math.Abs(character.Armor);
                    character.Armor = 0;
                    


                }
            }
        }
    }
}
