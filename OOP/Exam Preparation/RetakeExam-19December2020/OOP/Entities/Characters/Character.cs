﻿using System;
using WarCroft.Constants;
using WarCroft.Entities.Inventory;
using WarCroft.Entities.Items;

namespace WarCroft.Entities.Characters.Contracts
{
    public abstract class Character
    {
        private string name;
        private double baseHealth;
        private double health;
        private double baseArmor;
        private double armor;
        private double abilityPoints;
        private IBag bag;

        protected Character(string name, double health, double armor, double abilityPoints, IBag bag)
        {
            this.Name = name;
            this.baseHealth = health;
            this.Health = health;
            this.baseArmor = armor;
            this.Armor = armor;
            this.AbilityPoints = abilityPoints;
            this.Bag = bag;
        }

        
        public string Name
        {
            get => name; private set
            {
                if (value == null || value == " ")
                {
                    throw new ArgumentException(ExceptionMessages.CharacterNameInvalid);
                }
                name = value;
            }
        }
        public double BaseHealth { get => baseHealth; }
        public double Health
        {
            get => health;
            set
            {
                 if (value > this.BaseHealth)
                {
                    this.health = BaseHealth;
                }
                else if (value < 0)
                {
                    this.health = 0;
                }
                else
                {
                    health = value;
                }
            }
        }
        public double BaseArmor { get => baseArmor; }
        public double Armor
        {
            get => armor;
            set
            {
                if (value > 0)
                {
                    armor = value;
                }
                else
                {
                    armor = 0;
                }
            }
        }
        public double AbilityPoints { get => abilityPoints; private set => abilityPoints = value; }
        public IBag Bag { get => this.bag; private set => this.bag = value; }
        public bool IsAlive { get; set; } = true;

        protected void EnsureAlive()
        {
            if (!this.IsAlive)
            {
                throw new InvalidOperationException(ExceptionMessages.AffectedCharacterDead);
            }
        }

        public void TakeDamage(double hitPoints)
        {
            EnsureAlive();

            this.armor -= hitPoints;
            if (this.armor < 0)
            {
                this.health -= Math.Abs(this.Armor);
                this.armor = 0;
                if (this.health <= 0)
                {
                    this.IsAlive = false;
                    this.health = 0;
                }
            }
        }

        public void UseItem(Item item)
        {
            EnsureAlive();

            item.AffectCharacter(this);
        }
    }
}