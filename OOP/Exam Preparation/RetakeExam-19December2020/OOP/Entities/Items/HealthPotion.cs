﻿using WarCroft.Entities.Characters.Contracts;

namespace WarCroft.Entities.Items
{
    public class HealthPotion : Item
    {
        private const int potionWeight = 5;

        public HealthPotion()
            : base(potionWeight)
        {
            
        }

        public override void AffectCharacter(Character character)
        {
            base.AffectCharacter(character);
            if (character.IsAlive)
            {
                character.Health += 20;
            }
        }
    }
}
