﻿using System.Collections.Generic;

namespace WarCroft.Entities.Items
{
    public class ItemsPool
    {
        public ItemsPool()
        {
            PoolOfItems = new List<Item>();
        }
        public List<Item> PoolOfItems { get; set; }
    }
}
