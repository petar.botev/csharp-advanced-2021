﻿using System;
using System.Reflection;
using System.Linq;
using WarCroft.Constants;
using WarCroft.Entities.Characters;
using WarCroft.Entities.Items;
using System.Text;
using WarCroft.Entities.Characters.Contracts;

namespace WarCroft.Core
{
    public class WarController
	{
		private Party party = new Party();
		private ItemsPool items = new ItemsPool();

		public WarController()
		{
		}

		public string JoinParty(string[] args)
		{
			string characterType = args[0];
			string name = args[1];

			Assembly assembly = Assembly.GetExecutingAssembly();
			var typeCollection = assembly.GetTypes().Where(x => x.Namespace == "WarCroft.Entities.Characters");

			var typeOfInterrest = typeCollection.FirstOrDefault(x => x.Name == characterType);

            if (!typeCollection.Contains(typeOfInterrest))
            {
				//throw new ArgumentException(string.Format(ExceptionMessages.InvalidCharacterType, characterType));
				throw new ArgumentException($"Invalid character type \"{characterType}\"!");
			}

			object[] objParams = { name };

			var newCharacter = (Character)Activator.CreateInstance(typeOfInterrest, objParams);

			party.PartyCharacters.Add(newCharacter);

			//return string.Format(string.Format(SuccessMessages.JoinParty, name));
			return $"{name} joined the party!";
		}

		public string AddItemToPool(string[] args)
		{
			string itemName = args[0];

			Assembly assembly = Assembly.GetExecutingAssembly();

			var typeCollection = assembly.GetTypes().Where(x => x.Namespace == "WarCroft.Entities.Items");

			var itemOfInterest = typeCollection.FirstOrDefault(x => x.Name == itemName);

            if (itemOfInterest == null)
            {
				//throw new ArgumentException(string.Format(ExceptionMessages.InvalidItem, itemName));
				throw new ArgumentException($"Invalid item \"{itemName}\"!");
			}

			Object[] objParams = { itemName };

			var newItem = (Item)Activator.CreateInstance(itemOfInterest);

			items.PoolOfItems.Add(newItem);

			//return string.Format(SuccessMessages.AddItemToPool, itemName);
			return $"{itemName} added to pool.";
		}


		public string PickUpItem(string[] args)
		{
			string characterName = args[0];

			var character = party.PartyCharacters.FirstOrDefault(x => x.Name == characterName);

			if (character == null)
            {
				//throw new ArgumentException(string.Format(ExceptionMessages.CharacterNotInParty, characterName));
				throw new ArgumentException($"Character {characterName} not found!");
			}

            if (items.PoolOfItems.Count == 0)
            {
				//throw new InvalidOperationException(ExceptionMessages.ItemPoolEmpty);
				throw new InvalidOperationException("No items left in pool!");
			}

			var itemToPickUp = items.PoolOfItems.Last();

			character.Bag.AddItem(itemToPickUp);

			items.PoolOfItems.Remove(itemToPickUp);

			//return string.Format(SuccessMessages.PickUpItem, characterName, itemToPickUp.GetType().Name);
			return $"{characterName} picked up {itemToPickUp.GetType().Name}!";
		}

		public string UseItem(string[] args)
		{
			string characterName = args[0];
			string itemName = args[1];

			var character = party.PartyCharacters.FirstOrDefault(x => x.Name == characterName);

            if (character == null)
            {
				//throw new ArgumentException(string.Format(ExceptionMessages.CharacterNotInParty, characterName));
				throw new ArgumentException($"Character {characterName} not found!");
			}

			var item = character.Bag.GetItem(itemName);
			character.UseItem(item);

			//return string.Format(SuccessMessages.UsedItem, character.Name, itemName);
			return $"{characterName} used {item.GetType().Name}.";
		}

		public string GetStats()
		{
			var sortedCharacters = party.PartyCharacters.OrderByDescending(x => x.IsAlive).ThenByDescending(x => x.Health);

			StringBuilder strb = new StringBuilder();

            foreach (var character in sortedCharacters)
            {
                if (character.IsAlive)
                {
					strb.AppendLine($"{character.Name} - HP: {character.Health}/{character.BaseHealth}, AP: {character.Armor}/{character.BaseArmor}, Status: Alive");
				}
                else
                {
					strb.AppendLine($"{character.Name} - HP: {character.Health}/{character.BaseHealth}, AP: {character.Armor}/{character.BaseArmor}, Status: Dead");
				}
			}

			return strb.ToString().Trim();
		}

		public string Attack(string[] args)
		{
			string attackerName = args[0];
			string receiverName = args[1];

			var attacker = party.PartyCharacters.FirstOrDefault(x => x.Name == attackerName);
			var receiver = party.PartyCharacters.FirstOrDefault(x => x.Name == receiverName);

            if (attacker == null)
            {
				// throw new ArgumentException(string.Format(ExceptionMessages.CharacterNotInParty, attackerName));
				throw new ArgumentException($"Character {attackerName} not found!");
			}

            if (receiver == null)
            {
				//throw new ArgumentException(string.Format(ExceptionMessages.CharacterNotInParty, receiverName));
				throw new ArgumentException($"Character {receiverName} not found!");
			}

            if (attacker.GetType().Name != "Warrior")
            {
				throw new ArgumentException($"{attacker.Name} cannot attack!");
			}

			WarCroft.Entities.Characters.Warrior newWarrior = (WarCroft.Entities.Characters.Warrior)attacker;

			newWarrior.Attack(receiver);

			StringBuilder strb = new StringBuilder();

			strb.AppendLine($"{attackerName} attacks {receiverName} for {attacker.AbilityPoints} hit points! {receiverName} has {receiver.Health}/{receiver.BaseHealth} HP and {receiver.Armor}/{receiver.BaseArmor} AP left!");

            if (receiver.Health <= 0)
            {
				receiver.IsAlive = false;
				//strb.AppendLine(string.Format(SuccessMessages.AttackKillsCharacter, receiverName));
				strb.AppendLine($"{receiver.Name} is dead!");
			}

			return strb.ToString().Trim();
		}

		public string Heal(string[] args)
		{
			string healerName = args[0];
			string healerReceiverName = args[1];

			var healer = party.PartyCharacters.FirstOrDefault(x => x.Name == healerName);
			var receiver = party.PartyCharacters.FirstOrDefault(x => x.Name == healerReceiverName);

			if (healer == null)
			{
				//throw new ArgumentException(string.Format(ExceptionMessages.CharacterNotInParty, healerName));
				throw new ArgumentException($"Character {healerName} not found!");
			}

			if (receiver == null)
			{
				//throw new ArgumentException(string.Format(ExceptionMessages.HealerCannotHeal, healerReceiverName));
				throw new ArgumentException($"Character {healerReceiverName} not found!");
			}

            if (healer.GetType().Name != "Priest")
            {
				throw new ArgumentException($"{healer.Name} cannot heal!");
			}

			Priest priest = (Priest)healer;

			priest.Heal(receiver);

			StringBuilder strb = new StringBuilder();

			strb.AppendLine($"{healer.Name} heals {receiver.Name} for {healer.AbilityPoints}! {receiver.Name} has {receiver.Health} health now!");

			return strb.ToString().Trim();
		}
	}
}
