using NUnit.Framework;
using System;
using System.Linq;

namespace BankSafe.Tests
{
    public class BankVaultTests
    {
        private BankVault bankVault;
        [SetUp]
        public void Setup()
        {
            bankVault = new BankVault();
        }

        [Test]
        public void AddItemThrowsExceptionIfCellDoesntExists()
        {
            Item item = new Item("Ivan", "1");
            Assert.Throws<ArgumentException>(() => bankVault.AddItem("Key", item));
        }

        [Test]
        public void AddItemThrowsExceptionIfCellIsTaken()
        {
            Item item = new Item("Ivan", "1");
            
            bankVault.AddItem("B4", item);

            Assert.Throws<ArgumentException>(() => bankVault.AddItem("B4", item));
        }

        [Test]
        public void AddItemThrowsExceptionIfCellExists()
        {
            Item item = new Item("Ivan", "1");

            bankVault.AddItem("B4", item);

            Assert.Throws<InvalidOperationException>(() => bankVault.AddItem("B3", item));
        }

        [Test]
        public void RemoveItemThrowsExceptionIfCellDoesntExists()
        {
            Item item = new Item("Ivan", "1");
            Assert.Throws<ArgumentException>(() => bankVault.RemoveItem("Key", item));
        }

        [Test]
        public void RemoveItemThrowsExceptionIfItemInThatCellDoesntExists()
        {
            Item item = new Item("Ivan", "1");

            bankVault.AddItem("B4", item);

            Assert.Throws<ArgumentException>(() => bankVault.RemoveItem("B3", item));
        }

        [Test]
        public void RemoveItemCellShoulBeNull()
        {
            Item item = new Item("Ivan", "1");

            bankVault.AddItem("B4", item);
            bankVault.RemoveItem("B4", item);

            Assert.AreEqual(null, bankVault.VaultCells["B4"]);
        }

    }
}