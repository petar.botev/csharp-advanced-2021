﻿using Bakery.Models.BakedFoods.Contracts;
using Bakery.Models.Drinks.Contracts;
using Bakery.Models.Tables.Contracts;
using Bakery.Utilities.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bakery.Models.Tables
{
    public abstract class Table : ITable
    {
        
        private int numberOfPeople;
        private int capacity;
        private decimal price;
        private readonly ICollection<IBakedFood> foodOrders;
        private readonly ICollection<IDrink> drinkOrders;

        protected Table(int tableNumber, int capacity, decimal pricePerPerson)
        {
            this.TableNumber = tableNumber;
            this.Capacity = capacity;
            this.PricePerPerson = pricePerPerson;

            foodOrders = new List<IBakedFood>();
            drinkOrders = new List<IDrink>();
        }

        public int TableNumber { get; }

        public int Capacity 
        {
            get => capacity;
            private set
            {
                if (value < 0)
                {
                    throw new ArgumentException(ExceptionMessages.InvalidTableCapacity);
                }

                this.capacity = value;
            }
        }

        public int NumberOfPeople
        {
            get
            {
                return this.numberOfPeople;
            }
            private set
            {
                if (value <= 0)
                {
                    throw new ArgumentException(ExceptionMessages.InvalidNumberOfPeople);
                }

                this.numberOfPeople = value;
            }
        }

        public decimal PricePerPerson { get; }

        public bool IsReserved { get; private set; } = false;

        public decimal Price
        {
            get
            {
                return this.price;
            }
            private set
            {
                
                this.price = value;
            }
        }

        public void Clear()
        {
            foodOrders.Clear();
            drinkOrders.Clear();
            numberOfPeople = 0;
            this.IsReserved = false;
        }

        public decimal GetBill()
        {
            return price + foodOrders.Sum(x => x.Price) + drinkOrders.Sum(x => x.Price);
        }

        public string GetFreeTableInfo()
        {
            StringBuilder strb = new StringBuilder();
            strb.AppendLine($"Table: {this.TableNumber}");
            strb.AppendLine($"Type: {this.GetType().Name}");
            strb.AppendLine($"Capacity: {this.Capacity}");
            strb.AppendLine($"Price per Person: {this.PricePerPerson}");

            return strb.ToString().Trim();
        }

        public void OrderDrink(IDrink drink)
        {
            drinkOrders.Add(drink);
        }

        public void OrderFood(IBakedFood food)
        {
            foodOrders.Add(food);
        }

        public void Reserve(int numberOfPeople)
        {
            this.IsReserved = true;
            this.NumberOfPeople = numberOfPeople;
            this.Price = numberOfPeople * PricePerPerson;
        }
    }
}
