﻿using Easter.Models.Bunnies.Contracts;
using Easter.Models.Eggs.Contracts;
using Easter.Models.Workshops.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Easter.Models.Workshops
{
    public class Workshop : IWorkshop
    {
        public void Color(IEgg egg, IBunny bunny)
        {
            if (bunny.Energy > 0 && bunny.Dyes.Any(x => x.Power > 0))
            {
                while (bunny.Dyes.Where(x => x.Power > 0).ToList().Count > 0)
                {
                    var dye = bunny.Dyes.FirstOrDefault(x => x.Power > 0);
                    while (bunny.Energy > 0 && dye.Power > 0)
                    {
                        bunny.Work();
                        egg.GetColored();
                        dye.Use();

                        if (egg.IsDone())
                        {
                            break;
                        }
                    }

                    if (bunny.Energy <= 0)
                    {
                        break;
                    }
                    if (egg.IsDone())
                    {
                        break;
                    }
                }
            }
        }
    }
}
