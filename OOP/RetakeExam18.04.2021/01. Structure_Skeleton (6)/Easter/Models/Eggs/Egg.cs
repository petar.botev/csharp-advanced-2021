﻿using Easter.Models.Eggs.Contracts;
using Easter.Utilities.Messages;
using System;
using System.Collections.Generic;
using System.Text;

namespace Easter.Models.Eggs
{
    public class Egg : IEgg
    {
        private string name;
        private int energyRequired;

        public Egg(string name, int energyRequired)
        {
            this.Name = name;
            this.energyRequired = energyRequired;
        }

        public string Name
        {
            get
            {
                return name;
            }
            private set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(ExceptionMessages.InvalidEggName);
                }
                this.name = value;
            }
        }

        public int EnergyRequired
        {
            get
            {
                if (this.energyRequired < 0)
                {
                    return this.energyRequired = 0;
                }
                return energyRequired;
            }
        }

        public void GetColored()
        {
            this.energyRequired -= 10;
            if (this.energyRequired < 0)
            {
                this.energyRequired = 0;
            }
        }

        public bool IsDone()
        {
            if (this.energyRequired <= 0)
            {
                return true;
            }
            return false;
        }
    }
}
