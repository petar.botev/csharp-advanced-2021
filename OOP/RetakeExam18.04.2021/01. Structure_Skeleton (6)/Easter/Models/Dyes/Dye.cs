﻿using Easter.Models.Dyes.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace Easter.Models.Dyes
{
    public class Dye : IDye
    {
        private int power;

        public Dye(int power)
        {
            this.power = power;
        }

        public int Power
        {
            get
            {
                if (this.power < 0)
                {
                   return this.power = 0;
                }
                return power;
            }
        }

        public bool IsFinished()
        {
            if (this.Power <= 0)
            {
                return true;
            }
            return false;
        }

        public void Use()
        {
            this.power -= 10;
            if (this.power <= 0)
            {
                this.power = 0;
            }
        }
    }
}
