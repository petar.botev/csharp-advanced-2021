﻿using Easter.Core.Contracts;
using Easter.Models.Bunnies;
using Easter.Models.Bunnies.Contracts;
using Easter.Models.Dyes;
using Easter.Models.Dyes.Contracts;
using Easter.Models.Eggs;
using Easter.Models.Eggs.Contracts;
using Easter.Models.Workshops;
using Easter.Models.Workshops.Contracts;
using Easter.Repositories;
using Easter.Repositories.Contracts;
using Easter.Utilities.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Easter.Core
{
    public class Controller : IController
    {
        private IRepository<IBunny> bunniesRepo;
        private IRepository<IEgg> eggsRepo;
        private IWorkshop workshop;

        public Controller()
        {
            this.bunniesRepo = new BunnyRepository();
            this.eggsRepo = new EggRepository();
            this.workshop = new Workshop();
        }

        public string AddBunny(string bunnyType, string bunnyName)
        {
            IBunny bunny = null;
            if (bunnyType == "HappyBunny")
            {
                bunny = new HappyBunny(bunnyName);
            }
            if (bunnyType == "SleepyBunny")
            {
                bunny = new SleepyBunny(bunnyName);
            }
           
            if (bunny == null)
            {
                throw new InvalidOperationException(ExceptionMessages.InvalidBunnyType);
            }

            bunniesRepo.Add(bunny);

            return string.Format(OutputMessages.BunnyAdded, bunnyType, bunnyName);
        }

        public string AddDyeToBunny(string bunnyName, int power)
        {
            var bunny = bunniesRepo.Models.FirstOrDefault(x => x.Name == bunnyName);

            if (bunny == null)
            {
                throw new InvalidOperationException(ExceptionMessages.InexistentBunny);
            }
            IDye dye = new Dye(power);
            bunny.AddDye(dye);

            return string.Format(OutputMessages.DyeAdded, power, bunnyName);
        }

        public string AddEgg(string eggName, int energyRequired)
        {
            IEgg egg = new Egg(eggName, energyRequired);

            eggsRepo.Add(egg);

            return string.Format(OutputMessages.EggAdded, eggName);
        }

        public string ColorEgg(string eggName)
        {
            if (this.bunniesRepo.Models.Count() == 0)
            {
                throw new InvalidOperationException(ExceptionMessages.BunniesNotReady);
            }
            var bunnies = bunniesRepo.Models.Where(x => x.Energy >= 50).OrderByDescending(x => x.Energy);
            var egg = eggsRepo.FindByName(eggName);
            
            var bunny = bunnies.FirstOrDefault(x => x.Dyes.Any(x => x.Power > 0));
            if (bunny != null)
            {
                workshop.Color(egg, bunny);

                if (bunny.Energy == 0)
                {
                    this.bunniesRepo.Remove(bunny);
                }
            }

            if (egg.IsDone())
            {
                return string.Format(OutputMessages.EggIsDone, eggName);

            }
            else
            {
                return string.Format(OutputMessages.EggIsNotDone, eggName);
            }
        }

        public string Report()
        {
            StringBuilder strb = new StringBuilder();
            
            strb.AppendLine($"{eggsRepo.Models.Where(x => x.IsDone() == true).Count()} eggs are done!");
            strb.AppendLine($"Bunnys info:");
            foreach (var bunny in bunniesRepo.Models)
            {
                strb.AppendLine($"Name: {bunny.Name}");
                strb.AppendLine($"Energy: {bunny.Energy}");
                strb.AppendLine($"Dyes: {bunny.Dyes.Where(x => x.Power > 0).Count()} not finished");

            }

            return strb.ToString().Trim();
        }
    }
}
