﻿namespace Presents.Tests
{
    using NUnit.Framework;
    using System;
    using System.Linq;

    [TestFixture]
    public class PresentsTests
    {
        [Test]
        public void CreateThrowExcIfPressentIsNull()
        {
            Bag bag = new Bag();
            Assert.Throws<ArgumentNullException>(() => bag.Create(null));
        }

        [Test]
        public void CreateThrowExcIfPressentExists()
        {
            Bag bag = new Bag();
            Present present = new Present("ball", 12);
            bag.Create(present);
            
            Assert.Throws<InvalidOperationException>(() => bag.Create(present));
        }

        [Test]
        public void CreateWorks()
        {
            Bag bag = new Bag();
            Present present = new Present("ball", 12);
            var result = bag.Create(present);

            Assert.AreEqual($"Successfully added present {present.Name}.", result);
        }

        [Test]
        public void RemoveWorks()
        {
            Bag bag = new Bag();
            Present present = new Present("ball", 12);
            bag.Create(present);
            var result = bag.Remove(present);

            Assert.AreEqual(true, result);
        }

        [Test]
        public void GetPresentWithLeastMagicWorks()
        {
            Bag bag = new Bag();
            Present present = new Present("ball", 12);
            Present present2 = new Present("toy", 13);
            bag.Create(present);
            bag.Create(present2);
            var result = bag.GetPresentWithLeastMagic();

            Assert.AreEqual(present, result);
        }

        [Test]
        public void GetPresentWorks()
        {
            Bag bag = new Bag();
            Present present = new Present("ball", 12);
            Present present2 = new Present("toy", 13);
            bag.Create(present);
            bag.Create(present2);
            var result = bag.GetPresent("ball");

            Assert.AreEqual(present, result);
        }

        [Test]
        public void GetPresentsWorks()
        {
            Bag bag = new Bag();
            Present present = new Present("ball", 12);
            Present present2 = new Present("toy", 13);
            bag.Create(present);
            bag.Create(present2);
            var result = bag.GetPresents();

            Assert.AreEqual(2, result.Count);
            Assert.That(result.Any(x => x.Name == "ball"));
        }

        [Test]
        public void GetPresentsReturnZero()
        {
            Bag bag = new Bag();
            //Present present = new Present("ball", 12);
            //Present present2 = new Present("toy", 13);
            //bag.Create(present);
            //bag.Create(present2);
            var result = bag.GetPresents();

            Assert.AreEqual(0, result.Count);
        }
    }
}
