﻿using System;
using System.Globalization;

namespace P05BirthdayCelebrations
{
    class Program
    {
        static void Main()
        {
            City city = new City();
            string command;
            while ((command = Console.ReadLine()) != "End")
            {
                string[] commandArgs = command
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                Citizen citizen = null;

                if (commandArgs[0] == "Citizen" || commandArgs[0] == "Pet")
                {
                    if (commandArgs.Length == 3)
                    {
                        string petName = commandArgs[1];
                        DateTime petBirthday = DateTime.ParseExact(commandArgs[2], "dd/MM/yyyy", null);

                        citizen = new Pet(petName, petBirthday);
                    }
                    else
                    {
                        string name = commandArgs[1];
                        int age = int.Parse(commandArgs[2]);
                        string id = commandArgs[3];
                        DateTime birthdate = DateTime.ParseExact(commandArgs[4], "dd/MM/yyyy", null);

                        citizen = new Human(id, name, age, birthdate);
                    }

                    city.Birthdays.Add(citizen.Birthdate);

                }

            }

            string yearOfInterrest = Console.ReadLine();

            city.SpecialBirthdays(yearOfInterrest);
           
            Console.WriteLine(city);
        }
    }
}
