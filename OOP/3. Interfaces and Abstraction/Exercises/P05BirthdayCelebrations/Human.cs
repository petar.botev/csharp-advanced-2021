﻿using System;

namespace P05BirthdayCelebrations
{
    public class Human : Citizen
    {
        public Human(string id, string name, int age, DateTime birthday) 
            : base(id, name, age, birthday)
        {
        }
    }
}
