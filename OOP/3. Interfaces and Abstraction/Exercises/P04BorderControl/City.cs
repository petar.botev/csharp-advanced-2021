﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P04BorderControl
{
    public class City 
    {
        public City()
        {
            Citizens = new List<Citizen>();
            DetainedCitizens = new List<Citizen>();
        }

        public List<Citizen> Citizens { get; set; }
        public List<Citizen> DetainedCitizens { get; set; }


        public void Detained(string end)
        {
            DetainedCitizens = Citizens.Where(x => x.Id.EndsWith(end)).ToList();
        }

        public override string ToString()
        {

            return $"{string.Join(Environment.NewLine, DetainedCitizens.Select(x => x.Id))}";
        }
    }
}
