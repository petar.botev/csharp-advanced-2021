﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P04BorderControl
{
    public class Citizen
    {
        public Citizen(string id, string model)
        {
            this.Id = id;
            this.Model = model;
        }

        public Citizen(string id, string name, int age)
        {
            this.Id = id;
            this.Name = name;
            this.Age = age;
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string Model { get; set; }

    }
}
