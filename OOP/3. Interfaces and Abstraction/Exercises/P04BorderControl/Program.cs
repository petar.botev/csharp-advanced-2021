﻿using System;

namespace P04BorderControl
{
    class Program
    {
        static void Main()
        {
            City city = new City();
            string command;
            while ((command = Console.ReadLine()) != "End")
            {
                string[] commandArgs = command
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                Citizen citizen = null;

                if (commandArgs.Length == 3)
                {
                    string name = commandArgs[0];
                    int age = int.Parse(commandArgs[1]);
                    string humanId = commandArgs[2];

                    citizen = new Human(humanId, name, age);
                }
                else
                {
                    string model = commandArgs[0];
                    string robotId = commandArgs[1];

                    citizen = new Robot(robotId, model);
                }

                
                city.Citizens.Add(citizen);
            }

            string end = Console.ReadLine();

            city.Detained(end);

            Console.WriteLine(city);
        }
    }
}
