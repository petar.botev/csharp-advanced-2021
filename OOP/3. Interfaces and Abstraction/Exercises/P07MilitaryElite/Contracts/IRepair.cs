﻿namespace P07MilitaryElite.Models
{
    public interface IRepair
    {
        int HoursWorked { get;}
        string PartName { get; }
    }
}