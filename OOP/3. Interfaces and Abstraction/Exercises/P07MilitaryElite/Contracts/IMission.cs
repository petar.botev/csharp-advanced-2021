﻿using P07MilitaryElite.Enums;

namespace P07MilitaryElite.Models
{
    public interface IMission
    {
        string CodeName { get; }
        Missions State { get; }

        void CompleteMission();
    }
}