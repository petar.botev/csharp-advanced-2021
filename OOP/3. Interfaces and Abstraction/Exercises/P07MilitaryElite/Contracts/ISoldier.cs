﻿namespace P07MilitaryElite.Models
{
    public interface ISoldier
    {
        string FirstName { get; set; }
        int Id { get; set; }
        string LastName { get; set; }
    }
}