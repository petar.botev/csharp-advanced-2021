﻿namespace P07MilitaryElite.Models
{
    public interface IPrivate : ISoldier
    {
        decimal Salary { get; }
    }
}