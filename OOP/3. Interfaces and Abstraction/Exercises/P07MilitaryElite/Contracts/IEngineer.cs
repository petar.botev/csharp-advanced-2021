﻿using System.Collections.Generic;

namespace P07MilitaryElite.Models
{
    public interface IEngineer : ISpecialisedSoldier
    {
        List<IRepair> Repairs { get; }
    }
}