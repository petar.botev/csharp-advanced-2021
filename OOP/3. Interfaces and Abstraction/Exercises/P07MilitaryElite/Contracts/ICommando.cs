﻿using System.Collections.Generic;

namespace P07MilitaryElite.Models
{
    public interface ICommando : ISpecialisedSoldier
    {
        List<IMission> Missions { get; }
               
    }
}