﻿using System.Collections.Generic;

namespace P07MilitaryElite.Models
{
    public interface ILieutenantGeneral : IPrivate
    {
        List<ISoldier> Soldiers { get; }
    }
}