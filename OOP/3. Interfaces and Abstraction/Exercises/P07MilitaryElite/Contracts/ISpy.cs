﻿namespace P07MilitaryElite.Models
{
    public interface ISpy : ISoldier
    {
        int CodeNumber { get; set; }
    }
}