﻿using P07MilitaryElite.Enums;

namespace P07MilitaryElite.Models
{
    public interface ISpecialisedSoldier : IPrivate
    {
        Corps Corps { get; }
    }
}