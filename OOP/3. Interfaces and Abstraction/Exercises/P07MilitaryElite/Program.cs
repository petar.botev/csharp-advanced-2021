﻿using P07MilitaryElite.Core;
using System;

namespace P07MilitaryElite
{
    public class Program
    {
        static void Main()
        {
            CommandInterpreter interpreter = new CommandInterpreter();
            IEngine engine = new Engine(interpreter);
            engine.Run();
        }
    }
}
