﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P07MilitaryElite.Models
{
    public class LieutenantGeneral : Private, ILieutenantGeneral
    {
        public LieutenantGeneral(int id, string firstName, string lastName, decimal salary, List<ISoldier> soldiers)
            : base(id, firstName, lastName, salary)
        {
            this.Soldiers = soldiers;

        }

        public List<ISoldier> Soldiers { get; set; }

        public override string ToString()
        {
            StringBuilder strb = new StringBuilder();

            strb.AppendLine(base.ToString());
            strb.AppendLine("Privates:");

            foreach (var soldier in Soldiers)
            {
                strb.AppendLine($"  {soldier}");
            }

            return strb.ToString().Trim();
        }
    }
}
