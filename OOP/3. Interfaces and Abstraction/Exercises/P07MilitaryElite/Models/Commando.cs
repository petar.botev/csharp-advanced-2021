﻿using P07MilitaryElite.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace P07MilitaryElite.Models
{
    public class Commando : SpecialisedSoldier, ICommando
    {
        public Commando(int id, string firstName, string lastName, decimal salary, Corps corps, List<IMission> missions)
            : base(id, firstName, lastName, salary, corps)
        {
            this.Missions = missions;
        }

        public List<IMission> Missions { get; set; }
               
        public override string ToString()
        {
            StringBuilder strb = new StringBuilder();
            strb.AppendLine(base.ToString());

            strb.AppendLine($"Corps: {this.Corps}");
            strb.AppendLine("Missions:");

            foreach (var mission in Missions)
            {
                strb.AppendLine(mission.ToString());
            }

            return strb.ToString().Trim();
        }
    }
}
