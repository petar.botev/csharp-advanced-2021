﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P07MilitaryElite.Models
{
    public class Spy : Soldier, ISpy
    {
        public Spy(int id, string firstName, string lastName)
            : base(id, firstName, lastName)
        {
        }

        public int CodeNumber { get; set; }

        public override string ToString()
        {
            return $"Name: {this.FirstName} {this.LastName} Id: {this.Id}{Environment.NewLine}Code Number: {this.CodeNumber}";
        }
    }
}
