﻿using P07MilitaryElite.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace P07MilitaryElite.Models
{
    public class Engineer : SpecialisedSoldier, IEngineer
    {
        public Engineer(int id, string firstName, string lastName, decimal salary, Corps corps, List<IRepair> repairs)
            : base(id, firstName, lastName, salary, corps)
        {
            this.Repairs = repairs;
        }

        public List<IRepair> Repairs { get; }

        public override string ToString()
        {
            StringBuilder strb = new StringBuilder();
            strb.AppendLine(base.ToString());

            strb.AppendLine($"Corps: {this.Corps}");
            strb.AppendLine("Repairs:");

            foreach (var repair in Repairs)
            {
                strb.AppendLine($"  {repair.ToString()}");
            }

            return strb.ToString().Trim();
        }
    }
}
