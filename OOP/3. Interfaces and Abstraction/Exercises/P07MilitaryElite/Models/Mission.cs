﻿using P07MilitaryElite.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace P07MilitaryElite.Models
{
    public class Mission : IMission
    {

        public Mission(string codeName, Missions state)
        {
            CodeName = codeName;
            State = state;
        }

        public string CodeName { get; }

        public Missions State
        { 
            get;
            private set;
        }

        public void CompleteMission()
        {
            this.State = Missions.Finished;
        }

        public override string ToString()
        {
            return $"  Code Name: {this.CodeName} State: {this.State}";
        }
    }
}
