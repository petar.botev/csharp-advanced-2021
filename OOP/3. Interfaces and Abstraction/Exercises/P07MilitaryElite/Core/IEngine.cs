﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P07MilitaryElite.Core
{
    public interface IEngine
    {
        void Run();
    }
}
