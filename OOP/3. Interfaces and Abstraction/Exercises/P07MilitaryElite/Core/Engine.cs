﻿using System;

namespace P07MilitaryElite.Core
{
    public class Engine : IEngine
    {
        private CommandInterpreter interpreter;

        public Engine(CommandInterpreter interpreter)
        {
            this.interpreter = interpreter;
        }
        
        public void Run()
        {
            string command;
            while ((command = Console.ReadLine()) != "End")
            {
                string[] commandArgs = command
                    .Split(' ');

                string result = string.Empty;

                try
                {
                    interpreter.Read(commandArgs);
                }
                catch (Exception)
                {

                    
                }
            }

            foreach (var item in interpreter.Soldiers)
            {
                Console.WriteLine(item.ToString());
            }
        }
    }
}
