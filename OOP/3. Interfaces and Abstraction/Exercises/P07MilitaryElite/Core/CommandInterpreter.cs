﻿using P07MilitaryElite.Enums;
using P07MilitaryElite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P07MilitaryElite.Core
{
    public class CommandInterpreter : ICommandInterpreter
    {
        private readonly ICollection<ISoldier> soldiers;

        public CommandInterpreter()
        {
            this.soldiers = new List<ISoldier>();
        }

        public ICollection<ISoldier> Soldiers => soldiers;

        public void Read(string[] args)
        {

            int id = int.Parse(args[1]);
            string firstName = args[2];
            string lastName = args[3];
            decimal salary = 0;
            bool isValid = true;

            Private privateSoldier = null;

            switch (args[0])
            {
                case "Private":

                    salary = decimal.Parse(args[4]);

                    privateSoldier = new Private(id, firstName, lastName, salary);
                    soldiers.Add(privateSoldier);

                    //return privateSoldier.ToString();
                    break;

                case "LieutenantGeneral":

                    salary = decimal.Parse(args[4]);

                   

                    List<ISoldier> privates = new List<ISoldier>();

                    for (int i = 5; i < args.Length; i++)
                    {
                        ISoldier currentPrivate = Soldiers.FirstOrDefault(x => x.Id == int.Parse(args[i]));

                        privates.Add(currentPrivate);
                    }

                    ILieutenantGeneral lieutenantGeneral = new LieutenantGeneral(id, firstName, lastName, salary, privates);
                    soldiers.Add(lieutenantGeneral);
                    //return lieutenantGeneral.ToString();
                    break;

                case "Engineer":

                    salary = decimal.Parse(args[4]);
                    isValid = Enum.TryParse<Corps>(args[5], out Corps corps);

                    if (!isValid)
                    {
                        throw new ArgumentException("Corps is invalid");
                    }

                    List<IRepair> repairs = new List<IRepair>();

                    for (int i = 6; i < args.Length; i += 2)
                    {
                        string partName = args[i];
                        int hoursWorked = int.Parse(args[i + 1]);

                        IRepair repair = new Repair(partName, hoursWorked);

                        repairs.Add(repair);
                    }

                    IEngineer engineer = new Engineer(id, firstName, lastName, salary, corps, repairs);
                    soldiers.Add(engineer);

                    //return engineer.ToString();
                    break;

                case "Commando":

                    salary = decimal.Parse(args[4]);
                    isValid = Enum.TryParse<Corps>(args[5], out Corps corps2);

                    if (!isValid)
                    {
                        throw new ArgumentException("Invalid corps");
                    }

                    List<IMission> missions = new List<IMission>();

                    for (int i = 6; i < args.Length; i += 2)
                    {
                        string missionCode = args[i];
                        string missionState = args[i + 1];

                        //bool isValidMissionState = Enum.TryParse<Missions>(missionState, out Missions newMission);


                        if (missionState != Missions.inProgress.ToString())
                        {
                            continue;
                        }

                        IMission mission = new Mission(missionCode, Missions.inProgress);

                        missions.Add(mission);
                    }

                    ICommando commando = new Commando(id, firstName, lastName, salary, corps2, missions);
                    soldiers.Add(commando);
                    //return commando.ToString();
                    break;

                case "Spy":

                    int codeNumber = int.Parse(args[4]);

                    ISpy spy = new Spy(id, firstName, lastName)
                    {
                        CodeNumber = codeNumber
                    };

                    soldiers.Add(spy);
                    //return spy.ToString();
                    break;
            }
        }
    }
}
