﻿using P07MilitaryElite.Models;
using System.Collections.Generic;

namespace P07MilitaryElite.Core
{
    public interface ICommandInterpreter
    {
       void Read(string[] args);
    }
}
