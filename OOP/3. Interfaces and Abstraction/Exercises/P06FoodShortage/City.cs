﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P06FoodShortage
{
    public class City 
    {
        public City()
        {
            Birthdays = new List<DateTime>();
            SpecialBirthdates = new List<DateTime>();
        }

        public List<DateTime> Birthdays { get; set; }
        public List<DateTime> SpecialBirthdates { get; set; }

        public void SpecialBirthdays(string end)
        {
            SpecialBirthdates = Birthdays.Where(x => x.Year.ToString() == end).ToList();
        }

        public override string ToString()
        {
           
            return $"{string.Join(Environment.NewLine, SpecialBirthdates.Select(x => x.ToString("dd/MM/yyyy")))}";
        }
    }
}
