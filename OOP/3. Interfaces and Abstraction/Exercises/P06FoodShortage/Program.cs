﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace P06FoodShortage
{
    class Program
    {
        static void Main()
        {
            int numberOfPeople = int.Parse(Console.ReadLine());

            List<IBuyer> buyers = new List<IBuyer>();

            for (int i = 0; i < numberOfPeople; i++)
            {
                string[] commandArgs = Console.ReadLine()
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                IBuyer buyer = null;

                if (commandArgs.Length == 4)
                {
                    string name = commandArgs[0];
                    int age = int.Parse(commandArgs[1]);
                    string id = commandArgs[2];
                    DateTime birthday = DateTime.ParseExact(commandArgs[3], "dd/MM/yyyy", null);

                    buyer = new Human(id, name, age, birthday);
                                       
                    buyers.Add(buyer);
                }
                else
                {
                    string name = commandArgs[0];
                    int age = int.Parse(commandArgs[1]);
                    string group = commandArgs[2];

                    buyer = new Rebel(name, age, group);

                    buyers.Add(buyer);
                }
            }

            string commandName;
            while ((commandName = Console.ReadLine()) != "End")
            {
                IBuyer currentBuyer = buyers.FirstOrDefault(x => x.Name == commandName);
                if (currentBuyer != null)
                {
                    currentBuyer.BuyFood();
                }
            }

            Console.WriteLine(buyers.Sum(x => x.Food));
        }
    }
}
