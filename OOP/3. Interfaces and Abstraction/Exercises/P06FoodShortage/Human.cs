﻿using System;

namespace P06FoodShortage
{
    public class Human : Citizen, IBuyer
    {
        public Human(string id, string name, int age, DateTime birthday) 
            : base(id, name, age, birthday)
        {
        }

        public int Food { get; set; }

        public void BuyFood()
        {
            Food += 10;
        }
    }
}
