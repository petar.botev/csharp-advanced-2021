﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P06FoodShortage
{
    public class Citizen
    {

        public Citizen(string model, string id)
        {
            this.Model = model;
            this.Id = id;
        }

        public Citizen(string name, DateTime birthdate)
        {
            this.Name = name;
            this.Birthdate = birthdate;
        }

        public Citizen(string id, string name, int age, DateTime birthdate)
        {
            this.Id = id;
            this.Name = name;
            this.Age = age;
            this.Birthdate = birthdate;
        }

        public string Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string Model { get; set; }
        public DateTime Birthdate { get; set; }

    }
}
