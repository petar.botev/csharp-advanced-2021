﻿using System;
using System.Linq;

namespace P03Telephony
{
    public class StationaryPhone : ICalling
    {
        public string Calling(string number)
        {
            if (number.Any(x => !char.IsDigit(x)))
            {
                throw new ArgumentException("Invalid number!");
            }

            return $"Dialing... {number}";
        }
    }
}
