﻿using System;

namespace P03Telephony
{
    public class StartUp
    {
        public static void Main()
        {
            string[] phoneNumbers = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries);

            string[] sites = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries);

            ICalling call;
            IBrowsing browse;

            foreach (var phone in phoneNumbers)
            {
                try
                {
                    if (phone.Length == 7)
                    {
                        call = new StationaryPhone();
                        Console.WriteLine(call.Calling(phone));
                    }
                    else
                    {
                        call = new Smartphone();
                        Console.WriteLine(call.Calling(phone));
                    }
                }
                catch (ArgumentException ae)
                {

                    Console.WriteLine(ae.Message);
                }
            }

            foreach (var site in sites)
            {
                try
                {
                    browse = new Smartphone();
                    Console.WriteLine(browse.Browsing(site));
                }
                catch (ArgumentException ae)
                {

                    Console.WriteLine(ae.Message);
                }
            }
        }
    }
}
