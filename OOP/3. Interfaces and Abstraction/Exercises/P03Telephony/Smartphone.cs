﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P03Telephony
{
    public class Smartphone : ICalling, IBrowsing
    {
        public string Browsing(string site)
        {
            if (site.Any(x => char.IsDigit(x)))
            {
                throw new ArgumentException("Invalid URL!");
            }

            return $"Browsing: {site}!";
        }

        public string Calling(string number)
        {
            if (number.Any(x => !char.IsDigit(x)))
            {
                throw new ArgumentException("Invalid number!");
            }

            return $"Calling... {number}";
        }
    }
}
