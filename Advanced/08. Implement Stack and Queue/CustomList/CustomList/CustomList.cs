﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomList
{
    public class CustomList
    {
        private const int INITIAL_CAPACITY = 2;

        private int[] items;

        public CustomList()
        {
            this.items = new int[INITIAL_CAPACITY];
        }

        public int Count { get; private set; } 

        public int this[int index] 
        {
            get 
            {
                if (index >= Count)
                {
                    throw new ArgumentOutOfRangeException();
                }
                return items[index];
            }
            set 
            {
                if (index >= Count)
                {
                    throw new ArgumentOutOfRangeException();
                }
                items[index] = value;
            }
        }


        public void RemoveAt(int index)
        {
            if (index >= Count)
            {
                throw new ArgumentOutOfRangeException();
            }

            Shift(index);
            items[items.Length - 1] = default(int);
            Count--;

            if (items.Length / 2 > Count)
            {
                Shrink();
            }
        }

        private void Shrink()
        {
            int[] smallerArr = new int[Count];

            for (int i = 0; i < Count; i++)
            {
                smallerArr[i] = items[i];
            }

            items = smallerArr;
        }

        private void Shift(int index)
        {
            for (int i = index; i < items.Length - 1; i++)
            {
                items[i] = items[i + 1];
            }
        }

        public void Add(int element)
        {
            if (items.Length == Count)
            {
                Resize();
            }

            items[Count] = element;
            Count++;
        }

        public int[] Resize()
        {
            int[] newArray = new int[items.Length * 2];

            if (Count == items.Length)
            {
                for (int i = 0; i < items.Length; i++)
                {
                    newArray[i] = items[i];
                }

                items = newArray;
            }

            return items;
        }
    }
}
