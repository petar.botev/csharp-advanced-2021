﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P06SpeedRacing
{
    class Car
    {
        private string model;
        private double fuelAmount;
        private double fuelConsuptionPerKm;
        private double distanceTravelled;

        public Car(string model, double fuelAmount, double fuelConsuptionPerKm, double distanceTravelled)
        {
            this.Model = model;
            this.FuelAmount = fuelAmount;
            this.FuelConsuptionPerKm = fuelConsuptionPerKm;
            this.DistanceTravelled = distanceTravelled;
        }

        public string Model { get => model; set => model = value; }
        public double FuelAmount { get => fuelAmount; set => fuelAmount = value; }
        public double FuelConsuptionPerKm { get => fuelConsuptionPerKm; set => fuelConsuptionPerKm = value; }
        public double DistanceTravelled { get => distanceTravelled; set => distanceTravelled = value; }

        public void MoveTheCar(double kilomenters)
        {
            if ((kilomenters * FuelConsuptionPerKm) <= FuelAmount)
            {
                FuelAmount -= kilomenters * FuelConsuptionPerKm;
                DistanceTravelled += kilomenters;
            }
            else
            {
                Console.WriteLine("Insufficient fuel for the drive");
            }
        }


        public override string ToString()
        {
            return $"{Model} {FuelAmount:f2} {DistanceTravelled}";
        }
    }
}
