﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace P06SpeedRacing
{
    public class StartUp
    {
        static void Main()
        {
            int numberOfCars = int.Parse(Console.ReadLine());

            List<Car> cars = new List<Car>();

            for (int i = 0; i < numberOfCars; i++)
            {
                string[] carArgs = Console.ReadLine()
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                string model = carArgs[0];
                double fuelAmount = double.Parse(carArgs[1]);
                double fuelConsuptionPerKm = double.Parse(carArgs[2]);

                Car car = new Car(model, fuelAmount, fuelConsuptionPerKm, 0);

                cars.Add(car);
            }

            string command;
            while ((command = Console.ReadLine()) != "End")
            {
                string[] commandArgs = command
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                string commandName = commandArgs[0];
                string carModel =commandArgs[1];
                double kilometers = double.Parse(commandArgs[2]);

                Car car;
                car = cars.FirstOrDefault(x => x.Model == carModel);

                car.MoveTheCar(kilometers);
            }

            foreach (var car in cars)
            {
                Console.WriteLine(car.ToString());
            }
        }
    }
}
