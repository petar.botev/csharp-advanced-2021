﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SoftUniParking
{
    class Parking
    {
        private List<Car> cars;
        private int capacity;
        
        public Parking(int capacity)
        {
            this.Cars = new List<Car>();
            this.Capacity = capacity;
        }

        public int Capacity { get => capacity; set => capacity = value; }
        public List<Car> Cars { get => cars; set => cars = value; }
        public int Count => this.cars.Count;

        public string AddCar(Car car)
        {
            if (cars.Any(x => x.RegistrationNumber == car.RegistrationNumber))
            {
                return "Car with that registration number, already exists!";
            }

            if (Capacity == Cars.Count)
            {
                return "Parking is full!";
            }
            
                cars.Add(car);
                
                return $"Successfully added new car {car.Make} {car.RegistrationNumber}";
        }

        public string RemoveCar(string registrationNumber)
        {
            Car carToRemove = cars.FirstOrDefault(x => x.RegistrationNumber == registrationNumber);

            if (carToRemove != null)
            {
                cars.Remove(carToRemove);
               
                return $"Successfully removed {registrationNumber}";
            }
            else
            {
                return "Car with that registration number, doesn't exist!";
            }
        }

        public Car GetCar(string registrationNumber)
        {
            return cars.FirstOrDefault(x => x.RegistrationNumber == registrationNumber);
        }

        public void RemoveSetOfRegistrationNumber(List<string> registrationNumbers)
        {
            //foreach (var number in registrationNumbers)
            //{
            //    if (cars.Any(x => x.RegistrationNumber == number))
            //    {
            //        Car carToremove = cars.FirstOrDefault(x => x.RegistrationNumber == number);
            //        cars.Remove(carToremove);
            //    }
            //}

            cars = cars
                .Where(x => !registrationNumbers.Contains(x.RegistrationNumber))
                .ToList();
        }

    }
}
