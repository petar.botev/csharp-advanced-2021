﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P05DateModifier
{
    class DateModifier
    {
        private int dateDifference;

        public int DateDifference { get; set; }

        public double CalculateDateDifference(string firstDate, string secondDate)
        {
            DateTime firstD = DateTime.Parse(firstDate);
            DateTime secondD = DateTime.Parse(secondDate);

            return (secondD - firstD).TotalDays;
        }
    }
}
