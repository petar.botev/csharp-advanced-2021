﻿using System;

namespace P05DateModifier
{
    public class StartUp
    {
        static void Main()
        {
            string firstDate = Console.ReadLine();
            string secondDate = Console.ReadLine();

            DateModifier dateModifier = new DateModifier();
            
            Console.WriteLine(Math.Abs(dateModifier.CalculateDateDifference(firstDate, secondDate)));
        }
    }
}
