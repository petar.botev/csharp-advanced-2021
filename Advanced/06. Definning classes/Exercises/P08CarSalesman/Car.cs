﻿using System;

namespace P08CarSalesman
{
    class Car
    {
        private string model;
        private Engine engine;
        private object weight;
        private string color;

        public Car(string model, Engine engine)
        {
            this.Model = model;
            this.Engine = engine;
            this.Weight = "n/a";
            this.Color = "n/a";
        }

        public Car(string model, Engine engine, int weight)
            : this(model, engine)
        {
            this.Weight = weight;
        }

        public Car(string model, Engine engine, string color)
            : this(model, engine)
        {
            this.Color = color;
        }

        public Car(string model, Engine engine, int weight, string color)
            : this(model, engine, weight)
        {
            this.Color = color;
        }

        public string Model { get => model; set => model = value; }
        public object Weight
        {
            get => weight; set => weight = value;
        }

        public string Color { get => color; set => color = value; }
        internal Engine Engine { get => engine; set => engine = value; }

        public override string ToString()
        {
            return $"{Model}:{Environment.NewLine}  {Engine.Model}:{Environment.NewLine}    Power: {Engine.Power}{Environment.NewLine}    Displacement: {Engine.Displacement}{Environment.NewLine}    Efficiency: {Engine.Efficiency}{Environment.NewLine}  Weight: {Weight}{Environment.NewLine}  Color: {Color}";
        }
    }
}
