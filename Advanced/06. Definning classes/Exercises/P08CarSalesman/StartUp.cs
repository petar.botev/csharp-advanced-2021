﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace P08CarSalesman
{
    public class StartUp
    {
        static void Main()
        {
            int engineNumber = int.Parse(Console.ReadLine());

            List<Engine> engines = new List<Engine>();
            List<Car> cars = new List<Car>();

            for (int i = 0; i < engineNumber; i++)
            {
                string[] engineArgs = Console.ReadLine()
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                string engineModel = engineArgs[0];
                int power = int.Parse(engineArgs[1]);
                int displacement;
                string efficiency;

                Engine engine = null;

                if (engineArgs.Length == 2)
                {
                    engine = new Engine(engineModel, power);
                }

                if (engineArgs.Length == 3)
                {
                    if (int.TryParse(engineArgs[2], out int res))
                    {
                        displacement = int.Parse(engineArgs[2]);
                        engine = new Engine(engineModel, power, displacement);
                    }
                    else
                    {
                        efficiency = engineArgs[2];
                        engine = new Engine(engineModel, power, efficiency);
                    }
                }

                if (engineArgs.Length == 4)
                {
                    displacement = int.Parse(engineArgs[2]);
                    efficiency = engineArgs[3];

                    engine = new Engine(engineModel, power, displacement, efficiency);
                }

                engines.Add(engine);
            }

            int carNumber = int.Parse(Console.ReadLine());

            for (int i = 0; i < carNumber; i++)
            {
                string[] carArgs = Console.ReadLine()
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                string carModel = carArgs[0];
                Engine carEngine = engines.FirstOrDefault(x => x.Model == carArgs[1]);
                int weight;
                string color;

                Car car = null;

                if (carArgs.Length == 2)
                {
                    car = new Car(carModel, carEngine);
                }

                if (carArgs.Length == 3)
                {
                    if (int.TryParse(carArgs[2], out int res))
                    {
                        weight = int.Parse(carArgs[2]);
                        car = new Car(carModel, carEngine, weight);
                    }
                    else
                    {
                        color = carArgs[2];
                        car = new Car(carModel, carEngine, color);
                    }
                }

                if (carArgs.Length == 4)
                {
                    weight = int.Parse(carArgs[2]);
                    color = carArgs[3];

                    car = new Car(carModel, carEngine, weight, color);
                }

                cars.Add(car);
            }

            foreach (var car in cars)
            {
                Console.WriteLine(car.ToString());
            }
        }
    }
}
