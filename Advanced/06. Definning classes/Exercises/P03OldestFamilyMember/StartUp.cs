﻿using System;

namespace DefiningClasses
{
    public class StartUp
    {
        static void Main()
        {
            int numberPeople = int.Parse(Console.ReadLine());

            Family family = new Family();

            for (int i = 0; i < numberPeople; i++)
            {
                string[] personArgs = Console.ReadLine()
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                Person person = new Person(int.Parse(personArgs[1]), personArgs[0]);

                family.AddMember(person);
            }

            Person oldestPerson = family.GetOldestFamilyMember();

            Console.WriteLine($"{oldestPerson.Name} {oldestPerson.Age}");
        }
    }
}
