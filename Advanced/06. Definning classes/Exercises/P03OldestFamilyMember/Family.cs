﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DefiningClasses
{
    public class Family
    {
        private List<Person> people;

        public Family()
        {
            this.People = new List<Person>();
        }

        public List<Person> People { get => people; set => people = value; }

        public void AddMember(Person member)
        {
            People.Add(member);
        }

        public Person GetOldestFamilyMember()
        {
            int maxAge = People.Max(x => x.Age);
            Person person = People.FirstOrDefault(x => x.Age == maxAge);

            return person;
        }
    }
}
