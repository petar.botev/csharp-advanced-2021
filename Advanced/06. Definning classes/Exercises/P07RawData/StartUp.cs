﻿using P07RawData.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace P07RawData
{
    public class StartUp
    {
        static void Main()
        {
            int numberOfCars = int.Parse(Console.ReadLine());

            List<Car> cars = new List<Car>();

            for (int i = 0; i < numberOfCars; i++)
            {
                string[] carArgs = Console.ReadLine()
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                string model = carArgs[0];
                int engineSpeed = int.Parse(carArgs[1]);
                int enginePower = int.Parse(carArgs[2]);
                double cargoWeight = double.Parse(carArgs[3]);
                string cargoType = carArgs[4];
                double tire1Pressure = double.Parse(carArgs[5]);
                int tire1Age = int.Parse(carArgs[6]);
                double tire2Pressure = double.Parse(carArgs[7]);
                int tire2Age = int.Parse(carArgs[8]);
                double tire3Pressure = double.Parse(carArgs[9]);
                int tire3Age = int.Parse(carArgs[10]);
                double tire4Pressure = double.Parse(carArgs[11]);
                int tire4Age = int.Parse(carArgs[12]);

                Engine engine = new Engine(engineSpeed, enginePower);
                Cargo cargo = new Cargo(cargoType, cargoWeight);

                Tire tire1 = new Tire(tire1Pressure, tire1Age);
                Tire tire2 = new Tire(tire2Pressure, tire2Age);
                Tire tire3 = new Tire(tire3Pressure, tire3Age);
                Tire tire4 = new Tire(tire4Pressure, tire4Age);

                Tire[] tires = new Tire[] { tire1, tire2, tire3, tire4 };

                Car car = new Car(model, engine, cargo, tires);

                cars.Add(car);

            }


            string cargoCommand = Console.ReadLine();

            if (cargoCommand == "fragile")
            {
                cars = cars
                    .Where(x => x.Cargo.CargoType == cargoCommand)
                    .Where(x => x.Tires.Any(x => x.Pressure < 1))
                    .ToList();

            }
            else
            {
                cars = cars
                    .Where(x => x.Cargo.CargoType == cargoCommand)
                    .Where(x => x.Engine.EnginePower > 250)
                    .ToList();
            }

            foreach (var car in cars)
            {
                Console.WriteLine(car.Model);
            }
        }
    }
}
