﻿namespace P07RawData.Models
{
    class Car
    {
        private string model;
        private Engine engine;
        private Cargo cargo;
        private Tire[] tires;

        public Car(string model, Engine engine, Cargo cargo, Tire[] tires)
        {
            this.Model = model;
            this.Engine = engine;
            this.Cargo = cargo;
            this.Tires = tires;
        }

        public string Model { get => model; set => model = value; }
        internal Engine Engine { get => engine; set => engine = value; }
        internal Cargo Cargo { get => cargo; set => cargo = value; }
        internal Tire[] Tires { get => tires; set => tires = value; }
    }
}
