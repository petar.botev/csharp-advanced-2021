﻿namespace P07RawData.Models
{
    class Cargo
    {
        private string cargoType;
        private double cargoWeight;

        public Cargo(string cargoType, double cargoWeight)
        {
            this.CargoType = cargoType;
            this.CargoWeight = cargoWeight;
        }

        public string CargoType { get => cargoType; set => cargoType = value; }
        public double CargoWeight { get => cargoWeight; set => cargoWeight = value; }
    }
}
