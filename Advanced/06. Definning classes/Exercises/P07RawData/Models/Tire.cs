﻿namespace P07RawData.Models
{
    class Tire
    {
        private double pressure;
        private int tireAge;

        public Tire(double pressure, int tireAge)
        {
            this.Pressure = pressure;
            this.TireAge = tireAge;
        }

        public double Pressure { get => pressure; set => pressure = value; }
        public int TireAge { get => tireAge; set => tireAge = value; }
    }
}
