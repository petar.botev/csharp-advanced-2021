﻿using System.Collections.Generic;

namespace P09PokemonTrainer
{
    class Trainer
    {
        private string name;
        private int badges;
        private List<Pokemon> pokemons;

        public Trainer(string name)
        {
            this.Name = name;
            this.Badges = 0;
            this.Pokemons = new List<Pokemon>();
        }

        public string Name { get => name; set => name = value; }
        public int Badges { get => badges; set => badges = value; }
        internal List<Pokemon> Pokemons { get => pokemons; set => pokemons = value; }


        public override string ToString()
        {
            return $"{Name} {Badges} {Pokemons.Count}";
        }
    }
}
