﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace P09PokemonTrainer
{
    public class StartUp
    {
        static void Main()
        {
            List<Trainer> trainers = new List<Trainer>();

            Trainer trainer = null;

            string tournamentCommand;
            while ((tournamentCommand = Console.ReadLine()) != "Tournament")
            {
                string[] tournamentCommandArgs = tournamentCommand
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                string trainerName = tournamentCommandArgs[0];
                string pokemonName = tournamentCommandArgs[1];
                string pokemonElement = tournamentCommandArgs[2];
                int pokemonHealth = int.Parse(tournamentCommandArgs[3]);

                Pokemon pokemon = new Pokemon(pokemonName, pokemonElement, pokemonHealth);

                if (trainers.Any(x => x.Name == trainerName))
                {
                    trainer = trainers.FirstOrDefault(x => x.Name == trainerName);
                }
                else
                {
                    trainer = new Trainer(trainerName);
                    trainers.Add(trainer);
                }

                trainer.Pokemons.Add(pokemon);
            }

            string command;
            while ((command = Console.ReadLine()) != "End")
            {
                foreach (var item in trainers)
                {
                    if (item.Pokemons.Any(x => x.Element == command))
                    {
                        item.Badges++;
                    }
                    else
                    {
                        List<Pokemon> toRemove = new List<Pokemon>();

                        foreach (var pokemon in item.Pokemons)
                        {
                            pokemon.Health -= 10;

                            if (pokemon.Health <= 0)
                            {
                                toRemove.Add(pokemon);
                            }
                        }

                        foreach (var pokemonToRemove in toRemove)
                        {
                            item.Pokemons.Remove(pokemonToRemove);
                        }
                    }
                }
            }

            trainers = trainers.OrderByDescending(x => x.Badges).ToList();

            foreach (var trainerItem in trainers)
            {
                Console.WriteLine(trainerItem.ToString());
            }
        }
    }
}
