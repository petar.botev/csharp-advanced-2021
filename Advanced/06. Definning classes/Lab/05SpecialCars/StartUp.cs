﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CarManufacturer
{
    public class StartUp
    {
        static void Main()
        {
            List<Tire[]> tires = new List<Tire[]>();
            List<Engine> engines = new List<Engine>();
            List<Car> cars = new List<Car>();

            string createTires;

            while ((createTires = Console.ReadLine()) != "No more tires")
            {
                string[] tiresArgs = createTires
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                int year1 = int.Parse(tiresArgs[0]);
                double pressure1 = double.Parse(tiresArgs[1]);
                int year2 = int.Parse(tiresArgs[2]);
                double pressure2 = double.Parse(tiresArgs[3]);
                int year3 = int.Parse(tiresArgs[4]);
                double pressure3 = double.Parse(tiresArgs[5]);
                int year4 = int.Parse(tiresArgs[6]);
                double pressure4 = double.Parse(tiresArgs[7]);

                Tire tire1 = new Tire(year1, pressure1);
                Tire tire2 = new Tire(year2, pressure2);
                Tire tire3 = new Tire(year3, pressure3);
                Tire tire4 = new Tire(year4, pressure4);

                tires.Add(new Tire[] { tire1, tire2, tire3, tire4});
            }

            string createEngines;
            while ((createEngines = Console.ReadLine()) != "Engines done")
            {
                string[] createEnginesArgs = createEngines
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                int horsePower = int.Parse(createEnginesArgs[0]);
                double cubicCapacity = double.Parse(createEnginesArgs[1]);

                Engine engine = new Engine(horsePower, cubicCapacity);

                engines.Add(engine);
            }

            string createCars;
            while ((createCars = Console.ReadLine()) != "Show special")
            {
                string[] createCarsArgs = createCars
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                string carMake = createCarsArgs[0];
                string carModel = createCarsArgs[1];
                int carYear = int.Parse(createCarsArgs[2]);
                double fuelQuantity = double.Parse(createCarsArgs[3]);
                double fuelConsumption = double.Parse(createCarsArgs[4]);
                int engineIndex = int.Parse(createCarsArgs[5]);
                int tireIndex = int.Parse(createCarsArgs[6]);

                Car car = new Car(carMake, carModel, carYear, fuelQuantity, fuelConsumption, engines[engineIndex], tires[tireIndex]);

                cars.Add(car);
            }

            cars = cars
                .Where(x => x.Year >= 2017)
                .Where(x => x.Engine.HorsePower > 330)
                .Where(x => x.Tires.Sum(x => x.Pressure) >= 9 && x.Tires.Sum(x => x.Pressure) <= 10)
                .ToList();

            foreach (var car in cars)
            {
                double usedFuel = (car.FuelConsumption / 100) * 20;
                car.FuelQuantity -= usedFuel;

                Console.WriteLine($"Make: {car.Make}\nModel: {car.Model}\nYear: {car.Year}\nHorsePowers: {car.Engine.HorsePower}\nFuelQuantity: {car.FuelQuantity}");
            }
        }
    }
}
