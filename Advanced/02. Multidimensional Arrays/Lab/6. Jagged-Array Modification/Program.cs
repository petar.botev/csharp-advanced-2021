﻿using System;
using System.Linq;

namespace _6._Jagged_Array_Modification
{
    class Program
    {
        static void Main()
        {
            int size = int.Parse(Console.ReadLine());

            int[][] matrix = new int[size][];

            for (int i = 0; i < size; i++)
            {
                int[] rowValues = Console.ReadLine()
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                    .Select(int.Parse)
                    .ToArray();

                matrix[i] = rowValues;
            }

            string command;
            while ((command = Console.ReadLine()) != "END")
            {
                string[] commandArgs = command
                    .Split();

                switch (commandArgs[0])
                {
                    case "Add":

                        int rowIndex = int.Parse(commandArgs[1]);
                        int colIndex = int.Parse(commandArgs[2]);
                        int valueToAdd = int.Parse(commandArgs[3]);

                        if ((rowIndex >= size || colIndex >= size) || (rowIndex < 0 || colIndex < 0))
                        {
                            Console.WriteLine("Invalid coordinates");
                            
                        }
                        else
                        {
                            matrix[rowIndex][colIndex] += valueToAdd;
                        }

                        break;
                    case "Subtract":

                        int rowIndexS = int.Parse(commandArgs[1]);
                        int colIndexS = int.Parse(commandArgs[2]);
                        int valueToSubtract = int.Parse(commandArgs[3]);

                        if ((rowIndexS >= size || colIndexS >= size) || (rowIndexS < 0 || colIndexS < 0))
                        {
                            Console.WriteLine("Invalid coordinates");
                            
                        }
                        else
                        {
                            matrix[rowIndexS][colIndexS] -= valueToSubtract;

                        }

                        break;
                    default:
                        break;
                }
            }

            for (int i = 0; i < size; i++)
            {
                Console.WriteLine(string.Join(' ', matrix[i]));
            }
        }
    }
}
