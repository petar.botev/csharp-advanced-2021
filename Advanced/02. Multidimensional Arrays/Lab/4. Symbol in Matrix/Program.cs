﻿using System;

namespace _4._Symbol_in_Matrix
{
    class Program
    {
        static void Main()
        {
            int size = int.Parse(Console.ReadLine());

            char[][] matrix = new char[size][];

            for (int i = 0; i < size; i++)
            {
                char[] arr = Console.ReadLine().ToCharArray();

                matrix[i] = arr;
            }

            char compare = char.Parse(Console.ReadLine());

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (compare == matrix[i][j])
                    {
                        Console.WriteLine($"({i}, {j})");
                        return;
                    }
                }
            }

            Console.WriteLine($"{compare} does not occur in the matrix");
        }
    }
}
