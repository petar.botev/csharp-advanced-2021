﻿using System;
using System.Linq;

namespace _5._Square_With_Maximum_Sum
{
    class Program
    {
        static void Main()
        {
            int[] matrixSize = Console.ReadLine()
                .Split(", ", StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray();

            int[][] matrix = new int[matrixSize[0]][];

            for (int i = 0; i < matrixSize[0]; i++)
            {
                matrix[i] = Console.ReadLine()
                    .Split(", ", StringSplitOptions.RemoveEmptyEntries)
                    .Select(int.Parse)
                    .ToArray();
            }

            int maxSum = 0;
            int sum = 0;
            int firstRow = -1;
            int firstCol = -1;

            for (int i = 0; i < matrixSize[0] - 1; i++)
            {
                for (int j = 0; j < matrixSize[1] - 1; j++)
                {
                    for (int k = i; k < i + 2; k++)
                    {
                        for (int l = j; l < j + 2; l++)
                        {
                            sum += matrix[k][l];
                        }
                    }

                    if (sum > maxSum)
                    {
                        firstRow = i;
                        firstCol = j;
                        maxSum = sum;
                    }

                    sum = 0;
                }
            }

            for (int i = firstRow; i < firstRow + 2; i++)
            {
                for (int j = firstCol; j < firstCol + 2; j++)
                {
                    Console.Write($"{matrix[i][j]} ");
                }
                Console.WriteLine();
            }

            Console.WriteLine(maxSum);
        }
    }
}
