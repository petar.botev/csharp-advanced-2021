﻿using System;

namespace _7._Pascal_Triangle
{
    class Program
    {
        static void Main()
        {
            int size = int.Parse(Console.ReadLine());

            long[][] matrix = new long[size][];

            int count = 0;

            for (int i = 0; i < size; i++)
            {
                matrix[i] = new long[count + 1];
                count++;
            }

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < 1 + i; j++)
                {
                    if (j == 0 || j == 1 + i - 1)
                    {
                        matrix[i][j] = 1;
                    }
                    else
                    {
                        matrix[i][j] = matrix[i - 1][j] + matrix[i - 1][j - 1];
                    }
                }
            }

            for (int i = 0; i < size; i++)
            {
                Console.WriteLine(string.Join(' ', matrix[i]));
            }
        }
    }
}
