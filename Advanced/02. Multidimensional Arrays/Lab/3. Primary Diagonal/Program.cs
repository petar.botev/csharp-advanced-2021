﻿using System;
using System.Linq;

namespace _3._Primary_Diagonal
{
    class Program
    {
        static void Main()
        {
            int matrixSize = int.Parse(Console.ReadLine());

            int[,] matrix = new int[matrixSize, matrixSize];

            for (int i = 0; i < matrixSize; i++)
            {
                int[] rowArgs = Console.ReadLine()
                    .Split()
                    .Select(int.Parse)
                    .ToArray();

                for (int j = 0; j < matrixSize; j++)
                {
                    matrix[i,j] = rowArgs[j];
                }
            }
            
            int sum = 0;
            int row = 0;
            int col = 0;

            while (row < matrixSize)
            {
                sum += matrix[row, col];
                row++;
                col++;
            }

            Console.WriteLine(sum);
        }
    }
}
