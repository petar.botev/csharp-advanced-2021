﻿using System;
using System.Linq;

namespace _2._Sum_Matrix_Columns
{
    class Program
    {
        static void Main()
        {
            int[] matrixSize = Console.ReadLine()
                .Split(", ", StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray();

            int[][] matrix = new int[matrixSize[0]] [];

            for (int i = 0; i < matrixSize[0]; i++)
            {
                matrix[i] = Console.ReadLine()
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                    .Select(int.Parse)
                    .ToArray();
            }

            for (int i = 0; i < matrixSize[1]; i++)
            {
                int sum = 0;

                for (int j = 0; j < matrixSize[0]; j++)
                {
                    sum += matrix[j][i];
                }

                Console.WriteLine(sum);
            }
        }
    }
}
