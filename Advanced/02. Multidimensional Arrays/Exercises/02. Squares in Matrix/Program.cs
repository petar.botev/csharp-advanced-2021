﻿using System;
using System.Linq;

namespace _02._Squares_in_Matrix
{
    class Program
    {
        static void Main()
        {
            int[] matrixSize = Console.ReadLine()
               .Split(' ', StringSplitOptions.RemoveEmptyEntries)
               .Select(int.Parse)
               .ToArray();

            char[][] matrix = new char[matrixSize[0]][];

            for (int i = 0; i < matrixSize[0]; i++)
            {
                matrix[i] = Console.ReadLine()
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                    .Select(char.Parse)
                    .ToArray();
            }

            int numberSquares = 0;

            for (int i = 0; i < matrixSize[0] - 1; i++)
            {
                for (int j = 0; j < matrixSize[1] - 1; j++)
                {
                    if (matrix[i][j] == matrix[i][j + 1])
                    {
                        if (matrix[i][j + 1] == matrix[i + 1][j])
                        {
                            if (matrix[i + 1][j] == matrix[i + 1][j + 1])
                            {
                                numberSquares++;
                            }
                        }
                    }
                }
            }

            Console.WriteLine(numberSquares);
        }
    }
}
