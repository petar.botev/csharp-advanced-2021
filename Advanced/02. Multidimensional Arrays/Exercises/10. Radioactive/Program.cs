﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace _10._Radioactive
{
    class Program
    {
        static void Main()
        {
            int[] parameters = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray();

            char[][] matrix = new char[parameters[0]][];

            for (int i = 0; i < parameters[0]; i++)
            {
                matrix[i] = Console.ReadLine()
                    .ToCharArray();
                    
            }

            int startRowIndex = 0;
            int startColIndex = 0;

            StartParams(matrix, ref startRowIndex, ref startColIndex);

            matrix[startRowIndex][startColIndex] = '.';

            char[] commands = Console.ReadLine().ToCharArray();


            for (int i = 0; i < commands.Length; i++)
            {
                List<string> allBunnies = new List<string>();

                switch (commands[i])
                {
                    case 'L':

                        startColIndex--;
                        allBunnies = FindAllBunnies(matrix);
                        BunnyExpansion(matrix, allBunnies);
                        HitBunny(matrix, startRowIndex, startColIndex);
                        LeftOut(matrix, startRowIndex, startColIndex);

                        break;
                    case 'R':

                        startColIndex++;
                        allBunnies = FindAllBunnies(matrix);
                        BunnyExpansion(matrix, allBunnies);
                        HitBunny(matrix, startRowIndex, startColIndex);
                        RightOut(matrix, startRowIndex, startColIndex);

                        break;
                    case 'U':

                        startRowIndex--;
                        allBunnies = FindAllBunnies(matrix);
                        BunnyExpansion(matrix, allBunnies);
                        HitBunny(matrix, startRowIndex, startColIndex);
                        UpOut(matrix, startRowIndex, startColIndex);

                        break;
                    case 'D':

                        startRowIndex++;
                        allBunnies = FindAllBunnies(matrix);
                        BunnyExpansion(matrix, allBunnies);
                        HitBunny(matrix, startRowIndex, startColIndex);
                        DownOut(matrix, startRowIndex, startColIndex);

                        break;
                    default:
                        break;
                }
            }
        }

        private static void LeftOut(char[][] matrix, int startRowIndex, int startColIndex)
        {
            if (startColIndex < 0)
            {
                PrintMatrix(matrix);
                Console.WriteLine($"won: {startRowIndex} {startColIndex + 1}");
                System.Environment.Exit(0);
            }
        }

        private static void RightOut(char[][] matrix, int startRowIndex, int startColIndex)
        {
            if (startColIndex == matrix[startRowIndex].Length)
            {
                PrintMatrix(matrix);
                Console.WriteLine($"won: {startRowIndex} {startColIndex - 1}");
                System.Environment.Exit(0);
            }
        }

        private static void UpOut(char[][] matrix, int startRowIndex, int startColIndex)
        {
            if (startRowIndex < 0)
            {
                PrintMatrix(matrix);
                Console.WriteLine($"won: {startRowIndex + 1} {startColIndex}");
                System.Environment.Exit(0);
            }
        }

        private static void DownOut(char[][] matrix, int startRowIndex, int startColIndex)
        {
            if (startRowIndex == matrix.Length)
            {
                PrintMatrix(matrix);
                Console.WriteLine($"won: {startRowIndex - 1} {startColIndex}");
                System.Environment.Exit(0);
            }
        }
                

        private static void PrintMatrix(char[][] matrix)
        {
            for (int i = 0; i < matrix.Length; i++)
            {
                Console.WriteLine(string.Join("", matrix[i]));
            }
        }

        private static List<string> FindAllBunnies(char[][] matrix)
        {
            List<string> bunnyPossitions = new List<string>();

            for (int i = 0; i < matrix.Length; i++)
            {
                for (int j = 0; j < matrix[i].Length; j++)
                {
                    if (matrix[i][j] == 'B')
                    {
                        bunnyPossitions.Add($"{i},{j}");
                    }
                }
            }

            return bunnyPossitions;
        }

        private static void BunnyExpansion(char[][] matrix, List<string> bunnies)
        {
            for (int i = 0; i < bunnies.Count; i++)
            {
                int rowInd = bunnies[i].Split(',').Select(int.Parse).ToArray()[0];
                int colInd = bunnies[i].Split(',').Select(int.Parse).ToArray()[1];

                if (colInd > 0)
                {
                    
                    if (matrix[rowInd][colInd - 1] == 'P')
                    {
                        PrintMatrix(matrix);
                        Console.WriteLine($"dead: {rowInd} {colInd - 1}");
                        System.Environment.Exit(0);
                    }

                    matrix[rowInd][colInd - 1] = 'B';
                }

                if (matrix[rowInd].Length - 1 > colInd)
                {
                    if (matrix[rowInd][colInd + 1] == 'P')
                    {
                        PrintMatrix(matrix);
                        Console.WriteLine($"dead: {rowInd} {colInd - 1}");
                        System.Environment.Exit(0);
                    }

                    matrix[rowInd][colInd + 1] = 'B';
                }

                if (rowInd > 0)
                {
                    if (matrix[rowInd - 1][colInd] == 'P')
                    {
                        PrintMatrix(matrix);
                        Console.WriteLine($"dead: {rowInd} {colInd - 1}");
                        System.Environment.Exit(0);
                    }

                    matrix[rowInd - 1][colInd] = 'B';
                }

                if (rowInd < matrix.Length - 1)
                {
                    if (matrix[rowInd + 1][colInd] == 'P')
                    {
                        PrintMatrix(matrix);
                        Console.WriteLine($"dead: {rowInd} {colInd - 1}");
                        System.Environment.Exit(0);
                    }

                    matrix[rowInd + 1][colInd] = 'B';
                }
                
            }
        }

        private static void HitBunny(char[][] matrix, int startRowIndex, int startColIndex)
        {
            if (startRowIndex > -1 && startRowIndex < matrix.Length && startColIndex > -1 && startColIndex < matrix[startRowIndex].Length)
            {
                if (matrix[startRowIndex][startColIndex] == 'B')
                {
                    PrintMatrix(matrix);
                    Console.WriteLine($"dead: {startRowIndex} {startColIndex}");
                    System.Environment.Exit(0);
                }
            }
        }

        private static void StartParams(char[][] matrix, ref int startRowIndex, ref int startColIndex)
        {
            for (int i = 0; i < matrix.Length; i++)
            {
                for (int j = 0; j < matrix[i].Length; j++)
                {
                    if (matrix[i][j] == 'P')
                    {
                        startRowIndex = i;
                        startColIndex = j;
                    }
                }
            }
        }
    }
}
