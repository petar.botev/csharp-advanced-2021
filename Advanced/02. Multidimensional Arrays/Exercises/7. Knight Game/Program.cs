﻿using System;

namespace _7._Knight_Game
{
    class Program
    {
        static void Main()
        {
            int size = int.Parse(Console.ReadLine());

            char[][] matrix = new char[size][];

            for (int i = 0; i < size; i++)
            {
                matrix[i] = Console.ReadLine().Trim().ToCharArray();
            }

            int removesCount = 0;
            int countK = 0;

            while (true)
            {
                int maxCountK = 0;
                int rowIndex = 0;
                int colIndex = 0;

                for (int i = 0; i < size; i++)
                {
                    for (int j = 0; j < matrix[i].Length; j++)
                    {
                        if (matrix[i][j] == 'K')
                        {
                            //left
                            if (j - 2 > -1)
                            {
                                if (i - 1 > -1 && matrix[i - 1][j - 2] == 'K')
                                {
                                    countK++;
                                }
                                if (i + 1 < matrix.Length && matrix[i + 1][j - 2] == 'K')
                                {
                                    countK++;
                                }
                            }

                            //right
                            if (j + 2 < matrix[i].Length)
                            {
                                if (i - 1 > -1 && matrix[i - 1][j + 2] == 'K')
                                {
                                    countK++;
                                }
                                if (i + 1 < matrix.Length && matrix[i + 1][j + 2] == 'K')
                                {
                                    countK++;
                                }
                            }

                            //up
                            if (i - 2 > -1)
                            {
                                if (j - 1 > -1 && matrix[i - 2][j - 1] == 'K')
                                {
                                    countK++;
                                }
                                if (j + 1 < matrix[i].Length && matrix[i - 2][j + 1] == 'K')
                                {
                                    countK++;
                                }
                            }

                            //down
                            if (i + 2 < matrix.Length)
                            {
                                if (j - 1 > -1 && matrix[i + 2][j - 1] == 'K')
                                {
                                    countK++;
                                }
                                if (j + 1 < matrix[i].Length && matrix[i + 2][j + 1] == 'K')
                                {
                                    countK++;
                                }
                            }
                        }

                        if (maxCountK < countK)
                        {
                            maxCountK = countK;
                            rowIndex = i;
                            colIndex = j;
                        }
                        countK = 0;
                    }
                }

                if (maxCountK == 0)
                {
                    break;
                }
                else
                {
                    matrix[rowIndex][colIndex] = '0';
                    removesCount++;
                }
            }
            
            Console.WriteLine(removesCount);
        }
    }
}
