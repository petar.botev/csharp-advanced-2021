﻿using System;
using System.Linq;

namespace _09._Miner
{
    class Program
    {
        static void Main()
        {
            int size = int.Parse(Console.ReadLine());

            string[] commands = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries);

            char[][] matrix = new char[size][];

            for (int i = 0; i < size; i++)
            {
                matrix[i] = Console.ReadLine()
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                    .Select(char.Parse)
                    .ToArray();
            }

            int rowS, colS;
            int coalCount = CoalCount(matrix, out rowS, out colS);

            int coalsCollected = 0;
            int rowIndex = rowS;
            int colIndex = colS;
            bool isExit = false;
            
            for (int i = 0; i < commands.Length; i++)
            {
                switch (commands[i])
                {
                    case "left":
                        if (colS > 0)
                        {
                            colS--;
                            coalsCollected = Coals(matrix, rowS, colS, coalsCollected);
                            AllCoalsCollected(rowS, colS, coalCount, coalsCollected);

                            isExit = Exit(matrix, rowS, colS);
                        }
                        break;
                    case "right":
                        if (matrix.Length - 1 > colS)
                        {
                            colS++;
                            coalsCollected = Coals(matrix, rowS, colS, coalsCollected);
                            AllCoalsCollected(rowS, colS, coalCount, coalsCollected);

                            isExit = Exit(matrix, rowS, colS);
                        }
                        break;
                    case "up":
                        if (rowS > 0)
                        {
                            rowS--;
                            coalsCollected = Coals(matrix, rowS, colS, coalsCollected);
                            AllCoalsCollected(rowS, colS, coalCount, coalsCollected);

                            isExit = Exit(matrix, rowS, colS);
                        }
                        break;
                    case "down":
                        if (rowS < matrix.Length - 1)
                        {
                            rowS++;
                            coalsCollected = Coals(matrix, rowS, colS, coalsCollected);
                            AllCoalsCollected(rowS, colS, coalCount, coalsCollected);

                            isExit = Exit(matrix, rowS, colS);
                        }
                        break;
                    default:
                        break;
                }
            }

            if (coalsCollected < coalCount && isExit == false)
            {
                Console.WriteLine($"{coalCount - coalsCollected} coals left. ({rowS}, {colS})");
            }

        }

        private static void AllCoalsCollected(int rowS, int colS, int coalCount, int coalsCollected)
        {
            if (coalsCollected == coalCount)
            {
                Console.WriteLine($"You collected all coals! ({rowS}, {colS})");
            }
        }

        private static bool Exit(char[][] matrix, int rowS, int colS)
        {
            bool isExit = false;

            if (matrix[rowS][colS] == 'e')
            {
                Console.WriteLine($"Game over! ({rowS}, {colS})");
                isExit = true;
            }

            return isExit;
        }

        private static int Coals(char[][] matrix, int rowS, int colS, int coalsCollected)
        {
            if (matrix[rowS][colS] == 'c')
            {
                coalsCollected++;
                matrix[rowS][colS] = '*';
            }

            return coalsCollected;
        }

        private static int CoalCount(char[][] matrix, out int rowS, out int colS)
        {
            int coalsCount = 0;
            rowS = 0;
            colS = 0;
            for (int i = 0; i < matrix.Length; i++)
            {
                for (int j = 0; j < matrix.Length; j++)
                {
                    if (matrix[i][j] == 'c')
                    {
                        coalsCount++;
                    }
                    if (matrix[i][j] == 's')
                    {
                        rowS = i;
                        colS = j;
                    }
                }
            }

            return coalsCount;
        }
    }
}
