﻿using System;
using System.Linq;

namespace _04._Matrix_Shuffling
{
    class Program
    {
        static void Main()
        {
            int[] size = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray();

            string[][] matrix = new string[size[0]][];

            for (int i = 0; i < size[0]; i++)
            {
                string[] rowValues = Console.ReadLine()
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                matrix[i] = rowValues; 
            }

            string command;
            while ((command = Console.ReadLine()) != "END")
            {
                string[] commandArgs = command
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                if (commandArgs.Length == 5)
                {
                    string theCommand = commandArgs[0];
                    int x1 = int.Parse(commandArgs[1]);
                    int y1 = int.Parse(commandArgs[2]);
                    int x2 = int.Parse(commandArgs[3]);
                    int y2 = int.Parse(commandArgs[4]);

                    if (x1 < size[0] && x2 < size[0] && y1 < size[1] && y2 < size[1])
                    {
                        string temp = matrix[x1][y1];

                        matrix[x1][y1] = matrix[x2][y2];
                        matrix[x2][y2] = temp;


                        for (int i = 0; i < size[0]; i++)
                        {
                            Console.WriteLine(string.Join(' ', matrix[i]));
                        }
                    }
                    else
                    {
                        Console.WriteLine("Invalid input!");
                    }
                }
                else
                {
                    Console.WriteLine("Invalid input!");
                }
            }
        }
    }
}
