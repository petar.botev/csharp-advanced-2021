﻿using System;
using System.Linq;

namespace _01._Diagonal_Difference
{
    class Program
    {
        static void Main()
        {
            int size = int.Parse(Console.ReadLine());

            int[][] matrix = new int[size][];

            for (int i = 0; i < size; i++)
            {
                int[] rowValues = Console.ReadLine()
                    .Split()
                    .Select(int.Parse)
                    .ToArray();

                matrix[i] = rowValues;
            }

            int firstSum = 0;
            int secondSum = 0;

            for (int i = 0; i < size; i++)
            {
                firstSum += matrix[i][i];
            }

            int j = 0;
            for (int i = size - 1; i >= 0; i--)
            {
                secondSum += matrix[i][j];
                j++;
            }

            Console.WriteLine(Math.Abs(firstSum - secondSum));
        }
    }
}
