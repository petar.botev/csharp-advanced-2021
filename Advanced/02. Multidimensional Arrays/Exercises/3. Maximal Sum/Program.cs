﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _03._Maximal_Sum
{
    class Program
    {
        static void Main()
        {
            int[] parameters = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray();

            int[][] matrix = new int[parameters[0]][];

            for (int i = 0; i < parameters[0]; i++)
            {
                matrix[i] = Console.ReadLine()
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                    .Select(int.Parse)
                    .ToArray();
            }

            int sum = 0;
            int maxSum = 0;
            int rowIndex = 0;
            int colIndex = 0;

            for (int i = 0; i < parameters[0] - 2; i++)
            {
                for (int j = 0; j < parameters[1] - 2; j++)
                {
                    for (int k = i; k < 3 + i; k++)
                    {
                        for (int l = j; l < 3 + j; l++)
                        {
                            sum += matrix[k][l];
                        }
                    }

                    if (maxSum < sum)
                    {
                        rowIndex = i;
                        colIndex = j;
                        maxSum = sum;
                    }

                    sum = 0;
                }
            }

            Console.WriteLine($"Sum = {maxSum}");

            StringBuilder strb = new StringBuilder();

            for (int i = rowIndex; i < rowIndex + 3; i++)
            {
                for (int j = colIndex; j < colIndex + 3; j++)
                {
                    strb.Append($"{matrix[i][j]} ");
                }
                strb.ToString().Trim();
                strb.AppendLine();
            }

            Console.WriteLine(strb.ToString().Trim());
        }
    }
}
