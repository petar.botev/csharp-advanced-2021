﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _8._Bombs
{
    class Program
    {
        static void Main()
        {
            int size = int.Parse(Console.ReadLine());

            int[][] matrix = new int[size][];
            bool[][] boolMatrix = new bool[size][];

            for (int i = 0; i < size; i++)
            {
                boolMatrix[i] = new bool[size];
            }

            for (int i = 0; i < size; i++)
            {
                matrix[i] = Console.ReadLine()
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                    .Select(int.Parse)
                    .ToArray();
            }

            string[] bombCoordinates = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < bombCoordinates.Length; i++)
            {
                int[] intCoordinates = bombCoordinates[i]
                    .Split(',', StringSplitOptions.RemoveEmptyEntries)
                    .Select(int.Parse)
                    .ToArray();


                if (matrix[intCoordinates[0]][intCoordinates[1]] > 0)
                {
                    Tuple<int[][], bool[][]> tuple = Explossion(matrix, boolMatrix, intCoordinates[0], intCoordinates[1]);

                    matrix = tuple.Item1;
                    boolMatrix = tuple.Item2;
                }
            }

            Console.WriteLine($"Alive cells: {AliveCells(matrix).Item1}");
            Console.WriteLine($"Sum: {AliveCells(matrix).Item2}");

            for (int i = 0; i < matrix.Length; i++)
            {
                Console.WriteLine(string.Join(' ', matrix[i]));
            }
        }

        static Tuple<int[][], bool[][]> Explossion(int[][] matrix, bool[][] boolMatrix, int row, int col)
        {
            int explossionPower = matrix[row][col];
            
            matrix[row][col] -= matrix[row][col];
            
            // right
            if (col < matrix.Length - 1)
            {
                if (matrix[row][col + 1] > 0)
                {
                    matrix[row][col + 1] -= explossionPower;
                }

                if (row > 0)
                {
                    if (matrix[row - 1][col + 1] > 0)
                    {
                        matrix[row - 1][col + 1] -= explossionPower;
                       
                    }
                }

                if (row < matrix.Length - 1)
                {
                    if (matrix[row + 1][col + 1] > 0)
                    {
                        matrix[row + 1][col + 1] -= explossionPower;
                    }
                }
            }

            // left
            if (col > 0)
            {
                if (matrix[row][col - 1] > 0)
                {
                    matrix[row][col - 1] -= explossionPower;
                    
                }

                if (row > 0)
                {
                    if (matrix[row - 1][col - 1] > 0)
                    {
                        matrix[row - 1][col - 1] -= explossionPower;
                        
                    }
                }

                if (row < matrix.Length - 1)
                {
                    if (matrix[row + 1][col - 1] > 0)
                    {
                        matrix[row + 1][col - 1] -= explossionPower;

                    }
                }
            }

            // up
            if (row > 0)
            {
                if (matrix[row - 1][col] > 0)
                {
                    matrix[row - 1][col] -= explossionPower;
                    
                }
            }

            // down
            if (row < matrix.Length - 1)
            {
                if (matrix[row + 1][col] > 0)
                {
                    matrix[row + 1][col] -= explossionPower;
                    
                }
            }
            
            return new Tuple<int[][], bool[][]>(matrix, boolMatrix);
        }

        static Tuple<int, int> AliveCells(int[][] matrix)
        {
            int count = 0;
            int sum = 0;

            for (int i = 0; i < matrix.Length; i++)
            {
                for (int j = 0; j < matrix.Length; j++)
                {
                    if (matrix[i][j] > 0)
                    {
                        count++;
                        sum += matrix[i][j];
                    }
                }
            }

            return new Tuple<int, int>(count, sum);
        }
    }
}
