﻿using System;
using System.Linq;

namespace _6._Jagged_Array_Manipulator
{
    class Program
    {
        static void Main()
        {
            int rowCount = int.Parse(Console.ReadLine());

            decimal[][] matrix = new decimal[rowCount][];

            for (int i = 0; i < rowCount; i++)
            {
                matrix[i] = Console.ReadLine()
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                    .Select(decimal.Parse)
                    .ToArray();
            }

            for (int i = 0; i < rowCount - 1; i++)
            {
                if (matrix[i].Length == matrix[i + 1].Length)
                {
                    for (int j = 0; j < matrix[i].Length; j++)
                    {
                        matrix[i][j] *= 2;
                        matrix[i + 1][j] *= 2;
                    }
                }
                else
                {
                    for (int j = 0; j < matrix[i].Length; j++)
                    {
                        matrix[i][j] /= 2;
                    }

                    for (int j = 0; j < matrix[i + 1].Length; j++)
                    {
                        matrix[i + 1][j] /= 2;
                    }
                }
            }

            string command;

            while ((command = Console.ReadLine()) != "End")
            {
                string[] commandArgs = command
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                string theCommand = commandArgs[0];
                int x1 = int.Parse(commandArgs[1]);
                int y1 = int.Parse(commandArgs[2]);
                int value = int.Parse(commandArgs[3]);

                switch (theCommand)
                {
                    case "Add":
                        if (x1 < rowCount && x1 >= 0 && y1 < matrix[x1].Length && y1 >= 0)
                        {
                            matrix[x1][y1] += value;
                        }

                        break;
                    case "Subtract":
                        if (x1 < rowCount && x1 >= 0 && y1 < matrix[x1].Length && y1 >= 0)
                        {
                            matrix[x1][y1] -= value;
                        }

                        break;
                    default:
                        break;
                }

            }

            for (int i = 0; i < rowCount; i++)
            {
                Console.WriteLine(string.Join(' ', matrix[i]));
            }
        }
    }
}
