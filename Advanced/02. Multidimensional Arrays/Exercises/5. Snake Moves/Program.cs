﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _5._Snake_Moves
{
    class Program
    {
        static void Main()
        {
            int[] size = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray();

            char[][] matrix = new char[size[0]][];

            for (int i = 0; i < size[0]; i++)
            {
                matrix[i] = new char[size[1]];
            }

            Queue<char> snake = new Queue<char>(Console.ReadLine());

            for (int i = 0; i < size[0]; i++)
            {
                if (i % 2 == 0)
                {
                    for (int j = 0; j < size[1]; j++)
                    {
                        matrix[i][j] = snake.Peek();
                        snake.Enqueue(snake.Dequeue());
                    }
                }
                else
                {
                    for (int j = size[1] - 1; j >= 0; j--)
                    {
                        matrix[i][j] = snake.Peek();
                        snake.Enqueue(snake.Dequeue());
                    }
                }
            }

            for (int i = 0; i < size[0]; i++)
            {
                Console.WriteLine(string.Join("", matrix[i]));
            }
        }
    }
}
