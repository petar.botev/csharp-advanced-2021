﻿using System;
using System.Collections.Generic;

namespace _01._Crossroads
{
    class Program
    {
        static int passedCars = 0;

        static void Main()
        {
            int greenLightTime = int.Parse(Console.ReadLine());
            int windowTime = int.Parse(Console.ReadLine());

            Queue<string> cars = new Queue<string>();

            bool isCrashed = false;
            char result = ' ';
            string car = string.Empty;
            while ((car = Console.ReadLine()) != "END")
            {
                if (car == "green")
                {
                    result = GreenLight(cars, greenLightTime, windowTime);

                    if (result != ' ')
                    {
                        isCrashed = true;
                        break;
                    }
                }
                else
                {
                    cars.Enqueue(car);
                }
            }

            if (!isCrashed)
            {
                Console.WriteLine("Everyone is safe.");
                Console.WriteLine($"{passedCars} total cars passed the crossroads.");
            }
            else
            {
                Console.WriteLine("A crash happened!");
                Console.WriteLine($"{cars.Peek()} was hit at {result}.");
            }
        }

        static char GreenLight(Queue<string> cars, int greenLightTime, int window)
        {
            char carElementInCrossword = ' ';

            while (cars.Count > 0 && greenLightTime > 0)
            {
                if (cars.Peek().Length <= greenLightTime)
                {
                    greenLightTime -= cars.Peek().Length;
                    cars.Dequeue();
                    passedCars++;
                }
                else
                {
                    Queue<char> carInCrossword = new Queue<char>(cars.Peek().ToCharArray());

                    int tempGreenLightTime = greenLightTime;
                    int tempWindow = window;

                    for (int i = 0; i < greenLightTime; i++)
                    {
                        carInCrossword.Dequeue();
                        
                        if (carInCrossword.Count == 0)
                        {
                            break;
                        }
                    }

                    greenLightTime = 0;

                    for (int i = 0; i < window; i++)
                    {
                        carInCrossword.Dequeue();

                        if (carInCrossword.Count == 0)
                        {
                            break;
                        }
                    }

                    if (carInCrossword.Count > 0)
                    {
                        carElementInCrossword = carInCrossword.Peek();
                    }
                    else
                    {
                        cars.Dequeue();
                        passedCars++;
                    }
                }
            }

            return carElementInCrossword;
        }
    }
}
