﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _02._Parking_Feud
{
    class Program
    {
        static int distance = 0;
        static List<int> startingRowIndexes = new List<int>();
        static bool isParked = false;
        static int totalDistance = 0;
        static string parkingPlace = string.Empty;
        static bool haveConcurent = false;

        static void Main()
        {
            int[] parkingParams = Console.ReadLine()
                .Split()
                .Select(int.Parse)
                .ToArray();

            int matrixRow = parkingParams[0] + (parkingParams[0] - 1);
            int matrixCol = parkingParams[1] + 2;

            string[,] matrix = new string[matrixRow, matrixCol];

            int parkingPlaceRowNumber = 0;
            string[] letters = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J" };

            for (int i = 0; i < matrixRow; i++)
            {
                if (i % 2 == 0)
                {
                    parkingPlaceRowNumber++;
                }
                else
                {
                    startingRowIndexes.Add(i);
                }

                for (int j = 0; j < matrixCol; j++)
                {
                    if (i % 2 == 0)
                    {
                        if (j != 0 && j != matrixCol - 1)
                        {
                            matrix[i, j] = letters[j - 1] + parkingPlaceRowNumber;
                        }
                        else
                        {
                            matrix[i, j] = " ";
                        }
                    }
                    else
                    {
                        matrix[i, j] = " ";
                    }
                }
            }

            int myEntrance = int.Parse(Console.ReadLine());
            int myTempDistance = 0;
            int concurentDistance = 0;
            

            while (!isParked)
            {
                string[] parkingSpots = Console.ReadLine()
                .Split();

                for (int i = 0; i < parkingSpots.Length; i++)
                {

                    if (i == myEntrance - 1)
                    {
                        FindPlace(matrix, startingRowIndexes[i], parkingSpots[i]);
                        myTempDistance = distance;
                        
                        distance = 0;
                        break;
                    }
                }
                
                for (int i = 0; i < parkingSpots.Length; i++)
                {
                    isParked = false;
                    if (parkingSpots[myEntrance - 1] == parkingSpots[i] && i != myEntrance - 1)
                    {
                        haveConcurent = true;
                        FindPlace(matrix, startingRowIndexes[i], parkingSpots[i]);
                        concurentDistance = distance;
                        distance = 0;
                    }
                }

                if (concurentDistance < myTempDistance && haveConcurent == true)
                {
                    myTempDistance *= 2;
                    totalDistance += myTempDistance;
                }
                else
                {
                    totalDistance += myTempDistance;
                    break;
                }
            }

            Console.WriteLine($"Parked successfully at {parkingPlace}.");
            Console.WriteLine($"Total Distance Passed: {totalDistance}"); 
        }

        static void FindPlace(string[,] matrix, int startingRow, string spot)
        {
            int startingCol = 0;
            
            int spotRow = int.Parse(spot[1].ToString());

            while (!isParked)
            {
                for (int i = 1; i < matrix.GetLength(0) - 1; i++)
                {
                    startingCol++;
                    distance++;

                    if (matrix[startingRow - 1, startingCol] == spot || matrix[startingRow + 1, startingCol] == spot)
                    {
                        isParked = true;
                        if (matrix[startingRow - 1, startingCol] == spot)
                        {
                            parkingPlace = matrix[startingRow - 1, startingCol];
                        }
                        else
                        {
                            parkingPlace = matrix[startingRow + 1, startingCol];
                        }
                        
                        break;
                    }
                }

                if (isParked)
                {
                    break;
                }

                if (spotRow > startingRow)
                {
                    startingRow += 2;
                    distance += 2;
                }
                if (spotRow < startingRow)
                {
                    startingRow -= 2;
                    distance += 2;
                }

                for (int i = matrix.GetLength(0) - 1; i >= 0; i--)
                {
                    startingCol--;
                    distance++;

                    if (matrix[startingRow - 1, startingCol] == spot || matrix[startingRow + 1, startingCol] == spot)
                    {
                        isParked = true;
                        if (matrix[startingRow - 1, startingCol] == spot)
                        {
                            parkingPlace = matrix[startingRow - 1, startingCol];
                        }
                        else
                        {
                            parkingPlace = matrix[startingRow + 1, startingCol];
                        }

                        break;
                    }
                }

                if (isParked)
                {
                    break;
                }

                if (spotRow > startingRow)
                {
                    startingRow += 2;
                    distance += 2;
                }
                if (spotRow < startingRow)
                {
                    startingRow -= 2;
                    distance += 2;
                }
            }
        }
    }
}
