﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CocktailParty
{
    public class Ingredient
    {
        private string name;
        private int alcohol;
        private int quantity;

        public Ingredient(string name, int alcohol, int quantity)
        {
            this.Name = name;
            this.Alcohol = alcohol;
            this.Quantity = quantity;
        }

        public string Name { get => name; set => name = value; }
        public int Alcohol { get => alcohol; set => alcohol = value; }
        public int Quantity { get => quantity; set => quantity = value; }

        public override string ToString()
        {
            StringBuilder strb = new StringBuilder();

            strb.AppendLine($"Ingredient: { Name}");
            strb.AppendLine($"Quantity: {Quantity}");
            strb.AppendLine($"Alcohol: {Alcohol}");

            return strb.ToString().Trim();
        }
    }
}
