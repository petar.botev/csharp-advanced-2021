﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CocktailParty
{
    public class Cocktail
    {
        public Cocktail(string name, int capacity, int maxAlcoholLevel)
        {
            this.Name = name;
            this.Capacity = capacity;
            this.MaxAlcoholLevel = maxAlcoholLevel;
            this.Ingredients = new List<Ingredient>();
        }

        public List<Ingredient> Ingredients { get; set; }
        public string Name { get; set; }
        public int Capacity { get; set; }
        public int MaxAlcoholLevel { get; set; }
        public int CurrentAlcoholLevel { get => Ingredients.Sum(x => x.Alcohol); }

        public void Add(Ingredient ingredient)
        {
            if (this.Capacity >= Ingredients.Count + 1 && this.MaxAlcoholLevel >= Ingredients.Sum(x => x.Alcohol) + ingredient.Alcohol)
            {
                this.Ingredients.Add(ingredient);
            }
        }

        public bool Remove(string name)
        {
            var ingred = Ingredients.FirstOrDefault(x => x.Name == name);
            if (ingred != null)
            {
                Ingredients.Remove(ingred);
                return true;

            }
            else
            {
                return false;

            }
        }

        public Ingredient FindIngredient(string name)
        {
            return Ingredients.FirstOrDefault(x => x.Name == name);
        }

        public Ingredient GetMostAlcoholicIngredient()
        {
            List<Ingredient> sortedIngredients = Ingredients.OrderByDescending(x => x.Alcohol).ToList();

            //var ingrad = sortedIngredients.FirstOrDefault();
            return sortedIngredients[0];

        }

        //public int CurrentAlcoholLevel()
        //{
        //    int res = Ingredients.Sum(x => x.Alcohol);
        //    return res;
        //}
       

        public string Report()
        {
            Console.WriteLine($"Cocktail: {this.Name} - Current Alcohol Level: {CurrentAlcoholLevel}");

            StringBuilder strb = new StringBuilder();
            foreach (var ingrad in Ingredients)
            {
                strb.AppendLine(ingrad.ToString());
            }

            return strb.ToString().Trim();
        }
    }
}
