﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _05._Count_Symbols
{
    class Program
    {
        static void Main()
        {
            string input = Console.ReadLine();

            Dictionary<char, int> dict = new Dictionary<char, int>();

            for (int i = 0; i < input.Length; i++)
            {
                char character = input[i];

                if (dict.ContainsKey(character))
                {
                    dict[character]++;
                }
                else
                {
                    dict.Add(character, 1);
                }
            }

            dict = dict.OrderBy(x => x.Key).ToDictionary(k => k.Key, v => v.Value);

            foreach (var item in dict)
            {
                Console.WriteLine($"{item.Key}: {item.Value} time/s");
            }
        }
    }
}
