﻿using System;
using System.Collections.Generic;

namespace _04._Even_Times
{
    class Program
    {
        static void Main()
        {
            int count = int.Parse(Console.ReadLine());

            Dictionary<int, int> dict = new Dictionary<int, int>(count);

            for (int i = 0; i < count; i++)
            {
                int number = int.Parse(Console.ReadLine());

                if (dict.ContainsKey(number))
                {
                    dict[number]++;
                }
                else
                {
                    dict.Add(number, 1);
                }
            }

            foreach (var item in dict)
            {
                if (item.Value % 2 == 0)
                {
                    Console.WriteLine(item.Key);
                    return;
                }
            }
        }
    }
}
