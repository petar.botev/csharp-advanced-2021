﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _02._Sets_of_Elements
{
    class Program
    {
        static void Main()
        {
            int[] setsLength = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray();

            HashSet<int> set1 = new HashSet<int>(setsLength[0]);
            HashSet<int> set2 = new HashSet<int>(setsLength[1]);

            for (int i = 0; i < setsLength[0]; i++)
            {
                set1.Add(int.Parse(Console.ReadLine()));
            }

            for (int i = 0; i < setsLength[1]; i++)
            {
                set2.Add(int.Parse(Console.ReadLine()));
            }

            Console.WriteLine(string.Join(' ', set1.Intersect(set2)));
        }
    }
}
