﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _7._The_V_Logger
{
    class Program
    {
        static void Main()
        {
            Dictionary<string, HashSet<string>> followers = new Dictionary<string, HashSet<string>>();
            Dictionary<string, HashSet<string>> followings = new Dictionary<string, HashSet<string>>();
            List<string[]> commandList = new List<string[]>();

            string command;
            while ((command = Console.ReadLine()) != "Statistics")
            {
                string[] commandArgs = command
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                commandList.Add(commandArgs);

                string theCommand = commandArgs[1];
                string vloggername = commandArgs[0];

                if (theCommand == "joined" && !followers.ContainsKey(vloggername))
                {
                    followers.Add(vloggername, new HashSet<string>());
                    followings.Add(vloggername, new HashSet<string>());
                }
                else
                {
                    AddFollowers(followers, followings, commandArgs);
                }
            }


            followers = followers
                .OrderByDescending(x => x.Value.Count)
                .ThenBy(x => followings[x.Key].Count)
                .ToDictionary(k => k.Key, v => v.Value);

            if (followers.Count != 0)
            {
                Print(followers, followings);
            }
            else
            {
                Console.WriteLine($"The V-Logger has a total of {followers.Count} vloggers in its logs.");
            }
        }

        private static void AddFollowers(Dictionary<string, HashSet<string>> followers, Dictionary<string, HashSet<string>> followings, string[] commandArgs)
        {
            string vloggername = commandArgs[0];

            string secondVlogger = commandArgs[2];

            if (followers.ContainsKey(vloggername)
                && followers.ContainsKey(secondVlogger)
                && vloggername != secondVlogger)
            {
                followers[secondVlogger].Add(vloggername);
                followings[vloggername].Add(secondVlogger);
            }
        }

        private static void Print(Dictionary<string, HashSet<string>> followers, Dictionary<string, HashSet<string>> followings)
        {
            Console.WriteLine($"The V-Logger has a total of {followers.Count} vloggers in its logs.");

            int j = 0;

            foreach (var follower in followers)
            {
                var tempFollower = follower.Value;

                tempFollower = tempFollower.OrderBy(x => x).ToHashSet();

                if (j == 0)
                {
                    Console.WriteLine($"{j + 1}. {follower.Key} : {follower.Value.Count} followers, {followings[follower.Key].Count} following");

                    foreach (var item in tempFollower)
                    {
                        Console.WriteLine($"*  {item}");
                    }
                }
                else
                {
                    Console.WriteLine($"{j + 1}. {follower.Key} : {follower.Value.Count} followers, {followings[follower.Key].Count} following");
                }

                j++;
            }
        }
    }
}
