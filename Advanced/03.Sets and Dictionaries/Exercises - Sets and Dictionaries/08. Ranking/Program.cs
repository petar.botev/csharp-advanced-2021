﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _08._Ranking
{
    class Program
    {
        static void Main()
        {
            Dictionary<string, string> dictContests = new Dictionary<string, string>();

            string contest;
            while ((contest = Console.ReadLine()) != "end of contests")
            {
                string[] contestArgs = contest
                    .Split(':', StringSplitOptions.RemoveEmptyEntries);

                string theContest = contestArgs[0];
                string password = contestArgs[1];

                dictContests.Add(theContest, password);
            }

            List<Student> studentList = new List<Student>();
            List<Contest> contestList = new List<Contest>();

            string input;
            while ((input = Console.ReadLine()) != "end of submissions")
            {
                string[] inputArgs = input
                    .Split("=>", StringSplitOptions.RemoveEmptyEntries);

                string inputContest = inputArgs[0];
                string inputPassword = inputArgs[1];
                string userName = inputArgs[2];
                int points = int.Parse(inputArgs[3]);

                Student student = new Student();
                Contest newContest = new Contest();

                if (dictContests.ContainsKey(inputContest))
                {
                    if (dictContests[inputContest] == inputPassword)
                    {
                        Student std = studentList.FirstOrDefault(x => x.Name == userName);

                        if (std != null)
                        {
                            if (std.Contests.Any(x => x.Name == inputContest))
                            {
                                Contest cont = std.Contests.FirstOrDefault(x => x.Name == inputContest);

                                if (cont.Points < points)
                                {
                                    cont.Points = points;
                                }
                            }
                            else
                            {
                                newContest = new Contest();
                                newContest.Name = inputContest;
                                newContest.Password = inputPassword;
                                newContest.Points = points;

                                std.Contests.Add(newContest);
                            }

                        }
                        else
                        {
                            newContest = new Contest();
                            newContest.Name = inputContest;
                            newContest.Password = inputPassword;
                            newContest.Points = points;

                            student = new Student();

                            student.Name = userName;

                            student.Contests.Add(newContest);

                            studentList.Add(student);
                            //contestList.Add(newContest);

                        }

                    }
                }
            }

            Student bestStudent = BestStudent(studentList);
            List<Student> orderedStudents = OrderStudents(studentList);
            
            Print(orderedStudents, bestStudent);
        }

        static Student BestStudent(List<Student> students)
        {
            int bestResult = 0;
            int result = 0;
            Student best = new Student();

            foreach (var item in students)
            {
                result = item.Contests.Sum(x => x.Points);

                if (result > bestResult)
                {
                    bestResult = result;
                    best = item;
                }
            }

            return best;
        }

        static List<Student> OrderStudents(List<Student> students)
        {
            students = students.OrderBy(x => x.Name).ToList();

            foreach (var item in students)
            {
                List<Contest> temp = item.Contests.OrderByDescending(x => x.Points).ToList();

                item.Contests = temp;
            }

            return students;
        }

        static void Print(List<Student> students, Student bestStudent)
        {
            Console.WriteLine($"Best candidate is {bestStudent.Name} with total {bestStudent.Contests.Sum(x => x.Points)} points.");

            Console.WriteLine("Ranking:");

            StringBuilder strb = new StringBuilder();


            foreach (var student in students)
            {
                strb.AppendLine($"{student.Name}");
                foreach (var contest in student.Contests)
                {
                    strb.AppendLine($"#  {contest.Name} -> {contest.Points}");
                }
            }

            Console.WriteLine(strb.ToString());
        }
    }

    class Contest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public int Points { get; set; }
        public List<Student> Students { get; set; } = new List<Student>();
    }

    class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<Contest> Contests { get; set; } = new List<Contest>();
    }
}
