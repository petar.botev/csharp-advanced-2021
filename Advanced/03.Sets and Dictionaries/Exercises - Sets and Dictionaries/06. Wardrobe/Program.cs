﻿using System;
using System.Collections.Generic;

namespace _06._Wardrobe
{
    class Program
    {
        static void Main()
        {
            int number = int.Parse(Console.ReadLine());

            Dictionary<string, Dictionary<string, int>> dict = new Dictionary<string, Dictionary<string, int>>();

            for (int i = 0; i < number; i++)
            {
                string[] color = Console.ReadLine()
                    .Split(" -> ", StringSplitOptions.RemoveEmptyEntries);

                
                if (dict.ContainsKey(color[0]))
                {
                    AddClothes(dict, color);
                }
                else
                {
                    dict.Add(color[0], new Dictionary<string, int>());
                    AddClothes(dict, color);
                }
            }

            string[] searchFor = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries);
            string searchColor = searchFor[0];
            string searchDress = searchFor[1];

            foreach (var color in dict)
            {
                Console.WriteLine($"{color.Key} clothes:");

                foreach (var dress in color.Value)
                {
                    if (color.Key == searchColor && dress.Key == searchDress)
                    {
                        Console.WriteLine($"* {dress.Key} - {dress.Value} (found!)");
                    }
                    else
                    {
                        Console.WriteLine($"* {dress.Key} - {dress.Value}");
                    }
                }
            }


        }

        private static void AddClothes(Dictionary<string, Dictionary<string, int>> dict, string[] color)
        {
            string[] clothes = color[1]
                                    .Split(',', StringSplitOptions.RemoveEmptyEntries);
            for (int j = 0; j < clothes.Length; j++)
            {
                if (dict[color[0]].ContainsKey(clothes[j]))
                {
                    dict[color[0]][clothes[j]]++;
                }
                else
                {
                    dict[color[0]].Add(clothes[j], 1);
                }
            }
        }
    }
}
