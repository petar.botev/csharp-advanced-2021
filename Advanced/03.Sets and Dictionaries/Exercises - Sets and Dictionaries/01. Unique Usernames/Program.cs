﻿using System;
using System.Collections.Generic;

namespace _01._Unique_Usernames
{
    class Program
    {
        static void Main()
        {
            int namesNumber = int.Parse(Console.ReadLine());

            HashSet<string> set = new HashSet<string>();

            for (int i = 0; i < namesNumber; i++)
            {
                set.Add(Console.ReadLine());
            }


            Console.WriteLine(string.Join("\n", set));
        }
    }
}
