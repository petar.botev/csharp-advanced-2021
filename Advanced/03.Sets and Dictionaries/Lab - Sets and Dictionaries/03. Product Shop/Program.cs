﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _03._Product_Shop
{
    class Program
    {
        static void Main()
        {
            SortedDictionary<string, Dictionary<string, List<double>>> dict = new SortedDictionary<string, Dictionary<string, List<double>>>();

            string info;
            while ((info = Console.ReadLine()) != "Revision")
            {
                string[] infoArgs = info
                    .Split(", ", StringSplitOptions.RemoveEmptyEntries);

                string shop = infoArgs[0];
                string product = infoArgs[1];
                double price = double.Parse(infoArgs[2]);

                if (dict.ContainsKey(shop))
                {
                    if (dict[shop].ContainsKey(product))
                    {
                        dict[shop][product].Add(price);
                    }
                    else
                    {
                        dict[shop].Add(product, new List<double>());
                        dict[shop][product].Add(price);
                    }
                }
                else
                {
                    dict.Add(shop, new Dictionary<string, List<double>>());
                    dict[shop].Add(product, new List<double>());
                    dict[shop][product].Add(price);
                }
            }

            foreach (var item in dict)
            {
                Console.WriteLine($"{item.Key}->");

                foreach (var element in item.Value)
                {
                    Console.WriteLine($"Product: {element.Key}, Price: {string.Join(',', element.Value)}");
                }
            }
        }
    }
}
