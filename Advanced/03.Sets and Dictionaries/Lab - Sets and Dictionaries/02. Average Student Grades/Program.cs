﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _02._Average_Student_Grades
{
    class Program
    {
        static void Main()
        {
            int studentsNumber = int.Parse(Console.ReadLine());

            Dictionary<string, List<decimal>> dict = new Dictionary<string, List<decimal>>();

            for (int i = 0; i < studentsNumber; i++)
            {
                string[] studentInfo = Console.ReadLine()
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                if (dict.TryGetValue(studentInfo[0], out List<decimal> value))
                {
                    dict[studentInfo[0]].Add(decimal.Parse(studentInfo[1]));  
                }
                else
                {
                    dict.Add(studentInfo[0], new List<decimal>() { decimal.Parse(studentInfo[1]) });
                }

            }

            foreach (var item in dict)
            {
                Console.WriteLine($"{item.Key} -> {string.Join(' ', item.Value.Select(x => x.ToString("f2")))} (avg: {item.Value.Average():f2})");
            }
        }
    }
}
