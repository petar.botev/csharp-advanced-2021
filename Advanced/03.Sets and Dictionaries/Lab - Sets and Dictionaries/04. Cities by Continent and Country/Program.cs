﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _04._Cities_by_Continent_and_Country
{
    class Program
    {
        static void Main()
        {
            int continentsNumber = int.Parse(Console.ReadLine());

            Dictionary<string, Dictionary<string, List<string>>> dict = new Dictionary<string, Dictionary<string, List<string>>>();

            for (int i = 0; i < continentsNumber; i++)
            {
                string[] continentParams = Console.ReadLine()
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                string continent = continentParams[0];
                string country = continentParams[1];
                string city = continentParams[2];

                if (dict.TryGetValue(continent, out Dictionary<string, List<string>> value))
                {
                    if (dict[continent].ContainsKey(country))
                    {
                        dict[continent][country].Add(city);
                    }
                    else
                    {
                        dict[continent].Add(country, new List<string>() { city });
                    }
                }
                else
                {
                    dict.Add(continent, new Dictionary<string, List<string>>());
                    dict[continent].Add(country, new List<string>());
                    dict[continent][country].Add(city);
                }
            }

            foreach (var continent in dict)
            {
                Console.WriteLine($"{continent.Key}:");

                foreach (var country in continent.Value)
                {

                    Console.WriteLine($"{country.Key} -> {string.Join(", ", country.Value)}");

                }
            }
        }
    }
}
