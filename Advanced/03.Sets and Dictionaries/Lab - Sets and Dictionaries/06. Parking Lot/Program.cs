﻿using System;
using System.Collections.Generic;

namespace _06._Parking_Lot
{
    class Program
    {
        static void Main()
        {
            HashSet<string> inHash = new HashSet<string>();
            
            string car;
            while ((car = Console.ReadLine()) != "END")
            {
                string[] carInfo = car
                    .Split(", ", StringSplitOptions.RemoveEmptyEntries);

                if (carInfo[0] == "IN")
                {
                    inHash.Add(carInfo[1]);
                }
                else
                {
                    inHash.Remove(carInfo[1]);
                }
            }

            if (inHash.Count > 0)
            {
                foreach (var item in inHash)
                {
                    Console.WriteLine(item);
                }
            }
            else
            {
                Console.WriteLine("Parking Lot is Empty");
            }
        }
    }
}
