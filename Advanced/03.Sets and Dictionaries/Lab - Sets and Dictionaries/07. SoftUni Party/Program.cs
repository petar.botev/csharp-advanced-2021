﻿using System;
using System.Collections.Generic;

namespace _07._SoftUni_Party
{
    class Program
    {
        static void Main()
        {
            HashSet<string> vip = new HashSet<string>();
            HashSet<string> normal = new HashSet<string>();

            bool isParty = false;

            string command;
            while ((command = Console.ReadLine()) != "END")
            {
                if (command == "PARTY")
                {
                    isParty = true;
                    continue;
                }

                if (isParty == false)
                {
                    if (char.IsDigit(command[0]))
                    {
                        vip.Add(command);
                    }
                    else
                    {
                        normal.Add(command);
                    }
                }
                else
                {
                    if (char.IsDigit(command[0]))
                    {
                        vip.Remove(command);
                    }
                    else
                    {
                        normal.Remove(command);
                    }
                }
            }

            Console.WriteLine(vip.Count + normal.Count);

            if (vip.Count > 0)
            {
                Console.WriteLine(string.Join("\n", vip));
            }
            if (normal.Count > 0)
            {
                Console.WriteLine(string.Join("\n", normal));
            }
        }
    }
}
