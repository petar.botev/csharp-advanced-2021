﻿using System;

namespace GenericArrayCreator
{
    public class StartUp
    {
        static void Main()
        {
            string[] strings = ArrayCreator.Create(5, "Pesho");
            int[] integers = ArrayCreator.Create(10, 33);


            Console.WriteLine($"String: {string.Join(", ", strings)}");
            Console.WriteLine($"Integer: {string.Join(", ", integers)}");
        }
    }
}
