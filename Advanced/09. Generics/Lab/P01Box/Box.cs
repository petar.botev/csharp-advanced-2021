﻿namespace BoxOfT
{
    public class Box<T>
    {
        private const int INITIAL_SIZE = 8;

        public Box()
        {
            Arr = new T[INITIAL_SIZE];
        }

        public int Count { get; private set; }
        public T[] Arr { get; set; }

        public T Remove()
        {
            T element = Arr[Count - 1];

            Arr[Count - 1] = default(T);

            Count--;

            if (Count < Arr.Length / 2)
            {
                Shrink();
            }

            return element;
        }

        private void Shrink()
        {
            T[] shrinkedArr = new T[Arr.Length / 2];

            for (int i = 0; i < shrinkedArr.Length; i++)
            {
                shrinkedArr[i] = Arr[i];
            }

            Arr = shrinkedArr;
        }

        public void Add(T value)
        {

            Arr[Count] = value;
            Count++;

            if (Count == Arr.Length)
            {
                Resize();
            }
        }

        private void Resize()
        {
            T[] newArr = new T[Arr.Length * 2];

            for (int i = 0; i < Arr.Length; i++)
            {
                newArr[i] = Arr[i];
            }

            Arr = newArr;
        }
    }
}
