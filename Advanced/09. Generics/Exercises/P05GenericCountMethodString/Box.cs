﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P05GenericCountMethodString
{
    class Box<T> where T : IComparable
    {

        public int Compare(List<T> list, T element)
        {
            int count = 0;
            foreach (var item in list)
            {
                if (item.CompareTo(element) == 1)
                {
                    count++;
                }
            }

            return count;
        }
    }
}
