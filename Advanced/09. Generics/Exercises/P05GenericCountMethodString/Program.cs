﻿using System;
using System.Collections.Generic;

namespace P05GenericCountMethodString
{
    class Program
    {
        static void Main()
        {
            int number = int.Parse(Console.ReadLine());
            List<string> list = new List<string>();

            for (int i = 0; i < number; i++)
            {
                list.Add(Console.ReadLine());
            }

            string element = Console.ReadLine();

            Box<string> box = new Box<string>();

            Console.WriteLine(box.Compare(list, element));
        }
    }
}
