﻿using System;

namespace P09CustomLinkedList
{
    class StartUp
    {
        static void Main()
        {
            CustomDoublyLinkedList<int> custom = new CustomDoublyLinkedList<int>();

            custom.AddFirst(1);
            custom.AddFirst(3);
            custom.AddFirst(1);
            custom.AddFirst(5);

            custom.RemoveFirst();

            custom.AddFirst(6);
            custom.AddFirst(9);

            ListNode<int> current = custom.Head;

            for (int i = 0; i < custom.Count; i++)
            {
                Console.WriteLine(current.Value);
                current = current.Next;
            }
        }
    }
}
