﻿using System;

namespace P09CustomLinkedList
{
    public class CustomDoublyLinkedList<T>
    {
        public int Count { get; set; }
        public ListNode<T> Head { get; set; }
        public ListNode<T> Tail { get; set; }


        public T[] ToArray()
        {
            T[] arr = new T[Count];

            ListNode<T> currentNode = Head;

            int index = 0;

            while (currentNode != null)
            {
                arr[index] = currentNode.Value;
                currentNode = currentNode.Next;
                index++;
            }

            return arr;
        }

        public void ForEach(Action<T> action)
        {
            ListNode<T> currentNode = Head;

            while (currentNode != null)
            {
                action(currentNode.Value);
                currentNode = currentNode.Next;
            }
        }

        public T RemoveLast()
        {
            if (Count == 0)
            {
                throw new InvalidOperationException("The list is empty!");
            }

            T last = Tail.Value;

            Tail = Tail.Previous;

            if (Tail != null)
            {
                Tail.Next = null;
            }
            else
            {
                Head = null;
            }
            
            Count--;

            return last;
        }

        public T RemoveFirst()
        {
            if (Count == 0)
            {
                throw new InvalidOperationException("The list is empty!");
            }

            T first = Head.Value;

            Head = Head.Next;
            if (Head != null)
            {
                Head.Previous = null;
            }
            else
            {
                Tail = null;
            }
            
            Count--;

            return first;
        }

        public void AddLast(T element)
        {
            ListNode<T> newNode = new ListNode<T>(element);

            if (Head == null)
            {
                Head = newNode;
                Tail = newNode;
                Count++;

                return;
            }

            Tail.Next = newNode;
            newNode.Previous = Tail;
            Tail = newNode;
            Count++;
        }

        public void AddFirst(T element)
        {
            ListNode<T> newNode = new ListNode<T>(element);

            if (Head == null)
            {
                Head = newNode;
                Tail = newNode;
                Count++;

                return;
            }
           
            Head.Previous = newNode;
            newNode.Next = Head;
            Head = newNode;
            Count++;
        }
    }
}
