﻿using System;

namespace P02GenericBoxOfInteger
{
    class Program
    {
        static void Main()
        {
            int numberStrings = int.Parse(Console.ReadLine());

            for (int i = 0; i < numberStrings; i++)
            {
                Box<int> box = new Box<int>();

                box.Value = int.Parse(Console.ReadLine());

                Console.WriteLine(box.ToString());
            }
        }
    }
}
