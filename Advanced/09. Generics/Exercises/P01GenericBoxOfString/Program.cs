﻿using System;

namespace P01GenericBoxOfString
{
    class Program
    {
        static void Main()
        {
            int numberStrings = int.Parse(Console.ReadLine());
            

            for (int i = 0; i < numberStrings; i++)
            {
                Box<string> box = new Box<string>();

                box.Value = Console.ReadLine();

                Console.WriteLine(box.ToString());
                
            }
        }
    }
}
