﻿using System;
using System.Collections.Generic;
using System.Text;

namespace P01GenericBoxOfString
{
    class Box<T>
    {
        public T Value { get; set; }

        public override string ToString()
        {
            return $"{typeof(T)}: {Value}";
        }
    }
}
