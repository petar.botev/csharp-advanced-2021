﻿using System;
using System.Collections.Generic;

namespace P06GenericCountMethodDouble
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = int.Parse(Console.ReadLine());
            List<double> list = new List<double>();

            for (int i = 0; i < number; i++)
            {
                list.Add(double.Parse(Console.ReadLine()));
            }

            double element = double.Parse(Console.ReadLine());

            Box<double> box = new Box<double>();

            Console.WriteLine(box.Compare(list, element));
        }
    }
}
