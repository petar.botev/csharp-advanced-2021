﻿using System;
using System.Collections.Generic;

namespace P07Tuple
{
    public class Program
    {
        public static void Main()
        {
            string[] firstArgs = Console.ReadLine()
                .Split();

            string fullName = $"{firstArgs[0]} {firstArgs[1]}";
            string address = firstArgs[2];

            P07Tuple.Tuple<string, string> tuple1 = new Tuple<string, string>(fullName, address);

            string[] secondArgs = Console.ReadLine()
                .Split();

            string name = secondArgs[0];
            int beers = int.Parse(secondArgs[1]);

            P07Tuple.Tuple<string, int> secondTuple = new Tuple<string, int>(name, beers);

            string[] thirdArgs = Console.ReadLine()
                .Split();

            int intNumber = int.Parse(thirdArgs[0]);
            double doubleNUmber = double.Parse(thirdArgs[1]);

            P07Tuple.Tuple<int, double> thirdTuple = new Tuple<int, double>(intNumber, doubleNUmber);

            Console.WriteLine(tuple1.ToString());
            Console.WriteLine(secondTuple.ToString());
            Console.WriteLine(thirdTuple.ToString());
        }
    }
}
