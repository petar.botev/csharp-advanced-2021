﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace P03GenericSwapMethodString
{
    class Program
    {
        static void Main()
        {
            int number = int.Parse(Console.ReadLine());

            List<string> list = new List<string>();

            for (int i = 0; i < number; i++)
            {
                list.Add(Console.ReadLine());
            }

            int[] indexes = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(x => int.Parse(x))
                .ToArray();

            Swap(list, indexes[0], indexes[1]);

            Print(list);
        }

        static void Swap<T>(List<T> list, int index1, int index2)
        {
            T temp = list[index1];
            list[index1] = list[index2];
            list[index2] = temp;
        }

        static void Print<T>(List<T> list)
        {
            foreach (var item in list)
            {
                Console.WriteLine($"{typeof(T)}: {item}");
            }
        }
    }
}
