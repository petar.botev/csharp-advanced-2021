﻿using System;

namespace P08Threeuple
{
    public class Program
    {
        public static void Main()
        {
            string[] firstArgs = Console.ReadLine()
                .Split();

            string fullName = $"{firstArgs[0]} {firstArgs[1]}";
            string address = firstArgs[2];
            string town = firstArgs[3];

            P08Threeuple.Threeuple<string, string, string> tuple1 = new Threeuple<string, string, string>(fullName, address, town);

            string[] secondArgs = Console.ReadLine()
                .Split();

            string name = secondArgs[0];
            int beers = int.Parse(secondArgs[1]);
            bool drunk = secondArgs[2] == "drunk" ? true : false;

            P08Threeuple.Threeuple<string, int, bool> secondTuple = new Threeuple<string, int, bool>(name, beers, drunk);

            string[] thirdArgs = Console.ReadLine()
                .Split();

            string personName = thirdArgs[0];
            double doubleNUmber = double.Parse(thirdArgs[1]);
            string bankName = thirdArgs[2];

            P08Threeuple.Threeuple<string, double, string> thirdTuple = new Threeuple<string, double, string>(personName, doubleNUmber, bankName);

            Console.WriteLine(tuple1.ToString());
            Console.WriteLine(secondTuple.ToString());
            Console.WriteLine(thirdTuple.ToString());
        }
    }
}
