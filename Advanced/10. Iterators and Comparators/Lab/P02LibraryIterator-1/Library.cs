﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace IteratorsAndComparators
{
    public class Library : IEnumerable<Book>
    {

        public Library(params Book[] books)
        {
            Books = new List<Book>(books);
        }
        public List<Book> Books { get; set; } 

        public IEnumerator<Book> GetEnumerator()
        {
            return new LibraryIterator(Books);
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    class LibraryIterator : IEnumerator<Book>
    {
        //private int index = -1;
        //private readonly List<Book> books;

        //public LibraryIterator(IEnumerable<Book> books)
        //{
        //    Reset();
        //    this.books = new List<Book>(books);
        //}

        //public void Dispose()
        //{

        //}

        //public bool MoveNext() => ++index < books.Count;
        //public void Reset() => index = -1;
        //public Book Current => books[index];
        //object IEnumerator.Current => this.Current;

        public List<Book> Books { get; set; }
        public int currentIndex { get; set; }

        public LibraryIterator(IEnumerable<Book> books)
        {
            Books = new List<Book>(books);
            this.Reset();
        }

        public Book Current => Books[currentIndex];

        object IEnumerator.Current => Current;

        public void Dispose() => throw new NotImplementedException();
        public bool MoveNext() => ++currentIndex < Books.Count;
        public void Reset() => currentIndex = -1;
    }
}
