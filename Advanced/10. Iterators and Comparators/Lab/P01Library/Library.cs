﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace IteratorsAndComparators
{
    class Library : IEnumerable<Book>
    {

        public Library(params Book[] books)
        {
            Books = books;
        }
        public Book[] Books { get; set; }

        public IEnumerator<Book> GetEnumerator()
        {
            foreach (var item in Books)
            {
                yield return item;
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
