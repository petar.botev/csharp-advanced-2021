﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IteratorsAndComparators
{
    class Program
    {
        static void Main()
        {
            Book bookOne = new Book("Animal Farm", 2003, "George Orwell");
            Book bookTwo = new Book("The Documents in the Case", 2003, "Dorothy Sayers", "Robert Eustace");
            Book bookThree = new Book("The Documents in the Case", 1930);

            List<Book> list = new List<Book>();

            list.Add(bookOne);
            list.Add(bookTwo);
            list.Add(bookThree);

            list.Sort();

            Library libraryOne = new Library();
            Library libraryTwo = new Library(bookOne, bookTwo, bookThree);

            
            foreach (var book in list)
            {
                Console.WriteLine(book);
            }
        }
    }
}
