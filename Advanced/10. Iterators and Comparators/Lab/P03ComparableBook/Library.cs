﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace IteratorsAndComparators
{
    public class Library : IEnumerable<Book>
    {
        public List<Book> Books { get; set; }

        public Library(params Book[] books)
        {
            Books = new List<Book>(books);
        }

        public IEnumerator<Book> GetEnumerator()
        {
            return new LibraryIterator(Books);
        }

        //public IEnumerator<Book> GetEnumerator()
        //{
        //    for (int i = 0; i < Books.Count; i++)
        //    {
        //        yield return Books[i];
        //    }
        //}
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        class LibraryIterator : IEnumerator<Book>
        {
            public List<Book> Books { get; set; }
            public int currentIndex { get; set; }

            public LibraryIterator(IEnumerable<Book> books)
            {
                Books = new List<Book>(books);
                this.Reset();
            }

            public Book Current => Books[currentIndex];

            object IEnumerator.Current => Current;

            public void Dispose()
            {

            }
            public bool MoveNext() => ++currentIndex < Books.Count;
            public void Reset() => currentIndex = -1;
        }
    }
}
