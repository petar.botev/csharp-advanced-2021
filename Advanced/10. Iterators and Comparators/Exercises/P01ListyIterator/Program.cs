﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace P01ListyIterator
{
    class Program
    {
        static void Main()
        {

            ListyIterator<string> iterator = null;
            string command;
            while ((command = Console.ReadLine()) != "END")
            {
                string[] commandArgs = command.Split();
                switch (commandArgs[0])
                {
                    case "Create":
                        string[] parameters = commandArgs.Skip(1).ToArray();

                        iterator = new ListyIterator<string>(parameters);

                        break;
                    case "Move":
                        Console.WriteLine(iterator.Move()); 
                        break;
                    case "Print":
                        iterator.Print();
                        break;
                    case "HasNext":
                        Console.WriteLine(iterator.HasNext());
                        break;
                    case "END":

                        break;
                    default:
                        break;
                }
            }
        }

      
    }
}
