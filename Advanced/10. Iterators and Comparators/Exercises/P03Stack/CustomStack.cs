﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace P03Stack
{
    class CustomStack<T> : IEnumerable<T>
    {
        public CustomStack()
        {
            Elements = new List<T>();
        }

        public List<T> Elements { get; set; }

        public void Push(T item)
        {
            Elements.Add(item);
        }

        public T Pop()
        {
            if (Elements.Count == 0)
            {
                throw new ArgumentNullException("No elements");
            }

            T lastElement = Elements.Last();
            Elements.RemoveAt(Elements.Count - 1);
            return lastElement;
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < 2; i++)
            {
                for (int j = Elements.Count - 1; j >= 0; j--)
                {
                    yield return Elements[j];
                }
            }
            
        }

        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
    }
}
