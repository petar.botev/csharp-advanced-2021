﻿using System;

namespace P03Stack
{
    class Program
    {
        static void Main()
        {
            CustomStack<string> stack = new CustomStack<string>();
            string command;
            while ((command = Console.ReadLine()) != "END")
            {
                try
                {
                    string[] commandArgs = command.Split(new char[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries);
                    if (commandArgs[0] == "Push")
                    {
                        for (int i = 1; i < commandArgs.Length; i++)
                        {
                            stack.Push(commandArgs[i]);
                        }
                    }
                    else if (commandArgs[0] == "Pop")
                    {
                        stack.Pop();
                    }
                }
                catch (ArgumentNullException ane)
                {

                    Console.WriteLine(ane.ParamName);
                }
                
            }

            foreach (var item in stack)
            {
                Console.WriteLine(item);
            }
        }
    }
}
