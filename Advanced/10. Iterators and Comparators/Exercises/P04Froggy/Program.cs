﻿using System;
using System.Linq;

namespace P04Froggy
{
    class Program
    {
        static void Main()
        {
            int[] numbers = Console.ReadLine()
                .Split(", ")
                .Select(int.Parse)
                .ToArray();

            Lake lake = new Lake() { StoneNumbers = numbers.ToList() };

            Console.WriteLine(string.Join(", ", lake));
        }
    }
}
