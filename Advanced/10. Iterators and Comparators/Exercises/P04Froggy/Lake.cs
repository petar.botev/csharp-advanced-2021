﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace P04Froggy
{
    public class Lake : IEnumerable<int>
    {
        public Lake()
        {
            this.StoneNumbers = new List<int>();
        }
        public List<int> StoneNumbers { get; set; }

        public IEnumerator<int> GetEnumerator()
        {
            for (int i = 0; i < StoneNumbers.Count; i++)
            {
                if (i % 2 == 0)
                {
                    yield return StoneNumbers[i];
                }
                
            }
            for (int i = StoneNumbers.Count - 1; i >= 0; i--)
            {
                if (i % 2 != 0)
                {
                    yield return StoneNumbers[i];
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
    }
}
