﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace P02Collection
{
    public class ListyIterator<T> : IEnumerable<T>
    {
        private int index = 0;

        public ListyIterator(T[] collection)
        {
            Collection = new List<T>(collection);
        }

        public List<T> Collection { get; set; }


        public bool Move()
        {
            if (index < Collection.Count - 1)
            {
                index++;
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool HasNext()
        {
            if (index < Collection.Count - 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Print()
        {
            if (Collection.Count > 0)
            {
                Console.WriteLine($"{Collection[index]}");
            }
            else
            {
                Console.WriteLine("Invalid Operation!");
            }
        }

        public void PrintAll()
        {
            foreach (var item in Collection)
            {
                Console.Write($"{item} ");
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach (var item in Collection)
            {
                yield return item;
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
