﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace P05ComparingObjects
{
    public class Person : IComparable<Person>
    {
        private string name;
        private int age;
        private string town;

        public Person(string name, int age, string town)
        {
            this.Name = name;
            this.Age = age;
            this.Town = town;
        }

        public string Name { get => name; set => name = value; }
        public int Age { get => age; set => age = value; }
        public string Town { get => town; set => town = value; }

        public int CompareTo(Person other)
        {
            int compareNames = this.Name.CompareTo(other.Name);
            if (compareNames == 0)
            {
                int compareAge = this.Age.CompareTo(other.Age);
                if (compareAge == 0)
                {
                    return this.Town.CompareTo(other.Town);
                }
                return compareAge;
            }

            return compareNames;
        }
    }
}
