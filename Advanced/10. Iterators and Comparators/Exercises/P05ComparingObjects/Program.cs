﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace P05ComparingObjects
{
    class Program
    {
        static void Main()
        {
            List<Person> persons = new List<Person>();
            string command;
            while ((command = Console.ReadLine()) != "END")
            {
                string[] commandArgs = command
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                string name = commandArgs[0];
                int age = int.Parse(commandArgs[1]);
                string town = commandArgs[2];

                Person person = new Person(name, age, town);

                persons.Add(person);

            }

            int number = int.Parse(Console.ReadLine());
            int personCount = persons.Count();
            Person compareToPerson = persons[number - 1];
            persons.RemoveAt(number - 1);

            int equalTo = persons.Where(x => x.CompareTo(compareToPerson) == 0).Count();
            int notEqualTo = persons.Where(x => x.CompareTo(compareToPerson) != 0).Count();

            if (equalTo == 0)
            {
                Console.WriteLine("No matches");
            }
            else
            {
                Console.WriteLine($"{equalTo + 1} {notEqualTo} {personCount}");
            }
        }
    }
}
