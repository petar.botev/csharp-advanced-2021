﻿using System;

namespace MyLinkedList
{
    class Program
    {
        static void Main()
        {
            LinkedList linkedList = new LinkedList();

            linkedList.AddFirst(new Node() { Value = 4 });
            linkedList.AddFirst(new Node() { Value = 5 });
            linkedList.AddLast(new Node() { Value = 6 });
            linkedList.AddLast(new Node() { Value = 3 });

            linkedList.RemoveFirst();

            linkedList.Foreach(x => Console.WriteLine(x.Value));

            int[] arr = linkedList.ToArray();

            Node curr = linkedList.Head;

            while (curr != null)
            {
                Console.WriteLine(curr.Value);
                curr = curr.Next;
            }

        }
    }
}
