﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyLinkedList
{
    class LinkedList
    {
        private int Count { get; set; }
        public Node Head { get; set; }
        public Node Tail { get; set; }

        public int[] ToArray()
        {
            int[] array = new int[Count];

            Node curr = Head;
            int index = 0;
            while (curr != null)
            {
                array[index] = curr.Value;
                curr = curr.Next;
                index++;
            }

            return array;
        }


        public void Foreach(Action<Node> action)
        {
            Node currNode = Head;
            while (currNode != null)
            {
                action(currNode);
                currNode = currNode.Next;
            }
        }

        public void RemoveLast()
        {
            if (Tail.Previous != null)
            {
                Tail.Previous.Next = null;
                Tail = Tail.Previous;
                Count--;
            }
            else
            {
                Tail = null;
                Head = null;
                Count--;
            }
        }

        public void RemoveFirst()
        {
            if (Head.Next != null)
            {
                Head.Next.Previous = null;
                Head = Head.Next;
            }
            else
            {
                Head = null;
                Tail = null;
            }
            Count--;
        }

        public void AddFirst(Node newNode)
        {
            if (Head == null)
            {
                Head = newNode;
                Tail = newNode;
                Count++;
                return;
            }
            newNode.Next = Head;

            Head = newNode;
            Count++;
        }

        public void AddLast(Node newNode)
        {
            if (Tail == null)
            {
                Head = newNode;
                Tail = newNode;
                Count++;
                return;
            }

            newNode.Previous = Tail;
            Tail.Next = newNode;
            Tail = newNode;
            Count++;
        }
    }
}
