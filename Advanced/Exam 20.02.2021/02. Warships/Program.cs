﻿using System;
using System.Linq;

namespace _02._Warships
{
    class Program
    {
        static void Main()
        {
            int matrixSize = int.Parse(Console.ReadLine());
            char[][] matrix = new char[matrixSize][];
            string[] coordinates = Console.ReadLine()
                .Split(",", StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < matrixSize; i++)
            {
                matrix[i] = Console.ReadLine()
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                    .Select(char.Parse)
                    .ToArray();
            }
            int playerOneShipCount = 0;
            int playerTwoShipCount = 0;
            for (int i = 0; i < matrixSize; i++)
            {
                for (int j = 0; j < matrixSize; j++)
                {
                    if (matrix[i][j] == '<')
                    {
                        playerOneShipCount++;
                    }
                    if (matrix[i][j] == '>')
                    {
                        playerTwoShipCount++;
                    }
                }
            }
            int playerOneDestroiedShips = 0;
            int playerTwoDestroiedShips = 0;

            for (int i = 0; i < coordinates.Length; i++)
            {
                int[] currentCoordinates = coordinates[i]
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                    .Select(int.Parse)
                    .ToArray();
                int x = currentCoordinates[0];
                int y = currentCoordinates[1];
                if (x < 0 || y < 0 || x >= matrixSize || y >= matrixSize)
                {
                    continue;
                }
                if (i % 2 == 0)
                {
                    if (matrix[x][y] == '>')
                    {
                        matrix[x][y] = 'X';
                        playerTwoShipCount--;
                        playerOneDestroiedShips++;
                        if (playerTwoShipCount <= 0)
                        {
                            Console.WriteLine($"Player One has won the game! {playerOneDestroiedShips} ships have been sunk in the battle.");
                        }
                    }
                    else if (matrix[x][y] == '#')
                    {
                        matrix[x][y] = 'X';
                        Explosion(matrix, x, y, ref playerOneShipCount, ref playerTwoShipCount, ref playerOneDestroiedShips, ref playerTwoDestroiedShips, i);
                        if (playerTwoShipCount <= 0)
                        {
                            Console.WriteLine($"Player One has won the game! {playerOneDestroiedShips} ships have been sunk in the battle.");
                        }
                    }
                }
                else
                {
                    if (matrix[x][y] == '<')
                    {
                        matrix[x][y] = 'X';
                        playerOneShipCount--;
                        playerTwoDestroiedShips++;
                        if (playerOneShipCount <= 0)
                        {
                            Console.WriteLine($"Player Two has won the game! {playerTwoDestroiedShips} ships have been sunk in the battle.");
                        }
                    }
                    else if (matrix[x][y] == '#')
                    {
                        matrix[x][y] = 'X';
                        Explosion(matrix, x, y, ref playerOneShipCount, ref playerTwoShipCount, ref playerOneDestroiedShips, ref playerTwoDestroiedShips, i);
                        if (playerOneShipCount <= 0)
                        {
                            Console.WriteLine($"Player Two has won the game! {playerTwoDestroiedShips} ships have been sunk in the battle.");
                        }
                    }
                }
            }
            if (playerOneShipCount > 0 && playerTwoShipCount > 0)
            {
                Console.WriteLine($"It's a draw! Player One has {playerOneShipCount} ships left. Player Two has {playerTwoShipCount} ships left.");
            }
        }
        static void DestroiedShips(int i, ref int playerOneDestroiedShips, ref int playerTwoDestroiedShips)
        {
            if (i % 2 == 0)
            {
                playerOneDestroiedShips++;
            }
            else
            {
                playerTwoDestroiedShips++;
            }
        }
        static void Explosion(char[][] matrix, int x, int y, ref int playerOneShipCount, ref int playerTwoShipCount, ref int playerOneDestroiedShips, ref int playerTwoDestroiedShips, int i)
        {
            if (x - 1 >= 0)
            {
                if (matrix[x - 1][y] == '<')
                {
                    playerOneShipCount--;
                    DestroiedShips(i, ref playerOneDestroiedShips, ref playerTwoDestroiedShips);
                    
                    matrix[x - 1][y] = 'X';
                }
                if (matrix[x - 1][y] == '>')
                {
                    playerTwoShipCount--;
                    DestroiedShips(i, ref playerOneDestroiedShips, ref playerTwoDestroiedShips);
                    matrix[x - 1][y] = 'X';
                }
                if (y - 1 >= 0)
                {
                    if (matrix[x - 1][y - 1] == '<')
                    {
                        playerOneShipCount--;
                        DestroiedShips(i, ref playerOneDestroiedShips, ref playerTwoDestroiedShips);
                        matrix[x - 1][y - 1] = 'X';
                    }
                    if (matrix[x - 1][y - 1] == '>')
                    {
                        playerTwoShipCount--;
                        DestroiedShips(i, ref playerOneDestroiedShips, ref playerTwoDestroiedShips);
                        matrix[x - 1][y - 1] = 'X';
                    }
                }
                if (y + 1 >= 0)
                {
                    if (matrix[x - 1][y + 1] == '<')
                    {
                        playerOneShipCount--;
                        DestroiedShips(i, ref playerOneDestroiedShips, ref playerTwoDestroiedShips);
                        matrix[x - 1][y + 1] = 'X';
                    }
                    if (matrix[x - 1][y + 1] == '>')
                    {
                        playerTwoShipCount--;
                        DestroiedShips(i, ref playerOneDestroiedShips, ref playerTwoDestroiedShips);
                        matrix[x - 1][y + 1] = 'X';
                    }
                }
            }
            if (x + 1 < matrix.Length)
            {
                if (matrix[x + 1][y] == '<')
                {
                    playerOneShipCount--;
                    DestroiedShips(i, ref playerOneDestroiedShips, ref playerTwoDestroiedShips);
                    matrix[x + 1][y] = 'X';
                }
                if (matrix[x + 1][y] == '>')
                {
                    playerTwoShipCount--;
                    DestroiedShips(i, ref playerOneDestroiedShips, ref playerTwoDestroiedShips);
                    matrix[x + 1][y] = 'X';
                }
                if (y - 1 >= 0)
                {
                    if (matrix[x + 1][y - 1] == '<')
                    {
                        playerOneShipCount--;
                        DestroiedShips(i, ref playerOneDestroiedShips, ref playerTwoDestroiedShips);
                        matrix[x + 1][y - 1] = 'X';
                    }

                    if (matrix[x + 1][y - 1] == '>')
                    {
                        playerTwoShipCount--;
                        DestroiedShips(i, ref playerOneDestroiedShips, ref playerTwoDestroiedShips);
                        matrix[x + 1][y - 1] = 'X';
                    }
                }
                if (y + 1 < matrix.Length)
                {
                    if (matrix[x + 1][y + 1] == '<')
                    {
                        playerOneShipCount--;
                        DestroiedShips(i, ref playerOneDestroiedShips, ref playerTwoDestroiedShips);
                        matrix[x + 1][y + 1] = 'X';
                    }

                    if (matrix[x + 1][y + 1] == '>')
                    {
                        playerTwoShipCount--;
                        DestroiedShips(i, ref playerOneDestroiedShips, ref playerTwoDestroiedShips);
                        matrix[x + 1][y + 1] = 'X';
                    }
                }
            }
            if (y - 1 >= 0)
            {
                if (matrix[x][y - 1] == '<')
                {
                    playerOneShipCount--;
                    DestroiedShips(i, ref playerOneDestroiedShips, ref playerTwoDestroiedShips);
                    matrix[x][y - 1] = 'X';
                }

                if (matrix[x][y - 1] == '>')
                {
                    playerTwoShipCount--;
                    DestroiedShips(i, ref playerOneDestroiedShips, ref playerTwoDestroiedShips);
                    matrix[x][y - 1] = 'X';
                }
            }
            if (y + 1 < matrix.Length)
            {
                if (matrix[x][y + 1] == '<')
                {
                    playerOneShipCount--;
                    DestroiedShips(i, ref playerOneDestroiedShips, ref playerTwoDestroiedShips);
                    matrix[x][y + 1] = 'X';
                }

                if (matrix[x][y + 1] == '>')
                {
                    playerTwoShipCount--;
                    DestroiedShips(i, ref playerOneDestroiedShips, ref playerTwoDestroiedShips);
                    matrix[x][y + 1] = 'X';
                }
            }
        }
    }
}
