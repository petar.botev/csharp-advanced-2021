﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TheRace
{
    public class Race
    {
        private List<Racer> data;
        public Race(string name, int capacity)
        {
            Name = name;
            Capacity = capacity;
            this.data = new List<Racer>();
        }

        public string Name { get; set; }
        public int Capacity { get; set; }
        public int Count { get => data.Count; }


        public string Report()
        {
            StringBuilder strb = new StringBuilder();
            strb.AppendLine($"Racers participating at {Name}:");

            foreach (var item in data)
            {
                strb.AppendLine($"{item.ToString()}");
            }

            return strb.ToString().Trim();
        }

        public void Add(Racer racer)
        {
            if (Capacity > data.Count)
            {
                data.Add(racer);
            }
        }

        public bool Remove(string name)
        {
            var racer = data.FirstOrDefault(x => x.Name == name);

            if (racer != null)
            {
                data.Remove(racer);
                return true;
            }
            return false;
        }

        public Racer GetOldestRacer()
        {
            int oldest = 0;

            if (data.Count > 0)
            {
                oldest = data.Select(x => x.Age).Max();
            }

            return data.FirstOrDefault(x => x.Age == oldest);
        }

        public Racer GetRacer(string name)
        {
            return data.FirstOrDefault(x => x.Name == name);
        }

        public Racer GetFastestRacer()
        {
            int highestSpeed = data.Select(x => x.Car.Speed).Max();

            return data.FirstOrDefault(x => x.Car.Speed == highestSpeed);
        }


    }
}
