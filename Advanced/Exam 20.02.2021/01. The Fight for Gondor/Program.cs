﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _01._The_Fight_for_Gondor
{
    class Program
    {
        static void Main()
        {
            int wavesNumber = int.Parse(Console.ReadLine());
            
            List<int> plates = new List<int>(Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse));

            Stack<int> orcs = null;
            int countWaves = 1;

            for (int i = 0; i < wavesNumber; i++)
            {

                orcs = new Stack<int>(Console.ReadLine()
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                    .Select(int.Parse));

                while (orcs.Count > 0 && plates.Count > 0)
                {
                    if (plates[0] > orcs.Peek())
                    {
                        plates[0] = plates[0] - orcs.Peek();
                        
                        orcs.Pop();
                        
                    }
                    else if (plates[0] < orcs.Peek())
                    {
                        orcs.Push(orcs.Pop() - plates[0]);
                        plates.RemoveAt(0);
                    }
                    else if (plates[0] == orcs.Peek())
                    {
                        plates.RemoveAt(0);
                        orcs.Pop();
                    }
                }

                if (countWaves % 3 == 0)
                {
                        plates.Add(int.Parse(Console.ReadLine()));
                }

                countWaves++;

                if (plates.Count <= 0)
                {
                    break;
                }
            }

            if (plates.Count <= 0)
            {
                Console.WriteLine("The orcs successfully destroyed the Gondor's defense.");
                Console.WriteLine($"Orcs left: {string.Join(", ", orcs)}");
            }
            else if (orcs.Count <= 0)
            {
                Console.WriteLine("The people successfully repulsed the orc's attack.");
                Console.WriteLine($"Plates left: {string.Join(", ", plates)}");
            }
        }
    }
}
