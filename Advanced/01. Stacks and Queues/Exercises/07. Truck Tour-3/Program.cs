﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _07._Truck_Tour_3
{
    class Program
    {
        static void Main()
        {
            int numberOfPumps = int.Parse(Console.ReadLine());

            Queue<string> queue = new Queue<string>();
            Queue<string> paralel = new Queue<string>();

            for (int i = 0; i < numberOfPumps; i++)
            {
                var input = Console.ReadLine();
                queue.Enqueue(input);
                paralel.Enqueue(input);
            }

            
            for (int i = 0; i < numberOfPumps; i++)
            {
                int fuel = 0;
                bool isCyrcle = true;

                for (int j = 0; j < numberOfPumps; j++)
                {
                    
                    int[] pumpData = queue.Dequeue()
                        .Split()
                        .Select(int.Parse)
                        .ToArray();

                    queue.Enqueue(string.Join(" ", pumpData));

                    fuel += pumpData[0];
                    fuel -= pumpData[1];

                    if (fuel < 0)
                    {
                        isCyrcle = false;
                        
                        break;
                    }
                }

                if (isCyrcle)
                {
                    Console.WriteLine(i);
                    break;
                }

                paralel.Enqueue(paralel.Dequeue());
                queue = new Queue<string>(paralel);
            }
        }
    }
}
