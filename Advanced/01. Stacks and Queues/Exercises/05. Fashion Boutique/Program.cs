﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _05._Fashion_Boutique
{
    class Program
    {
        static void Main()
        {
            int[] clothes = Console.ReadLine()
                .Split()
                .Select(int.Parse)
                .ToArray();

            Stack<int> stack = new Stack<int>(clothes);

            int rackCapacity = int.Parse(Console.ReadLine());
            int startRackCapacity = rackCapacity;
            int numberRacks = 0;
            decimal sumValues = 0;

            while (stack.Count > 0)
            {
                if (rackCapacity > 0)
                {
                    if (rackCapacity - stack.Peek() < 0)
                    {
                        numberRacks++;
                        rackCapacity = startRackCapacity;

                        continue; ;
                    }
                    rackCapacity -= stack.Peek();
                    sumValues += stack.Pop();
                }
                else
                {
                    numberRacks++;
                    rackCapacity = startRackCapacity;

                }
            }

            if (rackCapacity < startRackCapacity)
            {
                numberRacks++;
            }

            Console.WriteLine(numberRacks);
        }
    }
}
