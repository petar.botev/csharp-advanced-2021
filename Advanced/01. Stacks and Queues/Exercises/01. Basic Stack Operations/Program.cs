﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _01._Basic_Stack_Operations
{
    class Program
    {
        static void Main()
        {
            int[] commands = Console.ReadLine()
                .Split()
                .Select(int.Parse)
                .ToArray();

            int pushElements = commands[0];
            int popElements = commands[1];
            int findElement = commands[2];

            int[] elements = Console.ReadLine()
                .Split()
                .Select(int.Parse)
                .ToArray();

            Stack<int> stack = new Stack<int>();

            for(int i = 0; i < pushElements; i++)
            {
                stack.Push(elements[i]);
            }

            for (int i = 0; i < popElements; i++)
            {
                stack.Pop();
            }

            var result = 0; ;

            if (stack.Count > 0)
            {
                result = stack.FirstOrDefault(x => x == findElement);

                if (result == 0)
                {
                    Console.WriteLine(stack.Min());
                }
                else
                {
                    Console.WriteLine("true");
                }
            }
            else
            {
                Console.WriteLine(0);
            }
        }
    }
}
