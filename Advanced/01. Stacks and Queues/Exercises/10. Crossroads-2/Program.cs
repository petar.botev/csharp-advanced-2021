﻿using System;
using System.Collections.Generic;

namespace _10._Crossroads_2
{
    class Program
    {
        static void Main()
        {
            int greenLightSeconds = int.Parse(Console.ReadLine());
            int freeWindowSeconds = int.Parse(Console.ReadLine());

            Queue<string> carsQueue = new Queue<string>();

            int carPassed = 0;

            int tempGreenSecconds = 0;
            int tempWindowSeconds = 0;

            string command;
            while ((command = Console.ReadLine()) != "END")
            {
               
                if (command != "green")
                {
                    carsQueue.Enqueue(command.Trim());
                }
                else
                {
                    tempGreenSecconds = greenLightSeconds;
                    tempWindowSeconds = freeWindowSeconds;

                    while (tempGreenSecconds > 0 && carsQueue.Count > 0)
                    {
                        PassingCar(carsQueue.Dequeue(), ref tempGreenSecconds, ref tempWindowSeconds);
                        carPassed++;
                    }
                }
            }

            Console.WriteLine("Everyone is safe.");
            Console.WriteLine($"{carPassed} total cars passed the crossroads.");
        }

        static void PassingCar(string car, ref int tempGreenSecconds, ref int tempWindowSeconds)
        {
            Queue<char> carElements = new Queue<char>(car.Trim().ToCharArray());

            while (tempGreenSecconds > 0 && carElements.Count > 0)
            {
                carElements.Dequeue();
                tempGreenSecconds--;
            }

            while (tempWindowSeconds > 0 && carElements.Count > 0)
            {
                carElements.Dequeue();
                tempWindowSeconds--;
            }

            if (carElements.Count > 0)
            {
                Crash(car, carElements.Dequeue());
            }
        }

        static void Crash(string car, char carElement)
        {
            Console.WriteLine("A crash happened!");
            Console.WriteLine($"{car} was hit at {carElement}.");

            System.Environment.Exit(1);
        }
    }
}
