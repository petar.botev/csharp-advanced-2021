﻿using System;
using System.Collections.Generic;

namespace _08._Balanced_Parenthesis
{
    class Program
    {
        static void Main()
        {
            string parenthesis = Console.ReadLine();

            Stack<char> stack = new Stack<char>();

            if (parenthesis.Length % 2 != 0)
            {
                Console.WriteLine("NO");
                return;
            }

            for (int i = 0; i < parenthesis.Length; i++)
            {
                if (parenthesis[i] == '(' || parenthesis[i] == '{' || parenthesis[i] == '[')
                {
                    stack.Push(parenthesis[i]);
                }
                else
                {
                    var tempChar = stack.Pop();
                    if (tempChar == parenthesis[i] - 1 || tempChar == parenthesis[i] - 2)
                    {

                    }
                    else
                    {
                        Console.WriteLine("NO");
                        return;
                    }
                    
                }
            }

            
            Console.WriteLine("YES");
        }
    }
}
