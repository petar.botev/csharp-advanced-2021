﻿using System;
using System.Collections.Generic;

namespace _09._Simple_Text_Editor
{
    class Program
    {
        static void Main()
        {
            int numberOfCommands = int.Parse(Console.ReadLine());

            string mainString = "";

            Stack<string> stack = new Stack<string>();

           
            for (int i = 0; i < numberOfCommands; i++)
            {
                string[] commandArgs = Console.ReadLine()
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                switch (commandArgs[0])
                {
                    case "1":
                        string strToAdd = commandArgs[1];

                        stack.Push(mainString);

                        mainString = mainString + strToAdd;

                        break;
                    case "2":
                        int eraze = int.Parse(commandArgs[1]);

                        stack.Push(mainString);

                        mainString = mainString.Substring(0, mainString.Length - eraze);

                        break;
                    case "3":
                        int index = int.Parse(commandArgs[1]);

                        Console.WriteLine(mainString[index - 1].ToString());
                        break;
                    case "4":
                        mainString = stack.Pop();
                        break;
                    default:
                        break;
                }

            }
        }
    }
}
