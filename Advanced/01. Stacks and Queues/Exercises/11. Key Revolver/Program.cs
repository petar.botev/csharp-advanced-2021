﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _11._Key_Revolver
{
    class Program
    {
        static void Main()
        {
            int bulletPrice = int.Parse(Console.ReadLine());
            int gunBarrel = int.Parse(Console.ReadLine());

            Stack<int> bullets = new Stack<int>(Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray());

            Queue<int> locks = new Queue<int>(Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray());

            int valueInTheSafe = int.Parse(Console.ReadLine());

            Shoot(bullets, locks, valueInTheSafe, bulletPrice, gunBarrel);
        }

        static void Shoot(Stack<int> bullets, Queue<int> locks, int valueInTheSafe, int bulletPrice, int gunBarrel)
        {
            int shootCounter = 0;
            bool isEnd = false;

            while (isEnd == false)
            {
                
                if (bullets.Pop() <= locks.Peek())
                {
                    shootCounter++;
                    Console.WriteLine("Bang!");

                    locks.Dequeue();
                }
                else
                {
                    shootCounter++;
                    Console.WriteLine("Ping!");
                }

                if (shootCounter == gunBarrel && bullets.Count > 0)
                {
                    Console.WriteLine("Reloading!");
                    shootCounter = 0;
                }

                valueInTheSafe -= bulletPrice;

                if (bullets.Count == 0 && locks.Count > 0)
                {
                    Console.WriteLine($"Couldn't get through. Locks left: {locks.Count}");
                    isEnd = true;
                }
                if (locks.Count == 0)
                {
                    Console.WriteLine($"{bullets.Count} bullets left. Earned ${valueInTheSafe}");
                    isEnd = true;
                }
            }
        }
    }
}
