﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _07._Truck_Tour_2
{
    class Program
    {
        static void Main()
        {
            int numberOfPumps = int.Parse(Console.ReadLine());

            Queue<string[]> queue = new Queue<string[]>();

            for (int i = 0; i < numberOfPumps; i++)
            {
                string[] parameters = Console.ReadLine()
                    .Split();

                queue.Enqueue(parameters);
            }
            
            int distance = 0;
            int fuel = 0;

            for (int i = 0; i < numberOfPumps; i++)
            {
                int fuelInTank = 0;
                bool isCyrcle = true;

                for (int j = 0; j < numberOfPumps; j++)
                {
                    fuel = int.Parse(queue.Peek()[0]);
                    distance = int.Parse(queue.Peek()[1]);

                    fuelInTank += fuel - distance;

                    queue.Enqueue(queue.Dequeue());

                    if (fuelInTank < 0)
                    {
                        isCyrcle = false;
                        break;
                    }
                }

                if (isCyrcle)
                {
                    Console.WriteLine(i);
                    break;
                }

                queue.Enqueue(queue.Dequeue());
            }
        }
    }
}
