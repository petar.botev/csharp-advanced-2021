﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _02._Basic_Queue_Operations
{
    class Program
    {
        static void Main()
        {
            int[] commands = Console.ReadLine()
                .Split()
                .Select(int.Parse)
                .ToArray();

            int enqueueElements = commands[0];
            int dequeueElements = commands[1];
            int findElement = commands[2];

            int[] elements = Console.ReadLine()
                .Split()
                .Select(int.Parse)
                .ToArray();

            Queue<int> queue = new Queue<int>();

            for(int i = 0; i < enqueueElements; i++)
            {
                queue.Enqueue(elements[i]);
            }

            for (int i = 0; i < dequeueElements; i++)
            {
                queue.Dequeue();
            }

            if (queue.Count > 0)
            {
                int result = queue.FirstOrDefault(x => x == findElement);

                if (result == 0)
                {
                    Console.WriteLine(elements.Min());
                }
                else
                {
                    Console.WriteLine("true");
                }
            }
            else
            {
                Console.WriteLine(0);
            }
        }
    }
}
