﻿using System;
using System.Collections.Generic;

namespace _10._Crossroads
{
    class Program
    {
        static void Main()
        {
            int greenLightSeconds = int.Parse(Console.ReadLine());
            int freeWindowSeconds = int.Parse(Console.ReadLine());

            int tempGreen = greenLightSeconds;
            int tempWindow = freeWindowSeconds;

            Queue<string> carsQueue = new Queue<string>();
            
            int carPassed = 0;

            string command;
            while ((command = Console.ReadLine()) != "END")
            {
                if (command != "green")
                {
                    carsQueue.Enqueue(command);
                }
                else
                {
                    while (carsQueue.Count > 0 && tempGreen > 0 && tempWindow > 0)
                    {
                        string car = carsQueue.Dequeue();

                        Queue<char> carElements = new Queue<char>(car.ToCharArray());

                        for (int i = 0; i < greenLightSeconds; i++)
                        {
                            

                            if (carElements.Count == 0)
                            {
                                carPassed++;
                                
                                if (carsQueue.Count > 0)
                                {
                                    car = carsQueue.Dequeue();
                                    carElements = new Queue<char>(car.ToCharArray());
                                    tempGreen = greenLightSeconds - i;
                                    i--;
                                    continue;
                                }
                                else
                                {
                                    break;
                                }

                                

                            }

                            carElements.Dequeue();
                            tempGreen--;
                        }

                        if (carElements.Count > 0)
                        {
                            for (int i = 0; i < freeWindowSeconds; i++)
                            {

                                if (carElements.Count > 0)
                                {
                                    carElements.Dequeue();
                                }
                                else
                                {
                                    break;
                                }

                                tempWindow--;

                            }

                            if (carElements.Count == 0)
                            {
                                carPassed++;
                            }
                        }

                        if (carElements.Count > 0)
                        {
                            Console.WriteLine("A crash happened!");
                            Console.WriteLine($"{car} was hit at {carElements.Dequeue()}.");
                            return;
                        }
                    }
                }
            }

            Console.WriteLine("Everyone is safe.");
            Console.WriteLine($"{carPassed} total cars passed the crossroads.");
        }
    }
}
