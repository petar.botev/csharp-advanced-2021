﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _07._Truck_Tour
{
    class Program
    {
        static void Main()
        {
            int numberOfPumps = int.Parse(Console.ReadLine());

            Queue<int> fuelQueue = new Queue<int>();
            Queue<int> distanceQueue = new Queue<int>();

            for(int i = 0; i < numberOfPumps; i++)
            {
                int[] parameters = Console.ReadLine()
                    .Split()
                    .Select(int.Parse)
                    .ToArray();

                fuelQueue.Enqueue(parameters[0]);
                distanceQueue.Enqueue(parameters[1]);
            }

            int counter = 0;
            int leftFuel = 0;

            for (int i = 0; i < numberOfPumps; i++)
            {
                if (fuelQueue.Peek() + leftFuel >= distanceQueue.Peek())
                {
                    leftFuel += (fuelQueue.Peek() - distanceQueue.Peek());

                    if (leftFuel < 0)
                    {
                        i = -1;

                        counter++;

                        fuelQueue.Enqueue(fuelQueue.Dequeue());
                        distanceQueue.Enqueue(distanceQueue.Dequeue());
                    }
                    else
                    {
                        fuelQueue.Enqueue(fuelQueue.Dequeue());
                        distanceQueue.Enqueue(distanceQueue.Dequeue());
                    }
                }
                else
                {
                    i = -1;

                    counter++;

                    fuelQueue.Enqueue(fuelQueue.Dequeue());
                    distanceQueue.Enqueue(distanceQueue.Dequeue());
                }

                if (counter == numberOfPumps - 1)
                {
                    break;
                }
            }

            Console.WriteLine(counter);
        }
    }
}
