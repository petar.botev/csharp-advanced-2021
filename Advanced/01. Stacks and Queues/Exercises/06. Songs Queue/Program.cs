﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _06._Songs_Queue
{
    class Program
    {
        static void Main()
        {
            string[] songs = Console.ReadLine()
                .Split(", ", StringSplitOptions.RemoveEmptyEntries);

            Queue<string> queue = new Queue<string>(songs);

            while (queue.Count > 0)
            {
                string[] commandArgs = Console.ReadLine()
                    .Split();

                switch (commandArgs[0])
                {
                    case "Play":
                        queue.Dequeue();
                        break;
                    case "Add":
                        string[] songName = commandArgs.Skip(1).ToArray();
                        string newSong = string.Join(' ', songName);

                        if (queue.Contains(newSong))
                        {
                            Console.WriteLine($"{newSong} is already contained!");
                        }
                        else
                        {
                            queue.Enqueue(newSong);
                        }
                        break;
                    case "Show":
                        Console.WriteLine(string.Join(", ", queue));
                        break;
                    default:
                        break;
                }
            }

            Console.WriteLine("No more songs!");
        }
    }
}
