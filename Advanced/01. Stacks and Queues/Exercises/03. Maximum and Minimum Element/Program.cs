﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _03._Maximum_and_Minimum_Element
{
    class Program
    {
        static void Main()
        {
            int numberQueries = int.Parse(Console.ReadLine());

            Stack<int> stack = new Stack<int>();

            for (int i = 0; i < numberQueries; i++)
            {
                int[] queries = Console.ReadLine()
                    .Split()
                    .Select(int.Parse)
                    .ToArray();

                switch (queries[0])
                {
                    case 1:
                        int element = queries[1];
                        stack.Push(element);
                        break;
                    case 2:
                        if (stack.Count > 0)
                        {
                            stack.Pop();
                        }
                        break;
                    case 3:
                        if (stack.Count > 0)
                        {
                            Console.WriteLine(stack.Max());
                        }
                        break;
                    case 4:
                        if (stack.Count > 0)
                        {
                            Console.WriteLine(stack.Min());
                        }
                        break;
                    default:
                        break;
                }
            }
            
            Console.WriteLine(string.Join(", ", stack));
        }
    }
}
