﻿using System;
using System.Collections.Generic;

namespace _1._Reverse_Strings
{
    class Program
    {
        static void Main()
        {
            string words = Console.ReadLine();

            Stack<string> stack = new Stack<string>();

            foreach (var letter in words)
            {
                stack.Push(letter.ToString());
            }

            Console.WriteLine(string.Join("", stack));
        }
    }
}
