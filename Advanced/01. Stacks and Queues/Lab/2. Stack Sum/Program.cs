﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _2._Stack_Sum
{
    class Program
    {
        static void Main()
        {
            string[] numbers = Console.ReadLine()
                 .Split(' ', StringSplitOptions.RemoveEmptyEntries);

            Stack<string> stack = new Stack<string>(numbers);

            string command;
            while ((command = Console.ReadLine().ToLower()) != "end")
            {
                string[] commandArgs = command
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                string theCommand = commandArgs[0];

                switch (theCommand)
                {
                    case "add":
                        string firstNum = commandArgs[1];
                        string secondNum = commandArgs[2];

                        stack.Push(firstNum);
                        stack.Push(secondNum);

                        break;
                    case "remove":

                        string num = commandArgs[1];

                        if (int.Parse(num) <= stack.Count())
                        {
                            for (int i = 0; i < int.Parse(num); i++)
                            {
                                stack.Pop();
                            }
                        }

                        break;

                    default:
                        break;
                }
            }

            Console.WriteLine($"Sum: {stack.Sum(int.Parse)}");
        }
    }
}
