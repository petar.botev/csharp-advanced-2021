﻿using System;
using System.Collections.Generic;

namespace _8._Traffic_Jam
{
    class Program
    {
        static void Main()
        {
            int carsPassed = int.Parse(Console.ReadLine());

            Queue<string> queue = new Queue<string>();
            int countOfCarsPassed = 0;

            string command;
            while ((command = Console.ReadLine()) != "end")
            {
                if (command != "green")
                {
                    queue.Enqueue(command);
                }
                else
                {
                    for (int i = 0; i < carsPassed; i++)
                    {
                        if (queue.Count > 0)
                        {
                            countOfCarsPassed++;
                            Console.WriteLine($"{queue.Dequeue()} passed!");
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }

            Console.WriteLine($"{countOfCarsPassed} cars passed the crossroads.");
        }
    }
}
