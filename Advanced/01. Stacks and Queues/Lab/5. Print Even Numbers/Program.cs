﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _5._Print_Even_Numbers
{
    class Program
    {
        static void Main()
        {
            Queue<int> queue = new Queue<int>(Console.ReadLine().Split().Select(int.Parse));

            List<int> list = new List<int>();

            while (queue.Count > 0)
            {
                int num = queue.Dequeue();

                if (num % 2 == 0)
                {
                    list.Add(num);
                }
            }

            Console.WriteLine(string.Join(", ", list));
        }
    }
}
