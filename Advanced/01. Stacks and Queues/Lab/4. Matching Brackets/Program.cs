﻿using System;
using System.Collections.Generic;

namespace _4._Matching_Brackets
{
    class Program
    {
        static void Main()
        {
            string expression = Console.ReadLine();

            Stack<int> stack = new Stack<int>();

            for(int i = 0; i < expression.Length; i++)
            {
                if (expression[i] == '(')
                {
                    stack.Push(i);
                }
                if (expression[i] == ')')
                {
                    int openInd = stack.Pop();

                    int length = i - openInd + 1;

                    string subExpression = expression.Substring(openInd, length);

                    Console.WriteLine(subExpression);
                }
            }
        }
    }
}
