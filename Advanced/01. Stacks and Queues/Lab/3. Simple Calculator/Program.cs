﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _3._Simple_Calculator
{
    class Program
    {
        static void Main()
        {
            string[] expression = Console.ReadLine()
                .Split();

            expression = expression.Reverse().ToArray();

            Stack<string> stack = new Stack<string>(expression);
            
            int sum = int.Parse(stack.Pop());

            while(stack.Count > 0)
            {
                string element = stack.Pop();

                if (element == "-")
                {
                    sum -= int.Parse(stack.Pop());
                }
                else if (element == "+")
                {
                    sum += int.Parse(stack.Pop());
                }
            }

            Console.WriteLine(sum);
        }
    }
}
