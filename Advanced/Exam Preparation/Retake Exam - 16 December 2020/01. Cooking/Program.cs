﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _01._Cooking
{
    class Program
    {
        static void Main()
        {
            Queue<int> liquids = new Queue<int>(Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray());

            Stack<int> ingradients = new Stack<int>(Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray());

            Dictionary<string, int> foodDict = new Dictionary<string, int>() { { "Bread", 25 }, { "Cake", 50 }, { "Pastry", 75 }, { "Fruit Pie", 100 } };
            Dictionary<string, int> backedFood = new Dictionary<string, int>() { { "Bread", 0 }, { "Cake", 0 }, { "Pastry", 0 }, { "Fruit Pie", 0 } };

            int liquidsCount = liquids.Count();

            while (liquids.Count > 0 && ingradients.Count > 0)
            {
                int liquid = liquids.Peek();
                int ingradient = ingradients.Peek();

                int sum = liquid + ingradient;

                if (foodDict.ContainsValue(sum))
                {
                    var food = foodDict.FirstOrDefault(x => x.Value == sum);

                    if (backedFood.ContainsKey(food.Key))
                    {
                        backedFood[food.Key]++;
                    }
                    else
                    {
                        backedFood.Add(food.Key, 1);
                    }

                    liquids.Dequeue();
                    ingradients.Pop();
                }
                else
                {
                    liquids.Dequeue();
                    ingradients.Push(ingradients.Pop() + 3);
                }
            }

            if (backedFood.Where(x => x.Value > 0).Count() == backedFood.Count)
            {
                Console.WriteLine("Wohoo! You succeeded in cooking all the food!");
            }
            else
            {
                Console.WriteLine("Ugh, what a pity! You didn't have enough materials to cook everything.");
            }

            if (liquids.Count > 0)
            {
                Console.WriteLine($"Liquids left: {string.Join(", ", liquids)}");
            }
            else
            {
                Console.WriteLine("Liquids left: none");
            }

            if (ingradients.Count > 0)
            {
                Console.WriteLine($"Ingredients left: {string.Join(", ", ingradients)}");
            }
            else
            {
                Console.WriteLine("Ingredients left: none");
            }

            backedFood = backedFood.OrderBy(x => x.Key).ToDictionary(k => k.Key, v => v.Value);

            foreach (var food in backedFood)
            {
                Console.WriteLine(string.Join("/n", $"{food.Key}: {food.Value}"));
            }
        }
    }
}
