﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BakeryOpenning
{
    public class Bakery
    {
        private List<Employee> employees;
        private string bakeryName;
        private int capacity;

        public Bakery(string bakeryName, int capacity)
        {
            this.BakeryName = bakeryName;
            this.Capacity = capacity;
            this.Employees = new List<Employee>();
            
        }

        public string BakeryName { get => bakeryName; set => bakeryName = value; }
        public int Capacity { get => capacity; set => capacity = value; }
        public List<Employee> Employees { get => employees; set => employees = value; }
        public int Count { get => Employees.Count;} 

        public void Add(Employee employee)
        {
            if (Employees.Count < Capacity)
            {
                Employees.Add(employee);
            }
        }

        public bool Remove(string name)
        {
            
            Employee employeeToRemove = Employees.FirstOrDefault(x => x.Name == name);

            if (employeeToRemove != null)
            {
                Employees.Remove(employeeToRemove);
                return true;
            }
            else
            {
                return false;
            }
        }

        public Employee GetOldestEmployee()
        {
            int maximalAge = Employees.Max(x => x.Age);

            return Employees.FirstOrDefault(x => x.Age == maximalAge);
        }

        public Employee GetEmployee(string name)
        {
            return Employees.FirstOrDefault(x => x.Name == name);
        }

        public string Report()
        {
            return $"Employees working at Bakery {bakeryName}:\n{string.Join("\n", Employees)}";
        }
    }
}
