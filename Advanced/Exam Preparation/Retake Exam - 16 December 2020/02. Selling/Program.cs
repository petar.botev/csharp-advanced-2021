﻿using System;

namespace _02._Selling
{
    class Program
    {
        static void Main()
        {
            int matrixSize = int.Parse(Console.ReadLine());
            char[][] matrix = new char[matrixSize][];

            for (int i = 0; i < matrixSize; i++)
            {
                matrix[i] = Console.ReadLine().ToCharArray();
            }

            int x = 0;
            int y = 0;

            for (int i = 0; i < matrixSize; i++)
            {
                for (int j = 0; j < matrixSize; j++)
                {
                    if (matrix[i][j] == 'S')
                    {
                        x = i;
                        y = j;
                    }
                }
            }

            decimal earnedMoney = 0;
            bool isOut = false;

            while (true)
            {
                string command = Console.ReadLine();

                switch (command)
                {
                    case "up":
                        if (x - 1 < 0)
                        {
                            matrix[x][y] = '-';
                            isOut = true;
                            break;
                        }

                        if (matrix[x - 1][y] == 'O')
                        {
                            matrix[x][y] = '-';
                            matrix[x - 1][y] = '-';
                            var move = MoveToPillar(x, y, matrix);
                            x = move.Item1;
                            y = move.Item2;
                            matrix[x][y] = 'S';

                            break;
                        }

                        if (char.IsDigit(matrix[x - 1][y]))
                        {
                            earnedMoney += decimal.Parse(matrix[x - 1][y].ToString());
                        }

                        matrix[x][y] = '-';
                        matrix[x - 1][y] = 'S';
                        x--;

                        break;
                    case "down":
                        if (x + 1 > matrixSize - 1)
                        {
                            matrix[x][y] = '-';
                            isOut = true;
                            break;
                        }

                        if (char.IsDigit(matrix[x + 1][y]))
                        {
                            earnedMoney += decimal.Parse(matrix[x + 1][y].ToString());
                        }

                        if (matrix[x + 1][y] == 'O')
                        {
                            matrix[x][y] = '-';
                            matrix[x + 1][y] = '-';
                            var move = MoveToPillar(x, y, matrix);
                            x = move.Item1;
                            y = move.Item2;
                            matrix[x][y] = 'S';

                            break;
                        }

                        matrix[x][y] = '-';
                        matrix[x + 1][y] = 'S';
                        x++;
                        break;
                    case "left":
                        if (y - 1 < 0)
                        {
                            matrix[x][y] = '-';
                            isOut = true;
                            break;
                        }

                        if (char.IsDigit(matrix[x][y - 1]))
                        {
                            earnedMoney += decimal.Parse(matrix[x][y - 1].ToString());
                        }

                        if (matrix[x][y - 1] == 'O')
                        {
                            matrix[x][y] = '-';
                            matrix[x][y - 1] = '-';
                            var move = MoveToPillar(x, y, matrix);
                            x = move.Item1;
                            y = move.Item2;
                            matrix[x][y] = 'S';

                            break;
                        }

                        matrix[x][y] = '-';
                        matrix[x][y - 1] = 'S';
                        y--;
                        break;
                    case "right":
                        if (y + 1 > matrixSize - 1)
                        {
                            matrix[x][y] = '-';
                            isOut = true;
                            break;
                        }

                        if (char.IsDigit(matrix[x][y + 1]))
                        {
                            earnedMoney += decimal.Parse(matrix[x][y + 1].ToString());
                        }

                        if (matrix[x][y + 1] == 'O')
                        {
                            matrix[x][y] = '-';
                            matrix[x][y + 1] = '-';
                            var move = MoveToPillar(x, y, matrix);
                            x = move.Item1;
                            y = move.Item2;
                            matrix[x][y] = 'S';

                            break;
                        }

                        matrix[x][y] = '-';
                        matrix[x][y + 1] = 'S';
                        y++;
                        break;

                    default:
                        break;
                }

                if (isOut == true || earnedMoney >= 50)
                {
                    if (earnedMoney < 50)
                    {
                        Console.WriteLine("Bad news, you are out of the bakery.");
                    }
                    else
                    {
                        Console.WriteLine("Good news! You succeeded in collecting enough money!");
                    }

                    Console.WriteLine($"Money: {earnedMoney}");

                    foreach (var row in matrix)
                    {
                        Console.WriteLine(row);
                    }
                    return;
                }
            }
        }

        static Tuple<int, int> MoveToPillar(int x, int y, char[][] matrix)
        {
            Tuple<int, int> pillarPossition = null;
            for (int i = 0; i < matrix.Length; i++)
            {
                for (int j = 0; j < matrix.Length; j++)
                {
                    if (matrix[i][j] == 'O')
                    {
                        pillarPossition = new Tuple<int, int>(i, j);
                    }
                }
            }

            return pillarPossition;
        }
    }
}
