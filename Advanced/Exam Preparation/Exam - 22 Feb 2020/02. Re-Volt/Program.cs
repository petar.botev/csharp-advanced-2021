﻿using System;

namespace _02._Re_Volt
{
    class Program
    {
        static void Main()
        {
            int matrixSize = int.Parse(Console.ReadLine());
            int numberOfCommands = int.Parse(Console.ReadLine());

            char[][] matrix = new char[matrixSize][];

            for (int i = 0; i < matrix.Length; i++)
            {
                matrix[i] = Console.ReadLine().Trim().ToCharArray();
            }

            int x = 0;
            int y = 0;

            for (int i = 0; i < matrixSize; i++)
            {
                for (int j = 0; j < matrixSize; j++)
                {
                    if (matrix[i][j] == 'f')
                    {
                        x = i;
                        y = j;
                    }
                }
            }


            bool isOnFinal = false;
            
            for (int i = 0; i < numberOfCommands; i++)
            {
                
                string command = Console.ReadLine().Trim();

                switch (command)
                {
                    case "up":
                        matrix[x][y] = '-';
                        x--;

                        if (x < 0)
                        {
                            x = matrixSize - 1;
                        }

                        if (matrix[x][y] == 'F')
                        {
                            isOnFinal = true;
                            matrix[x][y] = 'f';
                            break;
                        }

                        if (matrix[x][y] != 'B' && matrix[x][y] != 'T')
                        {
                            matrix[x][y] = 'f';
                        }

                        if (matrix[x][y] == 'B')
                        {
                            //matrix[x][y] = '-';
                            x--;
                            if (x < 0)
                            {
                                x = matrixSize - 1;
                            }

                            if (matrix[x][y] == 'F')
                            {
                                isOnFinal = true;
                                matrix[x][y] = 'f';
                                break;
                            }

                            matrix[x][y] = 'f';

                        }

                        if (matrix[x][y] == 'T')
                        {
                            //matrix[x][y] = '-';
                            x++;
                            if (x == matrixSize)
                            {
                                x = 0;
                            }
                            matrix[x][y] = 'f';
                        }
                        break;

                    case "down":

                        matrix[x][y] = '-';
                        x++;

                        if (x == matrixSize)
                        {
                            x = 0;
                        }
                        if (matrix[x][y] == 'F')
                        {
                            isOnFinal = true;
                            matrix[x][y] = 'f';
                            break;
                        }

                        if (matrix[x][y] != 'B' && matrix[x][y] != 'T')
                        {
                            matrix[x][y] = 'f';
                        }

                        if (matrix[x][y] == 'B')
                        {
                            //matrix[x][y] = '-';
                            x++;
                            if (x == matrixSize)
                            {
                                x = 0;
                            }

                            if (matrix[x][y] == 'F')
                            {
                                isOnFinal = true;
                                matrix[x][y] = 'f';
                                break;
                            }

                            matrix[x][y] = 'f';
                        }

                        if (matrix[x][y] == 'T')
                        {
                           //matrix[x][y] = '-';
                            x--;
                            if (x < 0)
                            {
                                x = matrixSize - 1;
                            }
                            matrix[x][y] = 'f';
                        }
                        break;

                    case "left":

                        matrix[x][y] = '-';
                        y--;

                        if (y < 0)
                        {
                            y = matrixSize - 1;
                        }
                        if (matrix[x][y] == 'F')
                        {
                            isOnFinal = true;
                            matrix[x][y] = 'f';
                            break;
                        }

                        if (matrix[x][y] != 'B' && matrix[x][y] != 'T')
                        {
                            matrix[x][y] = 'f';
                        }

                        if (matrix[x][y] == 'B')
                        {
                            //matrix[x][y] = '-';
                            y--;
                            if (y < 0)
                            {
                                y = matrixSize - 1;
                            }

                            if (matrix[x][y] == 'F')
                            {
                                isOnFinal = true;
                                matrix[x][y] = 'f';
                                break;
                            }
                            matrix[x][y] = 'f';
                        }

                        if (matrix[x][y] == 'T')
                        {
                            //matrix[x][y] = '-';
                            y++;
                            if (y == matrixSize)
                            {
                                y = 0;
                            }
                            matrix[x][y] = 'f';
                        }
                        break;

                    case "right":

                        matrix[x][y] = '-';
                        y++;

                        if (y == matrixSize)
                        {
                            y = 0;
                        }
                        if (matrix[x][y] == 'F')
                        {
                            isOnFinal = true;
                            matrix[x][y] = 'f';
                            break;
                        }

                        if (matrix[x][y] != 'B' && matrix[x][y] != 'T')
                        {
                            matrix[x][y] = 'f';
                        }

                        if (matrix[x][y] == 'B')
                        {
                            //matrix[x][y] = '-';
                            y++;
                            if (y == matrixSize)
                            {
                                y = 0;
                            }

                            if (matrix[x][y] == 'F')
                            {
                                isOnFinal = true;
                                matrix[x][y] = 'f';
                                break;
                            }
                            matrix[x][y] = 'f';
                        }

                        if (matrix[x][y] == 'T')
                        {
                            //matrix[x][y] = '-';
                            y--;
                            if (y < 0)
                            {
                                y = matrixSize - 1;
                            }
                            matrix[x][y] = 'f';
                        }
                        break;

                    default:
                        break;
                }

                
                if (isOnFinal)
                {
                    break;
                }
            }

            if (isOnFinal)
            {

                Console.WriteLine("Player won!");
            }
            else
            {
                Console.WriteLine("Player lost!");
            }

            for (int i = 0; i < matrixSize; i++)
            {
                Console.WriteLine(string.Join("", matrix[i]).Trim());
            }
        }
    }
}
