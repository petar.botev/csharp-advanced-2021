﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Guild
{
    public class Player
    {
        public Player(string name, string @class)
        {
            Name = name;
            Class = @class;
        }

        public string Name { get; set; }
        public string Class { get; set; }
        public string Rank { get; set; } = "Trial";
        public string Description { get; set; } = "n/a";

        public override string ToString()
        {
            StringBuilder strb = new StringBuilder();
            strb.AppendLine($"Player {Name}: {Class}");
            strb.AppendLine($"Rank: {Rank}");
            strb.AppendLine($"Description: {Description}");

            return strb.ToString().Trim();
        }
    }
}
