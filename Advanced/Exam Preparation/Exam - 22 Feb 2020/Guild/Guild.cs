﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Guild
{
    public class Guild
    {
        private List<Player> roster;
        public Guild(string name, int capacity)
        {
            Name = name;
            Capacity = capacity;
            this.roster = new List<Player>();
        }

        public string Name { get; set; }
        public int Capacity { get; set; }
        public int Count { get => roster.Count; }


        public void AddPlayer(Player player)
        {
            if (Capacity > roster.Count)
            {
                roster.Add(player);
            }
        }

        public bool RemovePlayer(string name)
        {
            if (roster.Any(x => x.Name == name))
            {
                Player playerToRemove = roster.FirstOrDefault(x => x.Name == name);
                roster.Remove(playerToRemove);
                return true;
            }
            return false;
        }

        public void PromotePlayer(string name)
        {
            Player player = roster.FirstOrDefault(x => x.Name == name);
            if (player != null)
            {
                player.Rank = "Member";
            }
        }

        public void DemotePlayer(string name)
        {
            Player player = roster.FirstOrDefault(x => x.Name == name);
            if (player != null)
            {
                player.Rank = "Trial";
            }
        }

        public Player[] KickPlayersByClass(string @class)
        {
            var kickedPlayers = roster.Where(x => x.Class == @class).ToArray();
            roster = roster.Where(x => x.Class != @class).ToList();

            return kickedPlayers;
        }

        public string Report()
        {
            StringBuilder strb = new StringBuilder();

            strb.AppendLine($"Players in the guild: {Name}");

            foreach (var player in roster)
            {
                strb.AppendLine(player.ToString());
            }

            return strb.ToString().Trim();
        }
    }
}
