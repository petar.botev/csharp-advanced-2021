﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _01._Lootbox
{
    class Program
    {
        static void Main()
        {
            Queue<int> firstToolBox = new Queue<int>(Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse));

            Stack<int> secondToolBox = new Stack<int>(Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse));

            List<int> items = new List<int>();

            while (firstToolBox.Count > 0 && secondToolBox.Count > 0)
            {
                int sum = firstToolBox.Peek() + secondToolBox.Peek();

                if (sum % 2 == 0)
                {
                    items.Add(sum);
                    firstToolBox.Dequeue();
                    secondToolBox.Pop();
                }
                else
                {
                    firstToolBox.Enqueue(secondToolBox.Pop());
                }
            }

            if (firstToolBox.Count == 0)
            {
                Console.WriteLine("First lootbox is empty");
            }
            else if (secondToolBox.Count == 0)
            {
                Console.WriteLine("Second lootbox is empty");
            }

            if (items.Sum() >= 100)
            {
                Console.WriteLine($"Your loot was epic! Value: {items.Sum()}");
            }
            else
            {
                Console.WriteLine($"Your loot was poor... Value: {items.Sum()}");
            }
        }
    }
}
