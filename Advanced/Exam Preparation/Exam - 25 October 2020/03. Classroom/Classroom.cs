﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassroomProject
{
    public class Classroom
    {
        private List<Student> students;
        //private int capacity;

        public Classroom(int capacity)
        {
            this.students = new List<Student>();
            this.Capacity = capacity;
        }

        public int Capacity { get; set; }
        public int Count { get { return students.Count; } }

        public string RegisterStudent(Student student)
        {
            if (Count < Capacity)
            {
                students.Add(student);
               return $"Added student {student.FirstName} {student.LastName}";
            }
            else
            {
                return $"No seats in the classroom";
            }
        }

        public string DismissStudent(string firstName, string lastName)
        {
            var student = students.FirstOrDefault(x => x.FirstName == firstName && x.LastName == lastName);
            var isRemoved = students.Remove(student);

            if (isRemoved)
            {
                return $"Dismissed student {firstName} {lastName}";
            }
            else
            {
                return "Student not found";
            }
        }

        public string GetSubjectInfo(string subject)
        {
            var studentsWithSubject = students.Where(x => x.Subject == subject).ToArray();

            if (studentsWithSubject.Length == 0)
            {
                return "No students enrolled for the subject";
            }
            else
            {
                StringBuilder strb = new StringBuilder();

                strb.AppendLine($"Subject: {subject}{Environment.NewLine}Students:");

                foreach (var student in studentsWithSubject)
                {
                    strb.AppendLine($"{student.FirstName} {student.LastName}");
                }

                //for (int i = 0; i < studentsWithSubject.Length; i++)
                //{
                //    strb.AppendLine($"{studentsWithSubject[i].FirstName} {studentsWithSubject[i].LastName}");
                //}

                return strb.ToString().Trim();
            }
        }

        public int GetStudentsCount()
        {
            return Count;
        }

        public Student GetStudent(string firstName, string lastName)
        {
            return students.FirstOrDefault(x => x.FirstName == firstName && x.LastName == lastName);
        }
    }
}
