﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _01._Scheduling
{
    class Program
    {
        static void Main()
        {
            Stack<int> tasks = new Stack<int>(Console.ReadLine()
                .Split(", ", StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray());

            Queue<int> threads = new Queue<int>(Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray());

            int taskToKill = int.Parse(Console.ReadLine());
            int thread = 0;

            while (true)
            {
                int task = tasks.Peek();
                thread = threads.Peek();

                if (task == taskToKill)
                {
                    break;
                }

                if (task <= thread)
                {
                    tasks.Pop();
                    threads.Dequeue();
                }
                else
                {
                    threads.Dequeue();
                }
            }

            Console.WriteLine($"Thread with value {thread} killed task {taskToKill}");
            Console.WriteLine(string.Join(' ', threads));
        }
    }
}
