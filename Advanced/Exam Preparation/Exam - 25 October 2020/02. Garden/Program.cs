﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _02._Garden
{
    class Program
    {
        static void Main()
        {
            int[] matrixSize = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray();

            int[][] matrix = new int[matrixSize[0]][];

            for (int i = 0; i < matrixSize[0]; i++)
            {
                matrix[i] = new int[matrixSize[1]];
            }

            List<int[]> possitions = new List<int[]>();
            string possition;
            while ((possition = Console.ReadLine()) != "Bloom Bloom Plow")
            {
                if (CheckCoordinates(possition, matrix))
                {
                    possitions.Add(possition.Split(' ', StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray());
                }
            }

            foreach (var place in possitions)
            {
                Bloom(place, matrix);
            }

            for (int i = 0; i < matrix.Length; i++)
            {
                Console.WriteLine(string.Join(' ', matrix[i]));
            }
        }

        static void Bloom(int[] place, int[][] matrix)
        {
            int x = place[0];
            int y = place[1];

            matrix[x][y]++;

            for (int i = 0; i < matrix.Length; i++)
            {
                if (i != x)
                {
                    matrix[i][y]++;
                }
            }

            for (int i = 0; i < matrix[0].Length; i++)
            {
                if (i != y)
                {
                    matrix[x][i]++;
                }
            }
        }

        static bool CheckCoordinates(string possition, int[][] matrix)
        {
            int x = int.Parse(possition[0].ToString());
            int y = int.Parse(possition[2].ToString());

            if ((x < 0 || x > matrix.Length - 1) || (y < 0 || y > matrix[0].Length - 1))
            {
                Console.WriteLine("Invalid coordinates.");
                return false;
            }
            return true;
        }
    }
}
