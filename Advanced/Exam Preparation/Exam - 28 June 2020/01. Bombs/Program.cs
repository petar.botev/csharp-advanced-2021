﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _01._Bombs
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue<long> effects = new Queue<long>(Console.ReadLine()
                .Split(", ", StringSplitOptions.RemoveEmptyEntries)
                .Select(long.Parse)
                .ToArray());

            Stack<long> casing = new Stack<long>(Console.ReadLine()
                .Split(", ", StringSplitOptions.RemoveEmptyEntries)
                .Select(long.Parse)
                .ToArray());

            Dictionary<string, long> dict = new Dictionary<string, long>() {
                { "Datura Bombs", 40 },
                { "Cherry Bombs", 60},
                { "Smoke Decoy Bombs", 120}
            };

            Dictionary<string, int> bombPouch = new Dictionary<string, int>() {
                { "Datura Bombs", 0 },
                { "Cherry Bombs", 0},
                { "Smoke Decoy Bombs", 0}
            };
        

            while (effects.Count > 0 && casing.Count > 0)
            {
                long sum = effects.Peek() + casing.Peek();
                if (dict.ContainsValue(sum))
                {
                    var bombType = dict.FirstOrDefault(x => x.Value == sum);
                    if (bombPouch.ContainsKey(bombType.Key))
                    {
                        bombPouch[bombType.Key]++;
                    }
                    else
                    {
                        bombPouch.Add(bombType.Key, 1);
                    }

                    effects.Dequeue();
                    casing.Pop();
                }
                else
                {
                    casing.Push(casing.Pop() - 5);
                }

                if (bombPouch.All(x => x.Value >= 3))
                {
                    break;
                }
            }

            PrintResult(effects, casing, bombPouch);
        }

        static void PrintResult(Queue<long> effects, Stack<long> casing, Dictionary<string, int> bombs)
        {
            if (!bombs.Any(x => x.Value < 3))
            {
                Console.WriteLine("Bene! You have successfully filled the bomb pouch!");
            }
            else
            {
                Console.WriteLine("You don't have enough materials to fill the bomb pouch.");
            }

            if (effects.Count == 0)
            {
                Console.WriteLine("Bomb Effects: empty");
            }
            else
            {
                Console.WriteLine($"Bomb Effects: {string.Join(", ", effects)}");
            }

            if (casing.Count == 0)
            {
                Console.WriteLine("Bomb Casings: empty");
            }
            else
            {
                Console.WriteLine($"Bomb Casings: {string.Join(", ", casing)}");
            }

            bombs = bombs.OrderBy(x => x.Key).ToDictionary(k => k.Key, v => v.Value);

            foreach (var bomb in bombs)
            {
                
                    Console.WriteLine($"{ bomb.Key }: { bomb.Value }");
                
            }
        }
    }
}
