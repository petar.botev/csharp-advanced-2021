﻿using System;
using System.Text;

namespace _02._Snake
{
    class Program
    {
        static void Main()
        {
            int matrixSize = int.Parse(Console.ReadLine());

            char[][] matrix = new char[matrixSize][];

            for (int i = 0; i < matrixSize; i++)
            {
                matrix[i] = Console.ReadLine().ToCharArray();
            }

            int x = 0;
            int y = 0;

            for (int i = 0; i < matrixSize; i++)
            {
                for (int j = 0; j < matrixSize; j++)
                {
                    if (matrix[i][j] == 'S')
                    {
                        x = i;
                        y = j;
                    }
                }
            }
            
            int foodCount = 0;
            string command = Console.ReadLine();
            StringBuilder strb = new StringBuilder();

            while (true)
            {
                SnakeMovement(matrix, ref x, ref y, command, ref foodCount);
                if (IsOut(matrix, ref x, ref y))
                {
                    strb.AppendLine("Game over!");
                    break;
                }

                else if (IsFoodEnough(foodCount))
                {
                    strb.AppendLine("You won! You fed the snake.");
                    break;
                }

                command = Console.ReadLine();
            }

            strb.AppendLine($"Food eaten: {foodCount}");

            foreach (var row in matrix)
            {
                strb.AppendLine(string.Join("", row));
            }

            Console.WriteLine(strb.ToString());
        }

        static void SnakeMovement(char[][] matrix, ref int x, ref int y, string command, ref int foodCount)
        {
            
            switch (command)
            {
                case "up":

                    matrix[x][y] = '.';
                    x--;

                    if (IsOut(matrix, ref x, ref y))
                    {
                        break;
                    }
                    

                    if (matrix[x][y] == 'B')
                    {
                        matrix[x][y] = '.';
                        MoveLayer(matrix, ref x, ref y);
                        matrix[x][y] = 'S';
                    }
                    else if (matrix[x][y] == '*')
                    {
                        foodCount++;
                        if (IsFoodEnough(foodCount))
                        {
                            matrix[x][y] = 'S';
                            break;
                        }
                    }

                    matrix[x][y] = 'S';

                    break;

                case "down":

                    matrix[x][y] = '.';
                    x++;

                    if (IsOut(matrix, ref x, ref y))
                    {
                        break;
                    }

                    if (matrix[x][y] == 'B')
                    {
                        matrix[x][y] = '.';
                        MoveLayer(matrix, ref x, ref y);
                        matrix[x][y] = 'S';
                    }
                    else if (matrix[x][y] == '*')
                    {
                        foodCount++;
                        if (IsFoodEnough(foodCount))
                        {
                            matrix[x][y] = 'S';
                            break;
                        }
                    }

                    matrix[x][y] = 'S';

                    break;

                case "left":

                    matrix[x][y] = '.';
                    y--;

                    if (IsOut(matrix, ref x, ref y))
                    {
                        break;
                    }
                   

                    if (matrix[x][y] == 'B')
                    {
                        matrix[x][y] = '.';
                        MoveLayer(matrix, ref x, ref y);
                        matrix[x][y] = 'S';
                    }
                    else if (matrix[x][y] == '*')
                    {
                        foodCount++;
                        if (IsFoodEnough(foodCount))
                        {
                            matrix[x][y] = 'S';
                            break;
                        }
                    }

                    matrix[x][y] = 'S';

                    break;

                case "right":

                    matrix[x][y] = '.';
                    y++;

                    if (IsOut(matrix, ref x, ref y))
                    {
                        break;
                    }


                    if (matrix[x][y] == 'B')
                    {
                        matrix[x][y] = '.';
                        MoveLayer(matrix, ref x, ref y);
                        matrix[x][y] = 'S';
                    }
                    else if (matrix[x][y] == '*')
                    {
                        foodCount++;
                        if (IsFoodEnough(foodCount))
                        {
                            matrix[x][y] = 'S';
                            break;
                        }
                    }

                    matrix[x][y] = 'S';

                    break;
                default:
                    break;
            }
        }

        static bool IsOut(char[][] matrix, ref int x, ref int y)
        {
            if (x < 0 || y < 0 || x >= matrix.Length || y >= matrix.Length)
            {
                return true;
            }
            return false;
        }

        static bool IsFoodEnough(int foodCount)
        {
            if (foodCount >= 10)
            {
                return true;
            }
            return false;
        }

        static void MoveLayer(char[][] matrix, ref int x, ref int y)
        {
            for (int i = matrix.Length - 1; i >= 0; i--)
            {
                for (int j = matrix.Length - 1; j >= 0; j--)
                {
                    if (matrix[i][j] == 'B')
                    {
                        x = i;
                        y = j;
                    }
                }
            }
        }
    }
}
