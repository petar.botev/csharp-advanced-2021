﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Parking
{
    public class Parking
    {
        private List<Car> cars;

        public Parking(string type, int capacity)
        {
            Type = type;
            Capacity = capacity;

            this.cars = new List<Car>();
        }

        public string Type { get; set; }
        public int Capacity { get; set; }
        public int Count { get => cars.Count; }


        public void Add(Car car)
        {
            if (Capacity > cars.Count)
            {
                cars.Add(car);
            }
        }

        public bool Remove(string manufacturer, string model)
        {
            Car carToRemove = carToRemove = cars.FirstOrDefault(c => c.Manufacturer == manufacturer && c.Model == model);
            if ((carToRemove != null))
            {
                cars.Remove(carToRemove);
                return true;
            }
            return false;
        }

        public Car GetLatestCar()
        {
            int maxYear = 0;

            if (cars.Count > 0)
            {
                maxYear = cars.Select(c => c.Year).Max();
            }

            return cars.FirstOrDefault(c => c.Year == maxYear);
        }

        public Car GetCar(string manufacturer, string model)
        {
            return cars.FirstOrDefault(c => c.Manufacturer == manufacturer && c.Model == model);
        }

        public string GetStatistics()
        {
            StringBuilder strb = new StringBuilder();

            strb.AppendLine($"The cars are parked in { Type }:");
            
            foreach (var car in cars)
            {
                strb.AppendLine(car.ToString());
            }

            return strb.ToString().Trim();
        }
    }
}
