﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VetClinic
{
    public class Clinic
    {
        private List<Pet> petData;
        private int capacity;
        public Clinic(int capacity)
        {
            this.Capacity = capacity;
            this.petData = new List<Pet>();
        }

        public int Capacity { get => capacity; set => capacity = value; }
        public int Count { get => petData.Count; }


        public void Add(Pet pet)
        {
            if (Capacity > petData.Count)
            {
                petData.Add(pet);
            }
        }

        public bool Remove(string name)
        {
            var pet = petData.FirstOrDefault(x => x.Name == name);

            return petData.Remove(pet);
        }

        public Pet GetPet(string name, string owner)
        {
            return petData.FirstOrDefault(x => x.Name == name && x.Owner == owner);
        }

        public Pet GetOldestPet()
        {
            int maxAge = petData.Select(x => x.Age).Max();
            return petData.FirstOrDefault(x => x.Age == maxAge);
        }

        public string GetStatistics()
        {
            StringBuilder strb = new StringBuilder();

            strb.AppendLine("The clinic has the following patients:");

            foreach (var pet in petData)
            {
                strb.AppendLine($"Pet {pet.Name} with owner: {pet.Owner}");
            }

            return strb.ToString().Trim();
        }
    }
}
