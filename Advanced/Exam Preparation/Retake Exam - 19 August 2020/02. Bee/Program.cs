﻿using System;

namespace _02._Bee
{
    class Program
    {
        static void Main()
        {
            int matrixSize = int.Parse(Console.ReadLine());

            char[][] matrix = new char[matrixSize][];

            for (int i = 0; i < matrixSize; i++)
            {
                matrix[i] = Console.ReadLine().ToCharArray();
            }

            int x = 0;
            int y = 0;

            for (int i = 0; i < matrixSize; i++)
            {
                for (int j = 0; j < matrixSize; j++)
                {
                    if (matrix[i][j] == 'B')
                    {
                        x = i;
                        y = j;
                    }
                }
            }

            int polinatedFlowers = 0;
            bool lost = false;
            string command;
            while ((command = Console.ReadLine()) != "End")
            {
                Move(command, ref x, ref y, matrix, ref polinatedFlowers);

                if (x < 0 || x >= matrixSize || y < 0 || y >= matrixSize)
                {
                    lost = true;
                    ChangeBeePossition(command, ref x, ref y, matrix, lost);
                    break;
                }
            }

            if (lost == true)
            {
                Console.WriteLine("The bee got lost!");
            }

            if (polinatedFlowers > 4)
            {
                Console.WriteLine($"Great job, the bee managed to pollinate {polinatedFlowers} flowers!");
            }
            else
            {
                int neededFlowers = 5 - polinatedFlowers;
                Console.WriteLine($"The bee couldn't pollinate the flowers, she needed {neededFlowers} flowers more");
            }

            PrintMatrix(matrix);
        }

        static void PrintMatrix(char[][] matrix)
        {
            for (int i = 0; i < matrix.Length; i++)
            {
                Console.WriteLine(string.Join("", matrix[i]));
            }
        }

        static void ChangeBeePossition(string command, ref int x, ref int y, char[][] matrix, bool lost)
        {
            switch (command)
            {
                case "up":
                    
                    matrix[x + 1][y] = '.';
                    if (lost == false)
                    {
                        matrix[x][y] = 'B';
                    }
                    break;

                case "down":
                    matrix[x - 1][y] = '.';
                    if (lost == false)
                    {
                        matrix[x][y] = 'B';
                    }
                    break;

                case "left":
                    matrix[x][y + 1] = '.';
                    if (lost == false)
                    {
                        matrix[x][y] = 'B';
                    }
                    break;

                case "right":
                    matrix[x][y - 1] = '.';
                    if (lost == false)
                    {
                        matrix[x][y] = 'B';
                    }
                    break;
                default:
                    break;
            }
        }

        static void Move(string command, ref int x, ref int y, char[][] matrix, ref int polinatedFlowers)
        {
            switch (command)
            {
                case "up":
                    x--;
                    if (x < 0)
                    {
                        break;
                    }
                    if (matrix[x][y] == 'f')
                    {
                        polinatedFlowers++;
                        ChangeBeePossition(command, ref x, ref y, matrix, false);
                    }
                    else if (matrix[x][y] == 'O')
                    {
                        ChangeBeePossition(command, ref x, ref y, matrix, false);
                        Bonus(command, ref x, ref y, matrix, ref polinatedFlowers, "up");
                    }
                    else
                    {
                        ChangeBeePossition(command, ref x, ref y, matrix, false);
                    }
                    break;
                case "down":
                    x++;
                    if (x == matrix.Length)
                    {
                        break;
                    }
                    if (matrix[x][y] == 'f')
                    {
                        polinatedFlowers++;
                        ChangeBeePossition(command, ref x, ref y, matrix, false);
                    }
                    else if (matrix[x][y] == 'O')
                    {
                        ChangeBeePossition(command, ref x, ref y, matrix, false);

                        Bonus(command, ref x, ref y, matrix, ref polinatedFlowers, "down");
                    }
                    else
                    {
                        ChangeBeePossition(command, ref x, ref y, matrix, false);
                    }

                    break;
                case "left":
                    y--;
                    if (y < 0)
                    {
                        break;
                    }
                    if (matrix[x][y] == 'f')
                    {
                        polinatedFlowers++;
                        ChangeBeePossition(command, ref x, ref y, matrix, false);
                    }
                    else if (matrix[x][y] == 'O')
                    {
                        ChangeBeePossition(command, ref x, ref y, matrix, false);
                        Bonus(command, ref x, ref y, matrix, ref polinatedFlowers, "left");
                    }
                    else
                    {
                        ChangeBeePossition(command, ref x, ref y, matrix, false);
                    }
                    break;
                case "right":
                    y++;
                    if (y == matrix.Length)
                    {
                        break;
                    }
                    if (matrix[x][y] == 'f')
                    {
                        polinatedFlowers++;
                        ChangeBeePossition(command, ref x, ref y, matrix, false);
                    }
                    else if (matrix[x][y] == 'O')
                    {
                        ChangeBeePossition(command, ref x, ref y, matrix, false);
                        Bonus(command, ref x, ref y, matrix, ref polinatedFlowers, "right");
                    }
                    else
                    {
                        ChangeBeePossition(command, ref x, ref y, matrix, false);
                    }
                    break;
                default:
                    break;
            }
        }

        static void Bonus(string command, ref int x, ref int y, char[][] matrix, ref int polinatedFlowers, string direction)
        {
            switch (direction)
            {
                case "up":
                    //x--;
                    Move(command, ref x, ref y, matrix, ref polinatedFlowers);
                    break;

                case "down":
                    //x++;
                    Move(command, ref x, ref y, matrix, ref polinatedFlowers);
                    break;

                case "left":
                    //y--;
                    Move(command, ref x, ref y, matrix, ref polinatedFlowers);
                    break;

                case "right":
                    //y++;
                    Move(command, ref x, ref y, matrix, ref polinatedFlowers);
                    break;
                default:
                    break;
            }
        }
    }
}
