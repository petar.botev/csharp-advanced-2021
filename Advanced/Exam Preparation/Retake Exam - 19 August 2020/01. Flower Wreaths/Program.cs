﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _01._Flower_Wreaths
{
    class Program
    {
        static void Main()
        {
            Stack<int> lilies = new Stack<int>(Console.ReadLine()
                .Split(", ", StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray());

            Queue<int> roses = new Queue<int>(Console.ReadLine()
                .Split(", ")
                .Select(int.Parse)
                .ToArray());

            int wreathsCount = 0;
            int valuesSum = 0;
            List<int> leftFlowers = new List<int>();

            while (lilies.Count > 0 && roses.Count > 0)
            {
                valuesSum = lilies.Peek() + roses.Peek();

                if (valuesSum == 15)
                {
                    wreathsCount++;
                    lilies.Pop();
                    roses.Dequeue();
                }
                else if (valuesSum > 15)
                {
                    lilies.Push(lilies.Pop() - 2);
                }
                else
                {
                    leftFlowers.Add(lilies.Pop());
                    leftFlowers.Add(roses.Dequeue());
                }
            }

            int additionalWreaths = leftFlowers.Sum() / 15;
            if (additionalWreaths >= 1)
            {
                wreathsCount += additionalWreaths;
            }

            int neededWreaths = 5 - wreathsCount;

            if (wreathsCount >= 5)
            {
                Console.WriteLine($"You made it, you are going to the competition with {wreathsCount} wreaths!");
            }
            else
            {
                Console.WriteLine($"You didn't make it, you need {neededWreaths} wreaths more!");
            }
        }
    }
}
