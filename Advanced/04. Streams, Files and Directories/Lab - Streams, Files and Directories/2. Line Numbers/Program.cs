﻿using System;
using System.IO;

namespace _2._Line_Numbers
{
    class Program
    {
        static void Main()
        {
            string filePath = Path.Combine("data", "Input.txt");
            string output = Path.Combine("data", "Output.docx");

            using (TextReader reader = new StreamReader(filePath))
            {
                string[] str = reader.ReadToEnd().Split("\n", StringSplitOptions.RemoveEmptyEntries);

                using (StreamWriter writer = new StreamWriter(output))
                {
                    for (int i = 0; i < str.Length; i++)
                    {
                        writer.Write($"{i + 1}. {str[i]}");
                    }
                }
            }
        }
    }
}
