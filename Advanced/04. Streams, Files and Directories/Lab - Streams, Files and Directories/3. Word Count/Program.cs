﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace _3._Word_Count
{
    class Program
    {
        static void Main()
        {
            string words = Path.Combine("data", "words.txt");
            string text = Path.Combine("data", "text.txt");

            string output = Path.Combine("data", "output.txt");

            string[] wordsArr = File.ReadAllText(words).Split(' ', StringSplitOptions.RemoveEmptyEntries);

            string[] textArr = File.ReadAllText(text).Split(new char[] { ',', '-', '!', '?', '.', ' '}, StringSplitOptions.RemoveEmptyEntries);

            Dictionary<string, int> dict = new Dictionary<string, int>();

            for (int i = 0; i < wordsArr.Length; i++)
            {
                dict.Add(wordsArr[i].ToLower(), 0);
            }

            for (int i = 0; i < textArr.Length; i++)
            {
                if (dict.ContainsKey(textArr[i].ToLower()))
                {
                    dict[textArr[i].ToLower()]++;
                }
            }

            dict = dict.OrderByDescending(x => x.Value).ToDictionary(k => k.Key, v => v.Value);

            StringBuilder strb = new StringBuilder();

            foreach (var item in dict)
            {
                strb.AppendLine($"{item.Key} - {item.Value}");
            }

            File.WriteAllText(output, strb.ToString());

        }
    }
}
