﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace _4._Merge_Files
{
    class Program
    {
        static void Main()
        {
            string firstFile = Path.Combine("data", "FileOne.txt");
            string secondFile = Path.Combine("data", "FileTwo.txt");

            string output = Path.Combine("data", "output.txt");

            int[] first = File.ReadAllText(firstFile).Split("\r\n", StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();
            int[] second = File.ReadAllText(secondFile).Split("\r\n", StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();

            List<int> list = new List<int>();

            list.AddRange(first);
            list.AddRange(second);
            list = list.OrderBy(x => x).ToList();

            File.WriteAllText(output, string.Join("\n", list));
        }
    }
}
