﻿using System;
using System.IO;

namespace _6._Folder_Size
{
    class Program
    {
        static void Main()
        {
            string[] files = Directory.GetFiles(@"C:\Users\petar\OneDrive\SoftUni C# Advanced - януари 2021\Advanced\04. Streams, Files and Directories\Lab - Streams, Files and Directories\Resources\06. Folder Size\TestFolder");

            double size = 0;

            for (int i = 0; i < files.Length; i++)
            {
                var info = new FileInfo(files[i]);

                size += info.Length;
            }

            size = size / 1024 / 1024;

            Console.WriteLine(size);
        }
    }
}
