﻿using System;
using System.IO;

namespace _1._Odd_Lines
{
    class Program
    {
        static void Main()
        {
            string filePath = Path.Combine("data", "Input.txt");
            string destPath = Path.Combine("data", "Output.txt");

            using FileStream fileStream = new FileStream(filePath, FileMode.Open);
            using StreamReader reader = new StreamReader(fileStream);
            string[] lines = reader.ReadToEnd().Split("\n", StringSplitOptions.RemoveEmptyEntries);

            using FileStream write = new FileStream(destPath, FileMode.Create);
            using StreamWriter writer = new StreamWriter(write);
            for (int i = 0; i < lines.Length; i++)
            {
                if (i % 2 != 0)
                {
                    writer.Write(lines[i]);
                }
            }
        }
    }
}
