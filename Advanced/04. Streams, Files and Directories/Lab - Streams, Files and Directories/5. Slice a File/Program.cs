﻿using System;
using System.IO;
using System.Linq;

namespace _5._Slice_a_File
{
    class Program
    {
        static void Main()
        {
            string path = Path.Combine("data", "SliceMe.txt");
            string output1 = Path.Combine("data", "Part-1.txt");
            string output2 = Path.Combine("data", "Part-2.txt");
            string output3 = Path.Combine("data", "Part-3.txt");
            string output4 = Path.Combine("data", "Part-4.txt");

            string[] lines = File.ReadAllLines(path);

            int linesInFile = lines.Length / 4;

            File.WriteAllLines(output1, lines.Take(linesInFile));
            File.WriteAllLines(output2, lines.Skip(linesInFile).Take(linesInFile));
            File.WriteAllLines(output3, lines.Skip(linesInFile * 2).Take(linesInFile));
            File.WriteAllLines(output4, lines.Skip(linesInFile * 3));
        }
    }
}
