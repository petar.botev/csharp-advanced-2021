﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace _2._Line_Numbers
{
    class Program
    {
        static void Main()
        {
            string path = Path.Combine("data", "text.txt");
            string output = Path.Combine("data", "output.txt");

            StringBuilder strb = new StringBuilder();

            using (StreamReader read = new StreamReader(path))
            {
                string[] lines = read.ReadToEnd().Trim().Split("\r\n");

                for (int i = 0; i < lines.Length; i++)
                {
                    int letters = lines[i].Count(x => char.IsLetter(x));
                    int punctuations = lines[i].Count(x => char.IsPunctuation(x));

                    strb.AppendLine($"Line {i + 1}: {lines[i]} ({letters})({punctuations})");
                }

                using (StreamWriter write = new StreamWriter(output))
                {
                    write.Write(strb.ToString().Trim());
                }
            }
        }
    }
}
