﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace _3._Word_Count
{
    class Program
    {
        static void Main()
        {
            string pathWords = Path.Combine("data", "words.txt");
            string pathText = Path.Combine("data", "text.txt");
            string pathExpected = Path.Combine("data", "expectedResult.txt");
            string actualResults = Path.Combine("data", "actualResults.txt");

            string[] words = File.ReadAllLines(pathWords)
                .Select(x => x.ToLower())
                .ToArray();
            string[] text = File.ReadAllText(pathText)
                .Split(new string[] { " ", "\r\n", "-", ".", "!", "?", "," }, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => x.ToLower())
                .ToArray();

            Dictionary<string, int> dict = new Dictionary<string, int>();

            for (int i = 0; i < text.Length; i++)
            {
                if (words.Contains(text[i]))
                {
                    if (dict.ContainsKey(text[i]))
                    {
                        dict[text[i]]++;
                    }
                    else
                    {
                        dict.Add(text[i], 1);
                    }
                }
            }

            dict = dict.OrderByDescending(x => x.Value).ToDictionary(k => k.Key, v => v.Value);

            StringBuilder strb = new StringBuilder();

            foreach (var item in dict)
            {
                strb.AppendLine($"{item.Key} - {item.Value}");
            }

            File.WriteAllText(pathExpected, strb.ToString());
        }
    }
}
