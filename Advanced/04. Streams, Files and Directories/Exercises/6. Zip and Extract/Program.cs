﻿using System;
using System.IO;
using System.IO.Compression;

namespace _6._Zip_and_Extract
{
    class Program
    {
        static void Main()
        {
            string path = Path.Combine("data");
            string zipFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "myZip.zip");
            string extractFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "extractedFile.png");

            try
            {
                ZipFile.CreateFromDirectory(path, zipFilePath);
            }
            catch (Exception)
            {
                File.Delete(zipFilePath);


                ZipFile.CreateFromDirectory(path, zipFilePath);
            }

            try
            {
                ZipFile.ExtractToDirectory(zipFilePath, extractFilePath);
            }
            catch (Exception)
            {
                Directory.Delete(extractFilePath, true);

                ZipFile.ExtractToDirectory(zipFilePath, extractFilePath);
            }
        }
    }
}
