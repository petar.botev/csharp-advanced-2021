﻿using System;
using System.IO;

namespace _4._Copy_Binary_File
{
    class Program
    {
        static void Main()
        {
            string path = Path.Combine("data", "copyMe.png");
            string output = Path.Combine("data", "output.png");

            using (FileStream fileStrem = new FileStream(path, FileMode.Open))
            {
                using (BinaryReader reader = new BinaryReader(fileStrem))
                {
                    using (FileStream outputStream = new FileStream(output, FileMode.Create))
                    {
                        using (BinaryWriter writer = new BinaryWriter(outputStream))
                        {
                            byte[] buffer = new byte[4096];

                            long imageSize = fileStrem.Length;

                            int counter = -1;
                            long allBytes = 0;

                            while (counter != 0)
                            {
                                if (allBytes > imageSize - buffer.Length)
                                {
                                    buffer = new byte[4096];
                                }

                                counter = reader.Read(buffer, 0, buffer.Length);

                                allBytes += buffer.Length;
                                writer.Write(buffer);
                            }
                        }
                    }
                }
            }
        }
    }
}
