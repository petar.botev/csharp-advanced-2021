﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace _5._Directory_Traversal
{
    class Program
    {
        static void Main()
        {
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "chernici snimki");
            string output = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "output.txt");

            Dictionary<string, SortedDictionary<string, long>> dict = new Dictionary<string, SortedDictionary<string, long>>();

            string[] files = Directory.GetFiles(path);
            
            List<string> fileNames = new List<string>();

            files = files.Select(x => Path.GetFileName(x)).ToArray();
            
            for (int i = 0; i < files.Length; i++)
            {
                string extension = $".{files[i].Split('.').Last()}";
                long size = new FileInfo(Path.Combine(path, files[i])).Length;

                if (dict.ContainsKey(extension))
                {
                    dict[extension].Add(files[i], size);
                }
                else
                {
                    dict.Add(extension, new SortedDictionary<string, long>());
                    dict[extension].Add(files[i], size);
                }
            }

            int dictCount = dict.Count;

            dict = dict.OrderByDescending(x => x.Key.Count()).ThenBy(x => x.Key).ToDictionary(k => k.Key, v => v.Value);

            StringBuilder strb = new StringBuilder();

            foreach (var item in dict)
            {
                strb.AppendLine(item.Key);

                foreach (var file in item.Value)
                {
                    strb.AppendLine($"--{file.Key} - {file.Value}kb");
                }
            }

            File.WriteAllText(output, strb.ToString());
        }
    }
}
