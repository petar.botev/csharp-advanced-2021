﻿using System;
using System.IO;
using System.Linq;

namespace _1._Even_Lines
{
    class Program
    {
        static void Main()
        {
            string path = Path.Combine("data", "text.txt");
            string output = Path.Combine("data", "output.txt");

            using (FileStream stream = new FileStream(path, FileMode.Open))
            {
                using (TextReader readText = new StreamReader(stream))
                {
                    string[] rows = readText.ReadToEnd()
                        .Split("\n", StringSplitOptions.RemoveEmptyEntries);

                    using (FileStream write = new FileStream(output, FileMode.Create))
                    {
                        using (StreamWriter writer = new StreamWriter(write))
                        {
                            for (int i = 0; i < rows.Length; i++)
                            {
                                if (i % 2 == 0)
                                {
                                    string[] rowElements = rows[i].Split(new string[] { " ", "\n", "\r" }, StringSplitOptions.RemoveEmptyEntries);
                                    rowElements = rowElements.Reverse().ToArray();

                                    for (int j = 0; j < rowElements.Length; j++)
                                    {
                                        for (int k = 0; k < rowElements[j].Length; k++)
                                        {
                                            if (char.IsPunctuation(rowElements[j][k]) && rowElements[j][k] != '\'')
                                            {
                                                rowElements[j] = rowElements[j].Replace(rowElements[j][k], '@');
                                            }
                                        }
                                    }
                                    writer.WriteLine(string.Join(' ', rowElements));
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
