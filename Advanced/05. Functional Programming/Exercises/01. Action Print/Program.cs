﻿using System;
using System.Linq;

namespace _01._Action_Print
{
    class Program
    {
        static void Main()
        {
            string[] words = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries);

            Action<string[]> printWords = x => x.ToList().ForEach(x => Console.WriteLine(x));

            printWords(words);
        }
    }
}
