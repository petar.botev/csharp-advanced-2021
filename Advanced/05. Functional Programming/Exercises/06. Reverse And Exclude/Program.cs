﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _06._Reverse_And_Exclude
{
    class Program
    {
        static void Main()
        {
            int[] numbers = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray();

            int diviseor = int.Parse(Console.ReadLine());

            Predicate<int> isDevisible = x => x % diviseor == 0;

            Func<int[], Predicate<int>, List<int>> reverse = (x, y) =>
            {
                List<int> reversedArr = new List<int>();

                for (int i = x.Length - 1; i >= 0; i--)
                {
                    if (!isDevisible(x[i]))
                    {
                        reversedArr.Add(x[i]);
                    }
                }

                return reversedArr;
            };

            Console.WriteLine(string.Join(' ', reverse(numbers, isDevisible)));
        }
    }
}
