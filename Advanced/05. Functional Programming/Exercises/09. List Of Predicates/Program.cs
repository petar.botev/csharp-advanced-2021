﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _09._List_Of_Predicates
{
    class Program
    {
        static void Main()
        {
            int[] numbers = Enumerable.Range(1, int.Parse(Console.ReadLine())).ToArray();

            int[] divisors = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray();

            Func<int, int, bool> isDivisible = (x, y) => x % y == 0;

            List<int> result = new List<int>();

            foreach (var number in numbers)
            {
                bool isDev = true;

                foreach (var divisor in divisors)
                {
                    if (!isDivisible(number, divisor))
                    {
                        isDev = false;
                        break;
                    }
                }

                if (isDev == true)
                {
                    result.Add(number);
                }
            }
            
            Console.WriteLine(string.Join(' ', result));
        }
    }
}
