﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _12._TriFunction
{
    class Program
    {
        static void Main()
        {
            int number = int.Parse(Console.ReadLine());

            string[] names = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries);
                       
            Func<string, int, bool> isEqualOrLarger = (x, y) =>
            {
                int sum = 0;
                x.ToCharArray().ToList().ForEach(x => sum += x);

                return sum >= y;
            };

            Func<string[], Func<string, int, bool>, string> biggerName = (x, y) => x.FirstOrDefault(x => y(x, number));

            Console.WriteLine(string.Join(' ', biggerName(names, isEqualOrLarger)));
        }
    }
}
