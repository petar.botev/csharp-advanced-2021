﻿using System;
using System.Linq;

namespace _04._Find_Evens_or_Odds
{
    class Program
    {
        static void Main()
        {
            int[] input = Console.ReadLine()
                 .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                 .Select(int.Parse)
                 .ToArray();

            int[] numbers = Enumerable.Range(input[0], (input[1] - input[0]) + 1).ToArray();

            string command = Console.ReadLine();

            Predicate<int> isEven = x => x % 2 == 0;

            Func<int[], int[]> takeNumbers = x =>
            {
                if (command == "even")
                {
                    return x.Where(x => isEven(x) == true).ToArray();
                }
                else
                {
                    return x.Where(x => isEven(x) == false).ToArray();
                }
            };

            Console.WriteLine(string.Join(' ', takeNumbers(numbers)));
        }
    }
}
