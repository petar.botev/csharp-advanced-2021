﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _11._The_Party_2
{
    class Program
    {
        static void Main()
        {
            List<string> names = Console.ReadLine()
               .Split(' ', StringSplitOptions.RemoveEmptyEntries)
               .ToList();

            Dictionary<string, string> conditionsDict = new Dictionary<string, string>();

            string command;
            while ((command = Console.ReadLine()) != "Print")
            {
                string[] commandArgs = command
                    .Split(';', StringSplitOptions.RemoveEmptyEntries);

                string theCommand = commandArgs[0];
                string filterType = commandArgs[1];
                string filterParameter = commandArgs[2];

                if (theCommand == "Add filter")
                {
                    conditionsDict.Add(filterParameter, filterType);
                }
                else
                {
                    conditionsDict.Remove(filterParameter);
                }
            }

            foreach (var filter in conditionsDict)
            {
                if (filter.Value == "Starts with")
                {
                    names = names.Where(x => !x.StartsWith(filter.Key)).ToList();
                }
                else if (filter.Value == "Ends with")
                {
                    names = names.Where(x => !x.EndsWith(filter.Key)).ToList();
                }
                else if (filter.Value == "Length")
                {
                    names = names.Where(x => x.Length != int.Parse(filter.Key)).ToList();
                }
                else if (filter.Value == "Contains")
                {
                    names = names.Where(x => !x.Contains(filter.Key)).ToList();
                }
            }

            Console.WriteLine(string.Join(' ', names));
        }
    }
}
