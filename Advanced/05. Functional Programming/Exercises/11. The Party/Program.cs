﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _11._The_Party
{
    class Program
    {
        static void Main()
        {
            List<string> names = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .ToList();

            Dictionary<string, List<string>> conditionsDict = new Dictionary<string, List<string>>();

            string command;
            while ((command = Console.ReadLine()) != "Print")
            {
                List<string> tempNames = new List<string>(names);

                string[] commandArgs = command
                    .Split(';', StringSplitOptions.RemoveEmptyEntries);

                string theCommand = commandArgs[0];
                string filterType = commandArgs[1];
                string filterParameter = commandArgs[2];

                if (theCommand == "Add filter")
                {
                    if (!conditionsDict.ContainsKey(filterParameter))
                    {
                        conditionsDict.Add(filterParameter, tempNames);
                        names = new List<string>(Filter(filterType, names, filterParameter));
                    }
                }
                else
                {
                    names = conditionsDict[filterParameter];
                    conditionsDict.Remove(filterParameter);
                }
            }

            Console.WriteLine(string.Join(' ', names));
        }

        static List<string> Filter(string filterType, List<string> names, string filterParam)
        {

            if (filterType == "Starts with")
            {
                names = names.Where(x => !x.StartsWith(filterParam)).ToList();
            }
            else if (filterType == "Ends with")
            {
                names = names.Where(x => !x.EndsWith(filterParam)).ToList();
            }
            else if (filterType == "Length")
            {
                names = names.Where(x => x.Length != int.Parse(filterParam)).ToList();
            }
            else if (filterType == "Contains")
            {
                names = names.Where(x => !x.Contains(filterParam)).ToList();
            }
           
            return names;
        }
    }
}
