﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _10._Predicate_Party_
{
    class Program
    {
        static void Main()
        {
            List<string> names = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .ToList();

            Predicate<string> isRemove = x => x == "Remove";

            string firstCommand = string.Empty;
            string secondCommand = string.Empty;
            string firstCondition = string.Empty;
            List<string> resultsNames = names;

            string command;

            while ((command = Console.ReadLine()) != "Party!")
            {
                string[] commandArgs = command
                    .Split(' ', StringSplitOptions.RemoveEmptyEntries);

                firstCommand = commandArgs[0];
                secondCommand = commandArgs[1];
                firstCondition = commandArgs[2];

                resultsNames = Execute(resultsNames, firstCommand, secondCommand, firstCondition);
            }

            if (resultsNames.Count > 0)
            {
                Console.WriteLine($"{string.Join(", ", resultsNames)} are going to the party!");
            }
            else
            {
                Console.WriteLine("Nobody is going to the party!");
            }
        }

        static List<string> Execute(List<string> names, string command, string secondCommand, string condition)
        {
            var conditionFunc = FulfillCondition(secondCommand, condition);
            
            if (command == "Remove")
            {
                var namesToRemove = conditionFunc(names);

                foreach (var name in namesToRemove)
                {
                    names = names.Where(x => x != name).ToList();
                }

                return names;
            }
            else 
            {
                var namesToDouble = conditionFunc(names);

                int namesCount = names.Count();

                foreach (var name in namesToDouble)
                {
                    int index = names.IndexOf(name);
                    names.Insert(index, name);
                }

                return names;
            }
        }

        static Func<List<string>, string[]> FulfillCondition(string secondCommand, string condition)
        {
            if (secondCommand == "StartsWith")
            {
                return x => x.Where(x => x.StartsWith(condition)).ToArray();
            }
            else if (secondCommand == "EndsWith")
            {
                return x => x.Where(x => x.EndsWith(condition)).ToArray();
            }
            else if (secondCommand == "Length")
            {
                return x => x.Where(x => x.Length == int.Parse(condition)).ToArray();
            }
            else
            {
                throw new ArgumentException("No such command.");
            }
        }
    }
}
