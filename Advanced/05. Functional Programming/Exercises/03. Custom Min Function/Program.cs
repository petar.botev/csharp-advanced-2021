﻿using System;
using System.Linq;

namespace _03._Custom_Min_Function
{
    class Program
    {
        static void Main()
        {
            int[] numbers = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray();

            Func<int[], int> minNumber = x =>
            {
                int minNum = int.MaxValue;

                foreach (var number in numbers)
                {
                    if (number < minNum)
                    {
                        minNum = number;
                    }
                }

                return minNum;
            };

            Console.WriteLine(minNumber(numbers));
        }
    }
}
