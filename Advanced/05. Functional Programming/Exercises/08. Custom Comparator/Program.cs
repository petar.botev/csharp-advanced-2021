﻿using System;
using System.Linq;

namespace _08._Custom_Comparator
{
    class Program
    {
        static void Main()
        {
            int[] numbers = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray();

            Func<int[], int[]> odds = x => x.Where(x => x % 2 != 0).ToArray();
            Func<int[], int[]> evens = x => x.Where(x => x % 2 == 0).ToArray();

            int[] oddsArr = odds(numbers);
            int[] evensArr = evens(numbers);

            int[] result = new int[oddsArr.Length + evensArr.Length];

            Array.Sort(evensArr);
            evensArr.CopyTo(result, 0);
            Array.Sort(odds(numbers));
            oddsArr.CopyTo(result, evensArr.Length);

            Console.WriteLine(string.Join(' ', result));
        }
    }
}
