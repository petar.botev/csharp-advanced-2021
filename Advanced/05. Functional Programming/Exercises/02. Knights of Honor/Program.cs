﻿using System;
using System.Linq;

namespace _02._Knights_of_Honor
{
    class Program
    {
        static void Main()
        {
            string[] words = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries);

            Action<string[]> addPrefix = x => x.ToList().ForEach(x => Console.WriteLine($"Sir {x}"));

            addPrefix(words);
        }
    }
}
