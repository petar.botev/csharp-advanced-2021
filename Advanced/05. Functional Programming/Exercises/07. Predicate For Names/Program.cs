﻿using System;
using System.Linq;

namespace _07._Predicate_For_Names
{
    class Program
    {
        static void Main()
        {
            int nameLenght = int.Parse(Console.ReadLine());

            string[] names = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries);

            Predicate<string> isLessLenght = x => x.Length <= nameLenght;

            Action<string[]> print = x => x.ToList().ForEach(x =>
            {
                if (isLessLenght(x))
                {
                    Console.WriteLine(x);
                }
            });

            print(names);
        }
    }
}
