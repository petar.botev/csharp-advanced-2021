﻿using System;
using System.Linq;

namespace _05._Applied_Arithmetics
{
    class Program
    {
        static void Main()
        {
            int[] numbers = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray();
           
            string command;
            while ((command = Console.ReadLine()) != "end")
            {
                var executeCalc = Execute()(command, numbers);
                numbers = executeCalc(numbers);
            }
        }

        private static Func<string, int[], Func<int[], int[]>> Execute() => (x, y) =>
        {
            if (x == "add")
            {
                return y => y.Select(y => y + 1).ToArray();
            }
            else if (x == "multiply")
            {
                return y => y.Select(y => y * 2).ToArray();
            }
            else if (x == "subtract")
            {
                return y => y.Select(y => y - 1).ToArray();
            }
            else if (x == "print")
            {
                Console.WriteLine(string.Join(' ', y));
                return y => y;
            }
            else
            {
                throw new ArgumentException("Command does not exists!");
            }
        };
    }
}
