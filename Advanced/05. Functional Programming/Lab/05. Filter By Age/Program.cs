﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _05._Filter_By_Age
{
    class Program
    {
        static void Main()
        {
            int number = int.Parse(Console.ReadLine());
            string name = string.Empty;
            int age = -1;

            Dictionary<string, int> dict = new Dictionary<string, int>();

            for (int i = 0; i < number; i++)
            {
                string[] args = Console.ReadLine()
                    .Split(", ", StringSplitOptions.RemoveEmptyEntries);
                name = args[0];
                age = int.Parse(args[1]);

                dict.Add(name, age);
            }

            string condition = Console.ReadLine();
            int ageValue = int.Parse(Console.ReadLine());
            string format = Console.ReadLine();

            var people = GetPeople(condition, ageValue);
            var print = Format(format, name, age);

            print(people(dict));
        }

        static Func<Dictionary<string, int>, Dictionary<string, int>> GetPeople(string condition, int ageValue)
        {
            if (condition == "younger")
            {
                return x => x.Where(x => x.Value < ageValue).ToDictionary(k => k.Key, v => v.Value);
            }
            else if (condition == "older")
            {
                return x => x.Where(x => x.Value >= ageValue).ToDictionary(k => k.Key, v => v.Value);
            }

            throw new ArgumentException("No such conditions!");
        }

        static Action<Dictionary<string, int>> Format(string format, string name, int age)
        {
            if (format == "name")
            {
                return x => x.ToList().ForEach(x => Console.WriteLine(x.Key));
            }
            else if (format == "age")
            {
                return x => x.ToList().ForEach(x => Console.WriteLine(x.Value));

            }
            else if (format == "name age")
            {
                return x => x.ToList().ForEach(x => Console.WriteLine($"{x.Key} - {x.Value}"));

            }

            throw new ArgumentException("Format does not exists");
        }
    }
}
