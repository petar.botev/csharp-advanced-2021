﻿using System;
using System.Linq;

namespace _02._Sum_Numbers
{
    class Program
    {
        static void Main()
        {
            Func<int[], int> getCount = x => x.Count();
            Func<int[], int> calcSum = x => x.Sum();

            int[] numbers = Console.ReadLine()
                .Split(", ", StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray();

            Console.WriteLine(getCount(numbers));
            Console.WriteLine(calcSum(numbers));
        }
    }
}
