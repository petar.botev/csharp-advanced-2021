﻿using System;
using System.Linq;

namespace _04._Add_VAT
{
    class Program
    {
        static void Main()
        {
            decimal[] values = Console.ReadLine()
                .Split(", ", StringSplitOptions.RemoveEmptyEntries)
                .Select(decimal.Parse)
                .ToArray();

            Func<decimal[], decimal[]> addVAT = x => x.Select(x => x + x * (20m / 100m)).ToArray();

            Console.WriteLine(string.Join("\n", addVAT(values).Select(x => x.ToString("f2"))));
        }
    }
}
