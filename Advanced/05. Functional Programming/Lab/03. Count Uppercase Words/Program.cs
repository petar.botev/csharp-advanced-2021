﻿using System;
using System.Linq;

namespace _03._Count_Uppercase_Words
{
    class Program
    {
        static void Main()
        {
            string[] inputString = Console.ReadLine()
                .Split(' ', StringSplitOptions.RemoveEmptyEntries);

            Action<string[]> printUppercaseWords = x => x.ToList().ForEach(x => 
            {
                if (char.IsUpper(x[0]))
                {
                    Console.WriteLine(x);
                }
            });

            printUppercaseWords(inputString);
        }
    }
}
