﻿using System;
using System.Linq;

namespace _01._Sort_Even_Numbers
{
    class Program
    {
        static void Main()
        {
            int[] input = Console.ReadLine()
                .Split(", ", StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray();

            Func<int[], int[]> orderAcsending = x => x.OrderBy(x => x).ToArray();

            Func<int[], int[]> getEvenNumbers = x => x.Where(x => x % 2 == 0).ToArray();

            Console.WriteLine(string.Join(", ", orderAcsending(getEvenNumbers(input))));
        }
    }
}
