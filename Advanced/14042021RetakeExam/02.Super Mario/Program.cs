﻿using System;
using System.Linq;

namespace _02.Super_Mario
{
    public class Program
    {
        static int lives;
        static int marioPossitionX = 0;
        static int marioPossitionY = 0;

        static int x = marioPossitionX;
        static int y = marioPossitionY;

        static char[][] matrix;

        static int rows;
        static bool findPrinces = false;
        static bool isDead = false;
        static bool isOut = false;

        static void Main(string[] args)
        {
            lives = int.Parse(Console.ReadLine());

            rows = int.Parse(Console.ReadLine());

            matrix = new char[rows][];

            for (int i = 0; i < matrix.Length; i++)
            {
                matrix[i] = Console.ReadLine().ToCharArray();
            }

            for (int i = 0; i < matrix.Length; i++)
            {
                for (int j = 0; j < matrix.Length; j++)
                {
                    if (matrix[i][j] == 'M')
                    {
                        marioPossitionX = i;
                        marioPossitionY = j;
                    }
                }
            }

            while (lives > 0)
            {
                string[] moves = Console.ReadLine().Split();

                //int marioPossitionX = 0;
                //int marioPossitionY = 0;

                int monsterX = int.Parse(moves[1]);
                int monsterY = int.Parse(moves[2]);

                matrix[monsterX][monsterY] = 'B';

               

                if (moves[0] == "W")
                {
                    matrix[marioPossitionX][marioPossitionY] = '-';
                    x--;
                    Move();
                    if (isOut == true)
                    {
                        marioPossitionX++;
                        isOut = false;
                    }
                }
                else if (moves[0] == "S")
                {
                    matrix[marioPossitionX][marioPossitionY] = '-';
                    x++;
                    Move();
                    if (isOut == true)
                    {
                        marioPossitionX--;
                        isOut = false;
                    }
                }
                else if (moves[0] == "A")
                {
                    matrix[marioPossitionX][marioPossitionY] = '-';
                    y--;
                    Move();
                    if (isOut == true)
                    {
                        marioPossitionY++;
                        isOut = false;
                    }
                }
                else if (moves[0] == "D")
                {
                    matrix[marioPossitionX][marioPossitionY] = '-';
                    y++;
                    Move();
                    if (isOut == true)
                    {
                        marioPossitionY--;
                        isOut = false;
                    }
                }

                if (findPrinces)
                {
                    break;
                }
            }

            if (isDead == true)
            {
                Console.WriteLine($"Mario died at {marioPossitionX};{marioPossitionY}.");
            }
            else
            {
                Console.WriteLine($"Mario has successfully saved the princess! Lives left: {lives}");
            }

            for (int i = 0; i < matrix.Length; i++)
            {
                if (i == matrix.Length - 1)
                {
                    Console.Write(string.Join("", matrix[i]));
                }
                else
                {
                    Console.WriteLine(string.Join("", matrix[i]));
                }
            }
        }
       

        static public void Move()
        {
            OutOfMatrix();
            if (isOut == true)
            {
                return;
            }
            lives--;
            IsDead();
            Fight();
            if (isDead == true)
            {
                return;
            }
            FindPrincess();
            if (findPrinces == true)
            {
                return;
            }
            matrix[marioPossitionX][marioPossitionY] = 'M';
            
        }

        static public void OutOfMatrix()
        {
            if (marioPossitionX > rows - 1 || marioPossitionX < 0 || marioPossitionY > rows - 1 || marioPossitionY < 0)
            {
                lives--;
                isOut = true;
                //return;
            }
        }

        static public void IsDead()
        {
            if (lives <= 0)
            {
                matrix[marioPossitionX][marioPossitionY] = 'X';
                isDead = true;
            }
        }

        static public void Fight()
        {
            if (matrix[marioPossitionX][marioPossitionY] == 'B')
            {
                lives -= 2;

                if (lives <= 0)
                {
                    matrix[marioPossitionX][marioPossitionY] = 'X';
                    isDead = true;
                }
                else
                {
                    matrix[marioPossitionX][marioPossitionY] = 'M';
                }
            }
        }

        static void FindPrincess()
        {
            if (matrix[marioPossitionX][marioPossitionY] == 'P')
            {
                if (matrix[marioPossitionX][marioPossitionY] == 'P')
                {
                    matrix[marioPossitionX][marioPossitionY] = '-';
                    findPrinces = true;
                }
            }
        }
    }
}
