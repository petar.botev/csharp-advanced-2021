﻿using System;

using System.Collections.Generic;
using System.Linq;

namespace _01.Warm_Winter
{
    class Program
    {
        static void Main(string[] args)
        {
            var hatCollection = Console.ReadLine().Split().Select(x => int.Parse(x));
            var scarfCollection = Console.ReadLine().Split().Select(x => int.Parse(x));

            Stack<int> hats = new Stack<int>(hatCollection);
            Queue<int> scarfs = new Queue<int>(scarfCollection);

            List<int> sets = new List<int>();

            while (hats.Count > 0 && scarfs.Count > 0)
            {
                int hat = hats.Peek();
                int scarf = scarfs.Peek();
                if (hat > scarf)
                {
                    var currentHat = hats.Pop();
                    var currentScarf = scarfs.Dequeue();

                    var sumSet = currentHat + currentScarf;
                    sets.Add(sumSet);
                }
                else if (hat == scarf)
                {
                    scarfs.Dequeue();
                    var currentHat = hats.Pop();
                    currentHat++;
                    hats.Push(currentHat);
                }
                else
                {
                    hats.Pop();
                }
            }

            Console.WriteLine($"The most expensive set is: {sets.Max()}");

            Console.WriteLine(string.Join(" ", sets));
        }
    }
}
